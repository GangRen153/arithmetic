package com.zuochengyun.算法基础班.code33哈希函数有关结构;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

/**
 * @author 钢人
 */
public class Hash {

    public MessageDigest hash;

    public Hash(String algorithm) {
        try {
            hash = MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public String hashCode(String input) {
        return DatatypeConverter.printHexBinary(hash.digest(input.getBytes())).toUpperCase();
    }

    public static void main(String[] args) {
        System.out.println("支持的算法有");
        for (String str : Security.getAlgorithms("MessageDigest")) {
            System.out.println(str);
        }

        Hash hash = new Hash("MD5");
        String input1 = "zuochengyunzuochengyun1";
        String input2 = "zuochengyunzuochengyun2";
        System.out.println(hash.hashCode(input1));
        System.out.println(hash.hashCode(input2));
    }

}

package com.zuochengyun.算法基础班.code30Morris遍历;

/**
 * 一种遍历二叉树的方式，并且时间复杂度O(N)，额外空间复杂度O(1)
 * 通过利用原树中大量空闲指针的方式，达到节省空间的目的
 *
 * @author 钢人
 */
public class Code01_MorrisTraversal {

    public static class Node {
        public int value;
        Node left;
        Node right;
        public Node(int value) {
            this.value = value;
        }
    }

    /**
     * 传统的递归调用方式，无法优化最后一层孩子节点为 null 的时间复杂度遍历
     */
    public void process(Node head) {
        if (head == null) {
            return;
        }
        process(head.left);
        process(head.right);
    }

    /**
     * morris 递归二叉树
     */
    public static void morris(Node head) {
        // 当前节点，可能是左节点也可能是右节点
        Node cur = head;
        // 子节点
        Node mostRight;
        while (cur != null) {
            // 拿到当前节点的左子节点
            mostRight = cur.left;
            // 左子节点如果不是null，开始遍历左子节点的树结构
            if (mostRight != null) {
                // 检查一遍 cur.left.right 节点是否遍历过了
                // morris 的特点就是会把最后一级孩子节点是null的节点指向上级某个节点
                // 这时候需要判断一下，当前孩子节点有没有指向父节点
                // 如果是第一次遍历，一直遍历直到找到最右节点为止
                // 如果指向了父节点，说明之前遍历过一次，并且这个 mostRight 节点是最后一级节点
                // （这里root右叉上的最右节点始终走不动这一步）
                while (mostRight.right != null && mostRight.right != cur) {
                    mostRight = mostRight.right;
                }
                // 如果当前节点是null，说明 cur.left.right 这个分叉之前没遍历过，表示第一次遍历
                if (mostRight.right == null) {
                    // 将其 cur.left.right 最右节点指向 cur 父节点，标记一下，表示左叉还需要继续遍历
                    mostRight.right = cur;
                    // 既然知道了是第一次遍历，继续开始往下深度遍历 cur 父节点的左节点分叉，以此循环在检查 cur.left.left.right 的分叉
                    cur = cur.left;
                    continue;

                    // 如果不是null，说明之前检查调用已经遍历过了，也说明本轮遍历是遍历完左叉之后，开始遍历右叉的结论
                } else {
                    mostRight.right = null;
                }
            }
            // 遍历完左节点在遍历右节点，cur.right 也有可能是 cur.right 最上层的父节点
            // 除非是 root 节点的右叉的最右根节点才是null
            cur = cur.right;
        }
    }

    /**
     * morris 先序遍历
     */
    public static void morrisPre(Node head) {
        Node cur = head;
        Node mostRight;
        while (cur != null) {
            mostRight = cur.left;
            if (mostRight != null) {
                while (mostRight.right != null && mostRight.right != cur) {
                    mostRight = mostRight.right;
                }
                if (mostRight.right == null) {
                    System.out.print(cur.value + " ");
                    mostRight.right = cur;
                    cur = cur.left;
                    continue;
                } else {
                    mostRight.right = null;
                }
            } else {
                System.out.print(cur.value + " ");
            }
            cur = cur.right;
        }
        System.out.println();
    }

    /**
     * morris 中序遍历
     */
    public static void morrisIn(Node head) {
        Node cur = head;
        Node mostRight;
        while (cur != null) {
            mostRight = cur.left;
            if (mostRight != null) {
                while (mostRight.right != null && mostRight.right != cur) {
                    mostRight = mostRight.right;
                }
                if (mostRight.right == null) {
                    mostRight.right = cur;
                    cur = cur.left;
                    continue;
                } else {
                    mostRight.right = null;
                }
            }
            System.out.print(cur.value + " ");
            cur = cur.right;
        }
        System.out.println();
    }

    /**
     * morris 后续遍历
     */
    public static void morrisPos(Node head) {
        Node cur = head;
        Node mostRight;
        while (cur != null) {
            mostRight = cur.left;
            if (mostRight != null) {
                while (mostRight.right != null && mostRight.right != cur) {
                    mostRight = mostRight.right;
                }
                if (mostRight.right == null) {
                    mostRight.right = cur;
                    cur = cur.left;
                    continue;
                } else {
                    mostRight.right = null;
                    printEdge(cur.left);
                }
            }
            cur = cur.right;
        }
        printEdge(head);
        System.out.println();
    }

    public static void printEdge(Node head) {
        Node tail = reverseEdge(head);
        Node cur = tail;
        while (cur != null) {
            System.out.print(cur.value + " ");
            cur = cur.right;
        }
        reverseEdge(tail);
    }

    public static Node reverseEdge(Node from) {
        Node pre = null;
        Node next;
        while (from != null) {
            next = from.right;
            from.right = pre;
            pre = from;
            from = next;
        }
        return pre;
    }

}

package com.zuochengyun.算法基础班.code30Morris遍历;

/**
 * 给定一棵二叉树的头节点head
 * 求以head为头的树中，最小深度是多少？
 *
 * @author 钢人
 */
public class Code05_MinHeight {

    public static class Node {
        public int val;
        public Node left;
        public Node right;
        public Node(int x) {
            val = x;
        }
    }

    public static int minHeight1(Node head) {
        if (head == null) {
            return 0;
        }
        return p(head);
    }

    /**
     * 返回x为头的树，最小深度是多少
     */
    public static int p(Node x) {
        // 如果左右节点都是空的，返回深度为1（也就是x.left节点或者是x.right节点，也算一级深度）
        if (x.left == null && x.right == null) {
            return 1;
        }

        // 遍历找左节点的深度
        int leftH = Integer.MAX_VALUE;
        if (x.left != null) {
            leftH = p(x.left);
        }

        // 遍历找右节点的深度
        int rightH = Integer.MAX_VALUE;
        if (x.right != null) {
            rightH = p(x.right);
        }
        // 算上当前节点 x 的深度1
        return 1 + Math.min(leftH, rightH);
    }


    /**
     * morris 实现
     */
    public static int minHeight2(Node head) {
        Node cur = head;
        Node mostRight;
        int curLevel = 0;
        int minHeight = Integer.MAX_VALUE;
        while (cur != null) {
            mostRight = cur.left;
            if (mostRight != null) {
                int rightBoardSize = 1;
                while (mostRight.right != null && mostRight.right != cur) {
                    rightBoardSize++;
                    mostRight = mostRight.right;
                }
                // 第一次到达
                if (mostRight.right == null) {
                    curLevel++;
                    mostRight.right = cur;
                    cur = cur.left;
                    continue;
                } else {
                    if (mostRight.left == null) {
                        minHeight = Math.min(minHeight, curLevel);
                    }
                    curLevel -= rightBoardSize;
                    mostRight.right = null;
                }
            } else {
                curLevel++;
            }
            cur = cur.right;
        }
        int finalRight = 1;
        cur = head;
        while (cur.right != null) {
            finalRight++;
            cur = cur.right;
        }
        if (cur.left == null && cur.right == null) {
            minHeight = Math.min(minHeight, finalRight);
        }
        return minHeight;
    }

}

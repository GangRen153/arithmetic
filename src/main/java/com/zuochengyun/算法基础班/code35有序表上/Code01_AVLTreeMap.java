package com.zuochengyun.算法基础班.code35有序表上;

/**
 * avl 树
 * 
 * @author 钢人
 */
public class Code01_AVLTreeMap {

    public static class AVLNode<K extends Comparable<K>, V> {
        public K k;
        public V v;
        public AVLNode<K, V> l;
        public AVLNode<K, V> r;
        public int h;
        public AVLNode(K key, V value) {
            this.k = key;
            this.v = value;
        }
    }

    public static class AVLTreeMap<K extends Comparable<K>, V> {
        // 根节点
        public AVLNode<K, V> root;
        public int size;
        public AVLTreeMap() {
            root = null;
            size = 0;
        }

        /**
         * 右旋 cur 节点（cur向右下旋转，cur.l 向右上旋转）
         */
        private AVLNode<K, V> rightRotate(AVLNode<K, V> cur) {
            // cur 向右下旋，cut.l 向右上旋，作为头节点
            AVLNode<K, V> left = cur.l;
            // cur 节点的左节点设置成 cur.left.right
            // 因为cur左节点下的任意子节点一定比 cur 小
            cur.l = left.r;
            // left 成为
            left.r = cur;
            // 设置 cur.h 高度，自己的左右新节点高度+1，加1是因为算自己
            cur.h = Math.max((cur.l != null ? cur.l.h : 0), (cur.r != null ? cur.r.h : 0)) + 1;
            // 设置 left 节点的高度
            left.h = Math.max((left.l != null ? left.l.h : 0), (left.r != null ? left.r.h : 0)) + 1;
            return left;
        }

        /**
         * 左旋 cur 节点（cur节点向左下旋转，cur.r 向左上旋转）
         */
        private AVLNode<K, V> leftRotate(AVLNode<K, V> cur) {
            // cur.r 右节点向左上旋转
            AVLNode<K, V> right = cur.r;
            // 把根节点的左孩子给cur的右节点
            cur.r = right.l;
            right.l = cur;
            cur.h = Math.max((cur.l != null ? cur.l.h : 0), (cur.r != null ? cur.r.h : 0)) + 1;
            right.h = Math.max((right.l != null ? right.l.h : 0), (right.r != null ? right.r.h : 0)) + 1;
            return right;
        }

        /**
         * 调整 cur 节点
         */
        private AVLNode<K, V> maintain(AVLNode<K, V> cur) {
            if (cur == null) {
                return null;
            }
            int leftHeight = cur.l != null ? cur.l.h : 0;
            int rightHeight = cur.r != null ? cur.r.h : 0;
            // 两数差值的绝对值，如果高度差值小于2，说明是平衡的
            if (Math.abs(leftHeight - rightHeight) < 2) {
                return cur;
            }
            // 左孩子不平衡，调整左孩子节点，相对于cur节点看，属于 LL 违规
            if (leftHeight > rightHeight) {
                // 看左孩子下的左右高度分别是多少
                int leftLeftHeight = cur.l != null && cur.l.l != null ? cur.l.l.h : 0;
                int leftRightHeight = cur.l != null && cur.l.r != null ? cur.l.r.h : 0;
                // 说明还是左边节点比较高，那么结论还是 LL 违规
                if (leftLeftHeight >= leftRightHeight) {
                    cur = rightRotate(cur);
                } else {// 不然的话就是 LR 违规，在 LR 违规
                    // 先调整成 LL
                    cur.l = leftRotate(cur.l);
                    // 在调整 cur 的左右节点
                    cur = rightRotate(cur);
                }
            } else {
                int rightLeftHeight = cur.r != null && cur.r.l != null ? cur.r.l.h : 0;
                int rightRightHeight = cur.r != null && cur.r.r != null ? cur.r.r.h : 0;
                // RR 违规，直接左旋即可
                if (rightRightHeight >= rightLeftHeight) {
                    cur = leftRotate(cur);
                } else {// RL 违规
                    // 先调整右节点的左右节点，调整成 RR
                    cur.r = rightRotate(cur.r);
                    // 在调整整个 cur 的左右平衡
                    cur = leftRotate(cur);
                }
            }
            return cur;
        }

        /**
         * 如果包含 K，就返回 K 节点，否则取最接近 K 的节点
         */
        public AVLNode<K, V> findLastIndex(K key) {
            AVLNode<K, V> pre = root;
            AVLNode<K, V> cur = root;
            while (cur != null) {
                pre = cur;
                if (key.compareTo(cur.k) == 0) {// 等于 0，说明两个节点的 K 值一样，是同一个节点
                    break;
                } else if (key.compareTo(cur.k) < 0) {// key<cur 就往左节点上找
                    cur = cur.l;
                } else {// 反之右节点
                    cur = cur.r;
                }
            }
            // 找到最终相匹配的节点
            return pre;
        }

        /**
         * 返回大于或等于 key 的最小键，如果没有返回 null
         */
        public AVLNode<K, V> findLastNoSmallIndex(K key) {
            AVLNode<K, V> ans = null;
            AVLNode<K, V> cur = root;
            while (cur != null) {
                if (key.compareTo(cur.k) == 0) {
                    ans = cur;
                    break;
                } else if (key.compareTo(cur.k) < 0) {
                    ans = cur;
                    cur = cur.l;
                } else {
                    cur = cur.r;
                }
            }
            return ans;
        }

        /**
         * 返回小于或等于给 key 的最大键，如果没有这样返回 null
         */
        public AVLNode<K, V> findLastNoBigIndex(K key) {
            AVLNode<K, V> ans = null;
            AVLNode<K, V> cur = root;
            while (cur != null) {
                if (key.compareTo(cur.k) == 0) {
                    ans = cur;
                    break;
                } else if (key.compareTo(cur.k) < 0) {
                    cur = cur.l;
                } else {
                    ans = cur;
                    cur = cur.r;
                }
            }
            return ans;
        }

        public AVLNode<K, V> add(AVLNode<K, V> cur, K key, V value) {
            if (cur == null) {
                return new AVLNode<>(key, value);
            }
            if (key.compareTo(cur.k) < 0) {
                cur.l = add(cur.l, key, value);
            } else {
                cur.r = add(cur.r, key, value);
            }
            // 设置高度
            cur.h = Math.max((cur.l != null ? cur.l.h : 0), (cur.r != null ? cur.r.h : 0)) + 1;
            // 调整节点
            return maintain(cur);
        }


        public AVLNode<K, V> delete(AVLNode<K, V> cur, K key) {
            if (key.compareTo(cur.k) > 0) {// 从右树上找
                cur.r = delete(cur.r, key);
            } else if (key.compareTo(cur.k) < 0) {// 从左树上找
                cur.l = delete(cur.l, key);
            } else {
                if (cur.l == null && cur.r == null) {
                    cur = null;
                } else if (cur.l == null && cur.r != null) {
                    cur = cur.r;
                } else if (cur.l != null && cur.r == null) {
                    cur = cur.l;
                } else {
                    AVLNode<K, V> des = cur.r;
                    // 找到 cur 节点的右树最左树
                    while (des.l != null) {
                        des = des.l;
                    }
                    cur.r = delete(cur.r, des.k);
                    des.l = cur.l;
                    des.r = cur.r;
                    cur = des;
                }
            }
            if (cur != null) {
                cur.h = Math.max(cur.l != null ? cur.l.h : 0, cur.r != null ? cur.r.h : 0) + 1;
            }
            return maintain(cur);
        }

        public int size() {
            return size;
        }

        public boolean containsKey(K key) {
            if (key == null) {
                return false;
            }
            AVLNode<K, V> lastNode = findLastIndex(key);
            return lastNode != null && key.compareTo(lastNode.k) == 0;
        }

        public void put(K key, V value) {
            if (key == null) {
                return;
            }
            // 找到 key 节点，如果找不到就返回 key 最近的节点
            AVLNode<K, V> lastNode = findLastIndex(key);
            // 如果找到了，直接覆盖即可
            if (lastNode != null && key.compareTo(lastNode.k) == 0) {
                lastNode.v = value;
            } else {// 反之添加
                size++;
                root = add(root, key, value);
            }
        }

        public void remove(K key) {
            if (key == null) {
                return;
            }
            if (containsKey(key)) {
                size--;
                root = delete(root, key);
            }
        }

        public V get(K key) {
            if (key == null) {
                return null;
            }
            AVLNode<K, V> lastNode = findLastIndex(key);
            if (lastNode != null && key.compareTo(lastNode.k) == 0) {
                return lastNode.v;
            }
            return null;
        }

        /**
         * 返回最小的节点（也就是第一个）
         */
        public K firstKey() {
            if (root == null) {
                return null;
            }
            AVLNode<K, V> cur = root;
            while (cur.l != null) {
                cur = cur.l;
            }
            return cur.k;
        }

        /**
         * 返回最大的节点（也就是最后一个）
         */
        public K lastKey() {
            if (root == null) {
                return null;
            }
            AVLNode<K, V> cur = root;
            while (cur.r != null) {
                cur = cur.r;
            }
            return cur.k;
        }

        /**
         * 返回小于或等于给定键的最大键，如果没有这样的键，则 null
         */
        public K floorKey(K key) {
            if (key == null) {
                return null;
            }
            AVLNode<K, V> lastNoBigNode = findLastNoBigIndex(key);
            return lastNoBigNode == null ? null : lastNoBigNode.k;
        }

        /**
         * 返回大于或等于 key 的最小键，如果没有返回 null
         */
        public K ceilingKey(K key) {
            if (key == null) {
                return null;
            }
            AVLNode<K, V> lastNoSmallNode = findLastNoSmallIndex(key);
            return lastNoSmallNode == null ? null : lastNoSmallNode.k;
        }
    }

}

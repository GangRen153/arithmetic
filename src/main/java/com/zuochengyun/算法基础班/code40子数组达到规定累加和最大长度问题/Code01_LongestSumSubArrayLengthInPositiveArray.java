package com.zuochengyun.算法基础班.code40子数组达到规定累加和最大长度问题;

/**
 * 给定一个正整数组成的无序数组 arr，给定一个正整数值 K
 * 找到 arr 的所有子数组里，哪个子数组的累加和等于 K，并且是长度最大的
 * 返回其长度
 *
 * @author 钢人
 */
public class Code01_LongestSumSubArrayLengthInPositiveArray {

    public static int right(int[] arr, int K) {
        int max = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j < arr.length; j++) {
                if (valid(arr, i, j, K)) {
                    max = Math.max(max, j - i + 1);
                }
            }
        }
        return max;
    }

    // for test
    public static boolean valid(int[] arr, int L, int R, int K) {
        int sum = 0;
        for (int i = L; i <= R; i++) {
            sum += arr[i];
        }
        return sum == K;
    }

    /**
     * 滑动窗口
     */
    public static int getMaxLength(int[] arr, int K) {
        if (arr == null || arr.length == 0 || K <= 0) {
            return 0;
        }
        int left = 0;
        int right = 0;
        int sum = arr[0];
        int len = 0;
        while (right < arr.length) {
            if (sum == K) {
                len = Math.max(len, right - left + 1);
                // 向右移动一位，扩出来一个位置
                ++right;
                if (right < arr.length) {
                    sum += arr[right];
                }
            } else if (sum < K) {
                sum += arr[++right];
            } else {
                // 向右移动一位，扩出来一个位置
                sum -= arr[left++];
            }
        }
        return len;
    }

}

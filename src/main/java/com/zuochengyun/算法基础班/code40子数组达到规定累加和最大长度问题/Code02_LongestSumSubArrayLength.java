package com.zuochengyun.算法基础班.code40子数组达到规定累加和最大长度问题;

import java.util.HashMap;

/**
 * 给定一个任意值数组成的无序数组 arr，给定一个正整数值 K
 * 找到 arr 的所有子数组里，哪个子数组的累加和等于 K，并且是长度最大的
 * 返回其长度
 *
 * @author 钢人
 */
public class Code02_LongestSumSubArrayLength {

    /**
     * 前缀和处理
     */
    public static int maxLength(int[] arr, int k) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        // 计算所有子数组前缀和
        HashMap<Integer, Integer> map = new HashMap<>();
        int len = 0;
        int sum = 0;
        // 初始 0 位置的前缀和默认设置 -1
        map.put(0, -1);
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
            // 是否包含当前 sum-k 的前缀和出现的位置
            if (map.containsKey(sum - k)) {
                len = Math.max(i - map.get(sum - k), len);
            }
            // 添加前缀和最早出现的位置
            if (!map.containsKey(sum)) {
                map.put(sum, i);
            }
        }
        return len;
    }

}

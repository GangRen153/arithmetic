package com.zuochengyun.算法基础班.code46动态规划猜法和外部信息简化上;

/**
 * 消除字符
 * <p>
 * 单个字符不能消除，只能消除相邻相同的字符
 * 返回消除后剩余的字符尽量少
 *
 * @author 钢人
 */
public class Code03_DeleteAdjacentSameCharacter {

    public static int restMin1(String s) {
        if (s == null) {
            return 0;
        }
        if (s.length() < 2) {
            return s.length();
        }
        int minLen = s.length();
        for (int L = 0; L < s.length(); L++) {
            for (int R = L + 1; R < s.length(); R++) {
                if (canDelete(s.substring(L, R + 1))) {
                    minLen = Math.min(minLen, restMin1(s.substring(0, L) + s.substring(R + 1, s.length())));
                }
            }
        }
        return minLen;
    }

    public static boolean canDelete(String s) {
        char[] str = s.toCharArray();
        for (int i = 1; i < str.length; i++) {
            if (str[i - 1] != str[i]) {
                return false;
            }
        }
        return true;
    }

    public static int restMin2(String s) {
        if (s == null) {
            return 0;
        }
        if (s.length() < 2) {
            return s.length();
        }
        char[] str = s.toCharArray();
        return process(str, 0, str.length - 1, false);
    }

    /**
     * @param has 表示前面是否跟着和 str[L] 相同的字符
     */
    public static int process(char[] str, int L, int R, boolean has) {
        if (L > R) {
            return 0;
        }
        // 前面的字符如果和 str[L] 一样，返回1，反之返回0
        if (L == R) {
            return has ? 1 : 0;
        }

        int index = L;
        // 前面是否跟着和 str[L] 一样的字符
        int K = has ? 1 : 0;
        // 后面是否有连续跟着和 str[L] 一样的字符，统计
        while (index <= R && str[index] == str[L]) {
            K++;
            index++;
        }

        // 如果跟着和 str[L] 一样的字符，决定这此就消掉 + 后面字符消除调用
        int way1 = (K > 1 ? 0 : 1) + process(str, index, R, false);

        int way2 = Integer.MAX_VALUE;
        // 前面跟着相同的字符不再次一起消除，尝试和 index 位置到后面的字符一起消除
        for (int split = index; split <= R; split++) {
            // 如果发现了后面还有和 str[L] 位置的字符一样的，并且不是连续的
            if (str[split] == str[L] && str[split] != str[split - 1]) {
                // 尝试消除 index~split 中间的字符，如果消没了
                if (process(str, index, split - 1, false) == 0) {
                    // 就开始尝试消除 split ~ R 区间的字符，并且连同 index 之前的字符一同消掉
                    way2 = Math.min(way2, process(str, split, R, K != 0));
                }
            }
        }
        return Math.min(way1, way2);
    }


    // 优良尝试的动态规划版本
    public static int restMin3(String s) {
        if (s == null) {
            return 0;
        }
        if (s.length() < 2) {
            return s.length();
        }
        char[] str = s.toCharArray();
        int N = str.length;
        int[][][] dp = new int[N][N][2];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                for (int k = 0; k < 2; k++) {
                    dp[i][j][k] = -1;
                }
            }
        }
        return dpProcess(str, 0, N - 1, false, dp);
    }

    public static int dpProcess(char[] str, int L, int R, boolean has, int[][][] dp) {
        if (L > R) {
            return 0;
        }
        int K = has ? 1 : 0;
        if (dp[L][R][K] != -1) {
            return dp[L][R][K];
        }
        int ans = 0;
        if (L == R) {
            ans = (K == 0 ? 1 : 0);
        } else {
            int index = L;
            int all = K;
            while (index <= R && str[index] == str[L]) {
                all++;
                index++;
            }
            int way1 = (all > 1 ? 0 : 1) + dpProcess(str, index, R, false, dp);
            int way2 = Integer.MAX_VALUE;
            for (int split = index; split <= R; split++) {
                if (str[split] == str[L] && str[split] != str[split - 1]) {
                    if (dpProcess(str, index, split - 1, false, dp) == 0) {
                        way2 = Math.min(way2, dpProcess(str, split, R, all > 0, dp));
                    }
                }
            }
            ans = Math.min(way1, way2);
        }
        dp[L][R][K] = ans;
        return ans;
    }

}

package com.zuochengyun.算法基础班.code46动态规划猜法和外部信息简化上;

import java.util.LinkedList;

/**
 * 给定一个数组arr，和一个正数M，
 * 返回在arr的子数组在长度不超过M的情况下，最大的累加和
 *
 * @author 钢人
 */
public class Code04_MaxSumLengthNoMore {

    public static int maxSum(int[] arr, int M) {
        if (arr == null || arr.length == 0 || M < 1) {
            return 0;
        }
        int N = arr.length;
        int[] sum = new int[N];
        sum[0] = arr[0];
        // 求累加和
        for (int i = 1; i < N; i++) {
            sum[i] = sum[i - 1] + arr[i];
        }

        // 链表
        LinkedList<Integer> qmax = new LinkedList<>();
        int i = 0;
        int end = Math.min(N, M);
        for (; i < end; i++) {
            while (!qmax.isEmpty() && sum[qmax.peekLast()] <= sum[i]) {
                qmax.pollLast();
            }
            qmax.add(i);
        }

        int max = sum[qmax.peekFirst()];
        int L = 0;
        for (; i < N; L++, i++) {
            if (qmax.peekFirst() == L) {
                qmax.pollFirst();
            }
            while (!qmax.isEmpty() && sum[qmax.peekLast()] <= sum[i]) {
                qmax.pollLast();
            }
            qmax.add(i);
            max = Math.max(max, sum[qmax.peekFirst()] - sum[L]);
        }
        for (; L < N - 1; L++) {
            if (qmax.peekFirst() == L) {
                qmax.pollFirst();
            }
            max = Math.max(max, sum[qmax.peekFirst()] - sum[L]);
        }
        return max;
    }

}

package com.zuochengyun.算法基础班.code46动态规划猜法和外部信息简化上;

/**
 * 消除字符串，只能消除相邻一样的字符，消除之后就没有了
 * 得分 = 消除字符个数 * 个数
 * 例如：aababb
 * 消除aa得分4，在消除bb得分4，在单独消除a和b得分2，最终得分10
 * <p>
 * 测试链接 : <a href="https://leetcode.com/problems/remove-boxes/"/>
 *
 * @author 钢人
 */
public class Code02_RemoveBoxes {

    /**
     * @param arr 原字符数组
     * @param L 左边消到了什么位置
     * @param R 右边消到了什么位置
     * @param K 前面跟着几个和 L 位置相同的字符
     */
    public static int func1(int[] arr, int L, int R, int K) {
        if (L > R) {
            return 0;
        }
        // 其他后续得分 + 当前 L 位置字符单独消除，消除得分 K * K
        int ans = func1(arr, L + 1, R, 0) + (K + 1) * (K + 1);
        // L 位置字符被消除了，开始尝试 L+1 位置
        for (int i = L + 1; i <= R; i++) {
            // 如果发现有和 L 位置一样的字符，尝试跟 L 位置一起消除
            if (arr[i] == arr[L]) {
                // func1[0]:单独消除 L+1 位置的字符 + func1[1]:连着一起消除L位置的字符
                ans = Math.max(ans, func1(arr, L + 1, i - 1, 0) + func1(arr, i, R, K + 1));
            }
        }
        return ans;
    }


    public static int removeBoxes1(int[] boxes) {
        int N = boxes.length;
        int[][][] dp = new int[N][N][N];
        int ans = process1(boxes, 0, N - 1, 0, dp);
        return ans;
    }

    public static int process1(int[] boxes, int L, int R, int K, int[][][] dp) {
        if (L > R) {
            return 0;
        }
        if (dp[L][R][K] > 0) {
            return dp[L][R][K];
        }
        int ans = process1(boxes, L + 1, R, 0, dp) + (K + 1) * (K + 1);
        for (int i = L + 1; i <= R; i++) {
            if (boxes[i] == boxes[L]) {
                ans = Math.max(ans, process1(boxes, L + 1, i - 1, 0, dp) + process1(boxes, i, R, K + 1, dp));
            }
        }
        dp[L][R][K] = ans;
        return ans;
    }

}

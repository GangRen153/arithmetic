package com.zuochengyun.算法基础班.code46动态规划猜法和外部信息简化上;

/**
 * 给定一个正数数组，表示气球，值表示打爆气球的得分
 * 打爆一个气球的得分=左边距离自己没爆的气球分数（如果没有默认是1） * 自己 * 右边距离自己没爆的气球分数（如果没有默认是1）
 * 问：怎么打爆气球得分最高
 * <p>
 * 测试链接 : <a href="https://leetcode.com/problems/burst-balloons/"/>
 *
 * @author 钢人
 */
public class Code01_BurstBalloons {

    public static int maxCoins0(int[] arr) {
        int N = arr.length;
        int[] help = new int[N + 2];
        for (int i = 0; i < N; i++) {
            help[i + 1] = arr[i];
        }
        help[0] = 1;
        help[N + 1] = 1;
        return func(help, 1, N);
    }

    public static int func(int[] arr, int L, int R) {
        // 剩下最后一个气球的时候
        if (L == R) {
            return arr[L - 1] * arr[L] * arr[R + 1];
        }

        // 最后打爆 L 位置的气球 + 后续打爆的气球
        int max = func(arr, L + 1, R) + arr[L - 1] * arr[L] * arr[R + 1];
        // 最后打爆 R 位置的气球
        max = Math.max(max, func(arr, L, R - 1) + arr[L - 1] * arr[L] * arr[R + 1]);

        // 尝试中间位置，L和R位置都打爆了，所以从 L+1 开始，小于 R 位置
        for (int i = L + 1; i < R; i++) {
            // 打爆当前 i 位置的得分
            int last = arr[L - 1] * arr[i] * arr[R + 1];
            // 打爆左边的
            int left = func(arr, L, i - 1);
            // 打爆右边的
            int right = func(arr, i + 1, R);
            // 当前得分
            int cur = left + right + last;
            // 取最大
            max = Math.max(max, cur);
        }
        return max;
    }


    public static int maxCoins2(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        if (arr.length == 1) {
            return arr[0];
        }
        int N = arr.length;
        int[] help = new int[N + 2];
        help[0] = 1;
        help[N + 1] = 1;
        for (int i = 0; i < N; i++) {
            help[i + 1] = arr[i];
        }
        int[][] dp = new int[N + 2][N + 2];
        for (int i = 1; i <= N; i++) {
            dp[i][i] = help[i - 1] * help[i] * help[i + 1];
        }
        for (int L = N; L >= 1; L--) {
            for (int R = L + 1; R <= N; R++) {
                int ans = help[L - 1] * help[L] * help[R + 1] + dp[L + 1][R];
                ans = Math.max(ans, help[L - 1] * help[R] * help[R + 1] + dp[L][R - 1]);
                for (int i = L + 1; i < R; i++) {
                    ans = Math.max(ans, help[L - 1] * help[i] * help[R + 1] + dp[L][i - 1] + dp[i + 1][R]);
                }
                dp[L][R] = ans;
            }
        }
        return dp[1][N];
    }

}

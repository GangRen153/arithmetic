package com.zuochengyun.算法基础班.code32IndexTree和AC自动机;

/**
 * 计算数组 arr 指定区域范围的累加和
 *
 * 1>: 更新指定下标的值
 *
 * 生成一个 help 辅助前缀和数组
 * help 辅助数组的规则（假设数组下标是从1开始）
 * 例如：arr[1~16]
 * help[1]包含arr[1]
 * help[2]=arr[1,2]
 * help[3]=arr[3]
 * help[4]=arr[1,2,3,4]
 * help[5]=arr[5]
 * help[6]=arr[5,6]
 * help[7]=arr[7]
 * help[8]=arr[1,2,3,4,5,6,7,8]
 * help[9]=arr[9]
 * help[10]=arr[9,10]
 * help[11]=arr[11]
 * help[12]=arr[9,10,11,12]
 * help[13]=arr[13]
 * help[14]=arr[13,14]
 * help[15]=arr[15]
 * help[16]=arr[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
 * ...
 *
 * 给定一个 index 下标，得出这个下标锁管的原数组的前缀和范围
 * 计算方式，将 index 转为二进制，把二进制最后一个 1 抹掉，在 +1 之后得出的结果 newIndex
 * index 管的范围就是 newIndex ~ index 的范围
 * 例如：
 * index = 6 -> 0110
 * newIndex = 0100 + 1 = 0101
 * index 管的范围就是 0101~0110 = 5~6 的范围，其他同理
 *
 * @author 钢人
 */
public class Code01_IndexTree {

    public static class IndexTree {
        public int[] tree;
        public int N;
        public IndexTree(int size) {
            N = size;
            // 0 位置下标不使用，下标从 1 开始算，原数组默认给定 0
            tree = new int[N + 1];
        }

        /**
         * 拿出 1~index 位置的数累加和
         */
        public int sum(int index) {
            int res = 0;
            while (index > 0) {
                res += tree[index];
                // 推理和添加刚好逆向
                index -= index & -index;
            }
            return res;
        }

        /**
         * index 下标的值+d
         */
        public void add(int index, int d) {
            while (index <= N) {
                tree[index] += d;
                // index & -index 是取出 index 二进制的最后一个 1 的值
                // 例如：index=5 -> 110，index & -index = 10
                // 110+10 能得出 index 位置的数变了之后，后面数组累加和受影响的位置
                // 在举例：5位置的数变化之后，受影响的位置就能得出是6，8，16，32...
                index += index & -index;
            }
        }

    }

}

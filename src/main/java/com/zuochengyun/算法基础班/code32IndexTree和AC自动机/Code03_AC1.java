package com.zuochengyun.算法基础班.code32IndexTree和AC自动机;

import java.util.LinkedList;
import java.util.Queue;

/**
 * AC 自动机基本结构
 *
 * @author 钢人
 */
public class Code03_AC1 {

    public static class Node {
        // 有多少个字符串以该节点结尾
        public int end;
        // 匹配失败的时候，指向新的链路指针
        public Node fail;
        // 前缀树链路
        public Node[] nexts;

        public Node() {
            end = 0;
            fail = null;
            nexts = new Node[26];
        }
    }

    /**
     * AC 自动机结构
     */
    public static class ACAutomation {
        public Node root;

        public ACAutomation() {
            root = new Node();
        }

        /**
         * 添加敏感词
         */
        public void insert(String s) {
            char[] str = s.toCharArray();
            Node cur = root;
            int index = 0;
            for (int i = 0; i < str.length; i++) {
                // 找到 i 位置字符的链路下标位置
                index = str[i] - 'a';
                // 如果是null，那么创建一个新的节点
                if (cur.nexts[index] == null) {
                    cur.nexts[index] = new Node();
                }
                cur = cur.nexts[index];
            }
            // 统计路过该节点下标的数量
            cur.end++;
        }

        /**
         * 把所有关键词构建成前缀树结构
         */
        public void build() {
            // 根据宽度优先遍历构建前缀树，主要是为了处理 fail 指针的指向
            Queue<Node> queue = new LinkedList<>();
            queue.add(root);
            Node cur;
            Node cfail;
            while (!queue.isEmpty()) {
                cur = queue.poll();
                for (int i = 0; i < 26; i++) {
                    if (cur.nexts[i] == null) {
                        continue;
                    }
                    // 先将该节点的 fail 指针指向 root
                    // 如果后面没有 root 指向 i-'a' 位置的字符，那么这个字符的 fail 指针就指向root
                    // 如果有，那么这个 cur.nexts[i].fail 指针就将指向 root 连向 i-'a' 位置的字符下标
                    cur.nexts[i].fail = root;
                    cfail = cur.fail;
                    while (cfail != null) {// cfail 不是头节点的时候
                        if (cfail.nexts[i] != null) {// 并且 cfail[i] 位置后面还有连向其他字符
                            cur.nexts[i].fail = cfail.nexts[i];
                            break;
                        }
                        // 继续找 i 位置 i-'a' 字符连接到其他链路的字符
                        cfail = cfail.fail;
                    }
                    // 宽度添加遍历
                    queue.add(cur.nexts[i]);
                }
            }
        }

        /**
         * 统计敏感词出现的次数
         */
        public int containNum(String content) {
            char[] chars = content.toCharArray();
            Node cur = root;
            // 表示 fail 指向的那个链路节点
            Node follow;
            // content 每个字符所在的下标位置
            int index;
            // 统计敏感字符出现的次数
            int ans = 0;
            // 沿途寻找匹配每一个字符出现的次数
            for (int i = 0; i < chars.length; i++) {
                // 找到对应的下标位置
                index = chars[i] - 'a';
                // 在不是 root 节点的情况下，如果当前 chars[i]-'a' 字符没找到，就尝试找其他链路是否有 root 连向 chars[i]-'a' 的其他链路
                while (cur != root && cur.nexts[index] == null) {
                    cur = cur.fail;
                }
                // 如果 index 位置不是null，表示 index 位置后面还有链路连接着其他字符
                // 如果 index 位置是null，说明 chars[i]-'a' 字符后面没有其他可匹配到的后续链路了
                cur = cur.nexts[index] != null ? cur.nexts[index] : root;
                follow = cur;
                // follow 不是根节点的情况下，说明 chars[i]-'a' 字符后面还有其他链路可以匹配
                // 不管是不是从同一个敏感词匹配的链路，哪怕是跳到其他敏感词开始寻找后续匹配，至少后继是还可匹配的
                while (follow != root) {
                    // 如果 chars[i]-'a' 字符途径这个节点已经是最后一个，表示没有途径这条链路的敏感词字符
                    if (follow.end == -1) {
                        break;
                    }
                    // 把途径这个字符的敏感词都计算在内
                    ans += follow.end;
                    // 标记已经统计过了
                    follow.end = -1;
                    // 在试图从其他敏感词中查找匹配字符
                    follow = follow.fail;
                }
            }
            return ans;
        }
    }

}

package com.zuochengyun.算法基础班.code41四边形不等式上;

/**
 * 将给定数组 arr 拆成两部分，两部分数组的累加和尽量要大
 * 求 min {左部分累加和，右部分累加和}的最大值是多少？
 * 返回整个过程
 * <p>
 * 返回 [0,2,2,2,5,6,6]
 *
 * @author 钢人
 */
public class Code02_BestSplitForEveryPosition {

    public static int[] bestSplit1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return new int[0];
        }
        int N = arr.length;
        int[] ans = new int[N];
        ans[0] = 0;
        for (int range = 1; range < N; range++) {
            for (int s = 0; s < range; s++) {
                int sumL = 0;
                for (int L = 0; L <= s; L++) {
                    sumL += arr[L];
                }
                int sumR = 0;
                for (int R = s + 1; R <= range; R++) {
                    sumR += arr[R];
                }
                ans[range] = Math.max(ans[range], Math.min(sumL, sumR));
            }
        }
        return ans;
    }

    public static int sum(int[] sum, int L, int R) {
        return sum[R + 1] - sum[L];
    }

    public static int[] bestSplit2(int[] arr) {
        if (arr == null || arr.length == 0) {
            return new int[0];
        }

        int N = arr.length;
        int[] ans = new int[N];
        ans[0] = 0;
        int[] sum = new int[N + 1];
        for (int i = 0; i < N; i++) {
            sum[i + 1] = sum[i] + arr[i];
        }
        for (int range = 1; range < N; range++) {
            for (int s = 0; s < range; s++) {
                int sumL = sum(sum, 0, s);
                int sumR = sum(sum, s + 1, range);
                ans[range] = Math.max(ans[range], Math.min(sumL, sumR));
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{2, 3, 0, 1, 4, 2, 1};
        int[] ints = bestSplit1(arr);
        System.out.println();
    }

}

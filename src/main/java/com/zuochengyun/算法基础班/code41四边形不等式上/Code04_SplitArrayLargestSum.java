package com.zuochengyun.算法基础班.code41四边形不等式上;

/**
 * 给定一个数组 arr，里面的数表示画完每幅画所需要的时间
 * 在给定一个 k 表示有几个画家
 * k 个画家可以同时画画，但是只能同时画两幅相邻的画
 * 问：怎么画时间最短
 * <p>
 * 例如：
 * arr=[3,1,4]，num=2
 * 最好的分配方式为第一个画匠画 3 和 1，所需时间为 4。第二个画匠画 4，所需时间 为 4
 * <p>
 * 测试链接：<a href="https://leetcode.com/problems/split-array-largest-sum/"/>
 *
 * @author 钢人
 */
public class Code04_SplitArrayLargestSum {

    /**
     * 来自百度
     */
    public int splitArray(int[] nums, int k) {
        long[] sums = new long[nums.length];
        sums[0] = nums[0];
        for (int i = 1; i < sums.length; i++) {
            sums[i] = sums[i - 1] + nums[i];
        }
        return (int) process(nums, sums, 0, sums.length - 1, k);
    }

    public long process(int[] nums, long[] sums, int start, int end, int k) {
        if (k <= 0) {
            return Integer.MAX_VALUE;
        }
        if (k == 1) {
            if (start == 0) {
                return sums[end];
            } else {
                return sums[end] - sums[start - 1];
            }
        }
        long min = Long.MAX_VALUE;
        for (int i = start; i <= end; i++) {
            long leftSum;
            if (start == 0) {
                leftSum = sums[i];
            } else {
                leftSum = sums[i] - sums[start - 1];
            }
            min = Math.min(min, Math.max(leftSum, process(nums, sums, i + 1, end, k - 1)));
        }
        return min;
    }

    /**
     * 来自百度
     */
    public int splitArray2(int[] nums, int k) {
        long left = 0, right = 0;
        for (int n : nums) {
            right += n;
        }
        if (k == 1) {
            return (int) right;
        }
        long result = 0;
        long mid;
        while (left <= right) {
            mid = left + right >> 1;
            if (judge(mid, nums, k)) {
                result = mid;
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return (int) result;
    }

    private boolean judge(long mid, int[] nums, int m) {
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > mid) {
                return false;
            }
            if (sum + nums[i] > mid) {
                sum = nums[i];
                m--;
            } else {
                sum += nums[i];
            }
        }
        return m >= 1;
    }


    public static int splitArray3(int[] nums, int K) {
        int N = nums.length;
        int[] sum = new int[N + 1];
        for (int i = 0; i < N; i++) {
            sum[i + 1] = sum[i] + nums[i];
        }
        int[][] dp = new int[N][K + 1];
        // 每个人画画第一幅画的时候所需时间
        for (int j = 1; j <= K; j++) {
            dp[0][j] = nums[0];
        }
        // 第一个人画所有的画需要的时间
        for (int i = 1; i < N; i++) {
            dp[i][1] = sum(sum, 0, i);
        }

        // 计算完成每幅画最好时间
        for (int i = 1; i < N; i++) {
            // 在计算两个人画所有画的时候最好时间，以此累加，三个人画所有画的最好时间，四个人...K个人画所有画的最好时间
            for (int j = 2; j <= K; j++) {
                int ans = Integer.MAX_VALUE;
                // 枚举是完全不优化的！
                for (int leftEnd = 0; leftEnd <= i; leftEnd++) {
                    int leftCost = leftEnd == -1 ? 0 : dp[leftEnd][j - 1];
                    int rightCost = leftEnd == i ? 0 : sum(sum, leftEnd + 1, i);
                    int cur = Math.max(leftCost, rightCost);
                    if (cur < ans) {
                        ans = cur;
                    }
                }
                dp[i][j] = ans;
            }
        }
        return dp[N - 1][K];
    }

    public static int sum(int[] sum, int L, int R) {
        return sum[R + 1] - sum[L];
    }

}

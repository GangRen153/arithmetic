package com.zuochengyun.算法基础班.code41四边形不等式上;

/**
 * 石子合并问题
 *
 * 给定 N 个石子，两两合并，但是只能两个相邻的石子合并
 * 两个石子合并的代价是两个石子的相加和，问将 N 个石子合并成一个石子最小代价
 *
 * 例如：
 * 1538
 * 1. 5和3合，代价是8，剩下188
 * 2. 1和8合，代价的9，剩下98
 * 3. 9和8合，代价是17，最终代价是8+9+17=34
 *
 * 问给出一堆石子，合并最小代价是多少
 *
 * @author 钢人
 */
public class Code03_StoneMerge {

    public static int min1(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        int N = arr.length;
        int[] s = sum(arr);
        return process1(0, N - 1, s);
    }

    public static int process1(int L, int R, int[] sum) {
        // 如果合并的石子是属于同一个石子，不能合并，直接返回代价0
        if (L == R) {
            return 0;
        }
        int next = Integer.MAX_VALUE;
        for (int leftEnd = L; leftEnd < R; leftEnd++) {
            // 每次都是单独L位置合并 + L后面所有的数合并得到的结果
            next = Math.min(next, process1(L, leftEnd, sum) + process1(leftEnd + 1, R, sum));
        }
        // 返回本次合并的代价 arr[R] + arr[L]
        return next + w(sum, L, R);
    }

    public static int min2(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        int N = arr.length;
        int[] sum = sum(arr);
        int[][] dp = new int[N][N];
        for (int L = N - 2; L >= 0; L--) {
            for (int R = L + 1; R < N; R++) {
                int next = Integer.MAX_VALUE;
                for (int leftEnd = L; leftEnd < R; leftEnd++) {
                    // 每次都是单独L位置合并 + L后面所有的数合并得到的结果
                    next = Math.min(next, dp[L][leftEnd] + dp[leftEnd + 1][R]);
                }
                // 返回本次合并的代价 arr[R] + arr[L]
                dp[L][R] = next + w(sum, L, R);
            }
        }
        return dp[0][N - 1];
    }

    private static int w(int[] sum, int L, int R) {
        return sum[R + 1] - sum[L];
    }

    private static int[] sum(int[] arr) {
        int[] sum = new int[arr.length + 1];
        sum[0] = 0;
        for (int i = 0; i < arr.length; i++) {
            sum[i + 1] = sum[0] + arr[i];
        }
        return sum;
    }

}

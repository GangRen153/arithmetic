package com.zuochengyun.算法基础班.code41四边形不等式上;

/**
 * 将给定数组 arr 拆成两部分，两部分数组的累加和尽量要大
 * 求 min {左部分累加和，右部分累加和}的最大值是多少？
 *
 * 例如：
 * 2 301421 -> 2
 * 23 01421 -> 5
 * 230 1421 -> 5
 * 2301 421 -> 6
 * 23014 21 -> 3
 *
 * 最佳答案是 6
 *
 * @author 钢人
 */
public class Code01_BestSplitForAll {

    public static int bestSplit1(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        int N = arr.length;
        int ans = 0;
        for (int s = 0; s < N - 1; s++) {
            int sumL = 0;
            for (int L = 0; L <= s; L++) {
                sumL += arr[L];
            }
            int sumR = 0;
            for (int R = s + 1; R < N; R++) {
                sumR += arr[R];
            }
            ans = Math.max(ans, Math.min(sumL, sumR));
        }
        return ans;
    }

    public static int bestSplit2(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        int N = arr.length;
        int sumAll = 0;
        for (int num : arr) {
            sumAll += num;
        }

        int ans = 0;
        int sumL = 0;
        for (int s = 0; s < N; s++) {
            // 累加所部分数组的累加和
            sumL += arr[s];
            // 计算出右部分数组累加和
            int sumR = sumAll - sumL;
            // 取出左部分数组累加和 和 右部分数组累加和那个小，在比之前统计的取最大
            ans = Math.max(ans, Math.min(sumL, sumR));
        }
        return ans;
    }

}

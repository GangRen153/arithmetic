package com.zuochengyun.算法基础班.code09排序总结及链表问题;

import java.util.Stack;

/**
 * 给定一个字符串，判断是否是回文
 *
 * @author 钢人
 */
public class Code02_IsPalindromeList {

    public static class Node {
        public int value;
        public Node next;

        public Node(int v) {
            value = v;
        }
    }

    /**
     * 用栈实现，逆序比较
     * 哈希表方法特别简单（笔试用）
     */
    public static boolean isPalindrome1(Node head) {
        Stack<Node> stack = new Stack<>();
        Node cur = head;
        while (cur != null) {
            stack.push(cur);
            cur = cur.next;
        }
        while (head != null) {
            if (head.value == stack.pop().value) {
                return false;
            }
            head = head.next;
        }
        return true;
    }

    /**
     * 哈希表方法特别简单（笔试用）
     */
    public static boolean isPalindrome2(Node head) {
        if (head == null || head.next == null) {
            return true;
        }
        // 快指针
        Node cur = head;
        // 慢指针
        Node right = head.next;
        // 找到中间节点和最后一个节点
        while (cur.next != null && cur.next.next != null) {
            right = right.next;
            cur = cur.next.next;
        }
        Stack<Node> stack = new Stack<>();
        // 从中间节点开始遍历，把中间节点后半部分压入栈中
        while (right != null) {
            stack.push(right);
            right = right.next;
        }
        // 逆序链表后半部分和链表前半部分比较
        while (!stack.isEmpty()) {
            if (head.value != stack.pop().value) {
                return false;
            }
            head = head.next;
        }
        return true;
    }

    /**
     * 给定一个单链表的头节点head，请判断该链表是否为回文结构
     * 改原链表的方法就需要注意边界了（面试用）
     */
    public static boolean isPalindrome3(Node head) {
        if (head == null || head.next == null) {
            return true;
        }

        // 找到中点位置
        Node n1 = head;
        // 找到尾部位置
        Node n2 = head;
        while (n2.next != null && n2.next.next != null) {
            n1 = n1.next;
            n2 = n2.next.next;
        }

        // 将链表右半部分逆序过来
        n2 = n1.next;
        n1.next = null;
        Node n3 = null;
        while (n2 != null) {
            n3 = n2.next;
            n2.next = n1;
            n1 = n2;
            n2 = n3;
        }

        // 按照正序比较
        n3 = n1;
        n2 = head;
        boolean res = true;
        while (n1 != null && n2 != null) {
            if (n1.value != n2.value) {
                res = false;
                break;
            }
            n1 = n1.next;
            n2 = n2.next;
        }

        // 把逆序过来的部分在还原
        n1 = n3.next;
        n3.next = null;
        while (n1 != null) {
            n2 = n1.next;
            n1.next = n3;
            n3 = n1;
            n1 = n2;
        }
        return res;
    }

}

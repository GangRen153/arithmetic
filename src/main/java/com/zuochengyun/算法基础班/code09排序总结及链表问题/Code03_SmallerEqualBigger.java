package com.zuochengyun.算法基础班.code09排序总结及链表问题;

/**
 * 将单向链表按某值划分成左边小、中间相等、右边大的形式
 *
 * @author 钢人
 */
public class Code03_SmallerEqualBigger {

    public static class Node {
        public int value;
        public Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    /**
     * 把链表放入数组里，在数组上做partition（笔试用）
     */
    public static Node listPartition1(Node head, int pivot) {
        if (head == null) {
            return head;
        }

        Node cur = head;
        int i = 0;
        // 统计有多少个节点
        while (cur != null) {
            i++;
            cur = cur.next;
        }
        Node[] nodeArr = new Node[i];
        i = 0;
        cur = head;
        // 把链表的节点放到数组中
        for (i = 0; i != nodeArr.length; i++) {
            nodeArr[i] = cur;
            cur = cur.next;
        }
        // 分割数组
        arrPartition(nodeArr, pivot);
        // 重新把数组中排好序的节点串成链表
        for (i = 1; i < nodeArr.length; i++) {
            nodeArr[i - 1].next = nodeArr[i];
        }
        // 最后一个节点置null
        nodeArr[i - 1].next = null;
        // 返回头节点
        return nodeArr[0];
    }

    private static void arrPartition(Node[] nodeArr, int pivot) {
        int small = -1;
        int big = nodeArr.length;
        int index = 0;
        while (index != big) {
            // index 位置的数小，往左边换
            if (nodeArr[index].value < pivot) {
                swap(nodeArr, ++small, index++);

            // 等于 pivot， 不动
            } else if (nodeArr[index].value == pivot) {
                index++;

            // 大于 pivot，往右边换
            } else {
                swap(nodeArr, --big, index);
            }
        }
    }

    public static void swap(Node[] nodeArr, int a, int b) {
        Node tmp = nodeArr[a];
        nodeArr[a] = nodeArr[b];
        nodeArr[b] = tmp;
    }


    /**
     * 分成小、中、大三部分，再把各个部分之间串起来（面试用）
     */
    public static Node listPartition2(Node head, int pivot) {
        Node sH = null, sT = null, eH = null, eT = null, mH = null, mT = null, next = null;
        while (head != null) {
            next = head.next;
            head.next = null;
            if (head.value < pivot) {
                if (sH == null) {
                    sH = head;
                    sT = head;
                } else {
                    sH.next = head;
                    sT.next = head;
                }
            } else if (head.value == pivot) {
                if (eH == null) {
                    eH = head;
                    eT = head;
                } else {
                    eH.next = head;
                    eT.next = head;
                }
            } else {
                if (mH == null) {
                    mH = head;
                    mT = head;
                } else {
                    mH.next = head;
                    mT = head;
                }
            }
            head = next;
        }

        // 把 <pivot、=pivot、>pivot的三部分链表连起来
        if (sT != null) {
            sT.next = eH;
            eT = eT == null ? sT : eT;
        }
        if (eT != null) {
            eT.next = mH;
        }
        return sH != null ? sH : (eH != null ? eH : mH);
    }

}

package com.zuochengyun.算法基础班.code09排序总结及链表问题;

import java.util.HashMap;

/**
 * random 指针是单链表节点结构中新增的指针，random 可能指向链表中的任意一个节点，也可能指向 null。
 * 给定一个由 Node 节点类型组成的无环单链表的头节点 head，请实现一个函数完成这个链表的复制，并返回复制的新链表的头节点。
 * <p>
 * 深度克隆
 * <p>
 * 测试链接 : <a href="https://leetcode.com/problems/copy-list-with-random-pointer/"/>
 *
 * @author 钢人
 */
public class Code04_CopyListWithRandom {

    public static class Node {
        int val;
        Node next;
        Node random;

        public Node(int val) {
            this.val = val;
            this.next = null;
            this.random = null;
        }
    }

    /**
     * 用 map 容器实现
     */
    public static Node copyRandomList1(Node head) {
        HashMap<Node, Node> map = new HashMap<>();
        Node cur = head;
        // 克隆新的节点，key是旧的节点，value是new出来的克隆节点
        while (cur != null) {
            map.put(cur, new Node(cur.val));
            cur = cur.next;
        }
        cur = head;
        // 把老的节点和新的节点都拿出来，再把老节点的属性值设置到新节点上
        while (cur != null) {
            map.get(cur).next = map.get(cur.next);
            map.get(cur).random = map.get(cur.random);
            cur = cur.next;
        }
        // 获取旧的头节点对应的value克隆头节点
        return map.get(head);
    }

    /**
     * 不用容器实现
     */
    public static Node copyRandomList2(Node head) {
        if (head == null) {
            return null;
        }

        // 遍历节点，把克隆节点插入到旧的节点中间
        Node cur = head;
        Node next;
        while (cur != null) {
            next = cur.next;
            cur.next = new Node(cur.val);
            cur.next.next = next;
            cur = next;
        }

        // 把克隆节点的 random 设置好
        cur = head;
        Node copy;
        while (cur != null) {
            next = cur.next.next;
            copy = cur.next;
            copy.random = cur.random != null ? cur.random.next : null;
            cur = next;
        }

        // 把克隆节点和旧节点分开
        Node res = head.next;
        cur = head;
        while (cur != null) {
            next = cur.next.next;
            copy = cur.next;
            cur.next = next;
            copy.next = next != null ? next.next : null;
            cur = next;
        }
        return res;
    }

}

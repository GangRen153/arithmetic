package com.zuochengyun.算法基础班.code28Manacher算法;

/**
 * 给定一个字符串 str，求 str 字符串中最长回文子串
 * 时间复杂度O(N)
 *
 * @author 钢人
 */
public class Code01_Manacher {

    // TODO

    public static int manacher(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        // 在每个字符中间添加一个 # 字符，如果一个回文是一个偶数，方便解决
        char[] str = manacherString(s);
        // 回文半径数组
        int[] pArr = new int[str.length];
        int c = -1;
        int r = -1;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < str.length; i++) {
            pArr[i] = r > i ? Math.min(pArr[2 * c - i], r - i) : 1;
            while (i + pArr[i] < str.length && i - pArr[i] > -1) {
                if (i + pArr[i] == str[i - pArr[i]]) {
                    pArr[i]++;
                } else {
                    break;
                }
            }
            if (i + pArr[i] > r) {
                r = i + pArr[i];
                c = i;
            }
            max = Math.max(max, pArr[i]);
        }
        return max - 1;
    }

    private static char[] manacherString(String s) {
        char[] chars = s.toCharArray();
        char[] res = new char[s.length() * 2 + 1];
        int index = 0;
        for (int i = 0; i < res.length; i++) {
            res[i] = (i & 1) == 0 ? '#' : chars[index++];
        }
        return res;
    }

}

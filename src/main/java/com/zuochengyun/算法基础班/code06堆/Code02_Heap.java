package com.zuochengyun.算法基础班.code06堆;


/**
 * 堆结构实现
 *
 * 堆结构本身是一个数组，但是堆也是完全二叉树结构
 *
 * @author 钢人
 */
public class Code02_Heap {


    public static class MyMaxHeap {
        public int[] heap;
        private final int limit;
        private int heapSize;

        public MyMaxHeap(int limit) {
            heap = new int[limit];
            this.limit = limit;
            heapSize = 0;
        }
        public boolean isEmpty() {
            return heapSize == 0;
        }

        /**
         * 是否满了
         */
        public boolean isFull() {
            return heapSize == limit;
        }

        /**
         * 添加
         */
        public void push(int value) {
            if (heapSize == limit) {
                throw new RuntimeException("heap is full");
            }
            heap[heapSize] = value;
            heapInsert(heap, heapSize);
            heapSize++;
        }

        /**
         * 调整新添加进来的值位置
         */
        private void heapInsert(int[] arr, int index) {
            // 依次往上找，比较父节点谁大谁到上面
            while (arr[index] > arr[(index) / 2]) {
                swap(arr, index, (index) / 2);
                index = (index) / 2;
            }
        }

        /**
         * 弹出
         */
        public int pop() {
            int ans = heap[0];
            // 把头节点的值和尾节点的值做交换
            swap(heap, 0, --heapSize);
            heapify(heap, 0, heapSize);
            return ans;
        }

        private void heapify(int[] arr, int index, int heapSize) {
            // 找到左孩子
            int left = index * 2 + 1;
            // 在有左孩子的情况下
            while (left < heapSize) {
                // 选出一个较大的孩子节点和父节点调整位置
                // 右孩子=left+1，如果当前节点有右孩子 && 右孩子比左孩子大，那么首当其选择大的节点作为和父节点比较
                int largest = left + 1 < heapSize && arr[left+1] > arr[left] ? left + 1 : left;
                // 跟父节点比较，选出较大的节点，进行位置调整
                largest = arr[largest] > arr[index] ? largest : index;
                // 如果父节点是大，直接跳过，不需要调整堆结构
                if (largest == index) {
                    break;
                }
                // 将较大的节点 largest 和父节点交换位置
                swap(arr, largest, index);
                // 在选举到新的头节点
                index = largest;
                // 重新找到左节点
                left = largest * 2 + 1;
            }
        }

        private void swap(int[] arr, int l, int r) {
            int tmp = arr[l];
            arr[l] = arr[r];
            arr[r] = tmp;
        }
    }

    public static class RightMaxHeap {
        private int[] arr;
        private final int limit;
        private int size;
        public RightMaxHeap(int limit) {
            arr = new int[limit];
            this.limit = limit;
            size = 0;
        }

        public boolean isEmpty() {
            return size == 0;
        }

        public boolean isFull() {
            return size == limit;
        }

        public void push(int value) {
            if (size == limit) {
                throw new RuntimeException("heap is full");
            }
            // 直接添加，也不调整结构
            arr[size++] = value;
        }

        public int pop() {
            int maxIndex = 0;
            // 找出整个数组中最大的节点
            for (int i = 1; i < size; i++) {
                if (arr[i] > arr[maxIndex]) {
                    maxIndex = i;
                }
            }
            // 把最大的节点拿出来
            int ans = arr[maxIndex];
            // 把最后一个节点上的数填充到弹出位置上
            arr[maxIndex] = arr[--size];
            return ans;
        }
    }

}

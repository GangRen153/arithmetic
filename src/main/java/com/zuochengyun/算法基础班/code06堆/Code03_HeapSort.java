package com.zuochengyun.算法基础班.code06堆;

/**
 * 堆结构排序
 *
 * @author 钢人
 */
public class Code03_HeapSort {

    /**
     * 排序堆结构数组
     */
    public static void heapSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }

        // sort1(arr);
        // sort2(arr);
        sort3(arr);
    }

    /**
     * 一个一个排序 O(N)
     */
    private static void sort1(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            // O(logN)
            heapInsert(arr, i);
        }
    }

    /**
     * O(N)
     */
    private static void sort2(int[] arr) {
        for (int i = arr.length - 1; i >= 0; i--) {
            heapify(arr, i, arr.length);
        }
    }

    /**
     * O(N*logN)
     */
    public static void sort3(int[] arr) {
        int heapSize = arr.length;
        swap(arr, 0, --heapSize);
        while (heapSize > 0) {
            heapify(arr, 0, heapSize);
            swap(arr, 0, --heapSize);
        }
    }

    private static void heapify(int[] arr, int index, int heapSize) {
        // 拿到 index 的左孩子
        int left = index * 2 + 1;
        // 如果有左孩子，调整 index 和 左孩子结构
        while (left < heapSize) {
            // 如果有右孩子 && 右孩子比左孩子大，首当其选右孩子和父节点作为调整节点
            int largest = left + 1 < heapSize && arr[left + 1] > arr[left] ? left + 1 : left;
            // 比较要调整的孩子节点和头节点谁大，选一个大的节点
            largest = arr[largest] > arr[index] ? largest : index;
            // 如果孩子节点不大于自己头节点，不需要调整，直接跳过
            if (largest == index) {
                break;
            }
            // 调整孩子节点和父节点的位置
            swap(arr, index, largest);
            // 重新选择新的头节点
            index = largest;
            // 重新选取新的父节点下的孩子节点
            left = index * 2 + 1;
        }
    }

    private static void heapInsert(int[] arr, int index) {
        while (arr[index] > arr[(index - 1) / 2]) {
            swap(arr, index, (index - 1) / 2);
            index = (index - 1) / 2;
        }
    }

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

}

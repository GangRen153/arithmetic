package com.zuochengyun.算法基础班.code06堆;

import java.util.PriorityQueue;

/**
 * 排序的是一个普通数组，而不是堆结构数组
 *
 * 已知一个几乎有序的数组。几乎有序是指，如果把数组排好顺序的话
 * 每个元素移动的距离一定不超过k，并且k相对于数组长度来说是比较小的。
 * 请选择一个合适的排序策略，对这个数组进行排序。
 *
 * @author 钢人
 */
public class Code04_SortArrayDistanceLessK {

    /**
     * 排序 arr 数组，但是每个元素移动的距离不能超过 k，找出最好的排序策略
     */
    public static void sortedArrDistanceLessK(int[] arr, int k) {
        if (arr == null || arr.length < 2 || k == 0) {
            return;
        }
        // 设定一个小根堆
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        int index = 0;
        // 把 arr 前 k 个数放到小根堆里
        for (; index < Math.min(arr.length - 1, k - 1); index++) {
            heap.add(arr[index]);
        }
        int i = 0;
        // 把前 k 个数从小根堆弹出
        for (; index < arr.length; i++, index++) {
            heap.add(arr[index]);
            arr[i] = heap.poll();
        }
        while (!heap.isEmpty()) {
            arr[i++] = heap.poll();
        }
    }

}

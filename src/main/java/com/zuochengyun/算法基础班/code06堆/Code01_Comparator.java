package com.zuochengyun.算法基础班.code06堆;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 没什么可说的，就是实现 Comparator 接口实现自定义排序
 *
 * @author 钢人
 */
public class Code01_Comparator {

    public static class MyComparator implements Comparator<MyComparator> {
        public int value;
        public String name;

        public MyComparator() {
        }
        public MyComparator(int value) {
            this.value = value;
        }

        @Override
        public int compare(MyComparator o1, MyComparator o2) {
            return o1.value - o2.value;
        }
    }

    public static void main(String[] args) {
        MyComparator[] comparators = new MyComparator[5];
        for (int i = 0; i < comparators.length; i++) {
            comparators[i] = new MyComparator((int) (Math.random() * 1000));
        }
        Arrays.sort(comparators, new MyComparator());
    }

}

package com.zuochengyun.算法基础班.code02运算符;

/**
 * 如何不用额外空间，将两个数的值交换
 *
 * &与：  全1为1，反则为0
 * |或：  有1为1，反则为0
 * ~非：  为1则0，为0则1
 * ^异或：不同为1，相同保持原位数
 *
 * @author 钢人
 */
public class Code01_Swap {

    public static void main(String[] args) {
        int a = 10;
        int b = 20;

        // 异或3次，交换两数的值
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        System.out.println(a + " - " + b);
    }

}

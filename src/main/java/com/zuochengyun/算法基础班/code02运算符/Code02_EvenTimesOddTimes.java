package com.zuochengyun.算法基础班.code02运算符;

/**
 * 处理异或问题
 *
 * @author 钢人
 */
public class Code02_EvenTimesOddTimes {

    /**
     * arr中，只有一种数，出现奇数次
     */
    public static void printOddTimesNum1(int[] arr) {

    }

    /**
     * arr中，有两种数，出现奇数次
     */
    public static void printOddTimesNum2(int[] arr) {

    }

    /**
     * 怎么把一个int类型的数，提取出最右侧的1来
     */
    public static int bit1counts(int N) {
        return 0;
    }

    public static void main(String[] args) {

    }

}

package com.zuochengyun.算法基础班.code24滑动窗口更新结构;

import java.util.LinkedList;

/**
 * 假设一个固定大小为 W 的窗口，依次划过arr，
 * 返回每一次滑出状况的最大值
 * 例如，arr = [4,3,5,4,3,3,6,7], W = 3
 * 窗口范围 L~R中的个数是3个
 * 返回：[5,5,5,4,6,7]
 *
 * @author 钢人
 */
public class Code01_SlidingWindowMaxArray {

    /**
     * 暴力解
     */
    public static int[] right(int[] arr, int w) {
        if (arr == null || arr.length < w || w < 1) {
            return null;
        }

        int N = arr.length;
        int[] res = new int[N - w + 1];
        int index = 0;
        int l = 0;
        int r = w - 1;
        while (r < N) {
            int max = arr[l];
            for (int i = l + 1; i <= r; i++) {
                max = Math.max(arr[i], max);
            }
            res[index++] = max;
            l++;
            r++;
        }
        return res;
    }

    /**
     * 滑动窗口实现
     */
    public static int[] getMaxWindow(int[] arr, int w) {
        if (arr == null || arr.length < w || w < 1) {
            return null;
        }

        int index = 0;
        int[] res = new int[arr.length - w + 1];
        LinkedList<Integer> qmax = new LinkedList<>();
        for (int R = 0; R < arr.length; R++) {
            // 把 <= arr[R] 的数全部弹出
            while (!qmax.isEmpty() && arr[qmax.peekLast()] <= arr[R]) {
                qmax.pollLast();
            }
            // 把 > arr[R] 的数留下，并且把 R 添加到队列中，这样队列中始终保持 > arr[R] 的数
            // 也就是让其 R 往右扩的时候，始终保持第一个数是本次子数组中最大的
            qmax.addLast(R);
            // 如果第一个下标=(R-W)，那么说明上一个while循环并没有弹出数，反而右添加了一个 R
            // 这时候队列中的数就超出了 W 的窗口大小，需要弹出第一个数，让其组成新的窗口数组
            if (qmax.peekFirst() == R - w) {
                qmax.pollFirst();
            }
            // 如果窗口大小 R 的边界在范围内，说明 W 窗口的 L 边界是 >= 0 的
            // 是一个完整的滑动窗口数组，表明可以计算其数组中的最大值
            if (R >= w - 1) {
                res[index++] = arr[qmax.peekFirst()];
            }
        }
        return res;
    }

    public static int[] generateRandomArray(int maxSize, int maxValue) {
        int[] arr = new int[(int) ((maxSize + 1) * Math.random())];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * (maxValue + 1));
        }
        return arr;
    }

    public static void main(String[] args) {
        int testTime = 100000;
        int maxSize = 100;
        int maxValue = 100;
        System.out.println("test begin");
        for (int i = 0; i < testTime; i++) {
            int[] arr = generateRandomArray(maxSize, maxValue);
            int w = (int) (Math.random() * (arr.length + 1));
            int[] ans1 = getMaxWindow(arr, w);
        }
        System.out.println("test finish");
    }

}

package com.zuochengyun.算法基础班.code24滑动窗口更新结构;

import java.util.LinkedList;

/**
 * 加油站良好出发点问题
 * <p>
 * 测试链接：<a href="https://leetcode.com/problems/gas-station"/>
 *
 * @author 钢人
 */
public class Code03_GasStation {

    // TODO

    public static int canCompleteCircuit(int[] gas, int[] cost) {
        boolean[] good = goodArray(gas, cost);
        for (int i = 0; i < gas.length; i++) {
            if (good[i]) {
                return i;
            }
        }
        return -1;
    }

    private static boolean[] goodArray(int[] g, int[] c) {
        int N = g.length;
        int M = N << 1;
        // 生成一个两倍 N 的数组长度
        int[] arr = new int[M];
        // 为了方便窗口滑动的时候，到了N位置的时候
        // N位置可以作为窗口的L位置，R滑到最后位置会返回到 0 位置
        // 这样就构成了窗口滑动，不然需要判断边界位置
        for (int i = 0; i < N; i++) {
            arr[i] = g[i] - c[i];
            arr[i + N] = g[i] - c[i];
        }
        // 生成整个数组的窗口 L~R 位置的前缀和
        for (int i = 1; i < M; i++) {
            arr[i] += arr[i - 1];
        }

        LinkedList<Integer> w = new LinkedList<>();
        // 求最小值滑动窗口
        for (int i = 0; i < N; i++) {
            while (!w.isEmpty() && arr[w.peekLast()] >= arr[i]) {
                w.pollLast();
            }
            w.addLast(i);
        }

        // 统计从每个加油站出发，能否转完一圈儿
        boolean[] ans = new boolean[N];
        for (int offset = 0, i = 0, j = N; j < M; offset = arr[i++], j++) {
            if (arr[w.peekFirst()] - offset >= 0) {
                ans[i] = true;
            }
            if (w.peekFirst() == i) {
                w.pollFirst();
            }
            while (!w.isEmpty() && arr[w.peekLast()] >= arr[j]) {
                w.pollLast();
            }
            w.addLast(j);
        }
        return ans;
    }

}

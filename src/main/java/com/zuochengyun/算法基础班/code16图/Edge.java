package com.zuochengyun.算法基础班.code16图;

/**
 * @author 钢人
 */
public class Edge {

    /**
     * 权重
     */
    public int weight;
    /**
     * 从哪个节点开始
     */
    public Node from;
    /**
     * 连向哪个节点
     */
    public Node to;

    public Edge(int weight, Node from, Node to) {
        this.weight = weight;
        this.from = from;
        this.to = to;
    }

}

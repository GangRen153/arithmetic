package com.zuochengyun.算法基础班.code16图;

import java.util.*;

/**
 * 拓扑排序
 *
 * 按照被指向节点的数量排序
 * 被其他节点指向的数量越少越靠前
 *
 * OJ链接：<a href="https://www.lintcode.com/problem/topological-sorting"/>
 * @author 钢人
 */
public class Code03_TopologicalOrderBFS {

    // 不要提交这个类
    public static class DirectedGraphNode {
        public int label;
        public ArrayList<DirectedGraphNode> neighbors;

        public DirectedGraphNode(int x) {
            label = x;
            neighbors = new ArrayList<DirectedGraphNode>();
        }
    }

    public static ArrayList<DirectedGraphNode> topSort(ArrayList<DirectedGraphNode> graph) {
        Map<DirectedGraphNode, Integer> indexGreeMap = new HashMap<>();
        // 默认初始化0
        for (DirectedGraphNode cur : graph) {
            indexGreeMap.put(cur, 0);
        }

        // 统计每个节点的入度连接数
        for (DirectedGraphNode cur : graph) {
            for (DirectedGraphNode next : cur.neighbors) {
                indexGreeMap.put(next, indexGreeMap.get(next) + 1);
            }
        }

        Queue<DirectedGraphNode> zeroQueue = new LinkedList<>();
        // 把入度是 0 的节点先添加到列表
        for (DirectedGraphNode cur : indexGreeMap.keySet()) {
            if (indexGreeMap.get(cur) == 0) {
                zeroQueue.add(cur);
            }
        }

        ArrayList<DirectedGraphNode> ans = new ArrayList<>();
        while (!zeroQueue.isEmpty()) {
            DirectedGraphNode cur = zeroQueue.poll();
            // 把入度是 0 的节点添加到数组
            ans.add(cur);
            for (DirectedGraphNode next : cur.neighbors) {
                indexGreeMap.put(next, indexGreeMap.get(next) - 1);
                // 等于 0 的话添加到队列
                if (indexGreeMap.get(next) == 0) {
                    zeroQueue.offer(next);
                }
            }
        }
        return ans;
    }

}

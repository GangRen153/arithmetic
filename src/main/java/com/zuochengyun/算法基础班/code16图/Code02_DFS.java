package com.zuochengyun.算法基础班.code16图;

import java.util.HashSet;
import java.util.Stack;

/**
 * 图深度优先遍历
 *
 * @author 钢人
 */
public class Code02_DFS {

    /**
     * 非递归方式实现
     */
    public static void dfs(Node start) {
        if (start == null) {
            return;
        }

        Stack<Node> stack = new Stack<>();
        HashSet<Node> set = new HashSet<>();
        stack.add(start);
        set.add(start);
        System.out.println(start.value);
        while (!stack.isEmpty()) {
            Node cur = stack.pop();
            for (Node next : cur.nexts) {
                if (!set.contains(next)) {
                    stack.push(cur);
                    stack.push(next);
                    set.add(next);
                    System.out.println(next.value);
                    break;
                }
            }
        }
    }

}

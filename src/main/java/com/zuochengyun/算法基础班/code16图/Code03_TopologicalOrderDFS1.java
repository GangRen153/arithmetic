package com.zuochengyun.算法基础班.code16图;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 拓扑排序
 *
 * 按节点连接数量排序
 *
 * OJ链接：<a href="https://www.lintcode.com/problem/topological-sorting"/>
 * @author 钢人
 */
public class Code03_TopologicalOrderDFS1 {

    // 不要提交这个类
    public static class DirectedGraphNode {
        public int label;
        public ArrayList<DirectedGraphNode> neighbors;

        public DirectedGraphNode(int x) {
            label = x;
            neighbors = new ArrayList<DirectedGraphNode>();
        }
    }

    public static class Record {
        public DirectedGraphNode node;
        public int deep;

        public Record(DirectedGraphNode n, int o) {
            node = n;
            deep = o;
        }
    }

    public static ArrayList<DirectedGraphNode> topSort(ArrayList<DirectedGraphNode> graph) {
        Map<DirectedGraphNode, Record> order = new HashMap<>();
        // 遍历统计每一个节点的连向深度数量
        for (DirectedGraphNode cur : graph) {
            f(cur, order);
        }

        // 把每一个节点统计出来的深度数量拿出来排序
        ArrayList<Record> recordArr = new ArrayList<>(order.values());
        recordArr.sort((o1, o2) -> o2.deep - o1.deep);

        ArrayList<DirectedGraphNode> ans = new ArrayList<>();
        for (Record r : recordArr) {
            ans.add(r.node);
        }
        return ans;
    }

    private static Record f(DirectedGraphNode cur, Map<DirectedGraphNode, Record> order) {
        if (order.containsKey(cur)) {
            return order.get(cur);
        }

        int follow = 0;
        for (DirectedGraphNode next : cur.neighbors) {
            follow = Math.max(follow, f(next, order).deep);
        }

        Record ans = new Record(cur, follow + 1);
        order.put(cur, ans);
        return ans;
    }

}

package com.zuochengyun.算法基础班.code16图;

/**
 * 图生成器
 *
 * 给定一个3*N的二维数组
 * [0]=权重
 * [1]=从哪个节点开始
 * [2]=到哪个节点
 *
 * 生成图结构
 *
 * @author 钢人
 */
public class GraphGenerator {

    public static Graph createGraph(int[][] matrix) {
        Graph graph = new Graph();
        for (int i = 0; i < matrix.length; i++) {
            int weight = matrix[i][0];
            int from = matrix[i][1];
            int to = matrix[i][2];

            // 节点不存在就创建
            if (!graph.nodes.containsKey(from)) {
                graph.nodes.put(from, new Node(from));
            }
            if (!graph.nodes.containsKey(to)) {
                graph.nodes.put(to, new Node(to));
            }

            // 拿到 from 节点
            Node fromNode = graph.nodes.get(from);
            // 拿到 to 节点
            Node toNode = graph.nodes.get(to);
            // 创建 from 有向边
            Edge edge = new Edge(weight, fromNode, toNode);
            // 添加 from 节点连向的节点和边
            fromNode.nexts.add(toNode);
            fromNode.edges.add(edge);

            // 更新边的出入度
            fromNode.out++;
            toNode.in++;
            // 图里添加一条边
            graph.edges.add(edge);
        }
        return graph;
    }

}

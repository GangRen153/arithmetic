package com.zuochengyun.算法基础班.code16图;

import java.util.*;

/**
 * 拓扑结构排序
 * <p>
 * 按照入度排序
 *
 * @author 钢人
 */
public class Code03_TopologySort {

    public static List<Node> sortedTopology(Graph graph) {
        // key：节点，value：节点入度
        HashMap<Node, Integer> inMap = new HashMap<>();
        // 收集所有入度为0的节点
        Queue<Node> zeroInQueue = new LinkedList<>();
        for (Node node : graph.nodes.values()) {
            inMap.put(node, node.in);
            if (node.in == 0) {
                zeroInQueue.add(node);
            }
        }

        // 入度越小排序越靠前
        List<Node> result = new ArrayList<>();
        while (!zeroInQueue.isEmpty()) {
            Node cur = zeroInQueue.poll();
            result.add(cur);
            // 遍历当前节点连向的所有节点
            for (Node next : cur.nexts) {
                // 被连向的节点入度--
                inMap.put(next, inMap.get(next) - 1);
                // 如果减完了，没有入度了，放到列表里
                if (inMap.get(next) == 0) {
                    zeroInQueue.add(next);
                }
            }
        }
        return result;
    }

}

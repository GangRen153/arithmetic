package com.zuochengyun.算法基础班.code15并查集;

/**
 * 给定一个 N * N 的二维数组，表示 0~(N-1)个人城市之家是否相连
 * 如果 arr[i][j] = 1表示两个城市相连，否则就表示不相连
 * 最终要把方阵中是1的位置，如果能连起来
 * 问，这个方阵中，有多少个能相连起来的城市
 * <p>
 * 例如：
 * [    0 1 2 3
 *    0[1,1,0,1]
 *    1[1,1,0,1]
 *    2[0,0,1,1]
 *    3[1,1,1,1]
 * ]
 * 如上方阵4个城市之间的关系，自己和自己一定相连
 * 在此方阵中，有两个城市是相连的
 * <p>
 * 测试链接：<a href="https://leetcode.com/problems/friend-circles/"/>
 *
 * @author 钢人
 */
public class Code01_FriendCircles {

    public static int findCircleNum(int[][] M) {
        // 表示有N个人
        int N = M.length;
        UnionFind find = new UnionFind(N);
        for (int i = 0; i < N; i++) {
            // 自己肯定认识自己，所以不需要从j=0开始遍历
            // i认识j的，j也一定认识i，所以只需要遍历i是否认识j就行，不需要遍历j是否认识i
            // 所以j从i+1开始
            for (int j = i + 1; j < N; j++) {
                // 两个城市之间互相相连了，才合并到一个
                if (M[i][j] == 1) {
                    System.out.println(i + " - " + j);
                    find.union(i, j);
                }
            }
        }
        return find.getSets();
    }

    public static class UnionFind {
        // i 的代表节点是谁
        private int[] parent;
        // i 所在代表节点的集合大小
        private int[] size;
        // 辅助栈结构
        private int[] help;
        // 集合个数（朋友圈个数）
        private int sets;

        public UnionFind(int n) {
            parent = new int[n];
            size = new int[n];
            help = new int[n];
            sets = n;
            for (int i = 0; i < n; i++) {
                // 自己是代表节点
                parent[i] = i;
                // 代表节点是1，表示只有自己认识自己，其他谁都不认识
                size[i] = 1;
            }
        }

        // 找到代表节点
        public int find(int i) {
            int hi = 0;
            while (i != parent[i]) {
                help[hi++] = i;
                i = parent[i];
            }
            for (hi--; hi >= 0; hi--) {
                parent[help[hi]] = i;
            }
            return i;
        }

        public void union(int i, int j) {
            int f1 = find(i);
            int f2 = find(j);
            // 如果两个代表节点不一样，说明两人不是一个朋友圈
            if (f1 != f2) {
                if (size[f1] >= size[f2]) {
                    size[f1] += size[f2];
                    parent[f2] = f1;
                } else {
                    size[f2] += size[f1];
                    parent[f1] = f2;
                }
                sets--;
            }
        }

        public int getSets() {
            return sets;
        }
    }

    public static void main(String[] args) {
        int[][] arr = new int[][]{
                {1, 1, 0, 0},
                {1, 1, 0, 1},
                {0, 0, 1, 0},
                {0, 1, 0, 1}
        };

        /*arr = new int[][]{
                {1, 0, 1},
                {0, 1, 0},
                {1, 0, 1}
        };*/

        //arr = new int[][]{{1,0,0},{0,1,0},{0,0,1}};
        int circleNum = findCircleNum(arr);
        System.out.println(circleNum);
    }

}

package com.zuochengyun.算法基础班.code18暴力递归动态规划1;

/**
 * 给定 N 张正数纸牌，有一个先手和一个后手
 * 每个人只能从最左边或最右边拿一张牌，在两个人知道牌大小的情况下
 * 问先手和后手谁先赢
 *
 * @author 钢人
 */
public class Code02_CardsInLine {

    public static int win1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int first = f1(arr, 0, arr.length - 1);
        int second = g1(arr, 0, arr.length - 1);
        return Math.max(first, second);
    }

    /**
     * 先手
     */
    public static int f1(int[] arr, int l, int r) {
        // 表示剩下最后一张牌了
        if (l == r) {
            return arr[l];
        }
        int p1 = g1(arr, l + 1, r);
        int p2 = g1(arr, l, r - 1);
        return Math.max(p1, p2);
    }

    /**
     * 后手
     */
    public static int g1(int[] arr, int l, int r) {
        // 表示先手把牌拿完了，没有牌了
        if (l == r) {
            return 0;
        }
        int p1 = f1(arr, l + 1, r);
        int p2 = f1(arr, l, r - 1);
        return Math.min(p1, p2);
    }

    // -------------------------------------------------------------------------------

    public static int win2(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        int N = arr.length;
        int[][] fmap = new int[N][N];
        int[][] gmap = new int[N][N];

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                fmap[i][j] = -1;
                gmap[i][j] = -1;
            }
        }
        int first = f2(arr, 0, arr.length - 1, fmap, gmap);
        int second = g2(arr, 0, arr.length - 1, fmap, gmap);
        return Math.max(first, second);
    }

    public static int f2(int[] arr, int L, int R, int[][] fmap, int[][] gmap) {
        if (fmap[L][R] != -1) {
            return fmap[L][R];
        }

        int ans;
        if (L == R) {
            ans = arr[L];
        } else {
            int p1 = arr[L] + g2(arr, L + 1, R, fmap, gmap);
            int p2 = arr[R] + g2(arr, L, R - 1, fmap, gmap);
            ans = Math.max(p1, p2);
        }
        fmap[L][R] = ans;
        return ans;
    }

    public static int g2(int[] arr, int L, int R, int[][] fmap, int[][] gmap) {
        if (gmap[L][R] != -1) {
            return gmap[L][R];
        }

        int ans;
        if (L == R) {
            ans = 0;
        } else {
            int p1 = f2(arr, L + 1, R, fmap, gmap);
            int p2 = f2(arr, L, R - 1, fmap, gmap);
            ans = Math.min(p1, p2);
        }
        gmap[L][R] = ans;
        return ans;
    }


    /** ------------------------------------------------------------------------------- */


    public static int win3(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        int N = arr.length;
        int[][] fmap = new int[N][N];
        int[][] gmap = new int[N][N];
        // 先手先拿的情况下，剩下最后一张牌的时候
        for (int i = 0; i < N; i++) {
            fmap[i][i] = arr[i];
        }
        for (int startCol = 1; startCol < N; startCol++) {
            int L = 0;
            int R = startCol;
            while (R < N) {
                fmap[L][R] = Math.max(arr[L] + gmap[L + 1][R], arr[R] + gmap[L][R - 1]);
                gmap[L][R] = Math.min(fmap[L + 1][R], fmap[L][R - 1]);
                L++;
                R++;
            }
        }
        return Math.max(fmap[0][N - 1], gmap[0][N - 1]);
    }

}

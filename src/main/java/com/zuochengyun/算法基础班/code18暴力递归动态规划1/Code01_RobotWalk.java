package com.zuochengyun.算法基础班.code18暴力递归动态规划1;

/**
 * 机器人行走问题
 *
 * 有一个机器人，在长度为 1~N 的坐标上移动
 * 从开始位置 start 移动到 aim 位置，
 * 可以向左也可以向右移动，单只能移动 k 步
 * 问，有多少种方式能走到 aim 位置
 *
 * @author 钢人
 */
public class Code01_RobotWalk {

    public static int ways1(int N, int start, int aim, int K) {
        if (N < 2 || start < 1 || start > N || aim < 1 || aim > N || K < 1) {
            return -1;
        }
        return process(start, K, aim, N);
    }

    private static int process(int cur, int rest, int aim, int n) {
        if (rest == 0) {
            return cur == aim ? 1 : 0;
        }
        if (cur == 1) {
            return process(2, rest - 1, aim, n);
        }
        if (cur == n) {
            return process(n - 1, rest - 1, aim, n);
        }
        return process(cur + 1, rest - 1, aim, n) + process(cur - 1, rest - 1, aim, n);
    }


    public static int ways2(int N, int start, int aim, int K) {
        if (N < 2 || start < 1 || start > N || aim < 1 || aim > N || K < 1) {
            return -1;
        }
        int[][] dp = new int[N+1][K+1];
        for (int i = 0; i < dp.length; i++) {
            for (int j = 0; j < dp[0].length; j++) {
                dp[i][j] = -1;
            }
        }
        return process2(start, K, aim, N, dp);
    }

    private static int process2(int cur, int rest, int aim, int n, int[][] dp) {
        if (dp[cur][rest] != -1) {
            return dp[cur][rest];
        }

        int ans;
        if (cur == 1) {
            ans = process2(2, rest - 1, aim, n, dp);
        } else if (cur == n) {
            ans = process2(n - 1, rest - 1, aim, n, dp);
        } else {
            ans = process2(cur + 1, rest - 1, aim, n, dp) + process2(cur - 1, rest - 1, aim, n, dp);
        }
        dp[cur][rest] = ans;
        return ans;
    }

    public static int ways3(int N, int start, int aim, int K) {
        if (N < 2 || start < 1 || start > N || aim < 1 || aim > N || K < 1) {
            return -1;
        }

        int[][] dp = new int[N+1][K+1];
        dp[aim][0] = 1;
        for (int rest = 1; rest < dp.length; rest++) {
            dp[1][rest] = dp[2][rest - 1];
            for (int cur = 2; cur < dp[0].length; cur++) {
                dp[cur][rest] = dp[cur - 1][rest - 1] + dp[cur + 1][rest - 1];
            }
            dp[N][rest] = dp[N - 1][rest - 1];
        }
        return dp[start][K];
    }

}

package com.zuochengyun.算法基础班.code20暴力递归动态规划3;

/**
 * 象棋盘问题
 * <p>
 * 求马始终从 [0][0] 位置出发，跳指定步数，但是不能跳出棋盘
 * 问跳到指定位置上有多少种方法
 *
 * @author 钢人
 */
public class Code02_HorseJump {

    public static int jump(int a, int b, int k) {
        if (a < 0 || b < 0 || k < 1) {
            return 0;
        }
        return process(0, 0, k, a, b);
    }

    private static int process(int x, int y, int rest, int a, int b) {
        if (x < 0 || x > 9 || y < 0 || y > 8) {
            return 0;
        }
        if (rest == 0) {
            return x == a && y == b ? 1 : 0;
        }
        int ways = process(x + 2, y + 1, rest - 1, a, b);
        ways += process(x + 1, y + 2, rest - 1, a, b);
        ways += process(x - 1, y + 2, rest - 1, a, b);
        ways += process(x - 2, y + 1, rest - 1, a, b);
        ways += process(x - 2, y - 1, rest - 1, a, b);
        ways += process(x - 1, y - 2, rest - 1, a, b);
        ways += process(x + 1, y - 2, rest - 1, a, b);
        ways += process(x + 2, y - 1, rest - 1, a, b);
        return ways;
    }


    public static int dp(int a, int b, int k) {
        int[][][] dp = new int[10][9][k + 1];
        dp[a][b][0] = 1;
        // 根据 jump 函数 return 决定
        for (int rest = 1; rest <= k; rest++) {
            for (int x = 0; x < 10; x++) {
                for (int y = 0; y < 9; y++) {
                    int ways = pick(dp,x + 2, y + 1, rest - 1);
                    ways += pick(dp,x + 1, y + 2, rest - 1);
                    ways += pick(dp,x - 1, y + 2, rest - 1);
                    ways += pick(dp,x - 2, y + 1, rest - 1);
                    ways += pick(dp,x - 2, y - 1, rest - 1);
                    ways += pick(dp,x - 1, y - 2, rest - 1);
                    ways += pick(dp,x + 1, y - 2, rest - 1);
                    ways += pick(dp,x + 2, y - 1, rest - 1);
                    dp[x][y][rest] = ways;
                }
            }
        }
        return dp[0][0][k];
    }

    private static int pick(int[][][] dp, int x, int y, int rest) {
        if (x < 0 || x > 9 || y < 0 || y > 8) {
            return 0;
        }
        return dp[x][y][rest];
    }

}

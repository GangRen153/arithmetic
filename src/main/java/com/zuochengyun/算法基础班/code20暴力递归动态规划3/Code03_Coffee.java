package com.zuochengyun.算法基础班.code20暴力递归动态规划3;

/**
 * 咖啡机问题
 *
 * <p>
 * 有多台泡咖啡机，但只有 1 台洗咖啡机
 * 泡咖啡机和冲咖啡机可以并发进行，泡咖啡机只能一杯一杯的泡，洗咖啡机也只能一杯一杯的洗
 * 但是杯子可以不洗等自动挥发
 * <p>
 *
 * 四个参数：
 * arr：每个咖啡机冲咖啡所需要的时间
 * n：人数
 * a：洗杯子的时间
 * b：杯子自动挥发的时间
 * 假设时间点从 0 开始，返回所有人喝完咖啡并洗完咖啡杯的全部过程结束后，至少来到什么时间点
 *
 * @author 钢人
 */
public class Code03_Coffee {

    public static int right(int[] arr, int n, int a, int b) {
        // TODO
        return 0;
    }

}

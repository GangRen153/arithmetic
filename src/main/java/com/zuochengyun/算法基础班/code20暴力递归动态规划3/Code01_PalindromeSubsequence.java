package com.zuochengyun.算法基础班.code20暴力递归动态规划3;

/**
 * 最长回文子序列
 * <p>
 * 一个字符串的逆序串和原始字符串的最长公共子序列
 * 就是这个字符串的最长回文子序列
 * <p>
 * 测试链接：<a href="https://leetcode.com/problems/longest-palindromic-subsequence/"/>
 *
 * @author 钢人
 */
public class Code01_PalindromeSubsequence {

    public static int lpsl1(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        char[] chars = s.toCharArray();
        return f(chars, 0, chars.length - 1);
    }

    private static int f(char[] chars, int L, int R) {
        if (L == R) {
            return 1;
        }
        // 如果还剩两个字符，这两个字符一样，则能组成两个回文，否则就只能组成一个回文
        if (L == R - 1) {
            return chars[L] == chars[R - 1] ? 2 : 1;
        }
        // 最长回文既不以 L 开头，也不以 R 结尾
        int p1 = f(chars, L + 1, R - 1);
        // 最长回文以 L 开头，也不以 R 结尾
        int p2 = f(chars, L, R - 1);
        // 最长回文不以 L 开头，以 R 结尾
        int p3 = f(chars, L + 1, R);
        // 最长回文以 L 开头，也以 R 结尾
        int p4 = chars[L] != chars[R] ? 0 : 2 + f(chars, L + 1, R - 1);
        return Math.max(Math.max(p1, p2), Math.max(p3, p4));
    }

    public static int lpsl2(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }

        int N = s.length();
        char[] chars = s.toCharArray();
        int[][] dp = new int[N][N];
        dp[N - 1][N - 1] = 1;
        for (int i = 0; i < N; i++) {
            dp[i][i] = 1;
            dp[i][i + 1] = chars[i] == chars[i + 1] ? 2 : 1;
        }

        for (int L = N - 3; L >= 0; L++) {
            for (int R = L + 2; R < N; R++) {
                int p1 = dp[L + 1][R - 1];
                int p2 = dp[L][R - 1];
                int p3 = dp[L + 1][R];
                int p4 = chars[L] != chars[R] ? 0 : 2 + dp[L + 1][R - 1];
                dp[L][R] = Math.max(Math.max(p1, p2), Math.max(p3, p4));
            }
        }
        // 返回 dp 位置依赖，根据调用函数 lpsl1 的 return 改的
        return dp[0][N - 1];
    }

}

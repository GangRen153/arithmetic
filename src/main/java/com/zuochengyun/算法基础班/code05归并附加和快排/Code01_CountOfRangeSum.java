package com.zuochengyun.算法基础班.code05归并附加和快排;

/**
 * 归并排序附加题
 *
 * 给定一个整数数组arr，两个整数 lower 和 upper
 * 返回 arr 中有多少个子数组的累加和在[lower,upper]范围上
 *
 * <a href="https://leetcode.com/problems/count-of-range-sum/">...</a>
 *
 * @author 钢人
 */
public class Code01_CountOfRangeSum {

    public static int countRangeSum(int[] nums, int lower, int upper) {
        if (nums == null || nums.length == 0) {
            return 0;
        }

        // 累加和数组
        long[] sum = new long[nums.length];
        sum[0] = nums[0];
        for (int i = 1; i < nums.length; i++) {
            sum[i] = sum[i - 1] + nums[i];
        }
        return process(sum, 0, sum.length - 1, lower, upper);
    }

    private static int process(long[] sum, int l, int r, int lower, int upper) {
        if (l == r) {
            return sum[l] > lower && sum[l] < upper ? 1 : 0;
        }
        int mid = l + (r - l >> 1);
        return process(sum, l, mid, lower, upper) + process(sum, mid + 1, r, lower, upper) + merge(sum, l, mid, r, lower, upper);
    }

    private static int merge(long[] arr, int l, int mid, int r, int lower, int upper) {
        int ans = 0;
        // 一个用于控制大于 lower 范围的下标，一个用于控制小于 upper 范围的下标
        int windowL = l;
        int windowR = l;
        // 遍历右半区数组
        for (int i = mid + 1; i <= r; i++) {
            // 拿到当前值的范围存在的范围
            long min = arr[i] - lower;
            long max = arr[i] - upper;
            // 如果
            while (windowR <= mid && arr[windowR] <= max) {
                windowR++;
            }
            while (windowL <= mid && arr[windowL] < min) {
                windowL++;
            }
            ans += windowR - windowL;
        }

        // 不懂为什么要归并排序
        long[] help = new long[r - l + 1];
        int i = 0;
        int p1 = l;
        int p2 = mid + 1;
        while (arr[p1] <= mid && arr[p2] <= r) {
            help[i++] = arr[p1] <= arr[p2] ? arr[p1++] : arr[p2++];
        }
        while (p1 <= mid) {
            help[i++] = arr[p1]++;
        }
        while (p2 <= r) {
            help[i++] = arr[p2++];
        }
        for (i = 0; i < help.length; i++) {
            arr[l + i] = help[i];
        }
        return ans;
    }

}

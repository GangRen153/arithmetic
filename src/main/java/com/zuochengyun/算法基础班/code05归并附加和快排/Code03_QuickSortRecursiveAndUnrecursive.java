package com.zuochengyun.算法基础班.code05归并附加和快排;

/**
 * 快排递归版和非递归版
 *
 * @author 钢人
 */
public class Code03_QuickSortRecursiveAndUnrecursive {

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    /**
     * 递归版本
     */
    public static void quickSort1(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        process(arr, 0, arr.length - 1);
    }

    private static void process(int[] arr, int l, int r) {
        if (l >= r) {
            return;
        }
        // 取中间值
        swap(arr, l + (int) (Math.random() * (r - l + 1)), r);
        int[] equalArea = netherlandsFlag(arr, l, r);
        process(arr, l, equalArea[0] - 1);
        process(arr, equalArea[1] + 1, r);
    }

    private static int[] netherlandsFlag(int[] arr, int l, int r) {
        if (l > r) {
            return new int[] { -1, -1 };
        }
        if (l == r) {
            return new int[] { l, r };
        }
        int less = l - 1;
        int more = r;
        int index = l;
        while (index < more) {
            if (arr[index] == arr[r]) {
                index++;
            } else if (arr[index] < arr[r]) {
                swap(arr, index++, ++less);
            } else {
                swap(arr, index, --more);
            }
        }
        swap(arr, more, r);
        return new int[] { less + 1, more };
    }

    /**
     * 快排非递归版本
     * 用栈实现
     */
    public static void quickSort2(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
    }

    /**
     * 快排非递归版本
     * 用队列实现
     */
    public static void quickSort3(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        // TODO
    }

}

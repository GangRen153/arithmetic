package com.zuochengyun.算法基础班.code05归并附加和快排;

/**
 * 划分值快排 之 荷兰国旗问题
 *
 * <= num 的数放左边，> num 的数放右边
 * 或者
 * < num 的数放左边，=num 的数放中间，> num 的数放右边
 *
 * @author 钢人
 */
public class Code02_PartitionAndQuickSort {

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    /**
     * >= arr[r] 位置的数放左边
     * < arr[r] 位置的数放右边
     */
    public static void quickSort1(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        process(arr, 0, arr.length - 1);
    }

    private static void process(int[] arr, int l, int r) {
        if (l >= r) {
            return;
        }
        // 划分数组
        int mid = partition(arr, l, r);
        process(arr, l, mid - 1);
        process(arr, mid + 1, r);
    }

    private static int partition(int[] arr, int l, int r) {
        if (l > r) {
            return -1;
        }
        if (l == r) {
            return l;
        }
        // 表示当前划分到了什么位置
        int lessEqual = l - 1;
        // 如果划分范围的左边界和右边界没划分完，直接遍历
        while (l < r) {
            // 如果左边的数 <= 划分值
            if (arr[l] <= arr[r]) {
                swap(arr, l, ++lessEqual);
            }
            l++;
        }
        swap(arr, ++lessEqual, r);
        // 返回当前划分范围到了数组arr的下标位置
        return lessEqual;
    }


    /**
     * > arr[r] 位置的数放左边
     * = arr[r] 位置的数放中间
     * < arr[r] 位置的数放右边
     */
    public static void quickSort2(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        process2(arr, 0, arr.length - 1);
    }

    private static void process2(int[] arr, int l, int r) {
        if (l >= r) {
            return;
        }
        int[] equalArea = netherlandsFlag(arr, l, r);
        process2(arr, l, equalArea[0] - 1);
        process2(arr, equalArea[1] + 1, r);
    }

    private static int[] netherlandsFlag(int[] arr, int l, int r) {
        if (l > r) {
            return new int[] {-1, -1};
        }
        if (l == r) {
            return new int[] {l, r};
        }

        // 小于划分值区域最新移动到的下标位
        int less = l - 1;
        // 大于划分值区域最新移动到的下标位
        int more = r;
        // 下标移动边界
        int index = l;
        while (index < more) {
            if (arr[index] == arr[r]) {
                index++;
            } else if (arr[index] < arr[r]) {
                swap(arr, index++, ++less);
            } else {
                swap(arr, index++, --more);
            }
        }
        swap(arr, more, r);
        return new int[] {less + 1, more};
    }

    public static void main(String[] args) {

    }

}

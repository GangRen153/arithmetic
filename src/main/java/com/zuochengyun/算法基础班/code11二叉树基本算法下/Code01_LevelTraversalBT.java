package com.zuochengyun.算法基础班.code11二叉树基本算法下;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 实现二叉树的按层遍历
 *
 * 宽度优先遍历
 *
 * @author 钢人
 */
public class Code01_LevelTraversalBT {

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int v) {
            value = v;
        }
    }

    /**
     * 按层打印每一个节点（先打印根节点，在打印左右子节点，自上而下）
     * @param head
     */
    public static void level(Node head) {
        if (head == null) {
            return;
        }
        Queue<Node> queue = new LinkedList<>();
        queue.add(head);
        while (!queue.isEmpty()) {
            Node cur = queue.poll();
            System.out.println(cur.value);
            if (cur.left != null) {
                queue.add(cur.left);
            }
            if (cur.right != null) {
                queue.add(cur.right);
            }
        }
    }

}

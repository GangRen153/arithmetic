package com.zuochengyun.算法基础班.code11二叉树基本算法下;

/**
 * 给定二叉树中的某个节点，返回该节点的中序后继节点
 *
 * 后继节点：例如   a
 *             b   c
 *            d e f
 *          生成中序数组[d,b,e,a,f,c]
 *          那么 d 的后继节点就是b
 *              b 的后继几点就是e
 *              e 的后继节点就是a
 *              。。。
 *              c 没有后继节点
 *
 * @author 钢人
 */
public class Code06_SuccessorNode {

    public static class Node {
        public int value;
        public Node left;
        public Node right;
        public Node parent;

        public Node(int data) {
            this.value = data;
        }
    }


    public static Node getSuccessorNode(Node node) {
        if (node == null) {
            return null;
        }
        // 一直找node节点的左节点
        if (node.right != null) {
            return getLeftMost(node.right);

            // 一直找node节点的父节点，并且父节点的右节点不是node，
            // 换句话说直到找到node节点是父节点的左节点为止，最终这个父节点就是node节点的后继节点
        } else {
            Node parent = node.parent;
            while (parent != null && parent.right == node) {
                node = parent;
                parent = node.parent;
            }
            return parent;
        }
    }

    /**
     * 一直去找 node 节点的左节点，直到没有左节点为止，就是这个 node 节点的后继节点
     */
    private static Node getLeftMost(Node node) {
        if (node == null) {
            return null;
        }
        while (node.left != null) {
            node = node.left;
        }
        return node;
    }

}

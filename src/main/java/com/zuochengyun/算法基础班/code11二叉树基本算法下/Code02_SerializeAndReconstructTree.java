package com.zuochengyun.算法基础班.code11二叉树基本算法下;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * 1): 先序的方式序列化和反序列化
 * 2): 宽度优先遍历的方式序列化和反序列化
 *
 * @author 钢人
 */
public class Code02_SerializeAndReconstructTree {

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int data) {
            this.value = data;
        }
    }

    /**
     * 先序序列化
     */
    public static Queue<String> preSerial(Node head) {
        Queue<String> ans = new LinkedList<>();
        pres(head, ans);
        return ans;
    }

    private static void pres(Node head, Queue<String> ans) {
        if (head == null) {
            ans.add(null);
        } else {
            ans.add(String.valueOf(head.value));
            pres(head.left, ans);
            pres(head.right, ans);
        }
    }

    /**
     * 中序序列化
     */
    public static Queue<String> inSerial(Node head) {
        Queue<String> ans = new LinkedList<>();
        ins(head, ans);
        return ans;
    }

    private static void ins(Node head, Queue<String> ans) {
        if (head == null) {
            ans.add(null);
        } else {
            ins(head.left, ans);
            ans.add(String.valueOf(head.value));
            ins(head.right, ans);
        }
    }

    /**
     * 中序序列化
     */
    public static Queue<String> posSerial(Node head) {
        Queue<String> ans = new LinkedList<>();
        poss(head, ans);
        return ans;
    }

    private static void poss(Node head, Queue<String> ans) {
        if (head == null) {
            ans.add(null);
        } else {
            ins(head.left, ans);
            ins(head.right, ans);
            ans.add(String.valueOf(head.value));
        }
    }

    /** -------------- 反序列化 -------------- */
    /**
     * 先序反序列化
     */
    public static Node buildByPreQueue(Queue<String> preList) {
        if (preList == null || preList.isEmpty()) {
            return null;
        }
        return preb(preList);
    }

    private static Node preb(Queue<String> preList) {
        String value = preList.poll();
        if (value == null) {
            return null;
        }
        Node head = new Node(Integer.parseInt(value));
        head.left = preb(preList);
        head.right = preb(preList);
        return head;
    }

    /**
     * 后序反序列化
     */
    public static Node buildByPosQueue(Queue<String> posList) {
        if (posList == null || posList.isEmpty()) {
            return null;
        }
        Stack<String> stack = new Stack<>();
        while (!posList.isEmpty()) {
            stack.push(posList.poll());
        }
        return posb(stack);
    }

    private static Node posb(Stack<String> posStack) {
        String value = posStack.pop();
        if (value == null) {
            return null;
        }
        Node head = new Node(Integer.parseInt(value));
        head.left = posb(posStack);
        head.right = posb(posStack);
        return head;
    }

    /** ------------------------ 宽度优先遍历序列化 ------------------------ */
    /**
     * 宽度优先遍历序列化
     */
    public static Queue<String> levelSerial(Node head) {
        Queue<String> ans = new LinkedList<>();
        // 设置 null 的原因是因为反序列化需要
        if (head == null) {
            ans.add(null);
        } else {
            ans.add(String.valueOf(head.value));
            Queue<Node> queue = new LinkedList<>();
            queue.add(head);
            while (!queue.isEmpty()) {
                Node node = queue.poll();
                if (node.left != null) {
                    ans.add(String.valueOf(node.left.value));
                    queue.add(node.left);
                } else {
                    ans.add(null);
                }
                if (node.right != null) {
                    ans.add(String.valueOf(node.right.value));
                    queue.add(node.right);
                } else {
                    ans.add(null);
                }
            }
        }
        return ans;
    }

    /**
     * 宽度优先遍历反序列化
     */
    public static Node buildByLevelQueue(Queue<String> levelList) {
        if (levelList == null || levelList.isEmpty()) {
            return null;
        }
        Node head = generateNode(levelList.poll());
        Queue<Node> queue = new LinkedList<>();
        if (head != null) {
            queue.add(head);
        }
        while (!queue.isEmpty()) {
            Node node = queue.poll();
            node.left = generateNode(levelList.poll());
            node.right = generateNode(levelList.poll());
            if (node.left != null) {
                queue.add(node.left);
            }
            if (node.right != null) {
                queue.add(node.right);
            }
        }
        return head;
    }

    private static Node generateNode(String value) {
        if (value == null) {
            return null;
        }
        return new Node(Integer.parseInt(value));
    }

}

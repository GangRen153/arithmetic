package com.zuochengyun.算法基础班.code11二叉树基本算法下;

import java.util.ArrayList;
import java.util.List;

/**
 * 多叉树转二叉树
 * <p>
 * 本题测试链接：<a href="https://leetcode.com/problems/encode-n-ary-tree-to-binary-tree"/>
 *
 * @author 钢人
 */
public class Code03_EncodeNaryTreeToBinaryTree {

    public static class Node {
        public int val;
        public List<Node> children;

        public Node() {
        }

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    class Codec {
        /**
         * 多叉树转为二叉树
         */
        public TreeNode encode(Node root) {
            if (root == null) {
                return null;
            }
            TreeNode head = new TreeNode(root.val);
            head.left = en(root.children);
            return head;
        }

        private TreeNode en(List<Node> children) {
            if (children == null || children.isEmpty()) {
                return null;
            }
            TreeNode head = null;
            TreeNode cur = null;
            for (Node child : children) {
                TreeNode node = new TreeNode(child.val);
                if (head == null) {
                    head = node;
                } else {
                    cur.right = node;
                }
                cur = node;
                cur.left = en(child.children);
            }
            return head;
        }

        /**
         * 二叉树转为多叉树
         */
        public Node decode(TreeNode root) {
            if (root == null) {
                return null;
            }
            return new Node(root.val, de(root.left));
        }

        private List<Node> de(TreeNode root) {
            List<Node> children = new ArrayList<>();
            while (root != null) {
                // 把二叉树所有左节点封装到 list 中返回
                Node cur = new Node(root.val, de(root.left));
                children.add(cur);
                // 重新循环递归右节点封装到 list 返回
                root = root.right;
            }
            return children;
        }
    }
}

package com.zuochengyun.算法基础班.code11二叉树基本算法下;

/**
 * 折纸问题
 *
 * @author 钢人
 */
public class Code07_PaperFolding {

    public static void printAllFolds(int N) {
        process(1, N, true);
        System.out.println();
    }

    public static void process(int i, int N, boolean down) {
        if (i > N) {
            return;
        }
        process(i + 1, N, true);
        System.out.print(down ? "凹 " : "凸 ");
        process(i + 1, N, false);
    }

}

package com.zuochengyun.算法基础班.code11二叉树基本算法下;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * 求树结构最大宽度
 *
 * @author 钢人
 */
public class Code05_TreeMaxWidth {

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int data) {
            this.value = data;
        }
    }

    /**
     * 用 map 实现
     */
    public static int maxWidthUseMap(Node head) {
        if (head == null) {
            return 0;
        }

        Map<Node, Integer> levelMap = new HashMap<>();
        levelMap.put(head, 1);
        Queue<Node> queue = new LinkedList<>();
        queue.add(head);
        // 标记统计的层数
        int curLevel = 1;
        // 统计当前层级的节点数量
        int curLevelNodes = 0;
        int max = 0;
        while (!queue.isEmpty()) {
            Node cur = queue.poll();
            Integer curNodeLevel = levelMap.get(cur);
            if (cur.left != null) {
                levelMap.put(cur.left, curNodeLevel + 1);
                queue.add(cur.left);
            }
            if (cur.right != null) {
                levelMap.put(cur.right, curNodeLevel + 1);
                queue.add(cur.right);
            }
            // 当前 cur 节点的层级，如果是在当前层级，累加节点数
            if (curNodeLevel == curLevel) {
                curLevelNodes++;

                // 如果不在同一层级，说明 queue 把同一层的节点都弹出了，开始进入下一层遍历
            } else {
                max = Math.max(max, curLevelNodes);
                // 进入下一层，用递增进行标记层级数
                curLevel++;
                // 下一层的数量置1
                curLevelNodes = 1;
            }
        }
        max = Math.max(max, curLevelNodes);
        return max;
    }

    /**
     * 不使用额外空间 map
     */
    public static int maxWidthNoMap(Node head) {
        if (head == null) {
            return 0;
        }
        Queue<Node> queue = new LinkedList<>();
        queue.add(head);

        // 当前层级的最后一个节点
        Node curEnd = head;
        // 下一层节点最后一个节点
        Node nextEnd = null;
        int max = 0;
        // 统计当前层级的节点数量
        int curLevelNodes = 0;
        while (!queue.isEmpty()) {
            Node cur = queue.poll();
            // 如果当前节点有左节点，添加到队列里，并且设置为下一层最新的结束节点
            // 可能这个节点不是下一层最后一个节点，等后面的 cur 节点继续向右遍历查找，看是否还有其他子节点
            if (cur.left != null) {
                queue.add(cur.left);
                nextEnd = cur.left;
            }
            if (cur.right != null) {
                queue.add(cur.right);
                nextEnd = cur.right;
            }
            curLevelNodes++;
            // 说明这一层节点遍历完了
            if (cur == curEnd) {
                max = Math.max(max, curLevelNodes);
                curLevelNodes = 0;
                // 下一层最后一个节点设置成当前层最后一个节点，用于遍历下一层节点作为条件
                curEnd = nextEnd;
            }
        }
        return max;
    }

}

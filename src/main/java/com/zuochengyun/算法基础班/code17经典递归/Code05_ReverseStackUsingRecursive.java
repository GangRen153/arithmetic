package com.zuochengyun.算法基础班.code17经典递归;

import java.util.Stack;

/**
 * 给你一个栈，请你逆序这个栈，不能申请额外的数据结构，
 * 只能使用递归函数。 如何实现?
 *
 * @author 钢人
 */
public class Code05_ReverseStackUsingRecursive {

    public static void reverse(Stack<Integer> stack) {
        if (stack == null || stack.isEmpty()) {
            return;
        }
        int i = f(stack);
        reverse(stack);
        stack.push(i);
    }

    /**
     * 返回栈底元素
     */
    private static int f(Stack<Integer> stack) {
        Integer pop = stack.pop();
        if (stack.isEmpty()) {
            return pop;
        }
        // 拿到栈中最后一个栈中的元素
        int last = f(stack);
        // 把之前弹出的元素压到栈顶
        stack.push(pop);
        // 把栈底元素返回
        return last;
    }

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        reverse(stack);
        System.out.println(stack);
    }

}

package com.zuochengyun.算法基础班.code17经典递归;

import java.util.ArrayList;
import java.util.List;

/**
 * 打印字符串的全排列
 *
 * 给定一个字符串，打印这个字符串中所有字符的所有排序的字符串
 *
 * @author 钢人
 */
public class Code04_PrintAllPermutations {

    public static List<String> permutation1(String s) {
        List<String> ans = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return ans;
        }
        char[] chars = s.toCharArray();
        List<Character> rest = new ArrayList<>();
        for (char c : chars) {
            rest.add(c);
        }

        String path = "";
        f(rest, ans, path);
        return ans;
    }

    private static void f(List<Character> rest, List<String> ans, String path) {
        if (rest.isEmpty()) {
            ans.add(path);
            return;
        }

        for (int i = 0; i < rest.size(); i++) {
            Character cur = rest.get(i);
            rest.remove(i);
            f(rest, ans, path + cur);
            rest.add(i, cur);
        }
    }


    public static List<String> permutation2(String s) {
        List<String> ans = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return ans;
        }
        char[] chars = s.toCharArray();
        g(chars, 0, ans);
        return ans;
    }

    private static void g(char[] chars, int index, List<String> ans) {
        if (chars.length == index) {
            ans.add(String.valueOf(chars));
            return;
        }
        for (int i = index; i < chars.length; i++) {
            swap(chars, i, index);
            g(chars, i + 1, ans);
            swap(chars, i, index);
        }
    }

    private static void swap(char[] chars, int i, int j) {
        char iChar = chars[i];
        chars[i] = chars[j];
        chars[j] = iChar;
    }

    public static void main(String[] args) {
        List<String> abc = permutation1("abc");
        System.out.println(abc);
    }

}

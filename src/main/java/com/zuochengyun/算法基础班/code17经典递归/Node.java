package com.zuochengyun.算法基础班.code17经典递归;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 钢人
 */
public class Node {

    /**
     * 当前节点值
     */
    public int value;
    /**
     * 有多少个节点连向当前节点
     */
    public int in;
    /**
     * 当前节点连向多少个节点
     */
    public int out;
    /**
     * 当前节点连向其他的节点
     */
    public List<Node> nexts;
    /**
     * 当前节点连向其他节点的所有边
     */
    public List<Edge> edges;

    public Node(int value) {
        this.value = value;
        in = 0;
        out = 0;
        nexts = new ArrayList<>();
        edges = new ArrayList<>();
    }

}

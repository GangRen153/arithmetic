package com.zuochengyun.算法基础班.code17经典递归;

/**
 * 圆盘题
 *
 * 给定 N 个圆盘，和 x 根柱子，最开始这些圆盘在 from 柱子上，分别是小压大
 * 目标是将 from 柱子上的圆盘全部移动到 to 柱子上，想要移动大的圆盘必须
 * 先移动上面小的圆盘，并且必须满足小压大
 * 求实现。并且打印每一步移动过程
 *
 * @author 钢人
 */
public class Code02_Hanoi {

    // TODO

}

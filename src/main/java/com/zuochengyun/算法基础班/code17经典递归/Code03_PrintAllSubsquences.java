package com.zuochengyun.算法基础班.code17经典递归;

import java.util.ArrayList;
import java.util.List;

/**
 * 打印 str 所有子序列
 *
 * @author 钢人
 */
public class Code03_PrintAllSubsquences {

    public static List<String> subs(String s) {
        List<String> ans = new ArrayList<>();
        if (s == null || s.length() == 0) {
            return ans;
        }
        String path = "";
        char[] chars = s.toCharArray();
        process1(chars, 0, ans, path);
        return ans;
    }

    private static void process1(char[] chars, int index, List<String> ans, String path) {
        if (chars.length == index) {
            ans.add(path);
            return;
        }
        // 不要 index 位置
        process1(chars, index + 1, ans, path);
        // 要 index 位置
        process1(chars, index + 1, ans, path + chars[index]);
    }

}

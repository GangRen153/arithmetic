package com.zuochengyun.算法基础班.code17经典递归;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 图结构
 *
 * @author 钢人
 */
public class Graph {
    /**
     * 图里面所有的节点
     */
    public Map<Integer, Node> nodes;
    /**
     * 图里面所有的边
     */
    public Set<Edge> edges;

    public Graph() {
        this.nodes = new HashMap<>();
        this.edges = new HashSet<>();
    }

}

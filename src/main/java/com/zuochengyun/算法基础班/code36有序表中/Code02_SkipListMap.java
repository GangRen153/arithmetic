package com.zuochengyun.算法基础班.code36有序表中;

import java.util.ArrayList;

/**
 * 跳表
 *
 * @author 钢人
 */
public class Code02_SkipListMap {

    public static class SkipListNode<K extends Comparable<K>, V> {

        public K key;
        public V val;
        public ArrayList<SkipListNode<K, V>> nextNodes;

        public SkipListNode(K k, V v) {
            key = k;
            val = v;
            nextNodes = new ArrayList<SkipListNode<K, V>>();
        }

        // TODO

    }



}

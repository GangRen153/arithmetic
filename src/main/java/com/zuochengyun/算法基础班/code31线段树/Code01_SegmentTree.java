package com.zuochengyun.算法基础班.code31线段树;

/**
 * 给定一个数组arr，用户希望你实现如下三个方法
 * 1）void add(int L, int R, int V) :  让数组arr[L…R]上每个数都加上V
 * 2）void update(int L, int R, int V) :  让数组arr[L…R]上每个数都变成V
 * 3）int sum(int L, int R) :让返回arr[L…R]这个范围整体的累加和
 * 怎么让这三个方法，时间复杂度都是O(logN)
 *
 * @author 钢人
 */
public class Code01_SegmentTree {

    /**
     * 暴力计算
     */
    public static class Right {
        public int[] arr;

        public Right(int[] origin) {
            arr = new int[origin.length + 1];
            for (int i = 0; i < origin.length; i++) {
                arr[i + 1] = origin[i];
            }
        }

        public void update(int L, int R, int C) {
            for (int i = L; i <= R; i++) {
                arr[i] = C;
            }
        }

        public void add(int L, int R, int C) {
            for (int i = L; i <= R; i++) {
                arr[i] += C;
            }
        }

        public long query(int L, int R) {
            long ans = 0;
            for (int i = L; i <= R; i++) {
                ans += arr[i];
            }
            return ans;
        }
    }

    /**
     * 线段树实现，为了方便计算，下标从1开始，数组 0 位置废弃不用
     */
    public static class SegmentTree {
        private int MAXN;
        // 原始数组
        private int[] arr;
        // 累加和辅助数组
        private int[] sum;
        // 懒添加辅助数组
        private int[] lazy;
        // 表示更新成多少
        private int[] change;
        // 表示是否要更新，因为change默认是0，不知道是要更新成0还是其他，所以要boolean[]update 来辅助
        private boolean[] update;

        public SegmentTree(int[] origin) {
            // 0 位置废弃不用，下标从 1 开始，所以数组长度+1
            this.MAXN = origin.length + 1;
            arr = new int[MAXN];
            for (int i = 0; i < MAXN; i++) {
                arr[i] = origin[i];
            }
            sum = new int[MAXN << 2];
            lazy = new int[MAXN << 2];
            change = new int[MAXN << 2];
            update = new boolean[MAXN << 2];
        }

        /**
         * 向 rt 位置的下级位置分发
         */
        public void pushUp(int rt) {
            // sum[rt] = rt*2 + rt*2+1
            sum[rt] = sum[rt << 1] + sum[rt << 1 | 1];
        }

        /**
         * @param rt 辅助数组下标位置
         * @param ln 左边的节点数量
         * @param rn 右边的节点数量
         */
        public void pushDown(int rt, int ln, int rn) {
            // 是否存在懒更新成指定值的，如果存在，先把懒更新的全部更新
            if (update[rt]) {
                // 向下级节左点位置下发
                update[rt << 1] = true;
                // 向下级节右点位置下发，相当于rt*2+1
                update[rt << 1 | 1] = true;
                change[rt << 1] = change[rt];
                change[rt << 1 | 1] = change[rt];
                // 懒添加的数，直接置0即可
                lazy[rt << 1] = 0;
                lazy[rt << 1 | 1] = 0;
                // 向下级左右节点位置更新累加和
                sum[rt << 1] = change[rt] * ln;
                sum[rt << 1 | 1] = change[rt] * rn;
                // 当前辅助节点位置懒更新状态情况，因为已经下发到孩子节点位置上了
                update[rt] = false;
            }
            // 懒添加
            if (lazy[rt] != 0) {
                // 向下懒更新到左右孩子节点
                lazy[rt << 1] += lazy[rt];
                lazy[rt << 1 | 1] += lazy[rt];
                // 累加和也向下懒更新到孩子节点
                sum[rt << 1] += lazy[rt] * ln;
                sum[rt << 1 | 1] += lazy[rt] * rn;
                // 当前懒更新置0
                lazy[rt] = 0;
            }
        }

        /**
         * 初始化 sum 数组
         *
         * @param l  左边界范围
         * @param r  右边界范围
         * @param rt sum 中的下标
         */
        public void build(int l, int r, int rt) {
            if (l == r) {
                sum[rt] = arr[l];
            }
            int mid = (l + r) >> 1;
            build(l, mid, rt << 1);
            build(mid + 1, r, rt << 1 | 1);
            pushUp(rt);
        }

        /**
         * @param L、R、C 在 L~R 的范围上下发任务 C 值
         * @param l、r   原数组的左右 边界
         * @param rt    头节点位置，初始进来是 root 节点位置，后面通过递归调用会慢慢向 root 节点的左右孩子节点的堆结构下标划分
         */
        public void add(int L, int R, int C, int l, int r, int rt) {
            // 说明L~R的范围能包揽住
            // 例如：树左节点为l=1,r=49，右节点为 l=50,r=100
            // 范围：L=10,R=100，这时L=50~R=100的范围就能l=15~r=100的范围的节点包揽
            // 剩下的L=10~R=49的范围，因为L=1~R=9的范围是不累加 C 的
            // 所以左节点L=1~R=49不能直接包揽，需要进一步划分
            if (L <= l && r <= R) {
                // 更新累加和
                sum[rt] += C * (r - l + 1);
                lazy[rt] += C;
                return;
            }
            // 找到中间下标（例如：l=50,r=100）中间孩子，就是75的位置，也就是左节点的右边界，右节点的左边界
            int mid = (l + r) >> 1;
            // 先把当前没有办法包揽的范围上向下下发直接懒更新的值
            // 因为下次要这次的更新懒更新要进行范围下发
            pushDown(rt, mid - l + 1, r - mid);
            // 往左边节点的位置下发
            if (L <= mid) {
                // 向左孩子节点的下标范围位置划分
                add(L, R, C, l, mid, rt << 1);
            }
            // 往右边节点的位置下发
            if (R > mid) {
                // 向右孩子节点的下标范围位置划分，rt<<1|1 = rt*2+1
                add(L, R, C, mid + 1, r, rt << 1 | 1);
            }
            // 更新 rt 位置的累加和（sum[rt]=sum[左孩子]+sum[右孩子]）
            pushUp(rt);
        }

        /**
         * @param L、R、C 在 L~R 的范围上下发任务 C 值
         * @param l、r   原数组的左右 边界
         * @param rt    头节点位置，初始进来是 root 节点位置，后面通过递归调用会慢慢向 root 节点的左右孩子节点的堆结构下标划分
         */
        public void update(int L, int R, int C, int l, int r, int rt) {
            if (L <= l && r <= R) {
                // 栈辅助数组位置标记要更新状态
                update[rt] = true;
                // 标记 rt 位置全量更新成 C 值
                change[rt] = C;
                // 更新累加和
                sum[rt] = C * (r - l + 1);
                // L~R 范围上全部改成 C 值，之前懒添加的值就无用了，直接置0
                lazy[rt] = 0;
                return;
            }
            // 找到中间位置（例如：l=50,r=100）中间孩子，就是75的位置，也就是左节点的右边界，右节点的左边界
            int mid = (l + r) >> 1;
            // 先把父节点向下更新
            pushDown(rt, mid - l + 1, r - mid);
            if (L <= mid) {
                update(L, R, C, l, mid, rt << 1);
            }
            if (R > mid) {
                update(L, R, C, mid + 1, r, rt << 1 | 1);
            }
            // 更新父节点 rt 位置的是累加和
            pushUp(rt);
        }

        /**
         * 求累加和问题，把每个节点懒更新的累加和加起来
         */
        public long query(int L, int R, int l, int r, int rt) {
            if (L <= l && r <= R) {
                return sum[rt];
            }

            int mid = (l + r) >> 1;
            pushDown(rt, mid - l + 1, r - mid);
            long ans = 0;
            if (L <= mid) {
                ans += query(L, R, l, mid, rt << 1);
            }
            if (R > mid) {
                ans += query(L, R, mid + 1, r, rt << 1 | 1);
            }
            return ans;
        }
    }

    public static void main(String[] args) {
        int[] origin = { 2, 1, 1, 2, 3, 4, 5 };
        SegmentTree seg = new SegmentTree(origin);
        // 整个区间的开始位置，规定从1开始，不从0开始 -> 固定
        int S = 1;
        // 整个区间的结束位置，规定能到N，不是N-1 -> 固定
        int N = origin.length;
        // 整棵树的头节点位置，规定是1，不是0 -> 固定
        int root = 1;
        // 操作区间的开始位置 -> 可变
        int L = 2;
        // 操作区间的结束位置 -> 可变
        int R = 5;
        // 要加的数字或者要更新的数字 -> 可变
        int C = 4;
        // 区间生成，必须在[S,N]整个范围上build
        seg.build(S, N, root);
        // 区间修改，可以改变L、R和C的值，其他值不可改变
        seg.add(L, R, C, S, N, root);
        // 区间更新，可以改变L、R和C的值，其他值不可改变
        seg.update(L, R, C, S, N, root);
        // 区间查询，可以改变L和R的值，其他值不可改变
        long sum = seg.query(L, R, S, N, root);
        System.out.println(sum);

        System.out.println("对数器测试开始...");
        System.out.println("测试结果 : " + (test() ? "通过" : "未通过"));

    }

    public static int[] genarateRandomArray(int len, int max) {
        int size = (int) (Math.random() * len) + 1;
        int[] origin = new int[size];
        for (int i = 0; i < size; i++) {
            origin[i] = (int) (Math.random() * max) - (int) (Math.random() * max);
        }
        return origin;
    }

    public static boolean test() {
        int len = 100;
        int max = 1000;
        int testTimes = 5000;
        int addOrUpdateTimes = 1000;
        int queryTimes = 500;
        for (int i = 0; i < testTimes; i++) {
            int[] origin = genarateRandomArray(len, max);
            SegmentTree seg = new SegmentTree(origin);
            int S = 1;
            int N = origin.length;
            int root = 1;
            seg.build(S, N, root);
            Right rig = new Right(origin);
            for (int j = 0; j < addOrUpdateTimes; j++) {
                int num1 = (int) (Math.random() * N) + 1;
                int num2 = (int) (Math.random() * N) + 1;
                int L = Math.min(num1, num2);
                int R = Math.max(num1, num2);
                int C = (int) (Math.random() * max) - (int) (Math.random() * max);
                if (Math.random() < 0.5) {
                    seg.add(L, R, C, S, N, root);
                    rig.add(L, R, C);
                } else {
                    seg.update(L, R, C, S, N, root);
                    rig.update(L, R, C);
                }
            }
            for (int k = 0; k < queryTimes; k++) {
                int num1 = (int) (Math.random() * N) + 1;
                int num2 = (int) (Math.random() * N) + 1;
                int L = Math.min(num1, num2);
                int R = Math.max(num1, num2);
                long ans1 = seg.query(L, R, S, N, root);
                long ans2 = rig.query(L, R);
                if (ans1 != ans2) {
                    return false;
                }
            }
        }
        return true;
    }

}

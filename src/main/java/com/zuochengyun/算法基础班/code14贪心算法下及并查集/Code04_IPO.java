package com.zuochengyun.算法基础班.code14贪心算法下及并查集;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * 输入: 正数数组costs、正数数组profits、正数K、正数M
 * costs[i]表示i号项目的花费
 * profits[i]表示i号项目在扣除花费之后还能挣到的钱(利润)
 * K表示你只能串行的最多做k个项目
 * M表示你初始的资金
 * 说明: 每做完一个项目，马上获得的收益，可以支持你去做下一个项目。不能并行的做项目。
 * 输出：你最后获得的最大钱数。
 *
 * @author 钢人
 */
public class Code04_IPO {

    /**
     * @param k       项目个数
     * @param w       初始资金
     * @param profits 除了扣除花费的前，能挣的净利润
     * @param capital 花费资金
     */
    public static int findMaximizedCapital(int k, int w, int[] profits, int[] capital) {
        // 最小成本
        PriorityQueue<Program> minCostQ = new PriorityQueue<>(new MinCostComparator());
        // 最大收益
        PriorityQueue<Program> maxProfitQ = new PriorityQueue<>(new MaxProfitComparator());
        for (int i = 0; i < profits.length; i++) {
            minCostQ.add(new Program(profits[i], capital[i]));
        }

        for (int i = 0; i < k; i++) {
            // 做到第 i 个项目的时候，把能做的项目全部拿出来放到最大收益大根堆里排序
            while (!minCostQ.isEmpty() && minCostQ.peek().c <= w) {
                maxProfitQ.add(minCostQ.poll());
            }
            // 如果做到 i 项目的时候，已经没有能做的项目了，把之前做的项目返回
            if (maxProfitQ.isEmpty()) {
                return w;
            }
            // 当前第 i 个项目做利润最大的项目累加
            w += maxProfitQ.poll().p;
        }
        return w;
    }

    public static class Program {
        public int p;
        public int c;

        public Program(int p, int c) {
            this.p = p;
            this.c = c;
        }
    }

    public static class MinCostComparator implements Comparator<Program> {
        @Override
        public int compare(Program o1, Program o2) {
            return o1.c - o2.c;
        }
    }

    public static class MaxProfitComparator implements Comparator<Program> {
        @Override
        public int compare(Program o1, Program o2) {
            return o2.p - o1.p;
        }
    }

}

package com.zuochengyun.算法基础班.code14贪心算法下及并查集;

import java.util.PriorityQueue;

/**
 * 一块金条切成两半，是需要花费和长度数值一样的铜板的。
 * 比如长度为20的金条，不管怎么切，都要花费20个铜板。 一群人想整分整块金条，怎么分最省铜板?
 * <p>
 * 例如,给定数组{10,20,30}，代表一共三个人，整块金条长度为60，金条要分成10，20，30三个部分。
 * <p>
 * 如果先把长度60的金条分成10和50，花费60; 再把长度50的金条分成20和30，花费50;一共花费110铜板。
 * 但如果先把长度60的金条分成30和30，花费60;再把长度30金条分成10和20，花费30;一共花费90铜板。
 * 输入一个数组，返回分割的最小代价。
 *
 * @author 钢人
 */
public class Code02_LessMoneySplitGold {

    /**
     * @param arr 金块儿数量
     */
    public static int lessMoney1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        return process(arr, 0);
    }

    /**
     * arr：所有还需要合并的金块儿
     * pre：表示之前花费总代价
     */
    private static int process(int[] arr, int pre) {
        if (arr.length == 1) {
            return pre;
        }
        int ans = Integer.MAX_VALUE;
        // 尝试 i 和 j 金块儿合并，i 和 j+1 金块儿合并。。。
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                // 除了 i 和 j 金块儿的其他金块进行尝试合并需要的代价
                int[] nexts = copyAndMergeTwo(arr, i, j);
                // pre + arr[i] + arr[j]：表示之前合并的代价+当前合并的代价
                ans = Math.min(ans, process(nexts, pre + arr[i] + arr[j]));
            }
        }
        return ans;
    }

    private static int[] copyAndMergeTwo(int[] arr, int i, int j) {
        int[] ans = new int[arr.length - 1];
        int ansi = 0;
        for (int arri = 0; arri < arr.length; arri++) {
            if (arri != i && arri != j) {
                ans[ansi++] = arr[arri];
            }
        }
        ans[ansi] = arr[i] + arr[j];
        return ans;
    }


    public static int lessMoney2(int[] arr) {
        // 先进行排序，以最小代价的合并放到前面
        PriorityQueue<Integer> pQ = new PriorityQueue<>();
        for (int i = 0; i < arr.length; i++) {
            pQ.add(arr[i]);
        }
        int sum = 0;
        int cur = 0;
        while (pQ.size() > 1) {
            // 把前面两个最小的合并代价进行合并
            cur = pQ.poll() + pQ.poll();
            // 之前的合并代价 + 当前这次的合并代价
            sum += cur;
            pQ.add(cur);
        }
        return sum;
    }

}

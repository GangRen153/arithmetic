package com.zuochengyun.算法基础班.code23暴力递归动态规划6;

/**
 * 给定一个正数数组arr，把 arr 中所有的数分成两个集合，尽量让两个集合的累加和接近
 * 返回：最接近的情况下，较小集合的累加和
 *
 * @author 钢人
 */
public class Code01_SplitSumClosed {

    public static int right(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }

        int sum = 0;
        for (int num : arr) {
            sum += num;
        }
        return process(arr, 0, sum / 2);
    }

    /**
     * 返回 arr 数组中，最接近 rest 且不超过 rest 的累加和
     */
    private static int process(int[] arr, int index, int rest) {
        // 如果没有数了，不存在选择问题，返回0即可
        if (index == arr.length) {
            return 0;
        }
        // 不使用 index 位置的数
        int p1 = process(arr, index + 1, rest);
        int p2 = 0;
        // 使用 index 位置的数，但 arr[index] 不能大于
        if (arr[index] <= rest) {
            // rest - arr[index] 是为了保证 arr[index] + process(...) 不大于 rest
            p2 = arr[index] + process(arr, index + 1, rest - arr[index]);
        }
        return Math.max(p1, p2);
    }


    public static int dp(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }

        int sum = 0;
        for (int num : arr) {
            sum += num;
        }
        sum /= 2;

        int N = arr.length;
        int[][] dp = new int[N + 1][sum + 1];
        for (int index = N - 1; index >= 0; index--) {
            for (int rest = 0; rest <= sum; rest++) {
                int p1 = dp[index + 1][rest];
                int p2 = 0;
                // 使用 index 位置的数，但 arr[index] 不能大于
                if (arr[index] <= rest) {
                    // rest - arr[index] 是为了保证 arr[index] + process(...) 不大于 rest
                    p2 = arr[index] + dp[index + 1][rest - arr[index]];
                }
                dp[index][rest] = Math.max(p1, p2);
            }
        }
        return dp[0][sum];
    }

}

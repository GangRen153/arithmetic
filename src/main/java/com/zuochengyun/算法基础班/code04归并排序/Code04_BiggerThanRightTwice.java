package com.zuochengyun.算法基础班.code04归并排序;

/**
 * 在一个数组中，
 * 对于每个数num，求有多少个后面的数 * 2 依然<num，求总个数
 *
 * @author 钢人
 */
public class Code04_BiggerThanRightTwice {

    public static int biggerTwice(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        return process(arr, 0, arr.length - 1);
    }

    public static int process(int[] arr, int l, int r) {
        if (l == r) {
            return 0;
        }
        int mid = l + ((r - l) >> 1);
        return process(arr, l, mid) + process(arr, mid + 1, r) + merge(arr, l, mid, r);
    }

    private static int merge(int[] arr, int l, int m, int r) {
        int ans = 0;
        // 右半区数组的左边界
        int windowR = m + 1;
        // 只遍历左数组，移动左数组指针遍历去挨个比较右半区数组每一位数*2
        for (int i = l; i <= m; i++) {
            // 如果满足左边的数 > 右边的数*2，右半区数组指针向右移
            while (windowR <= r && arr[i] > arr[windowR] * 2) {
                windowR++;
            }
            // ans 表示之前统计过的总数 + 当前 i 位置统计满足条件的总个数
            // windowR-m-1 表示右半区符合条件的个数，也就是右半区右边界移动到了哪儿 - 左半区最右边界
            ans += windowR - m - 1;
        }

        // 归并
        int[] help = new int[r - l + 1];
        int i = 0;
        int p1 = l;
        int p2 = m + 1;
        while (p1 <= m && p2 <= r) {
            help[i++] = arr[p1] <= arr[p2] ? arr[p1++] : arr[p2++];
        }
        while (p1 <= m) {
            help[i++] = arr[p1++];
        }
        while (p2 <= r) {
            help[i++] = arr[p2++];
        }
        for (i = 0; i < help.length; i++) {
            arr[l + i] = help[i];
        }
        return ans;
    }

}

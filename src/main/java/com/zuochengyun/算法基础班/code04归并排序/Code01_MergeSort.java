package com.zuochengyun.算法基础班.code04归并排序;

/**
 * 归并排序，递归版
 * <p>
 * 实现方式：将整个数组用二分法分成左部分和右部分
 * 对左部分和右部分进行合并排序，L~mid 位置为左部分数组范围
 * (mid+1)~R 位置为右部分数组范围，同时遍历，比较 L 和 mid+1 位置的数
 * 谁小谁拷贝到新数组中，拷贝完成后指针向右移动，以此类推
 *
 * @author 钢人
 */
public class Code01_MergeSort {

    /**
     * 递归版实现归并排序
     */
    public static void mergeSort(int[] arr) {
        if (arr != null || arr.length < 2) {
            return;
        }
        process(arr, 0, arr.length - 1);
    }

    private static void process(int[] arr, int L, int R) {
        if (L == R) {
            return;
        }
        int mid = L + (R - L >> 1);
        process(arr, L, mid);
        process(arr, mid + 1, R);
        merge(arr, L, mid, R);
    }

    public static void merge(int[] arr, int L, int M, int R) {
        int[] help = new int[R - L + 1];
        int i = 0;
        // 左部分数组的左边界位置
        int p1 = L;
        // 右部分数组的左边界位置
        int p2 = M + 1;
        // 在左部分数组和右部分数组都没有越界的情况下，看哪边的数小先拷贝谁
        while (p1 <= M && p2 <= R) {
            help[i++] = arr[p1] < arr[p2] ? arr[p1++] : arr[p2++];
        }
        // 如果是左部分数组没有遍历完，直接将左部分数组的数都拷贝到新数组里
        while (p1 <= M) {
            help[i++] = arr[p1++];
        }
        // 如果是右部分数组的数没有遍历完，直接将右部分数组的数拷贝到新数组里
        while (p2 <= R) {
            help[i++] = arr[p2++];
        }
        // 最后将新数组中的数拷贝到原数组 arr 中
        for (i = 0; i < help.length; i++) {
            arr[L + i] = help[i];
        }
    }

    /**
     * 非递归版并排
     *
     * 实现方式： 递归版是利用二分法，将数组分为左半区和右半区，用L、mid、R代表左半区数组和右半区数组边界范围
     *          非递归版是利用步长控制左右半区数组范围，每次排序之后步长向左移动，也就是扩大2倍进行范围上的排序
     */
    public static void mergeSort2(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }

        int N = arr.length;
        // 步长：0表示左半区左边界下标，(步长-1)始终表示左半区数组的右边界下标，
        // 右半区的左边界是 mergeSize，右半区右边界是 mergeSize*2，但是需要保证是整个数组的有效范围
        int mergeSize = 1;
        while (mergeSize < N) {
            // 左半区数组的左边界永远是0
            int L = 0;
            // 开始遍历数组步长的数组范围
            while (L < N) {
                // 整个数组中，右半部分剩下还需要排序的范围如果还不够步长？？？
                if (mergeSize >= N - L) {
                    break;
                }
                // 中间位置
                int M = L + mergeSize - 1;
                // 右边界，N-1-M表示原数组的剩余长度，如果剩余长度还够mergeSize
                // 右半区的右边界就是两个mergeSize的长度
                int R = M + Math.min(mergeSize, N - 1 - M);
                // 合并左半区和右半区数组范围排序
                merge(arr, L, M, R);
                // 合并完之后，将范围数组的左边界下标移动到新的范围数组左边界位置，开始新的归并
                L = R + 1;
            }
            // 步长向左移动，扩大2倍
            mergeSize <<= 1;
        }
    }

    public static void main(String[] args) {

    }

}

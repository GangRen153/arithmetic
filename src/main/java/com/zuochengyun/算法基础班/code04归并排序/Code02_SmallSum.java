package com.zuochengyun.算法基础班.code04归并排序;

/**
 * 在一个数组中，一个数左边比它小的数的总和，叫数的小和，所有数的小和累加起来，叫数组小和。求数组小和。
 * 例子： [1,3,4,2,5]
 * 1左边比1小的数：没有
 * 3左边比3小的数：1
 * 4左边比4小的数：1、3
 * 2左边比2小的数：1
 * 5左边比5小的数：1、3、4、 2
 * 所以数组的小和为1+1+3+1+1+3+4+2=16
 *
 * 求的是数组中每一位数的左边所有比自己小的数累加和
 *
 * @author 钢人
 */
public class Code02_SmallSum {

    /**
     * 1：要给数组排序
     * 2：求小和
     */
    public static int smallSum(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        return process(arr, 0, arr.length - 1);
    }

    private static int process(int[] arr, int l, int r) {
        if (l == r) {
            return 0;
        }
        // 二分
        int mid = l + ((r - l) >> 1);
        return process(arr, l, mid) + process(arr, mid + 1, r) + merge(arr, l, mid, r);
    }

    private static int merge(int[] arr, int l, int mid, int r) {
        int[] help = new int[r - l + 1];
        // help 数组的开始下标位置
        int i = 0;
        // 左半区数组的左边界
        int p1 = l;
        // 右半区数组的左边界
        int p2 = mid + 1;
        // 计算累加和
        int res = 0;
        while (p1 <= mid && p2 <= r) {
            // 计算逻辑在这一行，其他都属于归并排序部分
            res += arr[p1] < arr[p2] ? (r - p2 + 1) * arr[p1] : 0;
            help[i++] = arr[p1] < arr[p2] ? arr[p1++] : arr[p2++];
        }
        while (p1 <= mid) {
            help[i++] = arr[p1++];
        }
        while (p2 <= mid) {
            help[i++] = arr[p2++];
        }
        for (i = 0; i < help.length; i++) {
            arr[l+i] = help[i];
        }
        return res;
    }

    public static void main(String[] args) {

    }

}

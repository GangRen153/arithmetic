package com.zuochengyun.算法基础班.code04归并排序;

/**
 * 给定一个数组，求每个数的右边比自己小的数称为逆序对儿
 * 问整个数组中有多少个这样的逆序对儿
 * <p>
 * 就是求每个数有多少个比自己小的数
 *
 * @author 钢人
 */
public class Code03_ReversePair {

    public static int reverPairNumber(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        return process(arr, 0, arr.length - 1);
    }

    private static int process(int[] arr, int l, int r) {
        if (l == r) {
            return 0;
        }
        int mid = l + (r - l >> 1);
        return process(arr, l, mid) + process(arr, mid + 1, r) + merge(arr, l, mid, r);
    }

    private static int merge(int[] arr, int l, int m, int r) {
        int[] help = new int[r - l + 1];
        int i = help.length - 1;
        int p1 = m;
        int p2 = r;
        // 统计的是个数
        int res = 0;
        // 下标从右开始，从右往左遍历，拷贝的时候也是从右往左
        while (p1 >= l && p2 > m) {
            // 这里累加的是个数，而不是和，所以直接用下标相减即可
            res += arr[p1] > arr[p2] ? (p2 - m) : 0;
            help[i--] = arr[p1] > arr[p2] ? arr[p1--] : arr[p2--];
        }
        while (p1 >= l) {
            help[i--] = arr[p1--];
        }
        while (p2 > m) {
            help[i--] = arr[p2--];
        }
        for (i = 0; i < help.length; i++) {
            arr[l + i] = help[i];
        }
        return res;
    }

}

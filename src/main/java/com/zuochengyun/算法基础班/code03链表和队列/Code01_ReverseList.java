package com.zuochengyun.算法基础班.code03链表和队列;

/**
 * 列表反转
 *
 * @author 钢人
 */
public class Code01_ReverseList {

    public static class Node {
        public int value;
        public Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    public static class DoubleNode {
        public int value;
        public DoubleNode last;
        public DoubleNode next;

        public DoubleNode(int data) {
            value = data;
        }
    }

    /**
     * 单列表反转
     */
    public static Node reverseLinkedList(Node head) {
        Node pre = null;
        Node next = null;
        while (head != null) {
            next = head.next;
            pre = head;
            head.next = pre;
            head = next;
        }
        return pre;
    }

    public static DoubleNode reverseDoubleNode(DoubleNode head) {
        DoubleNode next = null;
        DoubleNode pre = null;
        while (head != null) {
            next = head.next;
            pre = head;
            head.next = pre;
            head.last = next;
            head = next;
        }
        return pre;
    }

    public static void main(String[] args) {

    }

}

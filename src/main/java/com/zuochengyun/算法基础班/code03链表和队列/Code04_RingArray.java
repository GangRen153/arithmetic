package com.zuochengyun.算法基础班.code03链表和队列;

/**
 * 队列：环形数组
 *
 * @author 钢人
 */
public class Code04_RingArray {

    public static class MyQueue {
        public int[] arr;
        public int size;
        public int pushi;
        public int polli;
        public int limit;

        public MyQueue(int size) {
            arr = new int[size];
            size = 0;
            polli = 0;
            pushi = 0;
            limit = size;
        }

        public void push(int value) {
            if (size == limit) {
                throw new RuntimeException("队列满了，不能再加了");
            }
            size++;
            arr[pushi] = value;
            pushi = nextIndex(pushi);
        }

        public int pop() {
            if (size == 0) {
                throw new RuntimeException("队列空了，不能再拿了");
            }
            size--;
            int ans = arr[polli];
            polli = nextIndex(polli);
            return ans;
        }

        public boolean isEmpty() {
            return size == 0;
        }

        private int nextIndex(int i) {
            return i < limit - 1 ? i + 1 : 0;
        }

    }

}

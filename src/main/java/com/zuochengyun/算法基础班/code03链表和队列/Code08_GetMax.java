package com.zuochengyun.算法基础班.code03链表和队列;

/**
 * 递归实现，给定一个数组，求 L~R 范围上的最大值
 *
 * @author 钢人
 */
public class Code08_GetMax {

    public static int getMax(int[] arr) {
        return process(arr, 0, arr.length - 1);
    }

    private static int process(int[] arr, int L, int R) {
        if (L == R) {
            return arr[L];
        }
        int mid = L + (R - L >> 1);
        int leftNum = process(arr, L, mid);
        int rightNum = process(arr, mid + 1, R);
        return Math.max(leftNum, rightNum);
    }

}

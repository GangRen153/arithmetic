package com.zuochengyun.算法基础班.code03链表和队列;

/**
 * 删除链表上指定值
 *
 * @author 钢人
 */
public class Code02_DeleteGivenValue {

    public static class Node {
        public int value;
        public Node next;
        public Node(int value) {
            this.value = value;
        }
    }

    public static Node removeValue(Node head, int num) {
        Node pre = head;
        Node cur = head;
        while (cur != null) {
            if (head.value == num) {
                pre.next = head.next;
            } else {
                pre = cur;
            }
            cur = cur.next;
        }
        return head;
    }

}

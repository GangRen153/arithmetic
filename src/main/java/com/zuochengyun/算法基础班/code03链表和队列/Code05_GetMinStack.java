package com.zuochengyun.算法基础班.code03链表和队列;

import java.util.Stack;

/**
 * 实现一个 pop、push、getMin操作的时间复杂度都是 O(1)
 *
 * @author 钢人
 */
public class Code05_GetMinStack {

    public static class MyStack1 {
        public Stack<Integer> stackData;
        public Stack<Integer> stackMin;
        public MyStack1() {
            this.stackData = new Stack<>();
            this.stackMin = new Stack<>();
        }

        public void push(int value) {
            if (stackMin.isEmpty() || value <= stackMin.peek()) {
                stackMin.push(value);
            } else {
                stackMin.push(stackMin.peek());
            }
            stackData.push(value);
        }

        public int pop() {
            if (stackData.isEmpty()) {
                throw new RuntimeException("Your stack is empty.");
            }
            stackMin.pop();
            return stackData.pop();
        }

        public int getMin() {
            return stackMin.peek();
        }
    }

}

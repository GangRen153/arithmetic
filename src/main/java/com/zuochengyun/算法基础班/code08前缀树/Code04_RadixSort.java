package com.zuochengyun.算法基础班.code08前缀树;

/**
 * 桶排序
 *
 * 不利用桶做数组，利用位统计数组排序
 *
 * 思路：
 *  例如：21，10，111，22，11，12
 *      021，010，111，022，012
 *      统计个位数的数字出现的次数                         [1,3,2,0,0,0,0,0,0,0]
 *      在统计个位数 <=0,<=1,<=2,<=3...的数有几个,得出结论：[1,4,6,6,6,6,6,6,6,6]
 *
 * @author 钢人
 */
public class Code04_RadixSort {

    public static void radixSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        radixSort(arr, 0, arr.length - 1, maxbits(arr));
    }

    /**
     * 计算出数组中最大的数一共有几位
     */
    private static int maxbits(int[] arr) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            max = Math.max(max, arr[i]);
        }
        int res = 0;
        while (max != 0) {
            res++;
            max /= 10;
        }
        return res;
    }

    public static void radixSort(int[] arr, int l, int r, int digit) {
        int radix = 10;
        int i = 0, j = 0;
        int[] help = new int[r - l + 1];
        // 遍历位的数量
        for (int d = 0; d < digit; d++) {
            // 表示 0-9 范围的数，也刚好下标位也是 0-9
            int[] count = new int[radix];
            for (i = l; i <= r; i++) {
                // 计算 arr[i] 第d位上的数是多少，例如：209, 1 -> 9
                j = getDigit(arr[i], d);
                // 统计位上的数有多少个
                count[j]++;
            }
            // 统计小于 i 位置上的数的个数，例如：统计小于1的数有多少个，小于2的数有多少个，小于3的数有多少个...
            for (i = 1; i < radix; i++) {
                count[i] = count[i] + count[i - 1];
            }
            // 数组在从右往左遍历
            for (i = r; i >= l; i--) {
                // 拿到每个数所在位的数
                j = getDigit(arr[i], d);
                // 把当前这个数放到一个临时数组中，排好序
                help[count[j] - 1] = arr[i];
                // 表示 arr[i] 这个数已经排过序了
                count[j]--;
            }
            // 将本次按位排好序的数组重新倒入到原数组中，下次遍历会将d进一位重新排序
            for (i = l, j = 0; i <= r; i++, j++) {
                arr[i] = help[j];
            }
        }
    }

    private static int getDigit(int x, int d) {
        return (x / ((int) Math.pow(10, d - 1))) % 10;
    }

}

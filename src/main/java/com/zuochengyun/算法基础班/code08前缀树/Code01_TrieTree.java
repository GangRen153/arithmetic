package com.zuochengyun.算法基础班.code08前缀树;

import java.util.HashMap;

/**
 * 前缀树
 * <p>
 * 1）单个字符串中，字符从前到后的加到一棵多叉树上
 * 2）字符放在路上，节点上有专属的数据项（常见的是pass和end值）
 * 3）所有样本都这样添加，如果没有路就新建，如有路就复用
 * 4）沿途节点的pass值增加1，每个字符串结束时来到的节点end值增加1
 * <p>
 * 可以完成前缀相关的查询
 *
 * @author 钢人
 */
public class Code01_TrieTree {

    public static class Node1 {
        /* 沿途经过多少个字符 */
        public int pass;
        public int end;
        public Node1[] nexts;

        public Node1() {
            pass = 0;
            end = 0;
            nexts = new Node1[26];
        }
    }

    public static class Trie1 {
        private Node1 root;

        public Trie1() {
            root = new Node1();
        }

        public void insert(String word) {
            if (word == null || word.length() == 0) {
                return;
            }
            char[] chars = word.toCharArray();
            Node1 node = root;
            // 沿途经过累加
            node.pass++;
            int path = 0;
            // 依次遍历字符，添加到链路书上
            for (int i = 0; i < chars.length; i++) {
                // 计算该字符应该落到 a-z（0-26） 哪个下标上
                path = chars[i] - 'a';
                if (node.nexts[path] == null) {
                    node.nexts[path] = new Node1();
                }
                // 将当前字符作为下一个字符的链路节点
                node = node.nexts[path];
                node.pass++;
            }
            // 统计在该节点上以该字符结尾的个数
            node.end++;
        }

        public void delete(String word) {
            if (search(word) == 0) {
                return;
            }
            char[] chars = word.toCharArray();
            Node1 node = root;
            node.pass--;
            int path = 0;
            for (int i = 0; i < chars.length; i++) {
                path = chars[path] - 'a';
                if (--node.nexts[path].pass == 0) {
                    node.nexts[path] = null;
                    return;
                }
                node = node.nexts[path];
            }
            node.end--;
        }

        /**
         * 检索这个单词出现过几次
         */
        public int search(String word) {
            if (word == null || word.length() == 0) {
                return 0;
            }
            char[] chars = word.toCharArray();
            Node1 node = root;
            int index;
            for (int i = 0; i < chars.length; i++) {
                index = chars[i] - 'a';
                if (node.nexts[index] == null) {
                    return 0;
                }
                node = node.nexts[index];
            }
            return node.end;
        }

        /**
         * 统计前缀数量
         */
        public int prefixNumber(String pre) {
            if (pre == null || pre.length() == 0) {
                return 0;
            }
            char[] chars = pre.toCharArray();
            Node1 node = root;
            int index = 0;
            for (int i = 0; i < chars.length; i++) {
                index = chars[i] - 'a';
                if (node.nexts[index].pass == 0) {
                    return 0;
                }
                node = node.nexts[index];
            }
            return node.pass;
        }
    }


    public static class Node2 {
        public int pass;
        public int end;
        public HashMap<Integer, Node2> nexts;

        public Node2() {
            pass = 0;
            end = 0;
            nexts = new HashMap<>();
        }
    }


    /**
     * 利用 hashMap 实现前缀树
     */
    public static class Trie2 {
        private Node2 root;

        public Trie2() {
            root = new Node2();
        }

        public void insert(String word) {
            if (word == null || word.length() == 0) {
                return;
            }
            Node2 node = root;
            char[] chars = word.toCharArray();
            for (int index : chars) {
                if (!node.nexts.containsKey(index)) {
                    node.nexts.put(index, new Node2());
                }
                node = node.nexts.get(index);
                node.pass++;
            }
            node.end++;
        }

        public void delete(String word) {
            if (search(word) == 0) {
                return;
            }
            Node2 node = root;
            char[] chars = word.toCharArray();
            for (int index : chars) {
                if (--node.nexts.get(index).pass == 0) {
                    node.nexts.remove(index);
                    return;
                }
                node = node.nexts.get(index);
            }
            node.end--;
        }

        private int search(String word) {
            if (word == null || word.length() == 0) {
                return 0;
            }
            Node2 node = root;
            char[] chars = word.toCharArray();
            for (int index : chars) {
                if (!node.nexts.containsKey(index)) {
                    return 0;
                }
                node = node.nexts.get(index);
            }
            return node.end;
        }

        public int prefixNumber(String pre) {
            if (pre == null || pre.length() == 0) {
                return 0;
            }
            char[] chars = pre.toCharArray();
            Node2 node = root;
            for (int index : chars) {
                if (!node.nexts.containsKey(index)) {
                    return 0;
                }
                node = node.nexts.get(index);
            }
            return node.pass;
        }
    }


    public static class Right {
        private HashMap<String, Integer> box;

        public Right() {
            box = new HashMap<>();
        }

        public void insert(String word) {
            if (!box.containsKey(word)) {
                box.put(word, 1);
            } else {
                box.put(word, box.get(word) + 1);
            }
        }

        public void delete(String word) {
            if (!box.containsKey(word)) {
                return;
            }
            if (box.get(word) == 1) {
                box.remove(word);
            } else {
                box.put(word, box.get(word) - 1);
            }
        }

        public int search(String word) {
            if (!box.containsKey(word)) {
                return 0;
            }
            return box.get(word);
        }

        public int prefixNumber(String pre) {
            int count = 0;
            for (String cur : box.keySet()) {
                if (cur.startsWith(pre)) {
                    count++;
                }
            }
            return count;
        }
    }

}

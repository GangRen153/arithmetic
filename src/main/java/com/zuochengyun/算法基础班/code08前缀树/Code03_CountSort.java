package com.zuochengyun.算法基础班.code08前缀树;

/**
 * 计数排序
 *
 * 桶排序，用桶实现排序
 *
 * 按位排序，按从小到大先从个位排，再按十位，百位，千位等，如果不够位数前面补0
 * 按照顺序遍历按位数倒入桶中，在遍历桶，按照先进先出在倒出桶外排好序，在重新按照顺序进一位排序
 * 以此类推直到结束位置，最大的数有几位数，就会进桶出桶几次
 *
 * 一般来讲，计数排序要求，样本是整数，且范围比较窄
 *
 * @author 钢人
 */
public class Code03_CountSort {

    /**
     * 计数排序
     */
    public static void countSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }

        // 找出 arr 中最大的一个
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            max = Math.max(max, arr[i]);
        }

        int[] bucket = new int[max + 1];
        for (int i = 0; i < arr.length; i++) {
            bucket[arr[i]]++;
        }
        int i = 0;
        for (int j = 0; j < bucket.length; j++) {
            while (bucket[j]-- > 0) {
                arr[i++] = j;
            }
        }

    }

}

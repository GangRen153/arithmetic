package com.zuochengyun.算法基础班.code37有序表下;

/**
 * 设计一个结构包含如下两个方法：
 * void add(int index, int num)：把num加入到index位置
 * int get(int index) ：取出index位置的值
 * void remove(int index) ：把index位置上的值删除
 *
 * 要求三个方法时间复杂度O(logN)
 *
 * @author 钢人
 */
public class Code03_AddRemoveGetIndexGreat {

    // TODO

}

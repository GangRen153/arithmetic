package com.zuochengyun.算法基础班.code13贪心算法上;

/**
 * 递归的方式查看是否是完全二叉树
 *
 * @author 钢人
 */
public class Code01_IsCBT {

    // TODO

    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int data) {
            this.value = data;
        }
    }

}

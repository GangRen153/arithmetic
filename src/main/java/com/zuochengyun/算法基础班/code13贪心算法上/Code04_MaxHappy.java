package com.zuochengyun.算法基础班.code13贪心算法上;

import java.util.ArrayList;
import java.util.List;

/**
 * 派对的最大快乐值
 * 这个公司现在要办party，你可以决定哪些员工来，哪些员工不来，规则：
 * 1.如果某个员工来了，那么这个员工的所有直接下级都不能来
 * 2.派对的整体快乐值是所有到场员工快乐值的累加
 * 3.你的目标是让派对的整体快乐值尽量大
 * 给定一棵多叉树的头节点boss，请返回派对的最大快乐值。
 *
 * @author 钢人
 */
public class Code04_MaxHappy {

    public static class Employee {
        /**
         * 快乐值
         */
        public int happy;
        /**
         * 下级
         */
        public List<Employee> nexts;

        public Employee(int h) {
            happy = h;
            nexts = new ArrayList<>();
        }
    }

    public static int maxHappy1(Employee boss) {
        if (boss == null) {
            return 0;
        }
        return process1(boss, false);
    }

    /**
     * @param cur 当前节点
     * @param up  上级节点来还是不来
     */
    private static int process1(Employee cur, boolean up) {
        // 上级节点来，cur 只能来
        if (up) {
            int ans = 0;
            for (Employee next : cur.nexts) {
                ans += process1(next, false);
            }
            return ans;
        } else {
            // 上级不来的情况下，cur 可以来也可以不来
            int p1 = cur.happy;
            int p2 = 0;
            for (Employee next : cur.nexts) {
                p1 += process1(next, true);
                p2 += process1(next, false);
            }
            return Math.max(p1, p2);
        }
    }


    public static class Info {
        public int no;
        public int yes;

        public Info(int n, int y) {
            no = n;
            yes = y;
        }
    }

    public static int maxHappy2(Employee boss) {
        Info all = process(boss);
        return Math.max(all.no, all.yes);
    }

    private static Info process(Employee x) {
        if (x == null) {
            return new Info(0, 0);
        }

        // 如果不来，快乐值就是0
        int no = 0;
        // 如果来，那么快乐值就是自己
        int yes = x.happy;
        for (Employee next : x.nexts) {
            Info nextInfo = process(next);
            no += Math.max(nextInfo.no, nextInfo.yes);
            yes += nextInfo.yes;
        }
        return new Info(no, yes);
    }


}

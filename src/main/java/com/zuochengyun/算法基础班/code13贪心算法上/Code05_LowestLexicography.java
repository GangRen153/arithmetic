package com.zuochengyun.算法基础班.code13贪心算法上;

import java.util.Arrays;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * 贪心算法
 * <p>
 * 给定一个由字符串组成的数组strs，
 * 必须把所有的字符串拼接起来，
 * 返回所有可能的拼接结果中，字典序最小的结果
 * <p>
 * 字典序：值的是英文字母的排序
 * 例如：a比b小，b比c小，c比d小。。。
 *
 * @author 钢人
 */
public class Code05_LowestLexicography {

    public static String lowestString1(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        TreeSet<String> ans = process(strs);
        // 把 strs 所有可能的拼接结果放到 TreeSet 中，最后返回第一个
        return ans.size() == 0 ? "" : ans.first();
    }

    /**
     * 返回 strs 全排列返回所有可能的拼接结果
     * 最终放到 TreeSet 中进行排序
     */
    private static TreeSet<String> process(String[] strs) {
        TreeSet<String> ans = new TreeSet<>();
        if (strs.length == 0) {
            ans.add("");
            return ans;
        }
        for (int i = 0; i < strs.length; i++) {
            // 第一个字符串选择 i 位置的字符串
            String first = strs[i];
            // 第二个字符串从 nexts 里面选取一个
            String[] nexts = removeIndexString(strs, i);
            TreeSet<String> next = process(nexts);
            for (String cur : next) {
                // 把 i 位置的字符串当作第一个位置的字符串拼接
                ans.add(first + cur);
            }
        }
        return ans;
    }

    private static String[] removeIndexString(String[] arr, int index) {
        int N = arr.length;
        String[] ans = new String[N - 1];
        int ansIndex = 0;
        for (int i = 0; i < N; i++) {
            if (i != index) {
                ans[ansIndex++] = arr[i];
            }
        }
        return ans;
    }


    public static class MyComparator implements Comparator<String> {
        @Override
        public int compare(String a, String b) {
            return (a + b).compareTo(b + a);
        }
    }

    public static String lowestString2(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        Arrays.sort(strs, new MyComparator());
        String res = "";
        for (int i = 0; i < strs.length; i++) {
            res += strs[i];
        }
        return res;
    }

    public static void main(String[] args) {
        String[] strings = new String[]{"vws", "sdfs", "afdsd", "sds", "hsd"};
        System.out.println(lowestString1(strings));
        System.out.println(lowestString2(strings));
    }

}

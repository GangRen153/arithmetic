package com.zuochengyun.算法基础班.code42四边形不等式下;

/**
 * 一座大楼有 0~N 层，地面算作第 0 层，最高的一层为第 N 层。
 * 已知棋子从第 0 层掉落肯定 不会摔碎，从第 i 层掉落可能会摔碎，也可能不会摔碎(1≤i≤N)。
 * 如果扔碎了，不能在继续使用，如果没扔碎，则可以继续使用
 * 给定整数 N 作为楼层数， 再给定整数 K 作为棋子数
 * 返回如果想找到棋子不会摔碎的最高层数，即使在最差的情况下扔 的最少次数。一次只能扔一个棋子。
 *
 * 测试链接：<a href="https://leetcode.com/problems/super-egg-drop"/>
 * @author 钢人
 */
public class Code02_ThrowChessPiecesProblem {

    public static int superEggDrop1(int kChess, int nLevel) {
        if (nLevel < 1 || kChess < 1) {
            return 0;
        }
        return process1(nLevel, kChess);
    }

    /**
     * @param rest 剩余楼层高度
     * @param k 剩余棋子个数
     * @return 至少需要扔几次
     */
    public static int process1(int rest, int k) {
        if (rest == 0) {
            return 0;
        }
        // 如果只有一个棋子，只能是一层一层的扔，所以直接返回剩余的楼层数
        if (k == 1) {
            return rest;
        }
        int min = Integer.MAX_VALUE;
        // 从 i 层开始扔
        for (int i = 1; i != rest + 1; i++) {
            // Math.max(碎了, 没碎)
            min = Math.min(min, Math.max(process1(i - 1, k - 1), process1(rest - i, k)));
        }
        return min + 1;
    }

    /**
     * 测试 ks 过不了
     */
    public static int superEggDrop2(int kChess, int nLevel) {
        if (nLevel < 1 || kChess < 1) {
            return 0;
        }
        if (kChess == 1) {
            return nLevel;
        }
        int[][] dp = new int[nLevel + 1][kChess + 1];
        // 只有一个棋子的时候，设置尝试次数=剩余楼层数
        for (int i = 1; i != dp.length; i++) {
            dp[i][1] = i;
        }

        for (int rest = 1; rest != dp.length; rest++) {
            for (int level = 2; level != dp[0].length; level++) {
                int min = Integer.MAX_VALUE;
                // 从 i 层开始扔
                for (int k = 1; k != rest + 1; k++) {
                    // Math.max(碎了, 没碎)
                    min = Math.min(min, Math.max(process1(k - 1, k - 1), process1(rest - k, k)));
                }
                dp[rest][level] = min + 1;
            }
        }
        return dp[nLevel][kChess];
    }

}

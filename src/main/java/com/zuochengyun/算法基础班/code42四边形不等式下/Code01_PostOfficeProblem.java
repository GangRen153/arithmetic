package com.zuochengyun.算法基础班.code42四边形不等式下;

/**
 * 给定一个数组，在x坐标上，表示居名点
 * 在给定一个 num，表示可以建设的邮局数量
 * 问：邮局分别设置在什么位置上，所有居名到邮局距离的总代价最小
 *
 * @author 钢人
 */
public class Code01_PostOfficeProblem {

    public static int min1(int[] arr, int num) {
        if (arr == null || num < 1 || arr.length < num) {
            return 0;
        }
        int N = arr.length;
        int[][] w = new int[N + 1][N + 1];
        // 遍历每个居名点
        for (int L = 0; L < N; L++) {
            for (int R = L + 1; R < N; R++) {
                // 当前最优位置 = （上一个居名点的位置 + 下一个居名点位置 - 上一次邮局的最优位置）
                w[L][R] = w[L][R - 1] + arr[R] - arr[(L + R) >> 1];
            }
        }

        int[][] dp = new int[N][num + 1];
        // 第一列上居名点的位置都是0
        for (int i = 0; i < N; i++) {
            dp[i][1] = w[0][N];
        }
        // 遍历居名点
        for (int i = 1; i < N; i++) {
            // 遍历邮局，邮局从 2 个开始，小于 num
            for (int j = 2; j <= Math.min(i, num); j++) {
                int ans = Integer.MAX_VALUE;
                for (int k = 0; k <= i; k++) {
                    ans = Math.min(ans, dp[k][j - 1] + w[k + 1][i]);
                }
                dp[i][j] = ans;
            }
        }
        return dp[N - 1][num];
    }

}

package com.zuochengyun.算法基础班.code43状态压缩动态规划;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个无向图矩阵数组，例如A-B城市的距离的5，那么B-A的距离也是5
 * 每个城市是互相都可以走通的
 * 问，从任意一个点出发，所有城市只经过一次的情况下，怎么走经过的距离最短
 *
 * @author 钢人
 */
public class Code02_TSP {

    public static int t1(int[][] matrix) {
        // 城市数量
        int N = matrix.length;
        List<Integer> set = new ArrayList<>();
        // 表示所有的城市还都没走
        for (int i = 0; i < N; i++) {
            set.add(1);
        }
        return func1(matrix, set, 0);
    }

    private static int func1(int[][] matrix, List<Integer> set, int start) {
        int cityNum = 0;
        // 统计还有多少城市没走
        for (int i = 0; i < set.size(); i++) {
            if (set.get(i) != null) {
                cityNum++;
            }
        }
        if (cityNum == 1) {
            return matrix[start][0];
        }

        // 表示从 start 这个城市出发
        set.set(start, null);
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < set.size(); i++) {
            if (set.get(i) != null) {
                // start~i 城市之间的距离 + 从i城市出发到其他城市的后续距离
                int cur = matrix[start][i] + func1(matrix, set, i);
                min = Math.min(min, cur);
            }
        }
        // 恢复现场
        set.set(start, 1);
        return min;
    }

    /**
     * 状态压缩递归
     */
    public static int t2(int[][] matrix) {
        int N = matrix.length; // 0...N-1
        // 7座城 1111111
        int allCity = (1 << N) - 1;
        return f2(matrix, allCity, 0);
    }

    public static int f2(int[][] matrix, int cityStatus, int start) {
        // 拿出最后一位1
        if (cityStatus == (cityStatus & (~cityStatus + 1))) {
            return matrix[start][0];
        }

        // 把start位的1去掉，
        cityStatus &= (~(1 << start));
        int min = Integer.MAX_VALUE;
        // 枚举所有的城市
        for (int move = 0; move < matrix.length; move++) {
            if ((cityStatus & (1 << move)) != 0) {
                int cur = matrix[start][move] + f2(matrix, cityStatus, move);
                min = Math.min(min, cur);
            }
        }
        // 恢复现场
        cityStatus |= (1 << start);
        return min;
    }

    /**
     * 缓存版本
     */
    public static int t3(int[][] matrix) {
        // TODO
        return 0;
    }

}

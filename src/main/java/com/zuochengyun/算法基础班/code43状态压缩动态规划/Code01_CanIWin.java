package com.zuochengyun.算法基础班.code43状态压缩动态规划;

/**
 * 给定一个整数数组 arr，在给定一个 rest
 * 先手和后手可以从数组种拿任意数，拿走之后这个数就不在数组中了，每次拿走一个数之后，rest要减去被拿走的数
 * 问谁拿拿的时候，面对 rest 是 <= 0的时候，谁输
 * 问：在先手和后谁绝顶聪明的情况下，先手是否能赢
 *
 * @author 钢人
 */
public class Code01_CanIWin {

    public static boolean canIWin0(int choose, int total) {
        if (total <= 0) {
            return false;
        }
        if ((choose * (choose + 1) >> 1) < total) {
            return false;
        }
        // 数组中的数是1~choose
        int[] arr = new int[choose];
        for (int i = 0; i < choose; i++) {
            arr[i] = i + 1;
        }
        return process(arr, total);
    }

    private static boolean process(int[] arr, int rest) {
        // 先手
        if (rest <= 0) {
            return false;
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == -1) {
                continue;
            }
            int cur = arr[i];
            arr[i] = -1;
            // 看后手是否能赢，如果后手输了，那么我就赢了
            boolean next = process(arr, rest - cur);
            arr[i] = cur;
            if (!next) {
                return true;
            }

        }
        return false;
    }

    /**
     * 状态压缩暴力递归，把数组改成状态表达
     * 前提条件 choose 不能大于 31
     */
    public static boolean canIWin1(int choose, int total) {
        if (total == 0) {
            return true;
        }
        if ((choose * (choose + 1) >> 1) < total) {
            return false;
        }
        return process1(choose, 0, total);
    }

    private static boolean process1(int choose, int status, int rest) {
        if (rest <= 0) {
            return false;
        }
        // 尝试 1~choose
        for (int i = 1; i <= choose; i++) {
            // 当前 i 位置是 0 的情况下，表示这个数没拿过
            if (((1 << i) & status) == 0) {
                // (status | (1 << i))：是将 i 位置变成 1
                if (!process1(choose, (status | (1 << i)), rest - i)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 状态压缩动态规划
     */
    public static boolean canIWin2(int choose, int total) {
        if (total == 0) {
            return true;
        }
        if ((choose * (choose + 1) >> 1) < total) {
            return false;
        }
        int[] dp = new int[1 << (choose + 1)];
        // dp[status] == 1  true
        // dp[status] == -1  false
        // dp[status] == 0  process(status) 没算过！去算！
        return process2(choose, 0, total, dp);
    }

    public static boolean process2(int choose, int status, int rest, int[] dp) {
        if (dp[status] != 0) {
            return dp[status] == 1;
        }
        boolean ans = false;
        if (rest > 0) {
            for (int i = 1; i <= choose; i++) {
                if (((1 << i) & status) == 0) {
                    if (!process2(choose, (status | (1 << i)), rest - i, dp)) {
                        ans = true;
                        break;
                    }
                }
            }
        }
        dp[status] = ans ? 1 : -1;
        return ans;
    }

}

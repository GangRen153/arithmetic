package com.zuochengyun.算法基础班.code43状态压缩动态规划;

/**
 * 有无限个 1*2 的砖，铺满 N*M 区域有多少种方法
 *
 * @author 钢人
 */
public class Code03_PavingTile {

    public static int ways1(int N, int M) {
        if (N < 1 || M < 1 || ((N * M) & 1) != 0) {
            return 0;
        }
        if (N == 1 || M == 1) {
            return 1;
        }
        // pre代表-1行的状况
        int[] pre = new int[M];
        for (int i = 0; i < pre.length; i++) {
            pre[i] = 1;
        }
        return process(pre, 0, N);
    }

    public static int process(int[] pre, int level, int N) {
        // 某一行上，如果铺设完了，看这一行是否有空格
        if (level == N) {
            for (int j : pre) {
                if (j == 0) {
                    return 0;
                }
            }
            return 1;
        }
        // 如果没有铺设完，标志一下哪些位置有空的，在 level 行进行铺设
        // 0 表示空格区域，1 表示铺设过了
        int[] op = getOp(pre);
        // 从新的行开始铺设，默认从 0 列开始尝试铺设
        return dfs(op, 0, level, N);
    }

    public static int dfs(int[] op, int col, int level, int N) {
        // 如果铺设到最后一列了，尝试从下一行开始铺设
        if (col == op.length) {
            return process(op, level + 1, N);
        }
        int ans = 0;
        // col 列上竖着摆
        ans += dfs(op, col + 1, level, N);
        // 横着摆
        if (col + 1 < op.length && op[col] == 0 && op[col + 1] == 0) {
            op[col] = op[col + 1] = 1;
            ans += dfs(op, col + 2, level, N);
            // 恢复现场
            op[col] = op[col + 1] = 0;
        }
        return ans;
    }

    public static int[] getOp(int[] pre) {
        int[] cur = new int[pre.length];
        for (int i = 0; i < pre.length; i++) {
            cur[i] = pre[i] ^ 1;
        }
        return cur;
    }


    /**
     * 状态压缩
     * 要求：Min (N,M) 不超过 32
     */
    public static int ways2(int N, int M) {
        // TODO
        if (N < 1 || M < 1 || ((N * M) & 1) != 0) {
            return 0;
        }
        if (N == 1 || M == 1) {
            return 1;
        }
        return -1;
    }

}

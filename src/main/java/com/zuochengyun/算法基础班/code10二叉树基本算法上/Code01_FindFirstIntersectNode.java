package com.zuochengyun.算法基础班.code10二叉树基本算法上;

/**
 * 给定两个链表，这两个链表可能是有环的，也可能是无环的，返回两个链表第一个相交的节点
 *
 * @author 钢人
 */
public class Code01_FindFirstIntersectNode {

    public static class Node {
        public int value;
        public Node next;

        public Node(int data) {
            this.value = data;
        }
    }

    public static Node getIntersectNode(Node head1, Node head2) {
        if (head1 == null || head2 == null) {
            return null;
        }
        Node loop1 = getLoopNode(head1);
        Node loop2 = getLoopNode(head2);
        // 如果头节点1和2都不是环形列表
        if (loop1 == null && loop2 == null) {
            return noLoop(head1, head2);
        }
        if (loop1 != null && loop2 != null) {
            return bothLoop(head1, loop1, head2, loop2);
        }
        return null;
    }


    /**
     * 找到 head 节点的第一个环形节点，如果没有环返回null
     */
    private static Node getLoopNode(Node head) {
        if (head == null || head.next == null || head.next.next == null) {
            return null;
        }
        // 快慢针
        Node slow = head.next;
        Node fast = head.next.next;

        // 用快慢针找出环形节点，如果有，快慢针把整个列表遍历一圈之后，一定能找到
        while (slow != fast) {
            if (fast.next == null || fast.next.next == null) {
                return null;
            }
            slow = slow.next;
            fast = fast.next.next;
        }
        // 如果撞上了，旧说明找到了环行节点
        // 在从头节点开始遍历一遍
        fast = head;
        while (slow != fast) {
            slow = slow.next;
            fast = fast.next;
        }
        return slow;
    }

    /**
     * 两个链表都是无环列表
     */
    private static Node noLoop(Node head1, Node head2) {
        if (head1 == null || head2 == null) {
            return null;
        }

        Node cur1 = head1;
        Node cur2 = head2;
        int n = 0;
        // 统计 head1和head2 个数
        while (cur1.next != null) {
            n++;
            cur1 = cur1.next;
        }
        while (cur2.next != null) {
            n--;
            cur2 = cur2.next;
        }
        // 如果两个列表有交集，存在共同的节点，那么两个链表遍历完之后，最后一个节点一定是同一个
        if (cur1 != cur2) {
            return null;
        }

        // 较长的列表作为 cur1 的头
        cur1 = n > 0 ? head1 : head2;
        // 较短的列表作为 cur2 的头
        cur2 = cur1 == head1 ? head2 : head1;
        // 取绝对值，两个列表相差多个节点
        n = Math.abs(n);
        // 把长的节点走到跟短的链表一样长的节点
        while (n != 0) {
            n--;
            cur1 = cur1.next;
        }
        // 从两个节点一样长的为止同时走，一直走到相交的为止
        while (cur1 != cur2) {
            cur1 = cur1.next;
            cur2 = cur2.next;
        }
        return cur1;
    }

    /**
     * 两个列表都是有环列表
     */
    private static Node bothLoop(Node head1, Node loop1, Node head2, Node loop2) {
        // 如果两个列表有相交的节点，那么这个节点一定在 loop1 和 loop2 这个环内上的节点
        Node cur1;
        Node cur2;
        // 如果两个环节点相等，从这两个链表的环节点开始遍历，看有没有相交的点
        // 这里和无环逻辑一致
        if (loop1 == loop2) {
            cur1 = head1;
            cur2 = head2;
            int n = 0;
            while (cur1 != loop1) {
                n++;
                cur1 = cur1.next;
            }
            while (cur2 != loop2) {
                n--;
                cur2 = cur2.next;
            }
            cur1 = n > 0 ? head1 : head2;
            cur2 = cur1 == head1 ? head2 : head1;
            n = Math.abs(n);
            while (n != 0) {
                n--;
                cur1 = cur1.next;
            }
            while (cur1 != cur2) {
                cur1 = cur1.next;
                cur2 = cur2.next;
            }
            return cur1;
        } else {
            cur1 = loop1.next;
            // 如果这两个列表有相交的点，那么一定会经过loop2点
            while (cur1 != loop1) {
                if (cur1 == loop2) {
                    return loop1;
                }
                cur1 = cur1.next;
            }
            return null;
        }
    }

}

package com.zuochengyun.算法基础班.code27KMP算法;

/**
 * 1）如何理解next数组
 * 2）如何利用next数组加速匹配过程，优化时的两个实质！（私货解释）
 *
 * 例如：str1：abcssabct 是否包含 str2：abcssabcz
 * 一开始比较的时候是从 str1 下标为 0 位置比较的，abcssabct 和 abcssabcz 比较到最后一位才发现不相等，但是前面都相等
 * 进行第二轮比较的时候，传统的暴力匹配方式是会从 str1 下标 1 的位置在开始尝试比较
 * 这时候 kmp 的比较算法不会从 str1 下标为 1 的位置开始比较，而是计算 str2 发生匹配出现错误的字符之前，那些相等的字符串 abcssabc 中
 * 划分出两部分前缀和后缀，将其比较看是否右出现重复相等的字符串，明显该 str2 中是有相等的前缀和后缀的
 * 分别是 str2[0-2]:abc，str2[5-7] 位置的字符串是相等的，那么就说明 str1 中也是同样如此，这样 str1 中的 前缀为 str1[0-2] 位置
 * 字符串就不需要比较了，直接移动到 str1 下标为 5 的位置开始和 str2 比较，以此类推可以排除掉很多不可能的遍历
 * 如果 str2 中并没有前缀和后缀相等的字符串，这种算法就和暴力匹配没什么区别了，比如 str2 是 abcdef，并无相同的前后缀可循
 *
 * @author 钢人
 */
public class Code01_KMP {

    // TODO

    public static int getIndexOf(String s1, String s2) {
        if (s1 == null || s2 == null || s2.length() < 1 || s1.length() < s2.length()) {
            return -1;
        }
        char[] str1 = s1.toCharArray();
        char[] str2 = s2.toCharArray();
        // str1 前后缀相同的位置
        int x = 0;
        // str2 前后缀相同的位置
        int y = 0;
        // 生成一个数组，这个数组表示 str2 字符串匹配的过程中，如果某一个位置失败了，下一次从哪个位置开始匹配
        int[] next = getNextArray(str2);
        // O(N)
        while (x < str1.length && y < str2.length) {
            // 如果相等，向后移动
            if (str1[x] == str2[y]) {
                x++;
                y++;

                // y == 0
            } else if (next[y] == -1) {
                x++;

                // 直接跳到 str2 匹配失败的位置，str2 从相应的位置开始和 str1 匹配
                // 也就是重新跳到 str2 下标为 y 之前匹配成功的字符串的最长相等前后缀的位置
                // 例如；abcssabcz，跳到下标为 5 的位置（目前是猜测）
            } else {
                y = next[y];
            }
        }
        return y == str2.length ? x - y : -1;
    }

    private static int[] getNextArray(char[] str2) {
        // 如果 str2 长度 只有 1，表示没办法跳着匹配，只能是挨个匹配
        if (str2.length == 1) {
            return new int[] { -1 };
        }
        int[] next = new int[str2.length];
        next[0] = -1;
        next[1] = 0;
        // 目前在哪个位置上求next数组的值
        int i = 2;
        // 当前是哪个位置的值再和 i-1 位置的字符比较
        int cn = 0;
        //  下标换算
        while (i < next.length) {
            // 如果匹配成功了，跳转下标位置向右移
            if (str2[i - 1] == str2[cn]) {
                next[i++] = ++cn;

                // cn 还能继续往左跳，还能往左尝试去找相同的字符
            } else if (cn > 0) {
                cn = next[cn];

                // 如果不能匹配了，那么表示不能往前跳了，确实没有相同的字符了，那就只能结束，标识 0
            } else {
                next[i++] = 0;
            }
        }
        return next;
    }

    public static String getRandomString(int possibilities, int size) {
        char[] ans = new char[(int) (Math.random() * size) + 1];
        for (int i = 0; i < ans.length; i++) {
            ans[i] = (char) ((int) (Math.random() * possibilities) + 'a');
        }
        return String.valueOf(ans);
    }

    public static void main(String[] args) {
        int possibilities = 5;
        int strSize = 20;
        int matchSize = 5;
        int testTimes = 5000000;
        System.out.println("test begin");
        for (int i = 0; i < testTimes; i++) {
            String str = getRandomString(possibilities, strSize);
            String match = getRandomString(possibilities, matchSize);
            if (getIndexOf(str, match) != str.indexOf(match)) {
                System.out.println("Oops!");
            }
        }
        System.out.println("test finish");
    }

}

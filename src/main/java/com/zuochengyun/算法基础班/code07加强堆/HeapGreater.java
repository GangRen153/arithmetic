package com.zuochengyun.算法基础班.code07加强堆;

import java.util.*;

/**
 * 加强堆
 *
 * 利用自定义包装对象，可以提供修改堆空间中对象里内容，并且能即时调整堆结构
 *
 * @author 钢人
 */
public class HeapGreater<T> {

    private List<T> heap;
    private Map<T, Integer> indexMap;
    private int heapSize;
    private Comparator<? super T> comp;

    public HeapGreater(Comparator<T> c) {
        this.heap = new ArrayList<>();
        this.indexMap = new HashMap<>();
        this.heapSize = 0;
        this.comp = c;
    }

    public boolean isEmpty() {
        return heapSize == 0;
    }

    public int size() {
        return heapSize;
    }

    public boolean contains(T obj) {
        return indexMap.containsKey(obj);
    }

    public T peek() {
        return heap.get(0);
    }

    public void push(T obj) {
        heap.add(obj);
        indexMap.put(obj, heapSize);
        heapInsert(heapSize);
        heapSize++;
    }

    public T pop() {
        T ans = heap.get(0);
        heapSize--;
        swap(0, heapSize);
        indexMap.remove(ans);
        heap.remove(heapSize);
        // 调整结构
        heapify(0);
        return ans;
    }

    public void remove(T t) {
        // 拿到最后一个节点的对象
        T replace = heap.get(heapSize - 1);
        int index = indexMap.get(t);
        indexMap.remove(t);
        // 如果删除的 t 不是堆中最后一个节点，那么就需要重新调整
        if (t != replace) {
            // 把最后一个节点设置到被删除的节点上
            heap.set(index, replace);
            // 同时也更新以下下标映射表
            indexMap.put(replace, index);
            // 放弃最后一个节点
            resign(replace);
        }
    }

    private void resign(T t) {
        // 调整指定对象节点
        heapInsert(indexMap.get(t));
        // 调整整个堆结构
        heapify(indexMap.get(t));
    }

    private void heapify(int index) {
        int left = index * 2 + 1;
        while (left < heapSize) {
            int best = left + 1 < heapSize && comp.compare(heap.get(left + 1), heap.get(left)) < 0 ? left + 1 : left;
            // 如果前面的 - 后面的 < 0，那么说明前面的比后面的小
            best = comp.compare(heap.get(best), heap.get(index)) < 0 ? best : index;
            if (best == index) {
                break;
            }
            swap(best, index);
            index = best;
            left = index * 2 + 1;
        }
    }

    private void heapInsert(int index) {
        int left = index / 2;
        // 比较器比较以下是否需要调整
        while (comp.compare(heap.get(index), heap.get(left)) < 0) {
            swap(index, left);
            index = left;
        }
    }

    private void swap(int i, int j) {
        T o1 = heap.get(i);
        T o2 = heap.get(j);
        heap.set(i, o2);
        heap.set(j, o1);
        indexMap.put(o1, j);
        indexMap.put(o2, i);
    }

}

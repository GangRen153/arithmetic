package com.zuochengyun.算法基础班.code07加强堆;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * 最大线段重合问题（用堆的实现
 * <p>
 * 给定很多线段，每个线段都有两个数[start, end]
 * 表示线段开始位置和结束位置，左右都是闭区间
 * 规定：
 * 1）线段的开始和结束位置一定都是整数值
 * 2）线段重合区域的长度必须 >= 1
 * 返回线段最多重合区域中，包含了几条线段
 * <p>
 * 例如：[1,4][2,5],这两条线段有重合区域为 2-4
 *
 * @author 钢人
 */
public class Code01_CoverMax {

    /**
     * 非堆结构方式实现
     */
    public static int maxCover1(int[][] lines) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        // 找出边界值
        for (int i = 0; i < lines.length; i++) {
            min = Math.min(min, lines[i][0]);
            max = Math.max(max, lines[i][1]);
        }
        int cover = 0;
        // 遍历min~max范围
        for (double p = min + 0.5; p < max; p += 1) {
            int cur = 0;
            // 循环整个数组，拿到每一个线段，看线段是否与 min++.5 的数
            for (int i = 0; i < lines.length; i++) {
                if (lines[i][0] < p && lines[i][1] > p) {
                    cur++;
                }
            }
            cover = Math.max(cover, cur);
        }
        return cover;
    }

    public static int maxCover2(int[][] m) {
        // 准备数据，将其包装到 line 对象
        Line[] lines = new Line[m.length];
        for (int i = 0; i < m.length; i++) {
            lines[i] = new Line(m[i][0], m[i][1]);
        }
        // 根据线段开始位置从小到大排序
        Arrays.sort(lines, Comparator.comparingInt(o -> o.end));
        // 小根堆，每一条线段的结尾数值，使用默认的
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        int max = 0;
        for (int i = 0; i < lines.length; i++) {
            // 如果开始位置大于之前的结束位置，说明不包含在内，不包含直接弹出
            // 剩下没弹出的说明当前 i 线段包含在内
            while (!heap.isEmpty() && heap.peek() <= lines[i].start) {
                heap.poll();
            }
            // 把当前 i 线段的结束位置放到堆中
            heap.add(lines[i].end);
            // 看当前 i 线段符合条件的重合线段数量和之前符合最多的数比较
            max = Math.max(max, heap.size());
        }
        return max;
    }

    public static class Line {
        public int start;
        public int end;

        public Line(int s, int e) {
            start = s;
            end = e;
        }
    }

    public static void main(String[] args) {
        int[][] arr = new int[][]{
                {2, 4},
                {5, 7},
                {8, 9}
        };
        System.out.println(maxCover1(arr));
    }

}

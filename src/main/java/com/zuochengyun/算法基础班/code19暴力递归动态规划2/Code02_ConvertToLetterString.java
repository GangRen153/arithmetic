package com.zuochengyun.算法基础班.code19暴力递归动态规划2;

/**
 * 规定1和A对应、2和B对应、3和C对应...26和Z对应
 * 那么一个数字字符串比如"111”就可以转化为:
 * "AAA"、"KA"和"AK"
 * 给定一个只有数字字符组成的字符串str，返回有多少种转化结果
 *
 * @author 钢人
 */
public class Code02_ConvertToLetterString {

    public static int number(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        return process(str.toCharArray(), 0);
    }

    private static int process(char[] chars, int index) {
        if (chars.length == index) {
            return 1;
        }
        // 1->A，2->B，0没有单独对应的字符，说明之前做的决定是错的
        if (chars[index] == '0') {
            return 0;
        }
        // i 单独转
        int ways = process(chars, index + 1);
        if (index + 1 < chars.length && (chars[index] - '0') * 10 + chars[index + 1] - '0' < 27) {
            ways += process(chars, index + 2);
        }
        return ways;
    }


    public static int dp1(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        char[] str = s.toCharArray();
        int N = str.length;
        int[] dp = new int[N + 1];
        dp[N] = 1;
        for (int i = N - 1; i >= 0; i--) {
            if (str[i] == '0') {
                continue;
            }
            int ways = dp[i + 1];
            if (i + 1 < str.length && (str[i] - '0') * 10 + str[i + 1] - '0' < 27) {
                ways += dp[i + 2];
            }
            dp[i] = ways;
        }
        return dp[0];
    }


}

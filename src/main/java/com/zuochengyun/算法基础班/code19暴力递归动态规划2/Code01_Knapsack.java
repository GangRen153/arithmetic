package com.zuochengyun.算法基础班.code19暴力递归动态规划2;

/**
 * 背包问题
 * <p>
 * 给定两个等长的数组和一个变量 bag
 * w：表示重量，v：表示价值，bag：表示自己的背包最大容量
 * 问：在自己背包容量范围内，怎么装价值最大
 *
 * @author 钢人
 */
public class Code01_Knapsack {

    /**
     * @param w   所有货物重量
     * @param v   每个重量对应的价值
     * @param tag 背包最大容量
     * @return 最大价值
     */
    public static int maxValue(int[] w, int[] v, int tag) {
        if (w == null || v == null || w.length < 1 || w.length != v.length) {
            return 0;
        }
        return process(w, v, 0, tag);
    }

    public static int process(int[] w, int[] v, int index, int rest) {
        if (rest < 0) {
            return -1;
        }
        if (w.length == index) {
            return 0;
        }
        int p1 = process(w, v, index + 1, rest);
        int p2 = 0;
        int next = process(w, v, index + 1, rest - w[index]);
        if (next != -1) {
            p2 = v[index] + next;
        }
        return Math.max(p1, p2);
    }


    /** ----------------------------------------------------------------------------------------- */

    public static int maxValue2(int[] w, int[] v, int tag) {
        if (w == null || v == null || w.length < 1 || w.length != v.length) {
            return 0;
        }

        int N = w.length;
        int[][] dp = new int[N][tag + 1];

        for (int index = N - 1; index >= 0; index--) {
            for (int rest = 0; rest <= tag; rest++) {
                int p1 = dp[index + 1][rest];
                int p2 = 0;
                int next = rest - w[index] < 0 ? -1 : dp[index + 1][rest - w[index]];
                if (next != -1) {
                    p2 = v[index] + next;
                }
                dp[index][rest] = Math.max(p1, p2);
            }
        }
        return dp[0][tag];
    }


}

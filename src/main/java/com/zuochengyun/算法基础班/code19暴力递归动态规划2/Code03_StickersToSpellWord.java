package com.zuochengyun.算法基础班.code19暴力递归动态规划2;

/**
 * 贴纸题
 *
 * 给定一个数组，里面是不同字符的贴纸，每张贴纸有无限张
 * 每张贴纸上可以剪碎，在给定一个目标字符串
 * 问最少几张贴纸剪完之后能拼接成指定字符
 *
 * 本题测试链接：<a href="https://leetcode.com/problems/stickers-to-spell-word"/>
 * @author 钢人
 */
public class Code03_StickersToSpellWord {

    public static int minStickers1(String[] stickers, String target) {
        if (stickers == null || stickers.length == 0 || target == null || target.length() == 0) {
            return 0;
        }
        int ans = process(stickers, target);
        return ans == Integer.MAX_VALUE ? 0 : ans;
    }

    private static int process(String[] stickers, String target) {
        if (target.length() == 0) {
            return 0;
        }
        int min = Integer.MAX_VALUE;
        for (String first : stickers) {
            // 本次剪掉之后，还剩多少字符需要剪掉，就是把 first 剪碎，把包含 target 的字符抵消掉
            String rest = minus(target, first);
            // 如果能抵消掉，在进行后面的尝试，如果抵消不了，说明这张贴纸无效，换一张贴纸继续尝试
            if (rest.length() != target.length()) {
                min = Math.min(min, process(stickers, rest));
            }
        }
        return min + (min == Integer.MAX_VALUE ? 0 : 1);
    }

    private static String minus(String target, String first) {
        char[] str1 = target.toCharArray();
        char[] str2 = first.toCharArray();
        int[] count = new int[26];
        for (char cha : str1) {
            count[cha - 'a']++;
        }
        for (char cha : str2) {
            count[cha - 'a']--;
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 26; i++) {
            if (count[i] > 0) {
                for (int j = 0; j < count[i]; j++) {
                    builder.append((char) (i + 'a'));
                }
            }
        }
        return builder.toString();
    }


    public static int process2(int[][] stickers, String t) {
        // TODO
        return 0;
    }


}

package com.zuochengyun.算法基础班.code38对数器找规律和数据量猜解法;

/**
 * int[] d，d[i]：i号怪兽的能力
 * int[] p，p[i]：i号怪兽要求的钱
 * 开始时你的能力是0，你的目标是从0号怪兽开始，通过所有的怪兽。
 * 如果你当前的能力，小于i号怪兽的能力，你必须付出p[i]的钱，贿赂这个怪兽，然后怪兽就会加入你，他的能力直接累加到你的能力上；
 * 如果你当前的能力，大于等于i号怪兽的能力，你可以选择直接通过，你的能力并不会下降，你也可以选择贿赂这个怪兽，然后怪兽就会加入你，他的能力直接累加到你的能力上。
 * 返回通过所有的怪兽，需要花的最小钱数。
 *
 * @author 钢人
 */
public class Code04_MoneyProblem {

    public static long func1(int[] d, int[] p) {
        return process1(d, p, 0, 0);
    }

    /**
     * @param d       怪兽能力
     * @param p       贿赂怪兽要求的钱
     * @param ability 当前你所具有的能力
     * @param index   来到了第index个怪兽的面前
     * @return 返回通过所有的怪兽，需要花的最小钱数。
     */
    public static long process1(int[] d, int[] p, int ability, int index) {
        if (index == d.length) {
            return 0;
        }
        // 怪兽比我厉害，我只能贿赂
        if (ability < d[index]) {
            return p[index] + process1(d, p, ability + d[index], index + 1);
        }
        return Math.min(
                // 选择贿赂怪兽
                p[index] + process1(d, p, ability + d[index], index + 1),
                // 选择直接跳过
                process1(d, p, ability, index + 1));
    }

    public static long func1DP(int[] d, int[] p) {
        int sum = 0;
        for (int num : d) {
            sum += num;
        }
        long[][] dp = new long[d.length + 1][sum + 1];
        for (int i = 0; i <= sum; i++) {
            dp[0][i] = 0;
        }
        for (int cur = d.length - 1; cur >= 0; cur--) {
            for (int hp = 0; hp <= sum; hp++) {
                if (hp + d[cur] > sum) {
                    continue;
                }
                if (hp < d[cur]) {
                    dp[cur][hp] = p[cur] + dp[cur + 1][hp + d[cur]];
                } else {
                    dp[cur][hp] = Math.min(p[cur] + dp[cur + 1][hp + d[cur]], dp[cur + 1][hp]);
                }
            }
        }
        return dp[0][0];
    }



    public static int minMoney2(int[] d, int[] p) {
        // 收集所有的钱
        int allMoney = 0;
        for (int j : p) {
            allMoney += j;
        }
        int n = d.length;
        for (int money = 0; money < allMoney; money++) {
            if (process2(d, p, n - 1, money) != -1) {
                return money;
            }
        }
        return allMoney;
    }

    /**
     * 通过所有怪兽，严格花费的钱严格等于money，money 从 0 开始
     * 如果 0 钱可以通过，肯定是花钱最少的
     * 循环次数是贿赂所有怪兽花的钱数，循环的过程中，只要0~总钱数最早出现
     * 如果出现了刚好花 money 钱，就说明是最少钱数
     * 返回的是我的能力
     */
    public static long process2(int[] d, int[] p, int index, int money) {
        // 如果没怪兽了，看是否刚好钱花完了
        if (index == -1) {
            return money == 0 ? 0 : -1;
        }
        // 不贿赂怪兽的情况下
        long preMaxAbility = process2(d, p, index - 1, money);
        // 我的能力是多少
        long p1 = -1;
        if (preMaxAbility != -1 && preMaxAbility >= d[index]) {
            p1 = preMaxAbility;
        }
        // 贿赂当前怪兽
        long preMaxAbility2 = process2(d, p, index - 1, money - p[index]);
        // 我的能力是多少
        long p2 = -1;
        if (preMaxAbility2 != -1) {
            p2 = d[index] + preMaxAbility2;
        }
        // 返回我的最大能力，返回 -1，说明我过不了关
        return Math.max(p1, p2);
    }

    public static long func2DP(int[] d, int[] p) {
        int sum = 0;
        for (int num : p) {
            sum += num;
        }
        // dp[i][j]含义：
        // 能经过0～i的怪兽，且花钱为j（花钱的严格等于j）时的武力值最大是多少？
        // 如果dp[i][j]==-1，表示经过0～i的怪兽，花钱为j是无法通过的，或者之前的钱怎么组合也得不到正好为j的钱数
        int[][] dp = new int[d.length][sum + 1];
        for (int i = 0; i < dp.length; i++) {
            for (int j = 0; j <= sum; j++) {
                dp[i][j] = -1;
            }
        }
        // 经过0～i的怪兽，花钱数一定为p[0]，达到武力值d[0]的地步。其他第0行的状态一律是无效的
        dp[0][p[0]] = d[0];
        for (int i = 1; i < d.length; i++) {
            for (int j = 0; j <= sum; j++) {
                // 可能性一，为当前怪兽花钱
                // 存在条件：
                // j - p[i]要不越界，并且在钱数为j - p[i]时，要能通过0～i-1的怪兽，并且钱数组合是有效的。
                if (j >= p[i] && dp[i - 1][j - p[i]] != -1) {
                    dp[i][j] = dp[i - 1][j - p[i]] + d[i];
                }
                // 可能性二，不为当前怪兽花钱
                // 存在条件：
                // 0~i-1怪兽在花钱为j的情况下，能保证通过当前i位置的怪兽
                if (dp[i - 1][j] >= d[i]) {
                    // 两种可能性中，选武力值最大的
                    dp[i][j] = Math.max(dp[i][j], dp[i - 1][j]);
                }
            }
        }
        int ans = 0;
        // dp表最后一行上，dp[N-1][j]代表：
        // 能经过0～N-1的怪兽，且花钱为j（花钱的严格等于j）时的武力值最大是多少？
        // 那么最后一行上，最左侧的不为-1的列数(j)，就是答案
        for (int j = 0; j <= sum; j++) {
            if (dp[d.length - 1][j] != -1) {
                ans = j;
                break;
            }
        }
        return ans;
    }

}

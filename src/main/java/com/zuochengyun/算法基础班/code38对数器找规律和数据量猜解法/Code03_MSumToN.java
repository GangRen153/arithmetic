package com.zuochengyun.算法基础班.code38对数器找规律和数据量猜解法;

/**
 * 定义一种数：可以表示成若干（数量>1）连续正数和的数
 * 比如:
 * 5 = 2+3，5就是这样的数
 * 12 = 3+4+5，12就是这样的数
 * 1不是这样的数，因为要求数量大于1个、连续正数和
 * 2 = 1 + 1，2 也不是，因为等号右边不是连续正数
 * 给定一个参数 N，返回是不是可以表示成若干连续正数和的数
 *
 * @author 钢人
 */
public class Code03_MSumToN {

    public static boolean isMSum1(int num) {
        // 从 1+...+num 开始尝试
        for (int start = 1; start <= num; start++) {
            int sum = start;
            // sum+=(j+1) 就是 start+(start+1)+(start+2)+...+num
            for (int j = start + 1; j < num; j++) {
                if (sum + j == num) {
                    return true;
                }
                if (sum + j > num) {
                    return false;
                }
                sum += j;
            }
        }
        return false;
    }

    /**
     * 最优解
     */
    public static boolean isMSum2(int num) {
//		return num == (num & (~num + 1));
//		return num == (num & (-num));
        return (num & (num - 1)) != 0;
    }

    public static void main(String[] args) {
        for (int num = 1; num < 200; num++) {
            System.out.println(num + " : " + isMSum1(num));
        }
        System.out.println("test begin");
        for (int num = 1; num < 5000; num++) {
            if (isMSum1(num) != isMSum2(num)) {
                System.out.println("Oops!");
            }
        }
        System.out.println("test end");

    }

}

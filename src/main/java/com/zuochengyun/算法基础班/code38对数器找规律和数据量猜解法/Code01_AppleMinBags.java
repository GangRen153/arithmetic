package com.zuochengyun.算法基础班.code38对数器找规律和数据量猜解法;

/**
 * 数据量猜解法
 * <p>
 * 用袋子装苹果
 * 给定 N 个苹果，有两种袋子，有一种袋子能装下6个苹果，还有一种苹果能装下8个苹果
 * 要求袋子必须装满，如果装不满放回-1，返回至少需要多少个袋子
 *
 * @author 钢人
 */
public class Code01_AppleMinBags {

    public static int minBags(int apple) {
        if (apple < 0) {
            return -1;
        }
        // 装 8 苹果的所需要的袋子
        int bag8 = apple / 8;
        // 还剩多少个苹果
        int rest = apple - bag8 * 8;
        // 剩下的苹果全用装6个苹果的袋子装
        while (rest > 0) {
            if (rest % 6 == 0) {
                return bag8 + (rest / 6);
            } else {
                bag8--;
                rest += 8;
            }
        }
        return -1;
    }

    /**
     * 最优解
     */
    public static int minBagAwesome(int apple) {
        // 奇数个苹果不管怎么装都不能正好装满，返回-1
        if ((apple & 1) != 0) {
            return -1;
        }
        // 剩下的都是装偶数个苹果
        if (apple < 18) {
            return apple == 0 ? 0 : (apple == 6 || apple == 8) ? 1
                    : (apple == 12 || apple == 14 || apple == 16) ? 2 : -1;
        }
        // 从18开始，结包理解规律得知
        return (apple - 18) / 8 + 3;
    }

    public static void main(String[] args) {
        // 暴力测试看数据规律
        for(int apple = 1; apple < 200;apple++) {
            System.out.println(apple + " : "+ minBags(apple));
        }
    }

}

package com.zuochengyun.算法基础班.code47动态规划猜法和外部信息简化下;

/**
 * 整型数组arr长度为n(3 <= n <= 10^4)，最初每个数字是<=200的正数且满足如下条件：
 * 1. arr[0]   <=   arr[1]
 * 2. arr[n-1] <=   arr[n-2]
 * 3. arr[i]   <=   max(arr[i-1], arr[i+1])
 * 但是在arr有些数字丢失了，比如k位置的数字之前是正数，丢失之后k位置的数字为0。
 * 请你根据上述条件， 计算可能有多少种不同的arr可以满足以上条件。
 * 比如 [6,0,9] 只有还原成 [6,9,9]满足全部三个条件，所以返回1种。
 *
 * @author 钢人
 */
public class Code02_RestoreWays {

    public static int ways0(int[] arr) {
        return process0(arr, 0);
    }

    private static int process0(int[] arr, int index) {
        // 如果到了最右边界位置了，说明结束了，看之前做所的决定整体数组是否符合规则，符合就返回1，不符合返回0
        if (index == arr.length) {
            return isValid(arr) ? 1 : 0;
        }
        // 如果不是0，说明不是 k 位置，继续往后找
        if (arr[index] != 0) {
            return process0(arr, index + 1);
        }

        // 如果是 0，说明到了 k 位置
        int ways = 0;
        for (int v = 1; v < 201; v++) {
            arr[index] = v;
            ways += process0(arr, index + 1);
        }
        arr[index] = 0;
        return ways;
    }

    /**
     * 校验数组是否符合规则
     */
    public static boolean isValid(int[] arr) {
        if (arr[0] > arr[1]) {
            return false;
        }
        if (arr[arr.length - 1] > arr[arr.length - 2]) {
            return false;
        }
        for (int i = 1; i < arr.length - 1; i++) {
            if (arr[i] <= Math.max(arr[i - 1], arr[i + 1])) {
                return false;
            }
        }
        return true;
    }

    // TODO

    public static int ways1(int[] arr) {
        int N = arr.length;
        if (arr[N - 1] != 0) {
            return process1(arr, N - 1, arr[N - 1], 2);
        }
        int ways = 0;
        for (int v = 1; v < 201; v++) {
            ways += process1(arr, N - 1, v, 2);
        }
        return ways;
    }

    /**
     * @param i 下标
     * @param v 尝试要变成的值
     * @param s     0 代表arr[i] < arr[i+1] 右大
     *              1 代表arr[i] == arr[i+1] 右=当前
     *              2 代表arr[i] > arr[i+1] 右小
     */
    public static int process1(int[] arr, int i, int v, int s) {
        // i 到数组第一位数了，说明只剩下一个数了
        if (i == 0) {
            // 如果0位置的数 <= 1位置的数 && 0位置的数是0或者等于前者 k 位置尝试的值，就符合条件，反之不符合
            return (s == 0 || s == 1) && (arr[0] == 0 || v == arr[0]) ? 1 : 0;
        }
        // 中间位置的数，i位置不是0，说明不是 k 位置 && i位置的数不是尝试要替换的值，说明尝试失败
        if (arr[i] != 0 && v != arr[i]) {
            return 0;
        }

        int ways = 0;
        // i <= i+1
        if (s == 0 || s == 1) {
            for (int pre = 1; pre < 201; pre++) {
                ways += process1(arr, i - 1, pre, pre < v ? 0 : (pre == v ? 1 : 2));
            }
        } else {
            for (int pre = v; pre < 201; pre++) {
                ways += process1(arr, i - 1, pre, pre == v ? 1 : 2);
            }
        }
        return ways;
    }

}

package com.zuochengyun.算法基础班.code25单调栈;

import java.util.Stack;

/**
 * 给定一个只包含正数的数组 arr，arr 中任何一个子数组 sub，
 * 一定都可以算出(sub累加和) * (sub中的最小值)是什么，
 * 那么所有子数组中，这个值最大是多少？
 *
 * @author 钢人
 */
public class Code02_AllTimesMinToMax {

    /**
     * 暴力解
     */
    public static int max1(int[] arr) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j < arr.length; j++) {
                int minNum = Integer.MAX_VALUE;
                int sum = 0;
                for (int k = i; k <= j; k++) {
                    sum += arr[k];
                    minNum = Math.min(minNum, arr[k]);
                }
                max = Math.max(max, minNum * sum);
            }
        }
        return max;
    }

    public static int max2(int[] arr) {
        int size = arr.length;
        // 前缀和（方便统计指定范围的子数组累加和）
        int[] sums = new int[size];
        sums[0] = arr[0];
        for (int i = 1; i < size; i++) {
            sums[i] = sums[i - 1] + arr[i];
        }

        int max = Integer.MIN_VALUE;
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < size; i++) {
            // 如果栈顶的数 >= arr[i] 位置的数，说明以栈底为最小值的子数组的范围只能截至到 i-1 位置
            // 如果把 i 位置的数也计算在以栈底为最小值的子数组中，这个子数组的累加和就会变小了
            // 所以栈底到栈顶的子数组就已经是最大累加和的子数组了，直接计算这个子数组的累加和 * 子数组的最小值
            while (!stack.isEmpty() && arr[stack.peek()] >= arr[i]) {
                // 弹出栈顶下标
                Integer pop = stack.pop();
                // 计算最大值，如果栈不是空的？？？
                max = Math.max(max, (stack.isEmpty() ? sums[i - 1] : (sums[i - 1] - sums[stack.peek()])) * arr[pop]);
            }
            stack.push(i);
        }
        // 计算栈中的子数组累加和 * 最小值
        while (!stack.isEmpty()) {
            // 弹出栈顶下标
            Integer pop = stack.pop();
            // 计算最大值，如果栈不是空的
            max = Math.max(max, (stack.isEmpty() ? sums[size - 1] : (sums[size - 1] - sums[stack.peek()])) * arr[pop]);
        }
        return max;
    }

}

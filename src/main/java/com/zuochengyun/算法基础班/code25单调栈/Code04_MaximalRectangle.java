package com.zuochengyun.算法基础班.code25单调栈;

import java.util.Stack;

/**
 * 给定一个二维数组matrix，其中的值不是 0 就是 1，
 * 返回全部由 1 组成的最大子矩形，内部有多少个 1（内部有多少个 1 相当于这个长方形占多少个格子）
 *
 * 思路：
 * 将二维数组第一行为直方图的底计算最大直方图，在一第二行，第三行，第四行。。。
 * 以此类推
 *
 * 测试链接：<a href="https://leetcode.com/problems/maximal-rectangle/"/>
 * @author 钢人
 */
public class Code04_MaximalRectangle {

    public static int maximalRectangle(char[][] map) {
        if (map == null || map.length == 0 || map[0].length == 0) {
            return 0;
        }
        int maxArea = 0;
        // 每一行的长度
        int[] height = new int[map[0].length];
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                // 计算以每一行为直方图底边，计算长方形最大长度，归 0 的位置说明不能组成长方形
                // 以此类推，从上往下计算高度
                height[j] = map[i][j] == '0' ? 0 : height[j] + 1;
            }
            // 单独统计0~i层底边的最大长方形
            maxArea = Math.max(maxRecFromBottom(height), maxArea);
        }
        return maxArea;
    }

    /**
     * 计算以某一行为底边的高度长方形中，最大区域面积
     */
    private static int maxRecFromBottom(int[] height) {
        if (height == null || height.length == 0) {
            return 0;
        }
        int maxArea = 0;
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < height.length; i++) {
            // 如果i位置 <= 栈顶元素，说明栈底位置到 i 位置宽度的矩阵落空了
            // 中间遇到了0，所以组不成长方形了，计算栈底~i位置的长方形
            while (!stack.isEmpty() && height[i] <= height[stack.peek()]) {
                int pop = stack.pop();
                int k = stack.isEmpty() ? -1 : stack.peek();
                // 宽度 * 高度计算出长方形面积
                int curArea = (i - k - 1) * height[pop];
                // 取最大值
                maxArea = Math.max(maxArea, curArea);
            }
            stack.push(i);
        }
        // 计算剩下的面积区域
        while (!stack.isEmpty()) {
            int j = stack.pop();
            int k = stack.isEmpty() ? -1 : stack.peek();
            int curArea = (height.length - k - 1) * height[j];
            maxArea = Math.max(maxArea, curArea);
        }
        return maxArea;
    }

}

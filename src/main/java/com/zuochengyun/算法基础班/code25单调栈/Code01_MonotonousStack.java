package com.zuochengyun.算法基础班.code25单调栈;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * 给定一个可能含有重复值的数组arr
 * 找出数组中每一个 i 位置的数距离自己本身左边最近比自己小的数和右边最小的数
 *
 * 那么到底怎么设计呢？
 *
 * @author 钢人
 */
public class Code01_MonotonousStack {

    /**
     * 无重复值
     */
    public static int[][] getNearLessNoRepeat(int[] arr) {
        int[][] res = new int[arr.length][2];
        // 栈由小到大压入栈中
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < arr.length; i++) {
            // i 位置的数如果比栈顶中的数小，则标识栈顶上的数距离自己右边最小的数找到了
            // 添加栈顶距离右边最小的数和距离左边最小的数
            while (!stack.isEmpty() && arr[stack.peek()] > arr[i]) {
                Integer pop = stack.pop();
                int leftLessIndex = stack.isEmpty() ? -1 : stack.peek();
                // 设置 pop 位置左边比自己小的数
                res[pop][0] = leftLessIndex;
                // 设置右边比自己小于的数
                res[pop][1] = i;
            }
            stack.push(i);
        }

        // 如果右边没有比自己小的数了，那么栈中剩下的数都是由小到大的数
        // 遍历取出
        while (!stack.isEmpty()) {
            int pop = stack.pop();
            int leftLessIndex = stack.isEmpty() ? -1 : stack.peek();
            res[pop][0] = leftLessIndex;
            // 因为栈中剩下的数始终是由小到大的，所以左边都是比自己小的数，右边都是比自己大的数
            res[pop][1] = -1;
        }
        return res;
    }

    public static int[][] getNearLess(int[] arr) {
        int[][] res = new int[arr.length][2];
        // 栈中的 list 是用来装连续重复值的，并无其他用处
        Stack<List<Integer>> stack = new Stack<>();
        for (int i = 0; i < arr.length; i++) {
            // 如果找到比 i-1 位置小的数了，开始弹出
            while (!stack.isEmpty() && arr[stack.peek().get(0)] > arr[i]) {
                // 弹出 i 位置左边的数，list 如果是多个，说明 i-1 位置的数是连续重复值
                List<Integer> pops = stack.pop();
                // 取出栈底距离左边最近的数比 pops[0] 小的数。stack.peek().size() - 1是最近的
                int leftLessIndex = stack.isEmpty() ? -1 : stack.peek().get(stack.peek().size() - 1);
                // 挨个遍历距离左边最近最小的数和距离右边距离最小的数
                for (Integer pop : pops) {
                    res[pop][0] = leftLessIndex;
                    res[pop][1] = i;
                }
            }
            // 如果 i 位置的数和 i-1 位置的数是重复值，直接添加到数组中
            if (!stack.isEmpty() && arr[stack.peek().get(0)] == arr[i]) {
                stack.peek().add(i);
            } else {
                // 如果不是重复值，创建一个新的数组添加到栈中
                List<Integer> list = new ArrayList<>();
                list.add(i);
                stack.add(list);
            }
        }
        // 把剩下的数拿出来
        while (!stack.isEmpty()) {
            List<Integer> pops = stack.pop();
            int leftLessIndex = stack.isEmpty() ? -1 : stack.peek().get(stack.peek().size() - 1);
            for (Integer pop : pops) {
                res[pop][0] = leftLessIndex;
                res[pop][1] = -1;
            }
        }
        return res;
    }


}

package com.zuochengyun.算法基础班.code39数据量猜解法和卡特兰数;

/**
 * 牛牛家里一共有n袋零食, 第i袋零食体积为v[i]，背包容量为w。
 * 牛牛想知道在总体积不超过背包容量的情况下,
 * 一共有多少种零食放法，体积为0也算一种放法
 * 1 <= n <= 30, 1 <= w <= 2 * 10^9
 * v[i] (0 <= v[i] <= 10^9）
 *
 * @author 钢人
 */
public class Code02_SnacksWays {

    public static int ways1(int[] arr, int w) {
        if (arr == null || arr.length == 0 || w < 0) {
            return 0;
        }
        return process(arr, 0, w);
    }

    private static int process(int[] arr, int index, int rest) {
        if (rest < 0) {
            return 0;
        }
        if (arr.length == index) {
            return 1;
        }
        return process(arr, index + 1, rest) + process(arr, index + 1, rest - arr[index]);
    }

    public static int ways2(int[] arr, int w) {
        if (arr == null || arr.length == 0 || w < 0) {
            return 0;
        }

        int N = arr.length;
        int[][] dp = new int[N + 1][w + 1];
        for (int i = 0; i <= w; i++) {
            dp[N][i] = 0;
        }
        for (int index = N - 1; index >= 0; index--) {
            for (int rest = 0; rest <= w; rest++) {
                dp[index][rest] = dp[index + 1][rest] + ((rest - arr[index] < 0) ? 0 : dp[index + 1][rest - arr[index]]);
            }
        }
        return dp[0][w];
    }

    public static int ways3(int[] arr, int w) {
        // TODO
        return 0;
    }


}

package com.zuochengyun.算法基础班.code22暴力递归动态规划5;

/**
 * 给定3个参数，N，M，K，怪兽有 N 滴血，等着英雄来砍自己
 * 英雄每一次打击，都会让怪兽流失[0~M]的血量，到底流失多少？每一次在[0~M]上等概率的获得一个值
 * 求 K 次打击之后，英雄把怪兽砍死的概率
 *
 * @author 钢人
 */
public class Code01_KillMonster {

    /**
     * @param N 血量
     * @param M 掉血范围
     * @param K 砍的次数
     */
    public static double right(int N, int M, int K) {
        if (N < 1 || M < 1 || K < 1) {
            return 0;
        }
        long all = (long) Math.pow(M + 1, K);
        long kill = process(K, M, N);
        return (double) ((double) kill / (double) all);
    }

    private static long process(int times, int M, int hp) {
        if (times == 0) {
            return hp <= 0 ? 1 : 0;
        }
        if (hp <= 0) {
            return (long) Math.pow(M + 1, times);
        }
        long ways = 0;
        for (int i = 0; i <= M; i++) {
            ways += process(times - 1, i, hp - i);
        }
        return ways;
    }


    public static double dp1(int N, int M, int K) {
        if (N < 1 || M < 1 || K < 1) {
            return 0;
        }
        long all = (long) Math.pow(M + 1, K);
        long[][] dp = new long[K + 1][N + 1];
        dp[0][0] = 1;
        for (int times = 1; times <= K; times++) {
            for (int hp = 1; hp <= N; hp++) {
                long ways = 0;
                for (int i = 0; i <= M; i++) {
                    if (hp - i >= 0) {
                        ways += dp[times - 1][hp - i];
                    } else {
                        ways += (long) Math.pow(M + 1, times);
                    }
                }
                dp[times][hp] = ways;
            }
        }
        long kill = dp[K][N];
        return (double) ((double) kill / (double) all);
    }

}

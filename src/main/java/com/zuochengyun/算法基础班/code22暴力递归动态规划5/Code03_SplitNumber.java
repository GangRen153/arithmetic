package com.zuochengyun.算法基础班.code22暴力递归动态规划5;

/**
 * 给定一个正数n，求n的裂开方法数，
 * 规定：后面的数不能比前面的数小
 * 比如4的裂开方法有：
 * 1+1+1+1、1+1+2、1+3、2+2、4
 * 5种，所以返回5
 *
 * @author 钢人
 */
public class Code03_SplitNumber {

    public static int ways(int n) {
        if (n < 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        return process(1, n);
    }

    private static int process(int pre, int rest) {
        if (rest == 0) {
            return 0;
        }
        if (rest < pre) {
            return 0;
        }

        int ways = 0;
        for (int first = 0; first <= pre; first++) {
            ways += process(first, rest - first);
        }
        return ways;
    }

    public static int dp(int n) {
        if (n < 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }

        int[][] dp = new int[n + 1][n + 1];
        for (int pre = 0; pre < dp.length; pre++) {
            dp[pre][0] = 1;
            dp[pre][pre] = 1;
        }
        for (int pre = n - 1; pre >= 1; pre--) {
            for (int rest = pre + 1; rest <= n; rest++) {
                int ways = 0;
                for (int first = 0; first <= pre; first++) {
                    ways += dp[first][rest - first];
                }
                dp[pre][rest] = ways;
            }
        }
        return dp[1][n];
    }

    public static int dp2(int n) {
        // TODO
        if (n < 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        // n+1:因为n是整数，在数组中要直接用作下标
        // 例如：n是8，为了方便下标计算，数组最后一位的下标是8，所以数组长度是n·                +1
        int[][] dp = new int[n + 1][n + 1];
        for (int pre = 1; pre <= n; pre++) {
            dp[pre][0] = 1;
            dp[pre][pre] = 1;
        }
        for (int pre = n - 1; pre >= 1; pre--) {
            for (int rest = pre + 1; rest <= n; rest++) {
                dp[pre][rest] = dp[pre + 1][rest];
                dp[pre][rest] += dp[pre][rest - pre];
            }
        }
        return dp[1][n];
    }

}

package com.zuochengyun.算法基础班.code01排序;

import java.util.Arrays;

/**
 * 插入排序
 *
 * @author 钢人
 */
public class Code03_InsertionSort {

    public static void insertSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        for (int i = 1; i < arr.length; ++i) {
            for (int j = i - 1; j >= 0; --j) {
                // 用 j 位置的数与 j + 1 位置的数比较，j 的范围在 0 ~ (i-1) 的范围上，j 位置不断的向前推移，直到推到 0 位置
                // 前面的数如果比后面的数大，那么前面的数往后移，如果不大于那么 j 位置的数不动，再用 j-1 位置上的数继续和前面的数比较
                if (arr[j] > arr[j + 1]) {
                    swap(arr, j, j + 1);
                }
            }
        }
    }

    private static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public static void main(String[] args) {
        int[] arr = {3, 6, 2, 6, 3, 46, 8};
        insertSort(arr);
        System.out.println(Arrays.toString(arr));
    }

}

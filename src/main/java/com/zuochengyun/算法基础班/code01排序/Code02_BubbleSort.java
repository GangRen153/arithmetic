package com.zuochengyun.算法基础班.code01排序;

import java.util.Arrays;

/**
 * 冒泡排序
 *
 * @author 钢人
 */
public class Code02_BubbleSort {

    public static void bubbleSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    swap(arr, i, j);
                }
            }
        }
    }

    private static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public static void main(String[] args) {
        int[] arr = {1, 5, 3, 7, 4, 8, 3};
        bubbleSort(arr);
        System.out.println(Arrays.toString(arr));
    }

}

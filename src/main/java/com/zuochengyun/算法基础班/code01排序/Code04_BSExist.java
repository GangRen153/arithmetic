package com.zuochengyun.算法基础班.code01排序;

/**
 * 二分法
 * <p>
 * 在一个有序数组中，找某个数是否存在
 *
 * @author 钢人
 */
public class Code04_BSExist {

    public static boolean exist(int[] arr, int num) {
        if (arr == null || arr.length == 0) {
            return false;
        }
        int L = 0;
        int R = arr.length - 1;
        while (L <= R) {
            int mid = L + ((R - L) >> 1);
            if (arr[mid] == num) {
                return true;
            } else if (arr[mid] > num) {
                R = mid - 1;
            } else {
                L = mid + 1;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 5, 7, 8, 9, 15, 22};
        System.out.println(exist(arr, 7));
    }

}

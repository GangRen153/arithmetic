package com.zuochengyun.算法基础班.code01排序;

/**
 * 二分
 * <p>
 * 在一个有序数组中，找<=某个数最右侧的位置
 *
 * @author 钢人
 */
public class Code05_BSNearRight {

    public static int nearestIndex(int[] arr, int value) {
        if (arr == null) {
            return -1;
        }
        int L = 0;
        int R = arr.length - 1;
        int index = -1;
        while (L <= R) {
            int mid = L + (R - L >> 1);
            if (arr[mid] <= value) {
                index = mid;
                L = mid + 1;
            } else {
                R = mid - 1;
            }
        }
        return index;
    }

    public static void main(String[] args) {

    }

}

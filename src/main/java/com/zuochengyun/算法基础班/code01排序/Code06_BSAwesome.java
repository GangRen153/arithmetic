package com.zuochengyun.算法基础班.code01排序;

/**
 * 局部最小值问题
 * <p>
 * 给定一个无序数组，包含正负数和0
 * 找到其中一个数比它左边的数小，也比它右边的数小
 * 那么这个数就是局部最小值，找出其中一个局部最小值（不是全部）
 *
 * @author 钢人
 */
public class Code06_BSAwesome {

    public static int getLessIndex(int[] arr) {
        if (arr == null) {
            return -1;
        }

        int L = 0;
        int R = arr.length - 1;
        while (L < R) {
            int mid = R - L >> 1;
            // mid > 左边的数，条件不成立
            if (arr[mid] > arr[mid - 1]) {
                R = mid - 1;
            } else {
                // mid 比左边的数小或等于的清空下，mid 和右边的数比较，如果相等直接返回即可，因为只需要找到其中一个即可
                if (arr[mid] <= arr[mid + 1]) {
                    return mid;
                }
                L = mid + 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] arr = {1, 73, 65, 6434, 647, 2, 786, 5};
        System.out.println(getLessIndex(arr));
    }

}

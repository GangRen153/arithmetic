package com.zuochengyun.算法基础班.code01排序;

import java.util.Arrays;

/**
 * 选择排序
 *
 * @author 钢人
 */
public class Code01_SelectionSort {

    public static void selectionSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        //  arr.length - 1：最后一位不用比，推算到最后一位一定是最大的一位
        for (int i = 0; i < arr.length - 1; ++i) {
            int minIndex = i;
            // 找出比 i ~ arr.length 范围上比 i 位置小的数里面，找出最小的数下标
            for (int j = i + 1; j < arr.length; ++j) {
                minIndex = arr[j] < arr[minIndex] ? j : minIndex;
            }
            // 把比 i 位置小的最后一个数拿出来和 i 位置交换，排在第一个
            swap(arr, i, minIndex);
        }
    }

    private static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public static void main(String[] args) throws Exception {
        int[] arr = {4,7,2,6,3,5,3};
        selectionSort(arr);
        System.out.println(Arrays.toString(arr));
    }


}

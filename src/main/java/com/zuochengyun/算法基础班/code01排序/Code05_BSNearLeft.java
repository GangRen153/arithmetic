package com.zuochengyun.算法基础班.code01排序;

/**
 * 二分
 * <p>
 * 在一个有序数组中，找>=某个数最左侧的位置
 *
 * @author 钢人
 */
public class Code05_BSNearLeft {

    public static int nearestIndex(int[] arr, int value) {
        if (arr == null || arr.length == 0) {
            return -1;
        }
        int L = 0;
        int R = arr.length - 1;
        int index = -1;
        while (L <= R) {
            int mid = L + (R - L >> 1);
            if (arr[mid] >= value) {
                index = mid;
                R = mid - 1;
            } else {
                L = mid + 1;
            }
        }
        return index;
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 4, 6, 8, 9, 20, 24, 33};
        System.out.println(nearestIndex(arr, 5));
    }

}

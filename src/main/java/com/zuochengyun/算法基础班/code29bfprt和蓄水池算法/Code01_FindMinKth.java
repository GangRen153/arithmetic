package com.zuochengyun.算法基础班.code29bfprt和蓄水池算法;

import java.util.PriorityQueue;

/**
 * 在无序数组中求第K小的数
 *
 * @author 钢人
 */
public class Code01_FindMinKth {

    /**
     * 堆结构
     */
    public static int minKth1(int[] arr, int k) {
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>();
        for (int i = 0; i < arr.length; i++) {
            maxHeap.add(arr[i]);
        }
        for (int i = k; i < arr.length; i++) {
            if (arr[i] < maxHeap.peek()) {
                maxHeap.poll();
                maxHeap.add(arr[i]);
            }
        }
        return maxHeap.peek();
    }

    /**
     * 快排
     */
    public static int minKth2(int[] arr, int k) {
        return process2(arr, 0, arr.length - 1, k - 1);
    }

    private static int process2(int[] arr, int L, int R, int index) {
        if (L == R) {
            return arr[L];
        }
        // L~R 随机选一个位置做划分值
        int pivot = arr[L + (int) (Math.random() * (R - L + 1))];
        // 找到比 pivot 小的数和大的数下标位置
        int[] range = partition(arr, L, R, pivot);
        if (index >= range[0] && index <= range[1]) {
            return arr[index];

            // 从左边找
        } else if (index < range[0]) {
            return process2(arr, L, range[0] - 1, index);

            // 从右边找
        } else {
            return process2(arr, range[1] + 1, R, index);
        }
    }

    private static int[] partition(int[] arr, int L, int R, int pivot) {
        int less = L - 1;
        int more = R + 1;
        int cur = L;
        while (cur < more) {
            if (arr[cur] < pivot) {
                swap(arr, ++less, cur++);
            } else if (arr[cur] > pivot) {
                swap(arr, cur, --more);
            } else {
                cur++;
            }
        }
        return new int[] { less + 1, more - 1 };
    }

    public static void swap(int[] arr, int i1, int i2) {
        int tmp = arr[i1];
        arr[i1] = arr[i2];
        arr[i2] = tmp;
    }

    /**
     * bfprt 算法（由美国5个人发明的算法），与快排相比，只是取中间划分值不一样
     */
    public static int minKth3(int[] arr, int k) {
        return bfprt(arr, 0, arr.length - 1, k - 1);
    }

    private static int bfprt(int[] arr, int L, int R, int index) {
        if (L == R) {
            return arr[L];
        }
        // 找到中间划分值
        int pivot = medianOfMedians(arr, L, R);
        // 找到比划分值小的数和大的数
        int[] range = partition(arr, L, R, pivot);
        if (index >= range[0] && index <= range[1]) {
            return arr[index];
        } else if (index < range[0]) {
            return bfprt(arr, L, range[0] - 1, index);
        } else {
            return bfprt(arr, range[1] + 1, R, index);
        }
    }

    private static int medianOfMedians(int[] arr, int L, int R) {
        int size = R - L + 1;
        // 除以5得出模（就因为是5个人发明的）
        int offset = size % 5 == 0 ? 0 : 1;
        // 把L~R个数划分成若干个长度为5的数组
        int[] mArr = new int[size / 5 + offset];
        for (int team = 0; team < mArr.length; team++) {
            int teamFirst = L + team * 5;
            // L ... L + 4
            // L +5 ... L +9
            // L +10....L+14
            mArr[team] = getMedian(arr, teamFirst, Math.min(R, teamFirst + 4));
        }
        // mArr 中找到中位数
        return bfprt(mArr, 0, mArr.length - 1, mArr.length / 2);
    }

    public static int getMedian(int[] arr, int L, int R) {
        insertionSort(arr, L, R);
        return arr[(L + R) / 2];
    }

    public static void insertionSort(int[] arr, int L, int R) {
        for (int i = L + 1; i <= R; i++) {
            for (int j = i - 1; j >= L && arr[j] > arr[j + 1]; j--) {
                swap(arr, j, j + 1);
            }
        }
    }

}

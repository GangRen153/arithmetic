package com.zuochengyun.大厂刷题班.code21;

/**
 * 树链刨分
 *
 * 给定一个数组，数组中的数表示数组的下标位置
 * 给定的这个数组一定是一个无环数组，这个数组的所有下标连起来可以构建成一颗树结构，被称之为树链儿
 *
 * 还有一个权重数组，表示每个数组位置的权重值
 * 例如：[1,2,3,4,0,5,6,7]
 *
 * 要求在这个链儿的某个范围上全部加某个树。减某个树等等
 *
 * @author 钢人
 */
public class TreeChainPartition {

    public static class TreeChain {
        // 时间戳 0 1 2 3 4
        private int tim;
        // 节点个数
        private int n;
        // 头节点
        private int h;
        // [i][j]:i是用来记录每个节点，j是用来记录i有多少个孩子，并且记录孩子是谁
        private int[][] tree;
        // 权重数组
        private int[] val;
        // father数组一个平移，因为标号是从1开始的，而下标是从0开始的，所以整体向右平移1个单位
        private int[] fa;
        // 深度数组
        private int[] dep;
        // 表示 i 位置节点的重儿子节点是谁，如果是0，表示 i 这个节点没有儿子，如果不是 0，j，i这个节点，重儿子是j
        private int[] son;
        // 表示 i 这个节点为头的子树有多少个节点
        private int[] siz;
        // top[i] = j，i这个节点，所在的重链，头是j
        private int[] top;
        // dfn[i] = j，i这个节点，在dfs序中是第j个
        private int[] dfn;
        // 如果原来的节点a，权重是10
        // 如果a节点在dfs序中是第5个节点, tnw[5] = 10
        private int[] tnw;
        // 线段树，在tnw上，玩连续的区间查询或者更新
        private SegmentTree seg;

        public TreeChain(int[] father, int[] values) {
            // 初始化数组空间
            initTree(father, values);
            dfs1(h, 0);
            // top;
            // dfn;
            // tnw;
            dfs2(h, h);
            seg = new SegmentTree(tnw);
            seg.build(1, n, 1);
        }

        private void initTree(int[] father, int[] values) {
            tim = 0;
            // 因为序号是从1开始的，而下标的从0开始的，所以空间要扩大1个，0弃而不用
            n = father.length + 1;
            tree = new int[n][];
            val = new int[n];
            fa = new int[n];
            dep = new int[n];
            son = new int[n];
            siz = new int[n];
            top = new int[n];
            dfn = new int[n];
            tnw = new int[n--];
            // 初始化权重数组
            int[] cnum = new int[n];
            for (int i = 0; i < n; i++) {
                val[i + 1] = values[i];
            }

            // 初始化数组链儿，找到头部位置
            for (int i = 0; i < n; i++) {
                // 因为整体都向右平移了1个单位，所以头节点位置也要加1
                if (father[i] == i) {
                    h = i + 1;
                } else {
                    cnum[father[i]]++;
                }
            }
            // 统计每个节点有多少个孩子，并且孩子是谁
            tree[0] = new int[0];
            for (int i = 0; i < n; i++) {
                tree[i + 1] = new int[cnum[i]];
            }
            for (int i = 0; i < n; i++) {
                if (i + 1 != h) {
                    tree[father[i] + 1][--cnum[father[i]]] = i + 1;
                }
            }
        }

        /**
         * @param u 当前节点
         * @param f u 的父节点
         */
        private void dfs1(int u, int f) {
            // 当前节点的父节点
            fa[u] = f;
            // 当前节点的深度
            dep[u] = dep[f] + 1;
            siz[u] = 1;
            int maxSize = -1;
            // 遍历当前 u 节点的所有直接孩子
            for (int v : tree[u]) {
                dfs1(v, u);
                // 统计孩子节点个数
                siz[u] += siz[v];
                // 找到当前节点权重较大的孩子
                if (siz[v] > maxSize) {
                    maxSize = siz[v];
                    son[u] = v;
                }
            }
        }

        /**
         * 找到每个节点的重链父节点是谁
         *
         * @param u 当前节点
         * @param t u 所在重链儿的头部
         */
        private void dfs2(int u, int t) {
            dfn[u] = ++tim;
            // 重链儿的头是t
            top[u] = t;
            // 权值
            tnw[tim] = val[u];
            // 如果u有儿子 siz[u] > 1
            if (son[u] != 0) {
                dfs2(son[u], t);
                for (int v : tree[u]) {
                    if (v != son[u]) {
                        dfs2(v, v);
                    }
                }
            }
        }

        // head为头的子树上，所有节点值+value
        // 因为节点经过平移，所以head(原始节点) -> head(平移节点)
        public void addSubtree(int head, int value) {
            // 原始点编号 -> 平移编号
            head++;
            // 平移编号 -> dfs编号 dfn[head]
            seg.add(dfn[head], dfn[head] + siz[head] - 1, value, 1, n, 1);
        }

        /**
         * 在 a~b 这个链儿上加v
         */
        public void addChain(int a, int b, int v) {
            a++;
            b++;
            // 如果不相等，表示不在一条链儿上，就去找公共的头节点，直到a和b是在一条链儿上位置
            while (top[a] != top[b]) {
                // 看谁的深度大，大的表示在小的上边儿
                if (dep[top[a]] > dep[top[b]]) {
                    seg.add(dfn[top[a]], dfn[a], v, 1, n, 1);
                    a = fa[top[a]];
                } else {
                    seg.add(dfn[top[b]], dfn[b], v, 1, n, 1);
                    b = fa[top[b]];
                }
            }
            // a和b是在一条链儿上的时候
            if (dep[a] > dep[b]) {
                seg.add(dfn[b], dfn[a], v, 1, n, 1);
            } else {
                seg.add(dfn[a], dfn[b], v, 1, n, 1);
            }
        }

        /**
         * 查询 a~b 范围上的累加和
         */
        public int queryChain(int a, int b) {
            a++;
            b++;
            int ans = 0;
            while (top[a] != top[b]) {
                if (dep[top[a]] > dep[top[b]]) {
                    ans += seg.query(dfn[top[a]], dfn[a], 1, n, 1);
                    a = fa[top[a]];
                } else {
                    ans += seg.query(dfn[top[b]], dfn[b], 1, n, 1);
                    b = fa[top[b]];
                }
            }
            if (dep[a] > dep[b]) {
                ans += seg.query(dfn[b], dfn[a], 1, n, 1);
            } else {
                ans += seg.query(dfn[a], dfn[b], 1, n, 1);
            }
            return ans;
        }
    }

    public static class SegmentTree {
        private int MAXN;
        private int[] arr;
        private int[] sum;
        private int[] lazy;
        public SegmentTree(int[] origin) {
            MAXN = origin.length;
            arr = origin;
            sum = new int[MAXN << 2];
            lazy = new int[MAXN << 2];
        }

        private void pushUp(int rt) {
            sum[rt] = sum[rt << 1] + sum[rt << 1 | 1];
        }

        private void pushDown(int rt, int ln, int rn) {
            if (lazy[rt] != 0) {
                lazy[rt << 1] += lazy[rt];
                sum[rt << 1] += lazy[rt] * ln;
                lazy[rt << 1 | 1] += lazy[rt];
                sum[rt << 1 | 1] += lazy[rt] * rn;
                lazy[rt] = 0;
            }
        }

        public void build(int l, int r, int rt) {
            if (l == r) {
                sum[rt] = arr[l];
                return;
            }
            int mid = (l + r) >> 1;
            build(l, mid, rt << 1);
            build(mid + 1, r, rt << 1 | 1);
            pushUp(rt);
        }

        public void add(int L, int R, int C, int l, int r, int rt) {
            if (L <= l && r <= R) {
                sum[rt] += C * (r - l + 1);
                lazy[rt] += C;
                return;
            }
            int mid = (l + r) >> 1;
            pushDown(rt, mid - l + 1, r - mid);
            if (L <= mid) {
                add(L, R, C, l, mid, rt << 1);
            }
            if (R > mid) {
                add(L, R, C, mid + 1, r, rt << 1 | 1);
            }
            pushUp(rt);
        }

        public int query(int L, int R, int l, int r, int rt) {
            if (L <= l && r <= R) {
                return sum[rt];
            }
            int mid = (l + r) >> 1;
            pushDown(rt, mid - l + 1, r - mid);
            int ans = 0;
            if (L <= mid) {
                ans += query(L, R, l, mid, rt << 1);
            }
            if (R > mid) {
                ans += query(L, R, mid + 1, r, rt << 1 | 1);
            }
            return ans;
        }

    }

}

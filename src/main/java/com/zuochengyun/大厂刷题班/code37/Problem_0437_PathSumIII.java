package com.zuochengyun.大厂刷题班.code37;

import java.util.HashMap;

/**
 * 给定一个二叉树，在给定一个 num
 * 问：树结构从上往下走，有多少个节点累加和等于 num
 *
 * @author 钢人
 */
public class Problem_0437_PathSumIII {

    public class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;
    }

    public static int pathSum(TreeNode root, int sum) {
        HashMap<Integer, Integer> preSumMap = new HashMap<>();
        preSumMap.put(0, 1);
        return process(root, sum, 0, preSumMap);
    }

    // 返回方法数
    public static int process(TreeNode x, int sum, int preAll, HashMap<Integer, Integer> preSumMap) {
        if (x == null) {
            return 0;
        }
        int all = preAll + x.val;
        int ans = 0;
        if (preSumMap.containsKey(all - sum)) {
            ans = preSumMap.get(all - sum);
        }
        if (!preSumMap.containsKey(all)) {
            preSumMap.put(all, 1);
        } else {
            preSumMap.put(all, preSumMap.get(all) + 1);
        }
        ans += process(x.left, sum, all, preSumMap);
        ans += process(x.right, sum, all, preSumMap);
        if (preSumMap.get(all) == 1) {
            preSumMap.remove(all);
        } else {
            preSumMap.put(all, preSumMap.get(all) - 1);
        }
        return ans;
    }


}

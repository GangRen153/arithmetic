package com.zuochengyun.大厂刷题班.code27;

import java.util.Arrays;

/**
 * 企鹅厂每年都会发文化衫，文化衫有很多种，厂庆的时候，企鹅们都需要穿文化衫来拍照
 * 一次采访中，记者随机遇到的企鹅，企鹅会告诉记者还有多少企鹅跟他穿一种文化衫
 * 我们将这些回答放在 answers 数组里，返回鹅厂中企鹅的最少数量。
 * 输入: answers = [1]    输出：2
 * 输入: answers = [1, 1, 2]    输出：5
 *
 * Leetcode题目：<a href="https://leetcode.com/problems/rabbits-in-forest/" />
 * @author 钢人
 */
public class Code02_MinPeople {

    public static int numRabbits(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        // 经过排序之后，说的一样话的人放到一起，按组自己消化
        Arrays.sort(arr);
        int x = arr[0];
        int c = 1;
        int ans = 0;
        for (int i = 1; i < arr.length; i++) {
            if (x != arr[i]) {
                ans += ((c + x) / (x + 1)) * (x + 1);
                x = arr[i];
                c = 1;
            } else {
                c++;
            }
        }
        return ans + ((c + x) / (x + 1)) * (x + 1);
    }

}

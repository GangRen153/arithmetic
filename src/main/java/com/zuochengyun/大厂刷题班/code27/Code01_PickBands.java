package com.zuochengyun.大厂刷题班.code27;

import java.util.Arrays;

/**
 * 给定多个项目 int[][]programs，每个项目都有三个数[a,b,c]，表示项目由 a 乐队 和 b 乐队参演，花费 c
 * 每个乐队可能在多个项目里都出现了，但只能挑一次
 * nums 是可以挑选的项目数量，所以一定会有 nums*2 只乐队被挑选出来
 * 返回一共挑 nums 轮，最少花费
 * <p>
 * 例如：
 * [
 * [9,1,100]
 * [1,4,130]
 * ]
 *
 * @author 钢人
 */
public class Code01_PickBands {

    public static int minCost(int[][] programs, int nums) {
        if (nums == 0 || programs == null || programs.length == 0) {
            return 0;
        }
        // 去掉重复乐队，返回个数
        int size = clean(programs);
        // 可以挑选的项目数量最大乐队数量
        int[] map1 = init(1 << (nums << 1));
        int[] map2;
        // 偶数
        if ((nums & 1) == 0) {
            f(programs, size, 0, 0, 0, nums >> 1, map1);
            map2 = map1;

            // 奇数
        } else {
            // 假如 nums == 7，先执行 4 次
            f(programs, size, 0, 0, 0, nums >> 1, map1);
            map2 = init(1 << (nums << 1));
            // 在执行 7-4 次
            f(programs, size, 0, 0, 0, nums - (nums >> 1), map2);
        }
        int mask = (1 << (nums << 1)) - 1;
        int ans = Integer.MAX_VALUE;
        for (int i = 0; i < map1.length; i++) {
            if (map1[i] != Integer.MAX_VALUE && map2[mask & (~i)] != Integer.MAX_VALUE) {
                ans = Math.min(ans, map1[i] + map2[mask & (~i)]);
            }
        }
        return ans == Integer.MAX_VALUE ? -1 : ans;
    }

    public static void f(int[][] programs, int size, int index, int status, int cost, int rest, int[] map) {
        if (rest == 0) {
            map[status] = Math.min(map[status], cost);
        } else {
            if (index != size) {
                f(programs, size, index + 1, status, cost, rest, map);
                int pick = (1 << programs[index][0]) | (1 << programs[index][1]);
                if ((pick & status) == 0) {
                    f(programs, size, index + 1, status | pick, cost + programs[index][2], rest - 1, map);
                }
            }
        }
    }

    public static int[] init(int size) {
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = Integer.MAX_VALUE;
        }
        return arr;
    }

    public static int clean(int[][] programs) {
        int x = 0;
        int y = 0;
        // 乐队原地排序
        for (int[] p : programs) {
            x = Math.min(p[0], p[1]);
            y = Math.max(p[0], p[1]);
            p[0] = x;
            p[1] = y;
        }

        // 根据乐队排序
        Arrays.sort(programs, (a, b) -> a[0] != b[0] ? (a[0] - b[0]) : (a[1] != b[1] ? (a[1] - b[1]) : (a[2] - b[2])));
        x = programs[0][0];
        y = programs[0][1];
        int n = programs.length;

        for (int i = 1; i < n; i++) {
            // 重复乐队，置null
            if (programs[i][0] == x && programs[i][1] == y) {
                programs[i] = null;
            } else {
                x = programs[i][0];
                y = programs[i][1];
            }
        }
        int size = 1;
        for (int i = 1; i < n; i++) {
            // 不重复的乐队
            if (programs[i] != null) {
                programs[size++] = programs[i];
            }
        }
        return size;
    }

}

package com.zuochengyun.大厂刷题班.code27;

import java.util.HashMap;

/**
 * 给定一个数组，在给定一个 target
 * 两数相加，返回其中一种组合即可
 *
 * @author 钢人
 */
public class Problem_0001_TwoSum {

    public static int[] twoSum(int[] nums, int target) {
        // key 某个之前的数   value 这个数出现的位置
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(target - nums[i])) {
                return new int[] { map.get(target - nums[i]), i };
            }
            map.put(nums[i], i);
        }
        return new int[] { -1, -1 };
    }

}

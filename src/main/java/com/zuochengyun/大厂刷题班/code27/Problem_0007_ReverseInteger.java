package com.zuochengyun.大厂刷题班.code27;

/**
 * 给定一个正数，反转
 * 例如：123 -> 321
 *
 * @author 钢人
 */
public class Problem_0007_ReverseInteger {

    public static int reverse(int x) {
        // 如果是 1 就是负数，反之是正数
        boolean neg = ((x >>> 31) & 1) == 1;
        // 如果是正数，转成负数（为了避免转的时候溢出）
        x = neg ? x : -x;
        int m = Integer.MIN_VALUE / 10;
        int o = Integer.MIN_VALUE % 10;
        int res = 0;
        while (x != 0) {
            // 校验是否溢出
            if (res < m || (res == m && x % 10 < o)) {
                return 0;
            }
            // 每次膜10，能得到最后一位，例如：123%10 能得到3
            res = res * 10 + x % 10;
            x /= 10;
        }
        return neg ? res : Math.abs(res);
    }

}

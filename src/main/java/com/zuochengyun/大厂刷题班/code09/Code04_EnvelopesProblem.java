package com.zuochengyun.大厂刷题班.code09;

import com.sun.xml.internal.messaging.saaj.soap.Envelope;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 俄罗斯套娃问题
 *
 * 给你一个二维整数数组 envelopes ，其中 envelopes[i] = [wi, hi] ，表示第 i 个信封的宽度和高度
 * 当另一个信封的宽度和高度都比这个信封大的时候，这个信封就可以放进另一个信封里
 * 请计算 最多能有多少个
 *
 * 测试链接 : <a href="https://leetcode.com/problems/russian-doll-envelopes/"/>
 * @author 钢人
 */
public class Code04_EnvelopesProblem {

    public static class Envelope {
        public int l;
        public int h;

        public Envelope(int weight, int height) {
            l = weight;
            h = height;
        }
    }

    public static int maxEnvelopes(int[][] matrix) {
        // 长度由小到大排序（长度一样，高度由大到小）
        Envelope[] arr = sort(matrix);
        int[] ends = new int[matrix.length];
        ends[0] = arr[0].h;
        // 统计 l 向右移动了几次
        int right = 0;
        int l, m, r;
        for (int i = 1; i < arr.length; i++) {
            l = 0;
            r = right;
            while (l <= r) {
                m = (l + r) / 2;
                if (arr[i].h > ends[m]) {
                    l = m + 1;
                } else {
                    r = m - 1;
                }
            }
            // right 统计的是 l 下标位置，如果不达标，l 下标不会增加
            right = Math.max(right, l);
            ends[l] = arr[i].h;
        }
        return right + 1;
    }

    public static Envelope[] sort(int[][] matrix) {
        Envelope[] res = new Envelope[matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            res[i] = new Envelope(matrix[i][0], matrix[i][1]);
        }
        // 长度由小到大排序（长度一样，高度由大到小）
        Arrays.sort(res, (o1, o2) -> o1.l != o2.l ? o1.l - o2.l : o2.h - o1.h);
        return res;
    }
}

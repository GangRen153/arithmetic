package com.zuochengyun.大厂刷题班.code09;

/**
 * 最长递增子序列
 *
 * 例如：nums = [10,9,2,5,3,7,101,18] -> [2,3,7,101] 或者 [2,5,7,101]
 * 答案 4
 *
 * 测试链接 : <a href="https://leetcode.com/problems/longest-increasing-subsequence"/>
 * @author 钢人
 */
public class Code03_LIS {

    public static int lengthOfLIS(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int[] ends = new int[arr.length];
        // 用来统计 i 位置之前的数有多少个数比自己小
        ends[0] = arr[0];
        int right = 0;
        int l, m, r;
        int max = 1;
        // 子序列以 i 位置结尾的情况下，尝试找最长递增子序列
        for (int i = 1; i < arr.length; i++) {
            // 从 0 开始找
            l = 0;
            r = right;
            // l ~ r 范围上找
            while (l <= r) {
                m = (l + r) / 2;
                if (arr[i] > ends[m]) {
                    l = m + 1;
                } else {
                    r = m - 1;
                }
            }
            right = Math.max(right, l);
            // 设置 l 位置之前的数有多少个比 i 位置的数小
            ends[l] = arr[i];
            // 统计前面有几个比 i 位置的数小
            max = Math.max(max, l + 1);
        }
        return max;
    }

}

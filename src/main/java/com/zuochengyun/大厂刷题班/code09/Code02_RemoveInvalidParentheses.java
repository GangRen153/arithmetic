package com.zuochengyun.大厂刷题班.code09;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个由括号组成的字符串
 * 给定一个无效的括号组成的字符串
 * 要求删掉最少个括号，保证最后有效，返回所有结果
 *
 * 测试链接 : <a href="https://leetcode.com/problems/remove-invalid-parentheses/"/>
 * @author 钢人
 */
public class Code02_RemoveInvalidParentheses {

    public static List<String> removeInvalidParentheses(String s) {
        List<String> ans = new ArrayList<>();
        remove(s, ans, 0, 0, new char[] { '(', ')' });
        return ans;
    }

    public static void remove(String s, List<String> ans, int checkIndex, int deleteIndex, char[] par) {
        for (int count = 0, i = checkIndex; i < s.length(); i++) {
            // 统计左括号个数
            if (s.charAt(i) == par[0]) {
                count++;
            }
            // 统计右括号个数
            if (s.charAt(i) == par[1]) {
                count--;
            }

            // 如果小于0，说明 str 括号字符第一次出现不合法的位置
            if (count < 0) {
                // 开始尝试删除括号
                for (int j = deleteIndex; j <= i; ++j) {
                    // 当前删除括号是右括号 &&
                    if (s.charAt(j) == par[1] && (j == deleteIndex || s.charAt(j - 1) != par[1])) {
                        remove(s.substring(0, j) + s.substring(j + 1, s.length()), ans, i, j, par);
                    }
                }
                // 终止本地收集
                return;
            }
        }
        // 反转字符串
        String reversed = new StringBuilder(s).reverse().toString();
        if (par[0] == '(') {
            remove(reversed, ans, 0, 0, new char[] { ')', '(' });
        } else {
            ans.add(reversed);
        }
    }

}

package com.zuochengyun.大厂刷题班.code09;

import java.util.HashMap;

/**
 * 步骤和问题
 *
 * 比如680，680 + 68 + 6 = 754，680的step sum叫754
 * 给定一个正数num，判断它是不是某个数的step sum
 *
 * @author 钢人
 */
public class Code05_IsStepSum {

    public static boolean isStepSum(int stepSum) {
        int L = 0;
        int R = stepSum;
        int M;
        int cur;
        while (L <= R) {
            M = L + ((R - L) >> 1);
            cur = stepSum(M);
            if (cur == stepSum) {
                return true;
            } else if (cur < stepSum) {
                L = M + 1;
            } else {
                R = M - 1;
            }
        }
        return false;
    }

    public static int stepSum(int num) {
        int sum = 0;
        while (num != 0) {
            sum += num;
            num /= 10;
        }
        return sum;
    }

}

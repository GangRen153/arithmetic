package com.zuochengyun.大厂刷题班.code09;

/**
 * 给定一个数组arr，长度为N，arr中的值不是0就是1，0代表灭灯，1代表亮灯
 * 每一栈灯都有开关，但是按下i号灯的开关，会同时改变i-1、i、i+2栈灯的状态
 *
 * 问题一：
 * 请问最少按下多少次开关,能让灯都亮起来
 * i为中间位置时，i号灯的开关能影响i-1、i和i+1
 * 0号灯的开关只能影响0和1位置的灯
 * N-1号灯的开关只能影响N-2和N-1位置的灯
 *
 * 问题二：
 * 请问最少按下多少次开关,能让灯都亮起来
 * i为中间位置时，i号灯的开关能影响i-1、i和i+1
 * 0号灯的开关能影响N-1、0和1位置的灯
 * N-1号灯的开关能影响N-2、N-1和0位置的灯
 *
 * @author 钢人
 */
public class Code01_LightProblem {

    /**
     * 无环改灯问题
     */
    public static int noLoopMinStep1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        if (arr.length == 1) {
            return arr[0] ^ 1;
        }
        if (arr.length == 2) {
            return arr[0] != arr[1] ? Integer.MAX_VALUE : (arr[0] ^ 1);
        }
        // 不变0位置的状态
        int p1 = process1(arr, 2, arr[0], arr[1]);
        // 改变0位置的状态
        int p2 = process1(arr, 2, arr[0] ^ 1, arr[1] ^ 1);
        if (p2 != Integer.MAX_VALUE) {
            p2++;
        }
        return Math.min(p1, p2);
    }

    /**
     * @param nextIndex cur + 1
     * @param preStatus cur - 1
     * @param curStatus cur
     */
    public static int process1(int[] arr, int nextIndex, int preStatus, int curStatus) {
        // 当前来到最后一个开关的位置，如果最两个值不一样，说明肯定有一个是关着的
        if (nextIndex == arr.length) {
            return preStatus != curStatus ? (Integer.MAX_VALUE) : (curStatus ^ 1);
        }
        // 上一个灯是关着的，当前开关一定要按，不然上一个灯就永远没办法亮了
        if (preStatus == 0) {
            // 当前灯按下开关
            curStatus ^= 1;
            // 影响大下一个灯
            int cur = arr[nextIndex] ^ 1;
            // 尝试后续开关
            int next = process1(arr, nextIndex + 1, curStatus, cur);
            return next == Integer.MAX_VALUE ? next : (next + 1);

            // 上一个灯是亮的，那么当前开关不能按下，不然永远都亮不了了
        } else {
            return process1(arr, nextIndex + 1, curStatus, arr[nextIndex]);
        }
    }

    /**
     * 无环改灯问题的迭代版本
     */
    public static int noLoopMinStep2(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        if (arr.length == 1) {
            return arr[0] == 1 ? 0 : 1;
        }
        if (arr.length == 2) {
            return arr[0] != arr[1] ? Integer.MAX_VALUE : (arr[0] ^ 1);
        }
        // 不按 0 位置开关情况
        int p1 = traceNoLoop(arr, arr[0], arr[1]);
        // 按 0 位置开关的情况
        int p2 = traceNoLoop(arr, arr[0] ^ 1, arr[1] ^ 1);
        p2 = (p2 == Integer.MAX_VALUE) ? p2 : (p2 + 1);
        return Math.min(p1, p2);
    }

    public static int traceNoLoop(int[] arr, int preStatus, int curStatus) {
        // 从 2 开始
        int i = 2;
        // 按下开关次数
        int op = 0;
        while (i != arr.length) {
            // 如果是灭的，当前位置灯需要按下开关
            if (preStatus == 0) {
                // 按下按钮次数++
                op++;
                // 更新上一盏灯状态，作为下一次循环使用
                preStatus = curStatus ^ 1;
                // 当前位置状态
                curStatus = arr[i++] ^ 1;

                // 上一盏灯本身就是开的，不需要按下开关
            } else {
                // 向后移动
                preStatus = curStatus;
                curStatus = arr[i++];
            }
        }
        // 看灯是否一样
        return (preStatus != curStatus) ? Integer.MAX_VALUE : (op + (curStatus ^ 1));
    }


    /**
     * 有环改灯问题
     */
    public static int loopMinStep1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        if (arr.length == 1) {
            return arr[0] == 1 ? 0 : 1;
        }
        if (arr.length == 2) {
            return arr[0] != arr[1] ? Integer.MAX_VALUE : (arr[0] ^ 1);
        }
        if (arr.length == 3) {
            return (arr[0] != arr[1] || arr[0] != arr[2]) ? Integer.MAX_VALUE : (arr[0] ^ 1);
        }

        // 0不变，1不变
        int p1 = process2(arr, 3, arr[1], arr[2], arr[arr.length - 1], arr[0]);
        // 0改变，1不变
        int p2 = process2(arr, 3, arr[1] ^ 1, arr[2], arr[arr.length - 1] ^ 1, arr[0] ^ 1);
        // 0不变，1改变
        int p3 = process2(arr, 3, arr[1] ^ 1, arr[2] ^ 1, arr[arr.length - 1], arr[0] ^ 1);
        // 0改变，1改变
        int p4 = process2(arr, 3, arr[1], arr[2] ^ 1, arr[arr.length - 1] ^ 1, arr[0]);
        p2 = p2 != Integer.MAX_VALUE ? (p2 + 1) : p2;
        p3 = p3 != Integer.MAX_VALUE ? (p3 + 1) : p3;
        p4 = p4 != Integer.MAX_VALUE ? (p4 + 2) : p4;
        return Math.min(Math.min(p1, p2), Math.min(p3, p4));
    }

    /**
     * @param nextIndex 下一个位置是
     * @param preStatus 前一个位置 nextIndex-2
     * @param curStatus 当前位置 nextIndex-1
     * @param endStatus N-1 位置的状态
     * @param firstStatus 0 位置的状态
     */
    public static int process2(int[] arr,
                               int nextIndex, int preStatus, int curStatus,
                               int endStatus, int firstStatus) {
        // 最后一个按钮
        if (nextIndex == arr.length) {
            return (endStatus != firstStatus || endStatus != preStatus) ? Integer.MAX_VALUE : (endStatus ^ 1);
        }

        int noNextPreStatus = 0;
        int yesNextPreStatus = 0;
        int noNextCurStatus =0;
        int yesNextCurStatus = 0;
        int noEndStatus = 0;
        int yesEndStatus = 0;

        // 当前没来到 N-2 位置
        if(nextIndex < arr.length - 1) {
            noNextPreStatus = curStatus;
            yesNextPreStatus = curStatus ^ 1;
            noNextCurStatus = arr[nextIndex];
            yesNextCurStatus = arr[nextIndex] ^ 1;

            // 当前来到的就是 N-2 位置
        } else if(nextIndex == arr.length - 1) {
            noNextPreStatus = curStatus;
            yesNextPreStatus = curStatus ^ 1;
            noNextCurStatus = endStatus;
            yesNextCurStatus = endStatus ^ 1;
            noEndStatus = endStatus;
            yesEndStatus = endStatus ^ 1;
        }

        // 上一个灯是灭的
        if(preStatus == 0) {
            int next = process2(arr, nextIndex + 1, yesNextPreStatus, yesNextCurStatus,
                    nextIndex == arr.length - 1 ? yesEndStatus : endStatus, firstStatus);
            return next == Integer.MAX_VALUE ? next : (next + 1);

            // 上一个灯是亮的
        } else {
            return process2(arr, nextIndex + 1, noNextPreStatus, noNextCurStatus,
                    nextIndex == arr.length - 1 ? noEndStatus : endStatus, firstStatus);

        }
    }

    /**
     * 有环改灯问题的迭代版本
     */
    public static int loopMinStep2(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        if (arr.length == 1) {
            return arr[0] == 1 ? 0 : 1;
        }
        if (arr.length == 2) {
            return arr[0] != arr[1] ? Integer.MAX_VALUE : (arr[0] ^ 1);
        }
        if (arr.length == 3) {
            return (arr[0] != arr[1] || arr[0] != arr[2]) ? Integer.MAX_VALUE : (arr[0] ^ 1);
        }

        // 0不变，1不变
        int p1 = traceLoop(arr, arr[1], arr[2], arr[arr.length - 1], arr[0]);
        // 0改变，1不变
        int p2 = traceLoop(arr, arr[1] ^ 1, arr[2], arr[arr.length - 1] ^ 1, arr[0] ^ 1);
        // 0不变，1改变
        int p3 = traceLoop(arr, arr[1] ^ 1, arr[2] ^ 1, arr[arr.length - 1], arr[0] ^ 1);
        // 0改变，1改变
        int p4 = traceLoop(arr, arr[1], arr[2] ^ 1, arr[arr.length - 1] ^ 1, arr[0]);
        p2 = p2 != Integer.MAX_VALUE ? (p2 + 1) : p2;
        p3 = p3 != Integer.MAX_VALUE ? (p3 + 1) : p3;
        p4 = p4 != Integer.MAX_VALUE ? (p4 + 2) : p4;
        return Math.min(Math.min(p1, p2), Math.min(p3, p4));
    }

    /**
     * @param preStatus 上一个位置的灯
     * @param curStatus 当前位置的灯
     * @param endStatus N-1 位置的灯
     * @param firstStatus 0 位置的灯
     */
    public static int traceLoop(int[] arr, int preStatus, int curStatus, int endStatus, int firstStatus) {
        int i = 3;
        int op = 0;
        while (i < arr.length - 1) {
            // 上一个位置的灯是灭的，需要按下当前位置的灯
            if (preStatus == 0) {
                op++;
                preStatus = curStatus ^ 1;
                curStatus = (arr[i++] ^ 1);

                // 上一个位置的灯是亮的，不需要按下
            } else {
                preStatus = curStatus;
                curStatus = arr[i++];
            }
        }

        // 处理最后的边界位置
        // N-2 位置的灯是灭的
        if (preStatus == 0) {
            op++;
            preStatus = curStatus ^ 1;
            endStatus ^= 1;
            curStatus = endStatus;

            // N-2 位置的灯是亮的
        } else {
            preStatus = curStatus;
            curStatus = endStatus;
        }
        return (endStatus != firstStatus || endStatus != preStatus) ? Integer.MAX_VALUE : (op + (endStatus ^ 1));
    }

}

package com.zuochengyun.大厂刷题班.code10;

/**
 * 给定一个数组，有一个小人儿，从 0 位置跳到 N-1 位置
 * 小人儿从 0 位置开始跳，小人儿每次能跳的步数范围是 0~数组i位置的数
 * 问，跳到 N-1 位置最少要几次
 *
 * 测试链接 : <a href="https://leetcode.com/problems/jump-game-ii/"/>
 * @author 钢人
 */
public class Code01_JumpGame {

    public static int jump(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int step = 0;
        int cur = 0;
        int next = 0;
        for (int i = 0; i < arr.length; i++) {
            if (cur < i) {
                // step 步内，最远能到 cur 位置
                step++;
                cur = next;
            }
            // next：允许多跳一步的情况下，最远能跳到哪儿
            // 比如 cur 在 6 位置上，6，3，3，2，1，1，1），next（3，3，2，1，1，1） 最远能跳到的位置是最后一个1
            // 所以直接从在 6 位置上跳跳的最远，允许多跳一步的所有情况都没有当前 cur 位置跳的远
            // 主要用来比较和上一次能跳到的最远位置和多条一步的情况下跳到的最远位置
            // 如果多跳一步跳到的最远位置还没 cur 跳的最远位置远，cur 位置直接跳过去就可以了
            next = Math.max(next, i + arr[i]);
        }
        return step;
    }

}

package com.zuochengyun.大厂刷题班.code10;

/**
 * 给定一个 n，表示有 n 个数从 1~n，给定一个 k，表示 1~n 个数任意排序有 k 个逆序对
 * 例如：n = 3，k = 2
 * n = [1,2,3]
 * 1,2,3 -> 1,2是升序，1,3是升序，2,3是升序，所以逆序对是0，不达标
 * 1,3,2 -> 1,3是升序，1,2是升序，3,2是降序，所以逆序对是1，不达标
 * 2,1,3 -> 2,1是降序，2,3是升序，1,3是升序，所以逆序对是1，不达标
 * 2,3,1 -> 2,3是升序，2,1是降序，3,1是降序，所以逆序对是2，达标
 * 3,2,1 -> 3,2是降序，3,1是降序，2,1是降序，所以逆序对是3，不达标
 * 3,1,2 -> 3,1是降序，3,2是降序，1,2是升序，所以逆序对是2，达标
 *
 * 问：有几个排列达标
 *
 * 测试链接 : <a href="https://leetcode.com/problems/k-inverse-pairs-array/"/>
 * @author 钢人
 */
public class Code03_KInversePairs {

    public static int kInversePairs(int n, int k) {
        if (n < 1 || k < 0) {
            return 0;
        }
        int[][] dp = new int[n + 1][k + 1];
        dp[0][0] = 1;
        int mod = 1000000007;
        for (int i = 1; i <= n; i++) {
            dp[i][0] = 1;
            for (int j = 1; j <= k; j++) {
                dp[i][j] = (dp[i][j - 1] + dp[i - 1][j]) % mod;
                if (j >= i) {
                    dp[i][j] = (dp[i][j] - dp[i - 1][j - i] + mod) % mod;
                }
            }
        }
        return dp[n][k];
    }

    public static int kInversePairs2(int n, int k) {
        if (n < 1 || k < 0) {
            return 0;
        }
        int[][] dp = new int[n + 1][k + 1];
        dp[0][0] = 1;
        for (int i = 1; i <= n; i++) {
            dp[i][0] = 1;
            for (int j = 1; j <= k; j++) {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
                if (j >= i) {
                    dp[i][j] -= dp[i - 1][j - i];
                }
            }
        }
        return dp[n][k];
    }

}

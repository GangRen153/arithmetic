package com.zuochengyun.大厂刷题班.code36;

import java.util.HashMap;

/**
 * 把一个 01 字符串切成多个部分，要求每一部分的 0 和 1 比例一样，同时要求尽可能多的划分
 * 比如 : 010100001
 * 要求，0~0 范围上切，0~1 范围上切，0~2 范围上切，0~(N-1)范围上切
 * 求出每种结果的等比例切出来的结果
 * 输入: str = "010100001"
 * 0~0：0
 * 0~1：01
 * 0~2：010
 * 0~3：01、01
 * 0~4：01010
 * 0~5：010、100
 * 0~6：0101000
 * 0~7：01010000
 * 0~8：010、100、001
 * 输出: ans = [1, 1, 1, 2, 1, 2, 1, 1, 3]
 *
 * @author 钢人
 */
public class Code02_Ratio01Split {

    public static int[] split(int[] arr) {

        // key : 分子
        // value : 属于key的分母表, 每一个分母，及其 分子/分母 这个比例，多少个前缀拥有
        HashMap<Integer, HashMap<Integer, Integer>> pre = new HashMap<>();
        int n = arr.length;
        int[] ans = new int[n];
        int zero = 0; // 0出现的次数
        int one = 0; // 1出现的次数
        for (int i = 0; i < n; i++) {
            if (arr[i] == 0) {
                zero++;
            } else {
                one++;
            }
            if (zero == 0 || one == 0) {
                ans[i] = i + 1;
            } else { // 0和1，都有数量 -> 最简分数
                int gcd = gcd(zero, one);
                int a = zero / gcd;
                int b = one / gcd;
                // a / b 比例，之前有多少前缀拥有？ 3+1 4 5+1 6
                if (!pre.containsKey(a)) {
                    pre.put(a, new HashMap<>());
                }
                if (!pre.get(a).containsKey(b)) {
                    pre.get(a).put(b, 1);
                } else {
                    pre.get(a).put(b, pre.get(a).get(b) + 1);
                }
                ans[i] = pre.get(a).get(b);
            }
        }
        return ans;
    }

    public static int gcd(int m, int n) {
        return n == 0 ? m : gcd(n, m % n);
    }

    public static void main(String[] args) {
        int[] arr = { 0, 1, 0, 1, 0, 1, 1, 0 };
        int[] ans = split(arr);
        for (int i = 0; i < ans.length; i++) {
            System.out.print(ans[i] + " ");
        }
    }

}

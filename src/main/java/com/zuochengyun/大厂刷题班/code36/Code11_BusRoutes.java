package com.zuochengyun.大厂刷题班.code36;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 来自三七互娱
 * 公交车最少换乘问题
 *
 * 1-1 1       4-4-4-4-4-x-5-5-5-5
 * |   |       |         |       |
 * 1-x-1-2-2-3-x         4       5
 *   |     |   |         |       |
 *   2-2-x-2   x-4-4-4-4-x-5-5-5-5
 *         |   |
 *         3-3-3
 * 例如：x 点是公交交汇的公交站，从某个站出发到另一个站，最少换乘
 *
 * Leetcode原题 : <a href="https://leetcode.com/problems/bus-routes/" />
 *
 * @author 钢人
 */
public class Code11_BusRoutes {

    public static int numBusesToDestination(int[][] routes, int source, int target) {
        if (source == target) {
            return 0;
        }
        int n = routes.length;
        // key : 车站
        // value : list -> 该车站拥有哪些线路！
        HashMap<Integer, ArrayList<Integer>> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < routes[i].length; j++) {
                if (!map.containsKey(routes[i][j])) {
                    map.put(routes[i][j], new ArrayList<>());
                }
                map.get(routes[i][j]).add(i);
            }
        }
        ArrayList<Integer> queue = new ArrayList<>();
        boolean[] set = new boolean[n];
        for (int route : map.get(source)) {
            queue.add(route);
            set[route] = true;
        }
        int len = 1;
        while (!queue.isEmpty()) {
            ArrayList<Integer> nextLevel = new ArrayList<>();
            for (int route : queue) {
                int[] bus = routes[route];
                for (int station : bus) {
                    if (station == target) {
                        return len;
                    }
                    for (int nextRoute : map.get(station)) {
                        if (!set[nextRoute]) {
                            nextLevel.add(nextRoute);
                            set[nextRoute] = true;
                        }
                    }
                }
            }
            queue = nextLevel;
            len++;
        }
        return -1;
    }

}

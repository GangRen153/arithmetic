package com.zuochengyun.大厂刷题班.code28;

/**
 * 给定一个数组，把不同的数都放到前面表示有效区，相等的放到右侧表示无效区
 * 例如：[0,0,1,2,2,3,4,4]
 * 返回 [0,1,2,3,4,...]
 * 原数组调整
 *
 * 用两个指针
 *
 * @author 钢人
 */
public class Problem_0026_RemoveDuplicatesFromSortedArray {

    public static int removeDuplicates(int[] nums) {
        if (nums == null) {
            return 0;
        }
        if (nums.length < 2) {
            return nums.length;
        }
        int done = 0;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[done]) {
                nums[++done] = nums[i];
            }
        }
        return done + 1;
    }

}

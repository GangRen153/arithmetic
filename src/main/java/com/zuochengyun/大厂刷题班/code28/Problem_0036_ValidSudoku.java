package com.zuochengyun.大厂刷题班.code28;

/**
 * 给定一个 9*9 的二维矩阵，也就是81个格子
 * 每9个格子为一个桶，也就是 3*3，一共有 9 个桶
 *
* 要求每一行 1~9 不重复，每一列 1~9 不重复，每一个桶 1~9 不重复
 * 返回是否有效
 *
 * @author 钢人
 */
public class Problem_0036_ValidSudoku {

    public static boolean isValidSudoku(char[][] board) {
        boolean[][] row = new boolean[9][10];
        boolean[][] col = new boolean[9][10];
        boolean[][] bucket = new boolean[9][10];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                int bid = 3 * (i / 3) + (j / 3);
                if (board[i][j] != '.') {
                    int num = board[i][j] - '0';
                    if (row[i][num] || col[j][num] || bucket[bid][num]) {
                        return false;
                    }
                    row[i][num] = true;
                    col[j][num] = true;
                    bucket[bid][num] = true;
                }
            }
        }
        return true;
    }

}

package com.zuochengyun.大厂刷题班.code28;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个 n，表示由 n 个左括号 n 个右括号
 * 返回所有合法组合
 *
 * @author 钢人
 */
public class Problem_0022_GenerateParentheses {

    public static List<String> generateParenthesis(int n) {
        char[] path = new char[n << 1];
        List<String> ans = new ArrayList<>();
        process(path, 0, 0, n, ans);
        return ans;
    }


    public static void process(char[] path, int index, int leftMinusRight, int leftRest, List<String> ans) {
        if (index == path.length) {
            ans.add(String.valueOf(path));
        } else {
            // index (   )
            if (leftRest > 0) {
                path[index] = '(';
                process(path, index + 1, leftMinusRight + 1, leftRest - 1, ans);
            }
            if (leftMinusRight > 0) {
                path[index] = ')';
                process(path, index + 1, leftMinusRight - 1, leftRest, ans);
            }
        }
    }

}

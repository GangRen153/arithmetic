package com.zuochengyun.大厂刷题班.code28;

/**
 * 整型数字转成罗马数字表示
 *
 * @author 钢人
 */
public class Problem_0012_IntegerToRoman {

    public static String[][] c = {
            // 如果当前字符下一个字符小，当前字符就表示负数，例如 I 和 V，
            {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"},
            // X:10 XC:90
            {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"},
            {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"},
            {"", "M", "MM", "MMM"}
    };

    public static String intToRoman(int num) {
        StringBuilder roman = new StringBuilder();
        roman.append(c[3][num / 1000 % 10])
                .append(c[2][num / 100 % 10])
                .append(c[1][num / 10 % 10])
                .append(c[0][num % 10]);
        return roman.toString();
    }

}

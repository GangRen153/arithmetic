package com.zuochengyun.大厂刷题班.code15;

/**
 * 股票问题 5
 * <p>
 * 每次卖出之后，需要等待一个时刻才能买入，买入之后卖出不需要等待，无限次买入卖出
 * 问：最好收益
 * <p>
 * 测试链接：<a href="https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-cooldown/" />
 *
 * @author 钢人
 */
public class Code05_BestTimeToBuyAndSellStockWithCooldown {

    public static int maxProfit1(int[] prices) {
        return process1(prices, false, 0, 0);
    }

    /**
     * @param prices    股票
     * @param buy       当前是否买入
     * @param index     当前时刻
     * @param buyPrices 买入股票的价钱
     */
    public static int process1(int[] prices, boolean buy, int index, int buyPrices) {
        if (index >= prices.length) {
            return 0;
        }

        // 当前是买入的，可以交易
        if (buy) {
            // 不交易
            int noSell = process1(prices, true, index + 1, buyPrices);
            // 交易
            int yesSell = prices[index] - buyPrices + process1(prices, false, index + 2, 0);
            return Math.max(noSell, yesSell);
        } else {// 当前没有买入股票，不能交易
            // 不买入
            int noBuy = process1(prices, false, index + 1, 0);
            // 买入
            int yesBuy = process1(prices, true, index + 1, prices[index]);
            return Math.max(noBuy, yesBuy);
        }
    }

    public static int maxProfit3(int[] prices) {
        if (prices.length < 2) {
            return 0;
        }
        int buy1 = Math.max(-prices[0], -prices[1]);
        int sell1 = Math.max(0, prices[1] - prices[0]);
        int sell2 = 0;
        for (int i = 2; i < prices.length; i++) {
            int tmp = sell1;
            sell1 = Math.max(sell1, buy1 + prices[i]);
            buy1 = Math.max(buy1, sell2 - prices[i]);
            sell2 = tmp;
        }
        return sell1;
    }

}

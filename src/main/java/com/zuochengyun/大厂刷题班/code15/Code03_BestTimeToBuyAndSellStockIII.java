package com.zuochengyun.大厂刷题班.code15;

/**
 * 股票问题 3
 *
 * 股票每次交易只能买一支股票，卖出之后才能再次买入，但是最多只能买入 2 次股票
 *
 * 测试链接：<a href="https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/" />
 * @author 钢人
 */
public class Code03_BestTimeToBuyAndSellStockIII {

    public static int maxProfit(int[] prices) {
        if (prices == null || prices.length < 2) {
            return 0;
        }
        int ans = 0;
        int doneOnceMinusBuyMax = -prices[0];
        int doneOnceMax = 0;
        int min = prices[0];
        for (int i = 1; i < prices.length; i++) {
            min = Math.min(min, prices[i]);
            ans = Math.max(ans, doneOnceMinusBuyMax + prices[i]);
            doneOnceMax = Math.max(doneOnceMax, prices[i] - min);
            doneOnceMinusBuyMax = Math.max(doneOnceMinusBuyMax, doneOnceMax - prices[i]);
        }
        return ans;
    }

}

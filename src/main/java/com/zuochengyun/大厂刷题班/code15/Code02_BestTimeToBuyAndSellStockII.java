package com.zuochengyun.大厂刷题班.code15;

/**
 * 股票问题 2
 * <p>
 * 在卖出股票之前手里最多只能有一支股票，
 * 有卖出之后才能再次买入，可以左无限次交易
 * 问：最多能赚多少钱
 * <p>
 * 测试链接：<a href="https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/" />
 *
 * @author 钢人
 */
public class Code02_BestTimeToBuyAndSellStockII {

    /**
     * 每一个波峰 + 波谷的收益
     */
    public static int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }

        int ans = 0;
        for (int i = 1; i < prices.length; i++) {
            // 当前时刻股票价格 如果比 上一刻股票的价格高，i 时刻卖出，赚取 i~(i-1) 时刻的差价
            ans += Math.max(prices[i] - prices[i-1], 0);
        }
        return ans;
    }

}

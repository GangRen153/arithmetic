package com.zuochengyun.大厂刷题班.code15;

/**
 * 股票问题 6
 *
 * 每次卖出之后，需要等待一个时刻才能买入，买入之后卖出不需要等待
 * 无限次买入卖出，但是每次卖出的时候要扣除手续费
 * 问：最好收益
 *
 * 测试链接：<a href="https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/" />
 * @author 钢人
 */
public class Code06_BestTimeToBuyAndSellStockWithTransactionFee {

    public static int maxProfit(int[] arr, int fee) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        int N = arr.length;
        // 0..0   0 -[0] - fee
        int bestbuy = -arr[0] - fee;
        // 0..0  卖  0
        int bestsell = 0;
        for (int i = 1; i < N; i++) {
            // 来到i位置了！
            // 如果在i必须买  收入 - 批发价 - fee
            int curbuy = bestsell - arr[i] - fee;
            // 如果在i必须卖  整体最优（收入 - 良好批发价 - fee）
            int cursell = bestbuy + arr[i];
            bestbuy = Math.max(bestbuy, curbuy);
            bestsell = Math.max(bestsell, cursell);
        }
        return bestsell;
    }

}

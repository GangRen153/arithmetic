package com.zuochengyun.大厂刷题班.code15;

/**
 * 股票问题 1
 *
 * 给定一个数组，数组下标表示时刻，数组中的值表示股票的价格
 * 同一时刻可以买入同时卖出，假设只能买卖一次
 * 问：最大收益是多少
 *
 * 测试链接：<a href="https://leetcode.com/problems/best-time-to-buy-and-sell-stock/" />
 * @author 钢人
 */
public class Code01_BestTimeToBuyAndSellStock {

	public static int maxProfit(int[] prices) {
		if (prices == null || prices.length == 0) {
			return 0;
		}
		// 在 0 号时刻卖掉
		int ans = 0;
		int min = prices[0];
		for (int i = 1; i < prices.length; i++) {
			// 找到 0~i 范围上最低价的股票
			min = Math.min(min, prices[i]);
			// 找出每次 i 时刻卖出的最大收益
			ans = Math.max(ans, prices[i] - min);
		}
		return ans;
	}

}

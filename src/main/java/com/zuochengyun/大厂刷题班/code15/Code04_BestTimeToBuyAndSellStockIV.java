package com.zuochengyun.大厂刷题班.code15;

/**
 * 股票问题 4
 *
 * 股票每次交易只能买一支股票，卖出之后才能再次买入，但是最多只能买入 K 次股票
 *
 * 测试链接：<a href="https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iv/" />
 * @author 钢人
 */
public class Code04_BestTimeToBuyAndSellStockIV {

    public static int maxProfit2(int K, int[] arr) {
        if (arr == null || arr.length == 0 || K < 1) {
            return 0;
        }
        int N = arr.length;
        if (K >= N / 2) {
            return allTrans(arr);
        }

        int[][] dp = new int[N][K + 1];

        // 第 0 时刻上卖出，收益肯定是 0
        for (int j = 1; j <= K; j++) {
            int p1 = dp[0][j];
            int best = Math.max(dp[1][j - 1] - arr[1], dp[0][j - 1] - arr[0]);
            dp[1][j] = Math.max(p1, best + arr[1]);
            // dp[1][j] 准备好一些枚举，接下来准备好的枚举
            for (int i = 2; i < N; i++) {
                p1 = dp[i - 1][j];
                int newP = dp[i][j - 1] - arr[i];
                best = Math.max(newP, best);
                dp[i][j] = Math.max(p1, best + arr[i]);
            }
        }
        return dp[N - 1][K];
    }

    public static int allTrans(int[] prices) {
        int ans = 0;
        for (int i = 1; i < prices.length; i++) {
            ans += Math.max(prices[i] - prices[i - 1], 0);
        }
        return ans;
    }

}

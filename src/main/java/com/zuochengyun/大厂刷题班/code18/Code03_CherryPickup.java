package com.zuochengyun.大厂刷题班.code18;

/**
 * 给定一个二维矩阵，不是0就是1，1 代表有桃，0 代表无桃
 * 有一个小人从 0,0 位置开始走，只能向右向下走，走到 N,N 位置
 * 再回到 0,0 位置，只能向左向上走
 * 问：最大捡桃数量
 *
 * 思路：
 * 假设有两个小人儿都在 0,0 位置，同时向右或向下往 N,N 位置走
 * 把两个人走过的路捡的桃累加就是最大捡桃数
 *
 * <a href="https://www.nowcoder.com/questionTerminal/8ecfe02124674e908b2aae65aad4efdf" />
 * @author 钢人
 */
public class Code03_CherryPickup {

    public static int cherryPickup(int[][] grid) {
        // TODO
        return 0;
    }

}

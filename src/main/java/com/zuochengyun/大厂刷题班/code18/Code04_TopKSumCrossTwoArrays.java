package com.zuochengyun.大厂刷题班.code18;

import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;

/**
 * 给定两个有序数组 arr1，arr2
 * 每次从 arr1 和 arr2 中分别拿一个数组成累加和
 * arr1 和 arr2 组成的累加和有 arr1.length * arr2.length 种情况
 * 问：在这种组成最大的和，前 topK 个大的数
 *
 * <a href="https://www.nowcoder.com/practice/7201cacf73e7495aa5f88b223bbbf6d1" />
 * @author 钢人
 */
public class Code04_TopKSumCrossTwoArrays {

    public static class Node {
        public int index1;// arr1中的位置
        public int index2;// arr2中的位置
        public int sum;// arr1[index1] + arr2[index2]的值

        public Node(int i1, int i2, int s) {
            index1 = i1;
            index2 = i2;
            sum = s;
        }
    }

    public static int[] topKSum(int[] arr1, int[] arr2, int topK) {
        if (arr1 == null || arr2 == null || topK < 1) {
            return null;
        }
        int N = arr1.length;
        int M = arr2.length;

        // 如果 kopK 比所有组合结果还大，直接设置成 N*M
        topK = Math.min(topK, N * M);
        int[] res = new int[topK];
        int resIndex = 0;

        PriorityQueue<Node> maxHeap = new PriorityQueue<>(Comparator.comparingInt(o -> o.sum));
        HashSet<Long> set = new HashSet<>();

        int i1 = N - 1;
        int i2 = M - 1;
        // 把 N,M 位置两个数租中最大的数先加到大根堆中
        maxHeap.add(new Node(i1, i2, arr1[i1] + arr2[i2]));
        // i1,i2的位置算出一个一维坐标，表示算过了
        set.add(x(i1, i2, M));

        // 收集 topK 个数
        while (resIndex != topK) {
            Node curNode = maxHeap.poll();
            res[resIndex++] = curNode.sum;
            // 拿到上一个最大累加和的下标
            i1 = curNode.index1;
            i2 = curNode.index2;
            // 移除掉
            set.remove(x(i1, i2, M));
            // i1-1 在没有越界的情况下，向左移动一位，和 i2 组合累加和
            if (i1 - 1 >= 0 && !set.contains(x(i1 - 1, i2, M))) {
                set.add(x(i1 - 1, i2, M));
                maxHeap.add(new Node(i1 - 1, i2, arr1[i1 - 1] + arr2[i2]));
            }
            // i2-1 在没有越界的情况下，向左移动一位，和 i1 组合累加和
            if (i2 - 1 >= 0 && !set.contains(x(i1, i2 - 1, M))) {
                set.add(x(i1, i2 - 1, M));
                maxHeap.add(new Node(i1, i2 - 1, arr1[i1] + arr2[i2 - 1]));
            }
        }
        return res;
    }

    public static long x(int i1, int i2, int M) {
        return (long) i1 * (long) M + (long) i2;
    }

}

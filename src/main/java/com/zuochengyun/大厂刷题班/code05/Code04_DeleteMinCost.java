package com.zuochengyun.大厂刷题班.code05;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * 给定两个字符串s1和s2，问s2最少删除多少字符可以成为s1的子串
 * 比如 s1 = "abcde"，s2 = "axbc"
 *
 * @author 钢人
 */
public class Code04_DeleteMinCost {

    public static int minCost1(String s1, String s2) {
        List<String> s2Subs = new ArrayList<>();
        // 生成所有子序列
        process(s2.toCharArray(), 0, "", s2Subs);
        s2Subs.sort((o1, o2) -> o2.length() - o1.length());
        for (String str : s2Subs) {
            // indexOf底层和KMP算法代价几乎一样，也可以用KMP代替
            if (s1.contains(str)) {
                return s2.length() - str.length();
            }
        }
        return s2.length();
    }

    public static void process(char[] str2, int index, String path, List<String> list) {
        if (index == str2.length) {
            list.add(path);
            return;
        }
        process(str2, index + 1, path, list);
        process(str2, index + 1, path + str2[index], list);
    }

    public static int minCost2(String s1, String s2) {
        // TODO
        return 0;
    }

}

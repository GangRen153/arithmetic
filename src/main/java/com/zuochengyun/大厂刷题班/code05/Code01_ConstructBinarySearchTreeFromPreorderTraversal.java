package com.zuochengyun.大厂刷题班.code05;

import java.util.Stack;

/**
 * 给定一个搜索二叉树先序遍历结果
 * 要求还原成搜索二叉树，返回头节点
 *
 * 测试链接 : h<a href="ttps://leetcode.com/problems/construct-binary-search-tree-from-preorder-traversal/"/>
 * @author 钢人
 */
public class Code01_ConstructBinarySearchTreeFromPreorderTraversal {

    public static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode() {
        }

        public TreeNode(int val) {
            this.val = val;
        }

        public TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public static TreeNode bstFromPreorder1(int[] pre) {
        if (pre == null || pre.length == 0) {
            return null;
        }
        return process1(pre, 0, pre.length - 1);
    }

    public static TreeNode process1(int[] pre, int L, int R) {
        if (L > R) {
            return null;
        }

        // 找到比 L 大的节点的右节点数组下标位置
        int firstBig = L + 1;
        for (; firstBig <= R; firstBig++) {
            if (pre[firstBig] > pre[L]) {
                break;
            }
        }
        // 创建当前 L 节点
        TreeNode head = new TreeNode(pre[L]);
        // 找到当前 L 节点的左节点
        head.left = process1(pre, L + 1, firstBig - 1);
        // 找到当前 L 节点的右节点
        head.right = process1(pre, firstBig, R);
        return head;
    }

}

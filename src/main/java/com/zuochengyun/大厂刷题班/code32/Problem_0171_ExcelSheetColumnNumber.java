package com.zuochengyun.大厂刷题班.code32;

/**
 * 给定一个字符
 *
 * A代表1
 * B代表2
 * C代表3
 * Z代表16
 *
 * AA代表27
 * AB代表28
 * ...
 *
 * 给定一个字符串，转成数字
 *
 * @author 钢人
 */
public class Problem_0171_ExcelSheetColumnNumber {

    public static int titleToNumber(String s) {
        char[] str = s.toCharArray();
        int ans = 0;
        for (int i = 0; i < str.length; i++) {
            ans = ans * 26 + (str[i] - 'A') + 1;
        }
        return ans;
    }

}

package com.zuochengyun.大厂刷题班.code32;

import java.util.HashSet;
import java.util.TreeSet;

/**
 * 判断一个数是否的快乐数
 *
 * 例如：14
 * 1²+4²=17 -> 1²+7²=50 -> 5²+0²=25 -> 2²+5² ... 最终能化成1的就是快乐数
 *
 * @author 钢人
 */
public class Problem_0202_HappyNumber {

    public static boolean isHappy1(int n) {
        HashSet<Integer> set = new HashSet<>();
        while (n != 1) {
            int sum = 0;
            while (n != 0) {
                int r = n % 10;
                sum += r * r;
                n /= 10;
            }
            n = sum;
            if (set.contains(n)) {
                break;
            }
            set.add(n);
        }
        return n == 1;
    }

    // 实验代码
    public static TreeSet<Integer> sum(int n) {
        TreeSet<Integer> set = new TreeSet<>();
        while (!set.contains(n)) {
            set.add(n);
            int sum = 0;
            while (n != 0) {
                sum += (n % 10) * (n % 10);
                n /= 10;
            }
            n = sum;
        }
        return set;
    }

    public static boolean isHappy2(int n) {
        while (n != 1 && n != 4) {
            int sum = 0;
            while (n != 0) {
                sum += (n % 10) * (n % 10);
                n /= 10;
            }
            n = sum;
        }
        return n == 1;
    }

    public static void main(String[] args) {
        for (int i = 1; i < 1000; i++) {
            System.out.println(sum(i));
        }
    }

}

package com.zuochengyun.大厂刷题班.code32;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个有序数组，在给定一个范围
 * 问在这个范围上缺哪些数并返回
 *
 * 例如：
 * [1,2,4,6,9] 1~9
 * 返回：["3","5","7->8"]
 *
 * @author 钢人
 */
public class Problem_0163_MissingRanges {

    public static List<String> findMissingRanges(int[] nums, int lower, int upper) {
        List<String> ans = new ArrayList<>();
        for (int num : nums) {
            if (num > lower) {
                ans.add(miss(lower, num - 1));
            }
            if (num == upper) {
                return ans;
            }
            lower = num + 1;
        }
        if (lower <= upper) {
            ans.add(miss(lower, upper));
        }
        return ans;
    }

    // 生成"lower->upper"的字符串，如果lower==upper，只用生成"lower"
    public static String miss(int lower, int upper) {
        String left = String.valueOf(lower);
        String right = "";
        if (upper > lower) {
            right = "->" + String.valueOf(upper);
        }
        return left + right;
    }

}

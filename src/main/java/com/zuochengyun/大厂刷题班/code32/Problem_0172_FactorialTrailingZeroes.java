package com.zuochengyun.大厂刷题班.code32;

/**
 * n 的阶层乘出来的结果末尾有几个 0
 * 例如：
 * 1*2*3*4*5*6*...*n
 *
 * @author 钢人
 */
public class Problem_0172_FactorialTrailingZeroes {

    public static int trailingZeroes(int n) {
        int ans = 0;
        while (n != 0) {
            // 计算因子
            n /= 5;
            ans += n;
        }
        return ans;
    }

}

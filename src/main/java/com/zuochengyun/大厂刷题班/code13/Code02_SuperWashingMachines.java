package com.zuochengyun.大厂刷题班.code13;

/**
 * 超级洗衣机问题
 *
 * 给定一个数组，表示洗衣机，每个位置的数表示洗衣机中的衣服数量
 * 刚开始每台洗衣机内的衣服是不相等的，要求让每台洗衣机的衣服一样
 * 但是每台洗衣机的衣服只能往左边的洗衣机扔或者往右边的洗衣机扔一件衣服
 * 问：至少扔几次能让所有洗衣机平均
 *
 * 测试链接 : <a href="https://leetcode.com/problems/super-washing-machines/" />
 * @author 钢人
 */
public class Code02_SuperWashingMachines {

    public static int findMinMoves(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int size = arr.length;
        int sum = 0;
        for (int i = 0; i < size; i++) {
            sum += arr[i];
        }
        if (sum % size != 0) {
            return -1;
        }

        // 平均每台洗衣机的数量
        int avg = sum / size;
        int leftSum = 0;
        int ans = 0;

        // 以 i 位置洗衣机位中心
        // 求 i 位置洗衣机左侧总体是需要补多少件衣服能平衡，右侧总体要补多少件衣服能平衡
        for (int i = 0; i < arr.length; i++) {
            int leftRest = leftSum - i * avg;
            int rightRest = (sum - leftSum - arr[i]) - (size - i - 1) * avg;
            if (leftRest < 0 && rightRest < 0) {
                ans = Math.max(ans, Math.abs(leftRest) + Math.abs(rightRest));
            } else {
                ans = Math.max(ans, Math.max(Math.abs(leftRest), Math.abs(rightRest)));
            }
            leftSum += arr[i];
        }
        return ans;
    }

}

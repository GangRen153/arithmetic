package com.zuochengyun.大厂刷题班.code13;

/**
 * 给定一个1~N范围的牌，每次从这个范围上等概率抽牌
 * 每次抽到牌之后分数累加，并且每次抽完之后会换新的牌
 * 在给定一个范围表示如果抽牌的累加和在这个范围上表示赢
 * 问：赢的概率是多少
 *
 * 例如：1~10 张牌，获胜的总分范围是17~21
 *
 * @author 钢人
 */
public class Code01_NCardsABWin {

    public static double f2(int N, int a, int b) {
        if (N < 1 || a >= b || a < 0 || b < 0) {
            return 0.0;
        }
        if (b - a >= N) {
            return 1.0;
        }
        // 所有参数都合法，并且b-a < N
        return p2(0, N, a, b);
    }

    public static double p2(int cur, int N, int a, int b) {
        if (cur >= a && cur < b) {
            return 1.0;
        }
        if (cur >= b) {
            return 0.0;
        }
        double w = 0.0;
        for (int i = 1; i <= N; i++) {
            w += p2(cur + i, N, a, b);
        }
        return w / N;
    }

    public static double p3(int cur, int N, int a, int b) {
        // TODO
        return 0.0;
    }

}

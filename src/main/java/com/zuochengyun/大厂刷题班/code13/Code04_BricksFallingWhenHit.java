package com.zuochengyun.大厂刷题班.code13;

/**
 * 消消游戏乐逻辑
 *
 * 给定一个矩形二维数组不是0就是1
 *
 * 1 表示砖头，0表示空心位置，什么都没有
 * 二维数组 0 行在上层是天花板，0 行的 1 砖头是黏在天花板上的
 * 每个砖头只有上下左右有黏性，1 能连在一起的表示一个连着的
 * 如果中间有某个砖头碎了下面没黏着的砖头会掉下来
 *
 * 在给定一个 N*2 的二维数组，里面是数表示这个二维矩阵的坐标
 * 表示炮弹打中的位置，炮弹会按数组的顺序打墙上的砖头
 * 打中的砖头会碎裂，碎掉的砖头下面的砖头没有黏着的砖头会掉下来
 * 问：炮弹每次打中墙面之后，掉下来的砖头数量，用数组返回每次掉下来的数量
 *
 * 测试链接 : <a href="https://leetcode.com/problems/bricks-falling-when-hit/" />
 * @author 钢人
 */
public class Code04_BricksFallingWhenHit {

    public static int[] hitBricks(int[][] grid, int[][] hits) {
         // TODO
        return null;
    }

    /**
     * 并查集
     */
    public static class UnionFind {

    }

}

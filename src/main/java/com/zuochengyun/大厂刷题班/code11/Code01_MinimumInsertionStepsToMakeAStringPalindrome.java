package com.zuochengyun.大厂刷题班.code11;

import java.util.ArrayList;
import java.util.List;

/**
 * 问题一：一个字符串至少需要添加多少个字符能整体变成回文串
 * 问题二：返回问题一的其中一种添加结果
 * 问题三：返回问题一的所有添加结果
 * <p>
 * 测试链接 : h<a href="https://leetcode.com/problems/minimum-insertion-steps-to-make-a-string-palindrome/" />
 *
 * @author 钢人
 */
public class Code01_MinimumInsertionStepsToMakeAStringPalindrome {

    public static int minInsertions(String s) {
        if (s == null || s.length() < 2) {
            return 0;
        }

        char[] str = s.toCharArray();
        int N = str.length;
        int[][] dp = new int[N][N];

        // 斜对角上只有一个字符的时候自己就是回文，不需要添加字符
        // 两个字符的时候看两个字符是不是一样，一样的情况下本身就是回文，不一样的情况下，添加一个字符就是回文
        for (int i = 0; i < N - 1; i++) {
            dp[i][i + 1] = str[i] == str[i + 1] ? 0 : 1;
        }

        // 普遍位置依赖，以最后一个字符结尾尝试添加字符
        // 从右下往右上遍历
        // 思路：将原始或者添加后的字符可以组成是回文字符，将其互相抵消，在移动指针
        for (int i = N - 3; i >= 0; i--) {
            for (int j = i + 2; j < N; j++) {
                dp[i][j] = Math.min(dp[i][j - 1], dp[i + 1][j]) + 1;
                if (str[i] == str[j]) {
                    dp[i][j] = Math.min(dp[i][j], dp[i + 1][j - 1]);
                }
            }
        }
        // 返回第一行的最后一个位置
        return dp[0][N - 1];
    }

    /**
     * 第二问
     */
    public static String minInsertionsOneWay(String s) {
        // 求添加最少字符 dp 表
        if (s == null || s.length() < 2) {
            return s;
        }
        char[] str = s.toCharArray();
        int N = str.length;
        int[][] dp = new int[N][N];
        for (int i = 0; i < N - 1; i++) {
            dp[i][i + 1] = str[i] == str[i + 1] ? 0 : 1;
        }
        for (int i = N - 3; i >= 0; i--) {
            for (int j = i + 2; j < N; j++) {
                dp[i][j] = Math.min(dp[i][j - 1], dp[i + 1][j]) + 1;
                if (str[i] == str[j]) {
                    dp[i][j] = Math.min(dp[i][j], dp[i + 1][j - 1]);
                }
            }
        }

        // 开始还原 dp 过程，得出添加最少个字符的结果
        int L = 0;
        int R = N - 1;
        // 添加最少字符回文字符串长度 = 原始字符 + dp[L][R]计算出来的添加最少字符个数
        char[] ans = new char[N + dp[L][R]];
        // 控制 ans 数组填的位置
        int ansl = 0;
        int ansr = ans.length - 1;

        // 匹配整个范围上的字符
        while (L < R) {
            if (dp[L][R - 1] == dp[L][R] - 1) {
                ans[ansl++] = str[R];
                ans[ansr--] = str[R--];
            } else if (dp[L + 1][R] == dp[L][R] - 1) {
                ans[ansl++] = str[L];
                ans[ansr--] = str[L++];
            } else {
                ans[ansl++] = str[L++];
                ans[ansr--] = str[R--];
            }
        }
        return String.valueOf(ans);
    }

    /**
     * 本题第三问，返回所有可能的结果
     */
    public static List<String> minInsertionsAllWays(String s) {
        List<String> ans = new ArrayList<>();
        if (s == null || s.length() < 2) {
            ans.add(s);
        } else {
            char[] str = s.toCharArray();
            int N = str.length;
            int[][] dp = new int[N][N];
            for (int i = 0; i < N - 1; i++) {
                dp[i][i + 1] = str[i] == str[i + 1] ? 0 : 1;
            }
            for (int i = N - 3; i >= 0; i--) {
                for (int j = i + 2; j < N; j++) {
                    dp[i][j] = Math.min(dp[i][j - 1], dp[i + 1][j]) + 1;
                    if (str[i] == str[j]) {
                        dp[i][j] = Math.min(dp[i][j], dp[i + 1][j - 1]);
                    }
                }
            }
            int M = N + dp[0][N - 1];
            char[] path = new char[M];
            process(str, dp, 0, N - 1, path, 0, M - 1, ans);
        }
        return ans;
    }

    public static void process(char[] str, int[][] dp, int L, int R, char[] path, int pl, int pr, List<String> ans) {
        if (L >= R) {
            if (L == R) {
                path[pl] = str[L];
            }
            ans.add(String.valueOf(path));
        } else {
            if (dp[L][R - 1] == dp[L][R] - 1) {
                path[pl] = str[R];
                path[pr] = str[R];
                process(str, dp, L, R - 1, path, pl + 1, pr - 1, ans);
            }
            if (dp[L + 1][R] == dp[L][R] - 1) {
                path[pl] = str[L];
                path[pr] = str[L];
                process(str, dp, L + 1, R, path, pl + 1, pr - 1, ans);
            }
            if (str[L] == str[R] && (L == R - 1 || dp[L + 1][R - 1] == dp[L][R])) {
                path[pl] = str[L];
                path[pr] = str[R];
                process(str, dp, L + 1, R - 1, path, pl + 1, pr - 1, ans);
            }
        }
    }

}

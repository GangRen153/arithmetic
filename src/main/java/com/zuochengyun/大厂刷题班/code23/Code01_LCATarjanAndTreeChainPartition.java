package com.zuochengyun.大厂刷题班.code23;

import java.util.HashSet;

/**
 * 树链儿刨分
 *
 * 给定一个数组，下标位和下标位上的数表示树节点的描述
 * 例如：
 * [5,3,2,4,1]
 * 0位置上的5 表示0的父节点是5
 * 1位置上的3 表示1的父节点是3
 * ...
 * 给定这样一个数组结构
 * 要求可以在这个数组上查询，或者在范围上全部加某个数，减某个数等
 *
 * 树链儿刨分或者并查集都可以解决
 *
 * @author 钢人
 */
public class Code01_LCATarjanAndTreeChainPartition {

    // 给定数组tree大小为N，表示一共有N个节点
    // tree[i] = j 表示点i的父亲是点j，tree一定是一棵树而不是森林
    // queries是二维数组，大小为M*2，每一个长度为2的数组都表示一条查询
    // [4,9], 表示想查询4和9之间的最低公共祖先
    // [3,7], 表示想查询3和7之间的最低公共祖先
    // tree和queries里面的所有值，都一定在0~N-1之间
    // 返回一个数组ans，大小为M，ans[i]表示第i条查询的答案

    // 暴力方法
    public static int[] query1(int[] father, int[][] queries) {
        int M = queries.length;
        int[] ans = new int[M];
        HashSet<Integer> path = new HashSet<>();
        for (int i = 0; i < M; i++) {
            int jump = queries[i][0];
            while (father[jump] != jump) {
                path.add(jump);
                jump = father[jump];
            }
            path.add(jump);
            jump = queries[i][1];
            while (!path.contains(jump)) {
                jump = father[jump];
            }
            ans[i] = jump;
            path.clear();
        }
        return ans;
    }

    public static int[] query2(int[] father, int[][] queries) {
        // TODO
        return null;
    }

    public static class TreeChain {
        private int n;
        private int h;
        private int[][] tree;
        private int[] fa;
        private int[] dep;
        private int[] son;
        private int[] siz;
        private int[] top;

        public TreeChain(int[] father) {
            initTree(father);
            dfs1(h, 0);
            dfs2(h, h);
        }

        private void initTree(int[] father) {
            n = father.length + 1;
            tree = new int[n][];
            fa = new int[n];
            dep = new int[n];
            son = new int[n];
            siz = new int[n];
            top = new int[n--];
            int[] cnum = new int[n];
            for (int i = 0; i < n; i++) {
                if (father[i] == i) {
                    h = i + 1;
                } else {
                    cnum[father[i]]++;
                }
            }
            tree[0] = new int[0];
            for (int i = 0; i < n; i++) {
                tree[i + 1] = new int[cnum[i]];
            }
            for (int i = 0; i < n; i++) {
                if (i + 1 != h) {
                    tree[father[i] + 1][--cnum[father[i]]] = i + 1;
                }
            }
        }

        private void dfs1(int u, int f) {
            fa[u] = f;
            dep[u] = dep[f] + 1;
            siz[u] = 1;
            int maxSize = -1;
            for (int v : tree[u]) {
                dfs1(v, u);
                siz[u] += siz[v];
                if (siz[v] > maxSize) {
                    maxSize = siz[v];
                    son[u] = v;
                }
            }
        }

        private void dfs2(int u, int t) {
            top[u] = t;
            if (son[u] != 0) {
                dfs2(son[u], t);
                for (int v : tree[u]) {
                    if (v != son[u]) {
                        dfs2(v, v);
                    }
                }
            }
        }

        public int lca(int a, int b) {
            a++;
            b++;
            while (top[a] != top[b]) {
                if (dep[top[a]] > dep[top[b]]) {
                    a = fa[top[a]];
                } else {
                    b = fa[top[b]];
                }
            }
            return (dep[a] < dep[b] ? a : b) - 1;
        }
    }
}

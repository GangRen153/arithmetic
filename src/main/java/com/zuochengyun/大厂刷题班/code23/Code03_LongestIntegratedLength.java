package com.zuochengyun.大厂刷题班.code23;

import java.util.HashSet;

/**
 * 给定一个数组，返回最长可整合子数组
 * 可整合表示，经过排序之后，数组中的数是连续的就是可整合的，重复数不可整合
 * 例如：
 * [2,5,1,3,4] 是可整合的
 * [2,5,1,2,3,4] 不可整合的
 * [2,5,1,8,3,4] 不可整合的
 *
 * @author 钢人
 */
public class Code03_LongestIntegratedLength {

    public static int maxLen(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int N = arr.length;
        // 收集范围上的子数组数
        HashSet<Integer> set = new HashSet<>();
        int ans = 1;
        for (int L = 0; L < N; L++) {
            // 每次尝试 L~R 范围要清空上一次的统计
            set.clear();
            int min = arr[L];
            int max = arr[L];
            set.add(arr[L]);
            // L..R
            for (int R = L + 1; R < N; R++) {
                // 如果出现了重复值，那么就不是可整合的，直接跳出
                if(set.contains(arr[R])) {
                    break;
                }
                set.add(arr[R]);
                // 拿到最小值
                min = Math.min(min, arr[R]);
                // 拿到最大值
                max = Math.max(max, arr[R]);
                // 如果L~R范围的个数刚好等于max-min的差值，就说明是可整合的，更新最大长度
                if(max - min == R - L) {
                    ans = Math.max(ans, R - L + 1);
                }
            }
        }
        return ans;
    }

}

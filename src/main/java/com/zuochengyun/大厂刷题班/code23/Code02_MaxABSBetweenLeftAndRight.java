package com.zuochengyun.大厂刷题班.code23;

/**
 * 给定一个数组，长度一定大于1
 * 从中间任意位置切一刀，切成左右两部分，保证左右都有数
 * 返回（左部分-右部分）最大绝对值
 *
 * @author 钢人
 */
public class Code02_MaxABSBetweenLeftAndRight {

    public static int maxABS2(int[] arr) {
        // 准备两个辅助数组
        int[] lArr = new int[arr.length];
        int[] rArr = new int[arr.length];
        lArr[0] = arr[0];
        rArr[arr.length - 1] = arr[arr.length - 1];
        // 收集左侧最大值数组
        for (int i = 1; i < arr.length; i++) {
            lArr[i] = Math.max(lArr[i - 1], arr[i]);
        }
        // 收集右侧最大值数组
        for (int i = arr.length - 2; i > -1; i--) {
            rArr[i] = Math.max(rArr[i + 1], arr[i]);
        }
        int max = 0;
        // 比较两侧收集到的辅助数组中，最大绝对值（左边最大-右边最大）
        for (int i = 0; i < arr.length - 1; i++) {
            max = Math.max(max, Math.abs(lArr[i] - rArr[i + 1]));
        }
        return max;
    }

}

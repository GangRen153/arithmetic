package com.zuochengyun.大厂刷题班.code29;

/**
 * 给定一个数组，原始数组是有序且有重复值的，但是数组被旋转了
 * 也就是原数组被分为了两部分，左部分跑到了右边，右部分跑到了左部分
 * 问：给定一个num，找num是否在这个数组里
 *
 * @author 钢人
 */
public class Problem_0033_SearchInRotatedSortedArray {

    public static int search(int[] arr, int num) {
        int L = 0;
        int R = arr.length - 1;
        int M = 0;
        while (L <= R) {
            M = L + ((R - L) >> 1);
            if (arr[M] == num) {
                return M;
            }

            if (arr[L] == arr[M] && arr[M] == arr[R]) {
                while (L != M && arr[L] == arr[M]) {
                    L++;
                }
                // L...M 一路都相等
                if (L == M) {
                    L = M + 1;
                    continue;
                }
            }

            if (arr[L] != arr[M]) {
                if (arr[M] > arr[L]) {
                    if (num >= arr[L] && num < arr[M]) {
                        R = M - 1;
                    } else { // 9    [L] == 2    [M]   =  7   M... R
                        L = M + 1;
                    }
                } else { // [L] > [M]    L....M  存在断点
                    if (num > arr[M] && num <= arr[R]) {
                        L = M + 1;
                    } else {
                        R = M - 1;
                    }
                }
            } else { /// [L] [M] [R] 不都一样，  [L] === [M] -> [M]!=[R]
                if (arr[M] < arr[R]) {
                    if (num > arr[M] && num <= arr[R]) {
                        L = M + 1;
                    } else {
                        R = M - 1;
                    }
                } else {
                    if (num >= arr[L] && num < arr[M]) {
                        R = M - 1;
                    } else {
                        L = M + 1;
                    }
                }
            }
        }
        return -1;
    }

}

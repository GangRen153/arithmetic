package com.zuochengyun.大厂刷题班.code29;

/**
 * 给定一个矩阵数组，从[0][0]位置开始走，只能向右向下走，走到[N][N]位置
 * 有多少种不同路径
 *
 * @author 钢人
 */
public class Problem_0062_UniquePaths {

    public static int uniquePaths(int m, int n) {
        // 长度
        int right = n - 1;
        // 所有格子
        int all = m + n - 2;
        long o1 = 1;
        long o2 = 1;
        // o1乘进去的个数 一定等于 o2乘进去的个数
        for (int i = right + 1, j = 1; i <= all; i++, j++) {
            o1 *= i;
            o2 *= j;
            long gcd = gcd(o1, o2);
            o1 /= gcd;
            o2 /= gcd;
        }
        return (int) o1;
    }

    // 调用的时候，请保证初次调用时，m和n都不为0
    public static long gcd(long m, long n) {
        return n == 0 ? m : gcd(n, m % n);
    }

}

package com.zuochengyun.大厂刷题班.code29;

/**
 * 给定一个数组，数的范围是0~9之间
 * 将数组加1，返回结果
 *
 * 例如：
 * [1,2,5,3,7] -> [1,2,5,3,8]
 * [9,9,9,9,9] -> [1,0,0,0,0,0]
 *
 * @author 钢人
 */
public class Problem_0066_PlusOne {

    public static int[] plusOne(int[] digits) {
        int n = digits.length;
        for (int i = n - 1; i >= 0; i--) {
            if (digits[i] < 9) {
                digits[i]++;
                return digits;
            }
            digits[i] = 0;
        }
        int[] ans = new int[n + 1];
        ans[0] = 1;
        return ans;
    }

}

package com.zuochengyun.大厂刷题班.code29;

/**
 * 计算 x 的 n 次方
 * 思路：
 * x 的 16次方 = n1*n2*n4*n8*n1
 *
 * @author 钢人
 */
public class Problem_0050_PowXN {

    public static int pow(int a, int n) {
        int ans = 1;
        int t = a;
        while (n != 0) {
            if ((n & 1) != 0) {
                ans *= t;
            }
            t *= t;
            n >>= 1;
        }
        return ans;
    }

}

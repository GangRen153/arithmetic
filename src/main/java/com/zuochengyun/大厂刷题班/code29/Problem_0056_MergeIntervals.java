package com.zuochengyun.大厂刷题班.code29;

import java.util.Arrays;

/**
 * 合并区间
 *
 * 给定一个数组，合并有效区间
 * 例如：[[1,3],[2,6],[8,10],[11,13],[13,14]]
 * 返回：[[1,6],[8,10],[11,14]]
 *
 * 思路：
 * 先按arr[0][i]排序，排完看arr[0][i]能排多远
 * 例如：[[1,2],[1,4],[1,3]],看1开头的组最远能排多远，答案是[1,4]
 * 再看[[2,3][2,6][2,4]],再看2开头的组是否包含在1开头的组最远位置范围内,答案是都在，所以能合并，最终结果1最远能合到[1,6]
 * 以此类推
 *
 * @author 钢人
 */
public class Problem_0056_MergeIntervals {

    public static int[][] merge(int[][] intervals) {
        if (intervals.length == 0) {
            return new int[0][0];
        }
        Arrays.sort(intervals, (a, b) -> a[0] - b[0]);
        int s = intervals[0][0];
        int e = intervals[0][1];
        int size = 0;
        for (int i = 1; i < intervals.length; i++) {
            if (intervals[i][0] > e) {
                intervals[size][0] = s;
                intervals[size++][1] = e;
                s = intervals[i][0];
                e = intervals[i][1];
            } else {
                e = Math.max(e, intervals[i][1]);
            }
        }
        intervals[size][0] = s;
        intervals[size++][1] = e;
        return Arrays.copyOf(intervals, size);
    }

}

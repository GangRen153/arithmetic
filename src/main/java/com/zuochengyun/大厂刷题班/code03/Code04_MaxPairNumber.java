package com.zuochengyun.大厂刷题班.code03;

import java.util.Arrays;

/**
 * 给定一个数组arr，代表每个人的能力值。再给定一个非负数k。
 * 如果两个人能力差值正好为k，那么可以凑在一起比赛，一局比赛只有两个人
 * 返回最多可以同时有多少场比赛
 *
 * 例如 arr=[1,3,5,7]，k=2
 * 结果：13、57 或者 35
 *
 * @author 钢人
 */
public class Code04_MaxPairNumber {

    /**
     * 排序 + 滑动窗口
     */
    public static int maxPairNum2(int[] arr, int k) {
        if (k < 0 || arr == null || arr.length < 2) {
            return 0;
        }
        Arrays.sort(arr);
        int ans = 0;
        int N = arr.length;
        int L = 0;
        int R = 0;
        // 标记是否已经参与比赛了
        boolean[] usedR = new boolean[N];
        while (L < N && R < N) {
            // L 位置是否已经参加比赛了
            if (usedR[L]) {
                L++;
            } else if (L >= R) {
                R++;
            } else {
                // 计算差值与 k 比较
                int distance = arr[R] - arr[L];
                if (distance == k) {
                    usedR[R] = true;
                    L++;
                    R++;
                    ans++;
                } else if (distance < k) {
                    R++;
                } else {
                    L++;
                }
            }
        }
        return ans;
    }

}

package com.zuochengyun.大厂刷题班.code03;

/**
 * 给定一个正方形矩阵，找出矩阵边框全是1的最大边框
 * 只要求边框一样，不包括边框内部
 *
 * 思路：
 *  根据原二维数组生成两个二维预处理数组
 *  分别为右预处理数组和下预处理数组
 *      类似与前缀数组
 *
 *  例如：
 *  原二维数组
 *      [1,1,1,0,1,1]
 *      [0,1,1,1,1,1]
 *      [1,0,1,1,1,1]
 *      [1,0,1,0,1,1]
 *
 * 右预处理数组（从右面开始累加计算前缀和）
 *      [3,2,1,0,2,1]
 *      [0,5,4,3,2,1]
 *      [1,0,4,3,2,1]
 *      [1,0,1,0,2,1]
 *
 * 下预处理数组（从下面开始累加计算前缀和）
 *      [1,2,4,0,4,4]
 *      [0,1,3,2,3,3]
 *      [2,0,2,1,2,2]
 *      [1,0,1,0,1,1]
 *
 * 测试链接 : <a href="https://leetcode.com/problems/largest-1-bordered-square/"/>
 *
 * @author 钢人
 **/
public class Code03_Largest1BorderedSquare {

    public static int largest1BorderedSquare(int[][] m) {
        int[][] right = new int[m.length][m[0].length];
        int[][] down = new int[m.length][m[0].length];
        setBorderMap(m, right, down);
        for (int size = Math.min(m.length, m[0].length); size != 0; size--) {
            if (hasSizeOfBorder(size, right, down)) {
                return size * size;
            }
        }
        return 0;
    }

    public static void setBorderMap(int[][] m, int[][] right, int[][] down) {
        int r = m.length;
        int c = m[0].length;
        if (m[r - 1][c - 1] == 1) {
            right[r - 1][c - 1] = 1;
            down[r - 1][c - 1] = 1;
        }
        for (int i = r - 2; i != -1; i--) {
            if (m[i][c - 1] == 1) {
                right[i][c - 1] = 1;
                down[i][c - 1] = down[i + 1][c - 1] + 1;
            }
        }
        for (int i = c - 2; i != -1; i--) {
            if (m[r - 1][i] == 1) {
                right[r - 1][i] = right[r - 1][i + 1] + 1;
                down[r - 1][i] = 1;
            }
        }
        for (int i = r - 2; i != -1; i--) {
            for (int j = c - 2; j != -1; j--) {
                if (m[i][j] == 1) {
                    right[i][j] = right[i][j + 1] + 1;
                    down[i][j] = down[i + 1][j] + 1;
                }
            }
        }
    }

    public static boolean hasSizeOfBorder(int size, int[][] right, int[][] down) {
        for (int i = 0; i != right.length - size + 1; i++) {
            for (int j = 0; j != right[0].length - size + 1; j++) {
                if (right[i][j] >= size && down[i][j] >= size && right[i + size - 1][j] >= size
                        && down[i][j + size - 1] >= size) {
                    return true;
                }
            }
        }
        return false;
    }

}

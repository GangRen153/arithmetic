package com.zuochengyun.大厂刷题班.code02;

import java.util.HashMap;

/**
 * 已知一个消息流会不断地吐出整数 1~N，但不一定按照顺序依次吐出
 * 如果上次打印的序号为i， 那么当i+1出现时
 * 请打印 i+1 及其之后接收过的并且连续的所有数
 * 直到1~N全部接收并打印完
 * 请设计这种接收并打印的结构
 *
 * @author 钢人
 */
public class Code03_ReceiveAndPrintOrderLine {

    public static class Node {
        public String info;
        public Node next;

        public Node(String str) {
            info = str;
        }
    }

    public static class MessageBox {
        // 头链表 map，链表连续节点的头节点，较小的那个节点
        private HashMap<Integer, Node> headMap;
        // 尾链表 map，链表连续节点的尾节点，较大的那个节点
        private HashMap<Integer, Node> tailMap;
        // 当前最大的点
        private int waitPoint;

        public void receive(int num, String info) {
            if (num < 1) {
                return;
            }
            // 当前添加进来的内容
            Node cur = new Node(info);
            // 以 num 节点作为头节点和尾节点
            headMap.put(num, cur);
            tailMap.put(num, cur);

            // 看尾节点表是否包含 num-1 节点，如果包含把它串起来，并且删掉
            if (tailMap.containsKey(num - 1)) {
                cur.next = headMap.get(num + 1);
                tailMap.remove(num - 1);
                headMap.remove(num);
            }

            // 看头节点表是否包含 num+1 节点，如果包含把它串起来，并且删掉
            if (headMap.containsKey(num + 1)) {
                cur.next = headMap.get(num + 1);
                tailMap.remove(num);
                headMap.remove(num + 1);
            }

            if (num == waitPoint) {
                print();
            }
        }

        private void print() {
            Node node = headMap.get(waitPoint);
            headMap.remove(waitPoint);
            while (node != null) {
                System.out.print(node.info + " ");
                node = node.next;
                waitPoint++;
            }
            tailMap.remove(waitPoint-1);
            System.out.println();
        }
    }

    public static void main(String[] args) {
        // MessageBox only receive 1~N
        MessageBox box = new MessageBox();
        // 1....
        System.out.println("这是2来到的时候");
        box.receive(2,"B"); // - 2"
        System.out.println("这是1来到的时候");
        box.receive(1,"A"); // 1 2 -> print, trigger is 1
        box.receive(4,"D"); // - 4
        box.receive(5,"E"); // - 4 5
        box.receive(7,"G"); // - 4 5 - 7
        box.receive(8,"H"); // - 4 5 - 7 8
        box.receive(6,"F"); // - 4 5 6 7 8
        box.receive(3,"C"); // 3 4 5 6 7 8 -> print, trigger is 3
        box.receive(9,"I"); // 9 -> print, trigger is 9
        box.receive(10,"J"); // 10 -> print, trigger is 10
        box.receive(12,"L"); // - 12
        box.receive(13,"M"); // - 12 13
        box.receive(11,"K"); // 11 12 13 -> print, trigger is 11

    }

}

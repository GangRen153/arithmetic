package com.zuochengyun.大厂刷题班.code02;

/**
 * 给定一个数组arr，只能对arr中的一个子数组排序，
 * 但是想让arr整体都有序
 * 返回满足这一设定的子数组中，最短的是多长
 * <p>
 * 测试链接 : <a href="https://leetcode.com/problems/shortest-unsorted-continuous-subarray/"/>
 *
 * @author 钢人
 */
public class Code06_MinLengthForSort {

    public static int findUnsortedSubarray(int[] nums) {
        if (nums == null || nums.length < 2) {
            return 0;
        }

        int N = nums.length;
        int right = -1;
        int max = Integer.MIN_VALUE;
        // 从左往右遍历，定位右侧不需要排序的数
        for (int i = 0; i < N; i++) {
            if (max > nums[i]) {
                right = i;
            }
            max = Math.max(max, nums[i]);
        }

        int min = Integer.MAX_VALUE;
        int left = N;
        // 从右往左遍历，定位左侧不需要排序的数
        for (int i = N - 1; i >= 0; i--) {
            if (min < nums[i]) {
                left = i;
            }
            min = Math.min(min, nums[i]);
        }
        // 计算右侧位置 - 左侧位置中间有多少个数，就是需要排序的最短长度
        return Math.max(0, right - left + 1);
    }

}

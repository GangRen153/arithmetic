package com.zuochengyun.大厂刷题班.code02;

import java.util.HashMap;

/**
 * 设计有 setAll() 功能的哈希表，就是将所有的 value 设置成给定值
 * 要求 setAll 方法，时间复杂度O(1)
 *
 * @author 钢人
 */
public class Code05_SetAll {

    public static class MyValue<V> {
        // put value
        public V value;
        // put 的时候当前时间戳
        public long time;

        public MyValue(V v, long t) {
            value = v;
            time = t;
        }
    }

    public static class MyHashMap<K, V> {
        private HashMap<K, MyValue<V>> map;
        private long time;
        private MyValue<V> setAll;

        public MyHashMap() {
            map = new HashMap<>();
            time = 0;
            setAll = new MyValue<V>(null, -1);
        }

        public void put(K key, V value) {
            map.put(key, new MyValue<V>(value, time++));
        }

        public void setAll(V value) {
            setAll = new MyValue<V>(value, time++);
        }

        public V get(K key) {
            if (!map.containsKey(key)) {
                return null;
            }
            // 比较一下 setAll 时候的时间戳和 put key 时候的时间戳哪个大取哪个
            if (map.get(key).time > setAll.time) {
                return map.get(key).value;
            } else {
                return setAll.value;
            }
        }
    }

}

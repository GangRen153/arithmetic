package com.zuochengyun.大厂刷题班.code02;

import java.util.Arrays;
import java.util.Comparator;
import java.util.TreeMap;

/**
 * 给定数组 hard 和 money，长度都为N
 * hard[i]表示i号的难度， money[i]表示i号工作的收入
 * 给定数组 ability，长度都为 M，ability[j] 表示j号人的能力
 * 每一号工作，都可以提供无数的岗位，难度和收入都一样
 * 但是人的能力必须 >= 这份工作的难度，才能上班
 * 返回一个长度为M的数组 ans，ans[j] 表示j号人能获得的最好收入
 *
 * @author 钢人
 */
public class Code01_ChooseWork {

    public static class Job {
        public int money;
        public int hard;

        public Job() {

        }
        public Job(int m, int h) {
            money = m;
            hard = h;
        }
    }

    public static int[] getMoneys(Job[] job, int[] ability) {
        // 根据工作难度排序
        Arrays.sort(job, (o1, o2) -> o1.hard != o2.hard ? (o1.hard - o2.hard) : (o2.money - o1.money));

        // 使用有序表
        TreeMap<Integer, Integer> map = new TreeMap<>();
        map.put(job[0].hard, job[0].money);
        // 上一份最好的工作
        Job pre = job[0];
        for (int i = 1; i < job.length; i++) {
            // 看当前的工作是否比上一份工作要求高并且给的钱多，因为上边已经排过序了，所以遍历的时候是从小到大遍历
            if (job[i].hard != pre.hard && job[i].money > pre.money) {
                // 如果上一份工作要求低还给的钱多，加到有序表中
                pre = job[i];
                map.put(pre.hard, pre.money);
            }
        }

        // 定义每个人能获得的最好收入数组
        int[] ans = new int[ability.length];
        for (int i = 0; i < ability.length; i++) {
            // ability[i] 当前人的能力 <= ability[i]  且离它最近的，也就是当前 j 号人能拿到的最好收入
            Integer key = map.floorKey(ability[i]);
            // 如果是 null，表示当前 j 号人没有能胜任的工作
            ans[i] = key != null ? map.get(key) : 0;
        }
        return ans;
    }

}

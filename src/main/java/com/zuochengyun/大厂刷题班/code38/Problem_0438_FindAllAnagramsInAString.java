package com.zuochengyun.大厂刷题班.code38;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 嵌套表问题
 *
 * 给定一个字符串 s，在给定一个字符串 p
 * 在 p 字符串中，不考虑字符顺序的情况下
 * p 在 s 中相等的起始位置
 * 例如：
 * s=ascsdddfgsacsdrr
 * p=sca
 * [0,10,11]
 *
 * @author 钢人
 */
public class Problem_0438_FindAllAnagramsInAString {

    public static List<Integer> findAnagrams(String s, String p) {
        List<Integer> ans = new ArrayList<>();
        if (s == null || p == null || s.length() < p.length()) {
            return ans;
        }
        char[] str = s.toCharArray();
        int N = str.length;
        char[] pst = p.toCharArray();
        int M = pst.length;
        HashMap<Character, Integer> map = new HashMap<>();
        for (char cha : pst) {
            if (!map.containsKey(cha)) {
                map.put(cha, 1);
            } else {
                map.put(cha, map.get(cha) + 1);
            }
        }
        int all = M;
        for (int end = 0; end < M - 1; end++) {
            if (map.containsKey(str[end])) {
                int count = map.get(str[end]);
                if (count > 0) {
                    all--;
                }
                map.put(str[end], count - 1);
            }
        }
        for (int end = M - 1, start = 0; end < N; end++, start++) {
            if (map.containsKey(str[end])) {
                int count = map.get(str[end]);
                if (count > 0) {
                    all--;
                }
                map.put(str[end], count - 1);
            }
            if (all == 0) {
                ans.add(start);
            }
            if (map.containsKey(str[start])) {
                int count = map.get(str[start]);
                if (count >= 0) {
                    all++;
                }
                map.put(str[start], count + 1);
            }
        }
        return ans;
    }

}

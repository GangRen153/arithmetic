package com.zuochengyun.大厂刷题班.code38;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个数组，数组中的数范围在下标1~N的范围上
 * 找出 1~N 范围上缺失的数字
 *
 * 利用下标循环怼
 *
 * @author 钢人
 */
public class Problem_0448_FindAllNumbersDisappearedInAnArray {

    public static List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> ans = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return ans;
        }
        int N = nums.length;
        for (int i = 0; i < N; i++) {
            // 从i位置出发，去玩下标循环怼
            walk(nums, i);
        }
        for (int i = 0; i < N; i++) {
            if (nums[i] != i + 1) {
                ans.add(i + 1);
            }
        }
        return ans;
    }

    public static void walk(int[] nums, int i) {
        // 不断从i发货
        while (nums[i] != i + 1) {
            int nexti = nums[i] - 1;
            if (nums[nexti] == nexti + 1) {
                break;
            }
            swap(nums, i, nexti);
        }
    }

    public static void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }

}

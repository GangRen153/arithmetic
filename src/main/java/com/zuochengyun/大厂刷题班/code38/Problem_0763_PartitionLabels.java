package com.zuochengyun.大厂刷题班.code38;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个字符串进行切割成多份儿
 * 要求相同字符必须都在一块儿，尽可能地切成多块儿
 * 返回最多能切成几块儿
 *
 * @author 钢人
 */
public class Problem_0763_PartitionLabels {

	public static List<Integer> partitionLabels(String s) {
		char[] str = s.toCharArray();
		int[] far = new int[26];
		for (int i = 0; i < str.length; i++) {
			far[str[i] - 'a'] = i;
		}
		List<Integer> ans = new ArrayList<>();
		int left = 0;
		int right = far[str[0] - 'a'];
		for (int i = 1; i < str.length; i++) {
			if (i > right) {
				ans.add(right - left + 1);
				left = i;
			}
			right = Math.max(right, far[str[i] - 'a']);
		}
		ans.add(right - left + 1);
		return ans;
	}

}

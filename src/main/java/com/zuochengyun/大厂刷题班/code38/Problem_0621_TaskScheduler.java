package com.zuochengyun.大厂刷题班.code38;

/**
 * 给定一些任务 a-z 任意字符
 * 执行完 a 的时候，后面在安排 a 的时候，a 后面必须要安排两个不一样的任务
 * 也就是执行相同任务，频率间隔必须在 k 以上
 * 比如：
 * k = 2
 * a→b→c，安排完b和c之后，才能继续安排a，也可以不安排a，b和c同理
 *
 * 也就是 k 一定要大于任务的种类
 *
 * @author 钢人
 */
public class Problem_0621_TaskScheduler {

    public static int leastInterval(char[] tasks, int free) {
        int[] count = new int[256];
        // 出现最多次的任务，到底是出现了几次
        int maxCount = 0;
        for (char task : tasks) {
            count[task]++;
            maxCount = Math.max(maxCount, count[task]);
        }
        // 有多少种任务，都出现最多次
        int maxKinds = 0;
        for (int task = 0; task < 256; task++) {
            if (count[task] == maxCount) {
                maxKinds++;
            }
        }
        // maxKinds : 有多少种任务，都出现最多次
        // maxCount : 最多次，是几次？
        // 砍掉最后一组剩余的任务数
        int tasksExceptFinalTeam = tasks.length - maxKinds;
        // 空格数量
        int spaces = (free + 1) * (maxCount - 1);
        // 到底几个空格最终会留下！
        int restSpaces = Math.max(0, spaces - tasksExceptFinalTeam);
        return tasks.length + restSpaces;
        // return Math.max(tasks.length, ((n + 1) * (maxCount - 1) + maxKinds));
    }

}

package com.zuochengyun.大厂刷题班.code38;

/**
 * 合并两个二叉树，如果两个叉树的节点一个有一个没有，把节点合并过去
 * 如果两个树都有节点，把两个节点的值相加
 * 例如：
 *      1       3           4
 *     ↙ ↘     ↙ ↘    →    ↙ ↘
 *    2   3       4       2   7
 * @author 钢人
 */
public class Problem_0617_MergeTwoBinaryTrees {

    public static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    // 当前，一棵树的头是t1，另一颗树的头是t2
    // 请返回，整体merge之后的头
    public static TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
        if (t1 == null) {
            return t2;
        }
        if (t2 == null) {
            return t1;
        }
        // t1和t2都不是空
        TreeNode merge = new TreeNode(t1.val + t2.val);
        merge.left = mergeTrees(t1.left, t2.left);
        merge.right = mergeTrees(t1.right, t2.right);
        return merge;
    }

}

package com.zuochengyun.大厂刷题班.code34;

/**
 * 有一个正数数组，数字的范围是 1~(N-1)
 * 其中有一个数是重复的，找出这个数
 *
 * 例如：
 * [1,2,3,4,5,6,1]
 * 7个数，数字范围是1~6
 *
 * @author 钢人
 */
public class Problem_0287_FindTheDuplicateNumber {

    public static int findDuplicate(int[] nums) {
        if (nums == null || nums.length < 2) {
            return -1;
        }
        int slow = nums[0];
        int fast = nums[nums[0]];
        while (slow != fast) {
            slow = nums[slow];
            fast = nums[nums[fast]];
        }
        fast = 0;
        while (slow != fast) {
            fast = nums[fast];
            slow = nums[slow];
        }
        return slow;
    }

}

package com.zuochengyun.大厂刷题班.code34;

/**
 * 生命游戏问题
 * 给定一个二维网格，0 代表有细胞，1 代表无细胞
 * 如果活细胞周围八个方向数量少于2个，这个细胞就会死亡
 * 如果活细胞周围八个方向数有2个或3个细胞，这个细胞就会存活
 * 如果活细胞周围八个方向数大于3个细胞，这个细胞就会窒息死亡
 * 如果死亡细胞周围正好有3个活细胞，则这个死亡细胞会复活
 *
 * 生成下一轮的细胞网格
 * 原地调整
 *
 * 游戏说明：<a href="https://www.bilibili.com/video/BV1rJ411n7ri" />
 * @author 钢人
 */
public class Problem_0289_GameOfLife {

    public static void gameOfLife(int[][] board) {
        int N = board.length;
        int M = board[0].length;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                // 八个方向周围有几个1
                int neighbors = neighbors(board, i, j);
                if (neighbors == 3 || (board[i][j] == 1 && neighbors == 2)) {
                    board[i][j] |= 2;
                }
            }
        }
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                board[i][j] >>= 1;
            }
        }
    }

    // b[i][j] 这个位置的数，周围有几个1
    public static int neighbors(int[][] b, int i, int j) {
        return f(b, i - 1, j - 1)
                + f(b, i - 1, j)
                + f(b, i - 1, j + 1)
                + f(b, i, j - 1)
                + f(b, i, j + 1)
                + f(b, i + 1, j - 1)
                + f(b, i + 1, j)
                + f(b, i + 1, j + 1);
    }

    // b[i][j] 上面有1，就返回1，上面不是1，就返回0
    public static int f(int[][] b, int i, int j) {
        return (i >= 0 && i < b.length && j >= 0 && j < b[0].length && (b[i][j] & 1) == 1) ? 1 : 0;
    }

}

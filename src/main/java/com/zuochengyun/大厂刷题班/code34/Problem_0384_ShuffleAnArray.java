package com.zuochengyun.大厂刷题班.code34;

/**
 * 给你一个整数数组 nums，设计算法来打乱一个没有重复元素的数组
 * 打乱后，数组的所有排列应该是等可能的
 *
 * @author 钢人
 */
public class Problem_0384_ShuffleAnArray {

    class Solution {
        private int[] origin;
        private int[] shuffle;
        private int N;

        public Solution(int[] nums) {
            origin = nums;
            N = nums.length;
            shuffle = new int[N];
            for (int i = 0; i < N; i++) {
                shuffle[i] = origin[i];
            }
        }

        public int[] reset() {
            return origin;
        }

        public int[] shuffle() {
            for (int i = N - 1; i >= 0; i--) {
                int r = (int) (Math.random() * (i + 1));
                int tmp = shuffle[r];
                shuffle[r] = shuffle[i];
                shuffle[i] = tmp;
            }
            return shuffle;
        }
    }

}

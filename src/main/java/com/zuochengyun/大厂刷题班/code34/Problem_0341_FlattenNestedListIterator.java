package com.zuochengyun.大厂刷题班.code34;

import java.util.Iterator;
import java.util.List;
import java.util.Stack;

/**
 * var 结构的迭代器（Integer 或 List）
 * 例如：var = [1,[1,2,3,[4,6]],71]
 * <p>
 * 迭代的时候，要依次 1,1,2,3,4,6,71 的返回
 * <p>
 * 思路：
 * 压成栈，next() 的时候直接弹出
 *
 * @author 钢人
 */
public class Problem_0341_FlattenNestedListIterator {

    public interface NestedInteger {

        /**
         * nested list.
         * @return true if this NestedInteger holds a single integer, rather than a
         */
        boolean isInteger();

        /**
         * single integer
         * Return null if this NestedInteger holds a nested list
         *
         * @return the single integer that this NestedInteger holds, if it holds a
         */
        Integer getInteger();

        /**
         * list
         * Return null if this NestedInteger holds a single integer
         *
         * @return the nested list that this NestedInteger holds, if it holds a nested
         */
        List<NestedInteger> getList();
    }

    public class NestedIterator implements Iterator<Integer> {
        private List<NestedInteger> list;
        private Stack<Integer> stack;
        private boolean used;

        public NestedIterator(List<NestedInteger> nestedList) {
            list = nestedList;
            stack = new Stack<>();
            stack.push(-1);
            used = true;
            hasNext();
        }

        @Override
        public Integer next() {
            Integer ans = null;
            if (!used) {
                ans = get(list, stack);
                used = true;
                hasNext();
            }
            return ans;
        }

        @Override
        public boolean hasNext() {
            if (stack.isEmpty()) {
                return false;
            }
            if (!used) {
                return true;
            }
            if (findNext(list, stack)) {
                used = false;
            }
            return !used;
        }

        private Integer get(List<NestedInteger> nestedList, Stack<Integer> stack) {
            int index = stack.pop();
            Integer ans = null;
            if (!stack.isEmpty()) {
                ans = get(nestedList.get(index).getList(), stack);
            } else {
                ans = nestedList.get(index).getInteger();
            }
            stack.push(index);
            return ans;
        }

        private boolean findNext(List<NestedInteger> nestedList, Stack<Integer> stack) {
            int index = stack.pop();
            if (!stack.isEmpty() && findNext(nestedList.get(index).getList(), stack)) {
                stack.push(index);
                return true;
            }
            for (int i = index + 1; i < nestedList.size(); i++) {
                if (pickFirst(nestedList.get(i), i, stack)) {
                    return true;
                }
            }
            return false;
        }

        private boolean pickFirst(NestedInteger nested, int position, Stack<Integer> stack) {
            if (nested.isInteger()) {
                stack.add(position);
                return true;
            } else {
                List<NestedInteger> actualList = nested.getList();
                for (int i = 0; i < actualList.size(); i++) {
                    if (pickFirst(actualList.get(i), i, stack)) {
                        stack.add(position);
                        return true;
                    }
                }
            }
            return false;
        }
    }

}

package com.zuochengyun.大厂刷题班.code34;

/**
 * 判断 n 是不是只含有 3 的因子
 *
 * @author 钢人
 */
public class Problem_0326_PowerOfThree {

    /**
     * 1162261467 是 3 最大的平方
     */
    public static boolean isPowerOfThree(int n) {
        return (n > 0 && 1162261467 % n == 0);
    }

}

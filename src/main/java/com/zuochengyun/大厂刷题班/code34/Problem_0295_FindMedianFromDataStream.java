package com.zuochengyun.大厂刷题班.code34;

import java.util.PriorityQueue;

/**
 * 有一个数字流，不断的往出吐数字
 * 求中位数
 *
 * 中位数是有序列表中间的数。如果列表长度是偶数，中位数则是中间两个数的平均值。
 * [2,3,4] 的中位数是 3
 * [2,3] 的中位数是 (2 + 3) / 2 = 2.5
 *
 * @author 钢人
 */
public class Problem_0295_FindMedianFromDataStream {

    class MedianFinder {
        private PriorityQueue<Integer> maxh;
        private PriorityQueue<Integer> minh;

        public MedianFinder() {
            maxh = new PriorityQueue<>((a, b) -> b - a);
            minh = new PriorityQueue<>((a, b) -> a - b);
        }

        /**
         * 流出来的数添加进去
         */
        public void addNum(int num) {
            if (maxh.isEmpty() || maxh.peek() >= num) {
                maxh.add(num);
            } else {
                minh.add(num);
            }
            balance();
        }

        /**
         * 返回中位数
         */
        public double findMedian() {
            if (maxh.size() == minh.size()) {
                return (double) (maxh.peek() + minh.peek()) / 2;
            } else {
                return maxh.size() > minh.size() ? maxh.peek() : minh.peek();
            }
        }

        private void balance() {
            if (Math.abs(maxh.size() - minh.size()) == 2) {
                if (maxh.size() > minh.size()) {
                    minh.add(maxh.poll());
                } else {
                    maxh.add(minh.poll());
                }
            }
        }
    }

}

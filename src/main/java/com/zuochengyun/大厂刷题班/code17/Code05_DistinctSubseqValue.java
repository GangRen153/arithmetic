package com.zuochengyun.大厂刷题班.code17;

import java.util.HashMap;

/**
 * 给定一个字符串Str
 * 返回Str的所有子序列中有多少不同的字面值，空字符面值也算
 *
 * Leetcode原题：<a href="https://leetcode.com/problems/distinct-subsequences-ii/" />
 * @author 钢人
 */
public class Code05_DistinctSubseqValue {

    public static int zuo1(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int m = 1000000007;
        char[] str = s.toCharArray();
        // 记录以某个字符结尾生成的子序列个数，因为后面计算的时候要减
        HashMap<Character, Integer> map = new HashMap<>();
        // 一个字符也没遍历的时候，有空集
        int all = 1;
        // 遍历到新的字符 x 的子序列可组成不同面值个数
        // 求必须以 x 结尾的子序列个数
        for (char x : str) {
            // 先把 x 字符出现之前组成不同面值的个数拿到
            int newAdd = all;
            // 如果当前 x 字符之前出现过，就说明当前 x 结尾的子序列算过一次，这次如果还算 x 结尾是字符子序列会导致重复
            // 所以需要把之前 x 结尾的字符出现过的子序列面值个数减去
            // 计算出的结果就是当前 x 字符结尾子序列能组成的面值数
            all = all + newAdd - (map.getOrDefault(x, 0));
            map.put(x, newAdd);
        }
        return all;
    }

    /**
     * 观察得出的优化解
     */
    public static int zuo2(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int m = 1000000007;
        char[] str = s.toCharArray();
        // 记录以某个字符结尾生成的子序列个数，因为后面计算的时候要减
        HashMap<Character, Integer> map = new HashMap<>();
        // 一个字符也没遍历的时候，有空集
        int all = 1;
        // 遍历到新的字符 x 的子序列可组成不同面值个数
        // 求必须以 x 结尾的子序列个数
        for (char x : str) {
            int newAdd = all;
            int curAll = all + newAdd - (map.getOrDefault(x, 0));
            // 当前以 x 结尾的子序列组成面值个数
            curAll = (curAll + newAdd) % m;
            // curAll - map.getOrDefault() + m：加 m 是为了避免 curAll 减完之后出现负数
            curAll = (curAll - (map.getOrDefault(x, 0)) + m) % m;
            all = curAll;
            map.put(x, newAdd);
        }
        return all;
    }

}

package com.zuochengyun.大厂刷题班.code17;

/**
 * 给定两个字符串S和T
 * 返回S的所有子序列中，有多少个子序列的字面值等于T
 *
 * 测试链接 : <a href="https://leetcode-cn.com/problems/21dk04/" />
 * @author 钢人
 */
public class Code04_DistinctSubseq {

    public static int numDistinct1(String S, String T) {
        char[] s = S.toCharArray();
        char[] t = T.toCharArray();
        return process(s, t, s.length, t.length);
    }

    public static int process(char[] s, char[] t, int i, int j) {
        // T 面值的字符用完了，说明匹配上了
        if (j == 0) {
            return 1;
        }
        // S 字符用完了，但是 T 还没抵消完，说明没匹配上
        if (i == 0) {
            return 0;
        }
        // 用 S 的第 i-1 位置的字符尝试抵消 T 面值的 j 位置字符
        int res = process(s, t, i - 1, j);
        // 如果 i 位置字符和 j 位置字符一样，可以尝试 i 和 j 位置字符互相抵消，返回后续可能
        if (s[i - 1] == t[j - 1]) {
            res += process(s, t, i - 1, j - 1);
        }
        return res;
    }

    public static int numDistinct2(String S, String T) {
        char[] s = S.toCharArray();
        char[] t = T.toCharArray();

        int[][] dp = new int[s.length + 1][t.length + 1];
        for (int j = 0; j <= t.length; j++) {
            dp[0][j] = 0;
        }
        for (int i = 0; i <= s.length; i++) {
            dp[i][0] = 1;
        }

        for (int i = 1; i <= s.length; i++) {
            for (int j = 1; j <= t.length; j++) {
                dp[i][j] = dp[i - 1][j] + (s[i - 1] == t[j - 1] ? dp[i - 1][j - 1] : 0);
            }
        }
        return dp[s.length][t.length];
    }

}

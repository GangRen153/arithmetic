package com.zuochengyun.大厂刷题班.code17;

import java.util.List;

/**
 * 给定一个字符串数组，仅允许两个字符串拼接
 * 问：有多少种回文
 *
 * 测试链接 : <a href="https://leetcode.com/problems/palindrome-pairs/" />
 * @author 钢人
 */
public class Code03_PalindromePairs {

    public static List<List<Integer>> palindromePairs(String[] words) {
        // TODO
        return null;
    }

    public static String reverse(String str) {
        char[] chs = str.toCharArray();
        int l = 0;
        int r = chs.length - 1;
        while (l < r) {
            char tmp = chs[l];
            chs[l++] = chs[r];
            chs[r--] = tmp;
        }
        return String.valueOf(chs);
    }

}

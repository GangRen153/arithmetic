package com.zuochengyun.大厂刷题班.code12;

import java.util.HashMap;

/**
 * 给定一个未排序的整数数组，返回最长的连续元素序列的长度
 * 例如：[100,4,200,1,3,2]，排序之后是 [1,2,3,4,100,200]
 * 返回最长连续元素序列的长度[1,2,3,4] -> 个数是 4，返回4
 *
 * 测试链接 : <a href="https://leetcode.com/problems/longest-consecutive-sequence/" />
 * @author 钢人
 */
public class Code03_LongestConsecutive {

    public static int longestConsecutive(int[] nums) {
        HashMap<Integer, Integer> map = new HashMap<>();
        int len = 0;
        for (int num : nums) {
            if (!map.containsKey(num)) {
                map.put(num, 1);
                int preLen = map.getOrDefault(num - 1, 0);
                int posLen = map.getOrDefault(num + 1, 0);
                int all = preLen + posLen + 1;
                map.put(num - preLen, all);
                map.put(num + posLen, all);
                len = Math.max(len, all);
            }
        }
        return len;
    }

}

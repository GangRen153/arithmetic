package com.zuochengyun.大厂刷题班.code12;

/**
 * 在两个都有序的数组中，找两个数组中整体第 K 小的数
 * 例如：arr1:[1,2,3],arr2:[5,7,8,9,10]
 * 第5小的数：7，第7小的数：9
 *
 * 测试链接 : <a href="https://leetcode.com/problems/median-of-two-sorted-arrays/" />
 * @author 钢人
 */
public class Code03_FindKthMinNumber {

    public static int findKthNum(int[] arr1, int[] arr2, int kth) {
        // 谁长谁是 longs
        int[] longs = arr1.length >= arr2.length ? arr1 : arr2;
        // 谁短谁是 shorts
        int[] shorts = arr1.length < arr2.length ? arr1 : arr2;
        int l = longs.length;
        int s = shorts.length;
        // 第 k 小的数有可能在 shorts 范围
        if (kth <= s) {
            return getUpMedian(shorts, 0, kth - 1, longs, 0, kth - 1);
        }
        if (kth > l) {
            if (shorts[kth - l - 1] >= longs[l - 1]) {
                return shorts[kth - l - 1];
            }
            if (longs[kth - s - 1] >= shorts[s - 1]) {
                return longs[kth - s - 1];
            }
            return getUpMedian(shorts, kth - l, s - 1, longs, kth - s, l - 1);
        }
        if (longs[kth - s - 1] >= shorts[s - 1]) {
            return longs[kth - s - 1];
        }
        return getUpMedian(shorts, 0, s - 1, longs, kth - s, kth - 1);
    }

    /**
     * 利用二分，划分中位数找到第 k 个数，排除左半区还是右半区
     * 最终取两个数组第 k 小的交集范围
     * 例如：第一个数组的右半区~第二个数组的左半区范围
     */
    public static int getUpMedian(int[] A, int s1, int e1, int[] B, int s2, int e2) {
        int mid1 = 0;
        int mid2 = 0;
        while (s1 < e1) {
            mid1 = (s1 + e1) / 2;
            mid2 = (s2 + e2) / 2;
            // 两个数组的中点相等
            if (A[mid1] == B[mid2]) {
                return A[mid1];
            }

            // 两个数组中点不等，奇数长度
            if (((e1 - s1 + 1) & 1) == 1) {
                if (A[mid1] > B[mid2]) {
                    if (B[mid2] >= A[mid1 - 1]) {
                        return B[mid2];
                    }
                    e1 = mid1 - 1;
                    s2 = mid2 + 1;
                } else {
                    if (A[mid1] >= B[mid2 - 1]) {
                        return A[mid1];
                    }
                    e2 = mid2 - 1;
                    s1 = mid1 + 1;
                }
            } else { // 偶数长度
                if (A[mid1] > B[mid2]) {
                    e1 = mid1;
                    s2 = mid2 + 1;
                } else {
                    e2 = mid2;
                    s1 = mid1 + 1;
                }
            }
        }
        return Math.min(A[s1], B[s2]);
    }

}

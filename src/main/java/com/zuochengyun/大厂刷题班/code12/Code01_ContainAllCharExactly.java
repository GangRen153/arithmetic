package com.zuochengyun.大厂刷题班.code12;

import java.util.Arrays;

/**
 * 给定长度为 n 的字符串 str，以及一个长度为 m 的字符串 aim
 * 问能否在 str 中找到一个长度为 m 的连续子串，
 * 使得这个子串刚好由 aim 的 m 个字符组成，顺序无所谓
 * 返回任意满足条件的一个子串的起始位置，未找到返回-1
 * <p>
 * 思路：
 * 滑动窗口 + map
 * <p>
 * 测试链接 : <a href="https://leetcode.com/problems/permutation-in-string/" />
 * @author 钢人
 */
public class Code01_ContainAllCharExactly {

    public static int containExactly1(String s, String a) {
        if (s == null || a == null || s.length() < a.length()) {
            return -1;
        }
        char[] aim = a.toCharArray();
        Arrays.sort(aim);
        String aimSort = String.valueOf(aim);
        for (int L = 0; L < s.length(); L++) {
            for (int R = L; R < s.length(); R++) {
                char[] cur = s.substring(L, R + 1).toCharArray();
                Arrays.sort(cur);
                String curSort = String.valueOf(cur);
                if (curSort.equals(aimSort)) {
                    return L;
                }
            }
        }
        return -1;
    }

    /**
     * @param s str
     * @param a aim
     * @return
     */
    public static int containExactly2(String s, String a) {
        if (s == null || a == null || s.length() < a.length()) {
            return -1;
        }

        char[] str = s.toCharArray();
        char[] aim = a.toCharArray();

        // str 从 0 位置开始遍历，找长度位 aim 的子串，词频一致即可
        for (int L = 0; L <= str.length - aim.length; L++) {
            // 从 L 位置开始尝试
            if (isCountEqual(str, L, aim)) {
                return L;
            }
        }
        return -1;
    }

    public static boolean isCountEqual(char[] str, int L, char[] aim) {
        int[] count = new int[256];
        // 统计 aim 词频
        for (int i = 0; i < aim.length; i++) {
            count[aim[i]]++;
        }

        // 遍历 str 的词频，与 aim 词频抵消
        for (int i = 0; i < aim.length; i++) {
            // 如果 count 位置在没有减减之前已经是 0 了，返回 false
            if (count[str[L + i]]-- == 0) {
                return false;
            }
        }
        return true;
    }

    public static int containExactly3(String s1, String s2) {
        if (s1 == null || s2 == null || s1.length() < s2.length()) {
            return -1;
        }
        char[] aim = s2.toCharArray();
        int M = aim.length;
        int[] count = new int[256];
        // 统计出 aim 的词频
        for (int i = 0; i < M; i++) {
            count[aim[i]]++;
        }

        // all 表示 aim 字符中，还有多少个没有抵消
        int all = M;
        char[] str = s1.toCharArray();
        int R = 0;
        // 最早的M个字符，让其窗口初步形成，找到最右位置R
        for (; R < M; R++) {
            if (count[str[R]]-- > 0) {
                all--;
            }
        }

        for (; R < str.length; R++) {
            // 如果所有字符都抵消了，直接返回
            if (all == 0) {
                return R - M;
            }
            // 如果 str[R] 位置字符抵消 aim 字符之前 > 0
            // 说明可以正常抵消 aim 中的字符，所以 all--
            if (count[str[R]]-- > 0) {
                all--;
            }
            // 再用逆向推 L 位置字符，如果抵消不了就++
            if (count[str[R - M]]++ >= 0) {
                all++;
            }
        }
        return all == 0 ? R - M : -1;
    }

}

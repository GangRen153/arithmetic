package com.zuochengyun.大厂刷题班.code12;

import java.util.Arrays;

/**
 * 给定一个字符串 str，一定不包含’.’和’*’
 * 在给定一个表达式串，只包含’.’和’*’
 * ’.’可以变成任何单个字符，但不能不变
 * ’*’需要和前面的字符搭配使用，但是’*’前面不是是 ’*’，‘*’可以变成变成0~N个前面的字符
 * <p>
 * 问：表达式串能否变成 str 串
 * <p>
 * 测试链接 : <a href="https://leetcode.com/problems/regular-expression-matching/" />
 *
 * @author 钢人
 */
public class Code04_RegularExpressionMatch {

    public static boolean isValid(char[] s, char[] e) {
        // s中不能有'.' or '*'
        for (int i = 0; i < s.length; i++) {
            if (s[i] == '*' || s[i] == '.') {
                return false;
            }
        }
        // 开头的e[0]不能是'*'，没有相邻的'*'
        for (int i = 0; i < e.length; i++) {
            if (e[i] == '*' && (i == 0 || e[i - 1] == '*')) {
                return false;
            }
        }
        return true;
    }

    public static boolean isMatch1(String str, String exp) {
        if (str == null || exp == null) {
            return false;
        }
        char[] s = str.toCharArray();
        char[] e = exp.toCharArray();
        return isValid(s, e) && process(s, e, 0, 0);
    }

    public static boolean process(char[] s, char[] e, int si, int ei) {
        // exp 没字符了，看 str 有没有匹配完
        if (ei == e.length) {
            return si == s.length;
        }

        // ei 没字符了 || ei 下一个字符不是 *
        if (ei + 1 == e.length || e[ei + 1] != '*') {
            // si 还有字符没匹配完 && si 位置的字符能被 ei 位置的字符匹配上 && 后续字符串能匹配上
            return si != s.length && (e[ei] == s[si] || e[ei] == '.') && process(s, e, si + 1, ei + 1);
        }

        // exp[ei] 后续还有字符，或者 ei 下一个字符是 *
        // si 还有字符没匹配完 && si 位置的字符能被 ei 位置的字符匹配上
        while (si != s.length && (e[ei] == s[si] || e[ei] == '.')) {
            if (process(s, e, si, ei + 2)) {
                return true;
            }
            si++;
        }
        // si 后续没字符了，只能是 ei 剩下的表达式串取匹配 si 位置的字符
        return process(s, e, si, ei + 2);
    }

    /**
     * 动态规划版本 + 斜率优化
     */
    public static boolean isMatch3(String str, String pattern) {
        if (str == null || pattern == null) {
            return false;
        }
        char[] s = str.toCharArray();
        char[] p = pattern.toCharArray();
        if (!isValid(s, p)) {
            return false;
        }
        int N = s.length;
        int M = p.length;
        boolean[][] dp = new boolean[N + 1][M + 1];
        dp[N][M] = true;

        for (int j = M - 1; j >= 0; j--) {
            dp[N][j] = (j + 1 < M && p[j + 1] == '*') && dp[N][j + 2];
        }
        // dp[0..N-2][M-1]都等于false，只有dp[N-1][M-1]需要讨论
        if (N > 0 && M > 0) {
            dp[N - 1][M - 1] = (s[N - 1] == p[M - 1] || p[M - 1] == '.');
        }
        for (int i = N - 1; i >= 0; i--) {
            for (int j = M - 2; j >= 0; j--) {
                boolean curChar = (s[i] == p[j]) || (p[j] == '.');
                if (p[j + 1] != '*') {
                    dp[i][j] = curChar && dp[i + 1][j + 1];
                } else {
                    if (curChar && dp[i + 1][j]) {
                        dp[i][j] = true;
                    } else {
                        dp[i][j] = dp[i][j + 2];
                    }
                }
            }
        }
        return dp[0][0];
    }

}

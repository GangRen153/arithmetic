package com.zuochengyun.大厂刷题班.code31;

import java.util.List;

/**
 * 给定一个字符串，在给定一个单词表
 * 要拆解这个字符串，拆解后要在单词表中存在
 * 问：这个字符串能否被拆解
 *
 * 例如：aaccdd
 * 单词表：[aa,cc,dd,aac,cddd,cd]
 * 可以拆解成 aa，cc，dd
 *
 * 思路：
 * 以 a 为前缀能否被分解
 * 在尝试以 aa 为前缀能否被分解
 * 在尝试 aac 为前缀能否被分解
 * ...
 *
 * 测试链接：<a href="https://www.lintcode.com/problem/107/" />
 * @author 钢人
 */
public class Problem_0139_WordBreak {

    public static class Node {
        public boolean end;
        public Node[] nexts;

        public Node() {
            end = false;
            nexts = new Node[26];
        }
    }

    public static int wordBreak2(String s, List<String> wordDict) {
        Node root = new Node();
        // 生成前缀树
        for (String str : wordDict) {
            char[] chs = str.toCharArray();
            Node node = root;
            int index;
            for (int i = 0; i < chs.length; i++) {
                index = chs[i] - 'a';
                if (node.nexts[index] == null) {
                    node.nexts[index] = new Node();
                }
                node = node.nexts[index];
            }
            node.end = true;
        }
        char[] str = s.toCharArray();
        int N = str.length;
        int[] dp = new int[N + 1];
        dp[N] = 1;
        for (int i = N - 1; i >= 0; i--) {
            Node cur = root;
            for (int end = i; end < N; end++) {
                cur = cur.nexts[str[end] - 'a'];
                if (cur == null) {
                    break;
                }
                if (cur.end) {
                    dp[i] += dp[end + 1];
                }
            }
        }
        return dp[0];
    }

}

package com.zuochengyun.大厂刷题班.code31;

import java.util.*;

/**
 * 给定一个数组，包含数字，加减乘除，遇到加减乘除，就将左侧离加减乘除最近的两个数相计算
 * 例如：
 * [2,6,*,3,+]
 * (2*6)+3
 *
 * @author 钢人
 */
public class Problem_0150_EvaluateReversePolishNotation {

    public static int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        for (String str : tokens) {
            // 遇到加减乘除就将最上边的两个数弹出，计算完在压回去
            if ("+".equals(str) || "-".equals(str) || "*".equals(str) || "/".equals(str)) {
                compute(stack, str);
            } else {
                stack.push(Integer.valueOf(str));
            }
        }
        return stack.peek();
    }

    public static void compute(Stack<Integer> stack, String op) {
        int num2 = stack.pop();
        int num1 = stack.pop();
        int ans = 0;
        switch (op) {
            case "+":
                ans = num1 + num2;
                break;
            case "-":
                ans = num1 - num2;
                break;
            case "*":
                ans = num1 * num2;
                break;
            case "/":
                ans = num1 / num2;
                break;
            default:
        }
        stack.push(ans);
    }

}

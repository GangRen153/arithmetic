package com.zuochengyun.大厂刷题班.code33;

/**
 * 给定单链表，给定删除节点，不给头节点，将其在列表上删除
 *
 * 思路：把要删的节点下一个节点的value值，覆盖到要删的节点上
 * 工程上是有问题的
 *
 * @author 钢人
 */
public class Problem_0237_DeleteNodeInLinkedList {

    public static class ListNode {
        int val;
        ListNode next;
    }

    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }

}

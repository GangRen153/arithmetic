package com.zuochengyun.大厂刷题班.code33;

/**
 * 给定一个 str1 和 str2，如果两个字符串所包含的字符种类和字符个数一样
 * 就称为变形词，问 str1 和 str2 是否是变形词
 *
 * @author 钢人
 */
public class Problem_0242_ValidAnagram {

    public static boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        char[] str1 = s.toCharArray();
        char[] str2 = t.toCharArray();
        int[] count = new int[256];
        for (char cha : str1) {
            count[cha]++;
        }
        for (char cha : str2) {
            if (--count[cha] < 0) {
                return false;
            }
        }
        return true;
    }

}

package com.zuochengyun.大厂刷题班.code33;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

/**
 * 修课程题
 * 给定一个二维数组 [i][j],表示i课程依赖于j课程，也就是想完i课程需要先完成j课程
 * 问，能否正常修完成课程
 * <p>
 * 思路
 * 拓扑结构找循环依赖解，有循环依赖则无法修完课
 *
 * @author 钢人
 */
public class Problem_0207_CourseSchedule {

    public static class Course {
        public int name;
        public int in;
        public ArrayList<Course> nexts;

        public Course(int n) {
            name = n;
            in = 0;
            nexts = new ArrayList<>();
        }
    }

    /**
     * @param numCourses    课程数量
     * @param prerequisites 课程依赖关系
     */
    public static boolean canFinish1(int numCourses, int[][] prerequisites) {
        if (prerequisites == null || prerequisites.length == 0) {
            return true;
        }
        // 一个编号 对应 一个课的实例
        HashMap<Integer, Course> nodes = new HashMap<>();
        for (int[] arr : prerequisites) {
            int to = arr[0];
            // from -> to
            int from = arr[1];
            if (!nodes.containsKey(to)) {
                nodes.put(to, new Course(to));
            }
            if (!nodes.containsKey(from)) {
                nodes.put(from, new Course(from));
            }
            Course t = nodes.get(to);
            Course f = nodes.get(from);
            f.nexts.add(t);
            t.in++;
        }
        int needPrerequisiteNums = nodes.size();
        Queue<Course> zeroInQueue = new LinkedList<>();
        for (Course node : nodes.values()) {
            if (node.in == 0) {
                zeroInQueue.add(node);
            }
        }
        int count = 0;
        while (!zeroInQueue.isEmpty()) {
            Course cur = zeroInQueue.poll();
            count++;
            for (Course next : cur.nexts) {
                if (--next.in == 0) {
                    zeroInQueue.add(next);
                }
            }
        }
        return count == needPrerequisiteNums;
    }

}

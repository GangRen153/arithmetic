package com.zuochengyun.大厂刷题班.code33;

/**
 * 给定一个原数组，求新数组
 * 新数组的规则是，原数组的下标位除了自己，其他下标位上所有的数相乘就是新数组下标位上的数
 * 不能用除法
 * 返回这个新数组
 * 例如：
 * [1, 2, 3,4]
 * [24,12,8,6]
 *
 * @author 钢人
 */
public class Problem_0238_ProductOfArrayExceptSelf {

    public int[] productExceptSelf(int[] nums) {
        int n = nums.length;
        int[] ans = new int[n];
        // 前缀累乘积数组
        ans[0] = nums[0];
        for (int i = 1; i < n; i++) {
            ans[i] = ans[i - 1] * nums[i];
        }
        int right = 1;
        // 从后往前填
        for (int i = n - 1; i > 0; i--) {
            ans[i] = ans[i - 1] * right;
            right *= nums[i];
        }
        ans[0] = right;
        return ans;
    }

}

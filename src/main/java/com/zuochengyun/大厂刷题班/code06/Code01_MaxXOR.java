package com.zuochengyun.大厂刷题班.code06;

/**
 * 数组中所有数都异或起来的结果，叫做异或和
 * 给定一个数组arr，返回arr的最大子数组异或和
 *
 * @author 钢人
 */
public class Code01_MaxXOR {

    public static int maxXorSubarray1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        // 前缀异或
        int[] eor = new int[arr.length];
        eor[0] = arr[0];
        for (int i = 1; i < arr.length; i++) {
            eor[i] = eor[i - 1] ^ arr[i];
        }
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j <= i; j++) {
                max = Math.max(max, j == 0 ? eor[i] : eor[i] ^ eor[j - 1]);
            }
        }
        return max;
    }

    public static class Node {
        public Node[] nexts = new Node[2];
    }

    public static class NumTrie {
        // TODO
    }

}

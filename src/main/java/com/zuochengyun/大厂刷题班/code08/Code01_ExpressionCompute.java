package com.zuochengyun.大厂刷题班.code08;

import java.util.LinkedList;

/**
 * 给定一个 str 的计算公式（如果是负数，会用括号括起来）
 * 求计算结果
 *
 * @author 钢人
 */
public class Code01_ExpressionCompute {

    public static int calculate(String str) {
        return f(str.toCharArray(), 0)[0];
    }

    public static int[] f(char[] str, int i) {
        LinkedList<String> que = new LinkedList<>();
        // 当前数字
        int cur = 0;
        int[] bra;
        while (i < str.length && str[i] != ')') {
            // 当前字符是数字，将数字取出
            if (str[i] >= '0' && str[i] <= '9') {
                cur = cur * 10 + str[i++] - '0';

                // 当前字符是符号
            } else if (str[i] != '(') {
                // 把之前得到的 × 或 ÷ 公式先计算出来
                addNum(que, cur);
                // 把当前符号添加到队列里
                que.addLast(String.valueOf(str[i++]));
                // 清空当前数
                cur = 0;
            } else {// 当前是左括号，将返回左括号到右括号内的结果
                bra = f(str, i + 1);
                cur = bra[0];
                i = bra[1] + 1;
            }
        }
        addNum(que, cur);
        return new int[] { getNum(que), i };
    }

    /**
     * 这里的计算结果，始终都只包含 + 或 - 的公式
     */
    public static void addNum(LinkedList<String> que, int num) {
        // 如果不是空的，计算链表里的数
        if (!que.isEmpty()) {
            int cur = 0;
            // 弹出最后一个元素，如果最后一个元素是+或-，不需要提前计算，加回队列里
            String top = que.pollLast();
            if ("+".equals(top) || "-".equals(top)) {
                que.addLast(top);
            } else {
                // 如果是 × 或 ÷，需要提前计算
                cur = Integer.valueOf(que.pollLast());
                num = "*".equals(top) ? (cur * num) : (cur / num);
            }
        }
        // 把最后的 × 或 ÷ 公式消掉，提前得到结果添加到队列里
        que.addLast(String.valueOf(num));
    }

    /**
     * 计算当前链表中的公式
     */
    public static int getNum(LinkedList<String> que) {
        int res = 0;
        boolean add = true;
        String cur;
        int num;
        while (!que.isEmpty()) {
            cur = que.pollFirst();
            // 加号
            if ("+".equals(cur)) {
                add = true;

                // 减号
            } else if ("-".equals(cur)) {
                add = false;

                // 数字
            } else {
                // 计算结果
                num = Integer.parseInt(cur);
                // 计算结果
                res += add ? num : (-num);
            }
        }
        return res;
    }

}

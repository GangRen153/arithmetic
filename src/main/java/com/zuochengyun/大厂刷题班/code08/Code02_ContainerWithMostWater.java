package com.zuochengyun.大厂刷题班.code08;

/**
 * 盛水最多容器问题
 *
 * 给定一个数组，在数组中可以选择任意两个下标作为杆儿装水
 * 问：怎么选装水最多
 *
 * 测试链接 : <a href="https://leetcode.com/problems/container-with-most-water/"/>
 * @author 钢人
 */
public class Code02_ContainerWithMostWater {

    public static int maxArea1(int[] h) {
        int max = 0;
        int N = h.length;
        for (int i = 0; i < N; i++) {
            for (int j = i + 1; j < N; j++) {
                max = Math.max(max, Math.min(h[i], h[j]) * (j - i));
            }
        }
        return max;
    }

    public static int maxArea2(int[] h) {
        int max = 0;
        int l = 0;
        int r = h.length - 1;
        while (l < r) {
            // 计算最大容量
            int capacity = Math.min(h[l], h[r]) * (r - l);
            max = Math.max(max, capacity);
            // 右边比左边小，左移，反之右移
            if (h[l] > h[r]) {
                r--;
            } else {
                l++;
            }
        }
        return max;
    }

}

package com.zuochengyun.大厂刷题班.code25;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 三元组
 * 给定一个数组，找出三个数相加是 k 的三元组，但要求三个数在字面值不能都一样
 * 例如：
 * [-3,1,2,-3,1,2,0,3]
 * [-3,1,2][-3,1,2] 就不行，因为三个数全一样
 * [-3,0,3][-3,1,2] 可以，因为三个数不是全一样
 *
 * 测试链接 : <a href="https://leetcode.com/problems/3sum/" />
 * @author 钢人
 */
public class Code02_3Sum {

    /**
     * 二元组
     */
    public static int findPairs(int[] nums, int k) {
        // 排序
        Arrays.sort(nums);
        int left = 0, right = 1;
        int result = 0;
        while (left < nums.length && right < nums.length) {
            if (left == right || nums[right] - nums[left] < k) {
                right++;
            } else if (nums[right] - nums[left] > k) {
                left++;
            } else {
                left++;
                result++;
                while (left < nums.length && nums[left] == nums[left - 1]) {
                    left++;
                }
            }
        }
        return result;
    }

    /**
     * 三个数相加和是 0 的三元组问题
     */
    public static List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        int N = nums.length;
        List<List<Integer>> ans = new ArrayList<>();
        // 三元组最后一个数，是arr[i]   之前....二元组 + arr[i]
        for (int i = N - 1; i > 1; i--) {
            if (i == N - 1 || nums[i] != nums[i + 1]) {
                // 找到所有等于当前 nums[i] 相反数的不同组合的二元组
                List<List<Integer>> nexts = twoSum(nums, i - 1, -nums[i]);
                // 收集所有达标的三元组
                for (List<Integer> cur : nexts) {
                    cur.add(nums[i]);
                    ans.add(cur);
                }
            }
        }
        return ans;
    }

    /**
     * nums[0...end]这个范围上，有多少个不同二元组，相加==target，全返回
     */
    public static List<List<Integer>> twoSum(int[] nums, int end, int target) {
        int L = 0;
        int R = end;
        List<List<Integer>> ans = new ArrayList<>();
        while (L < R) {
            if (nums[L] + nums[R] > target) {
                R--;
            } else if (nums[L] + nums[R] < target) {
                L++;
            } else { // nums[L] + nums[R] == target
                if (L == 0 || nums[L - 1] != nums[L]) {
                    List<Integer> cur = new ArrayList<>();
                    cur.add(nums[L]);
                    cur.add(nums[R]);
                    ans.add(cur);
                }
                L++;
            }
        }
        return ans;
    }

}

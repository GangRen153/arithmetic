package com.zuochengyun.大厂刷题班.code25;

import java.util.HashMap;
import java.util.Map;

/**
 * 给定一个 x 坐标数组，y 坐标数组，两个数组的长度一样
 * x和y在平面的一个点上，x数组和y数组构建成一条线
 * 问最多有多少个点在同一条线上
 *
 * 测试链接: <a href="https://leetcode.com/problems/max-points-on-a-line/" />
 * @author 钢人
 */
public class Code03_MaxPointsOnALine {

    public static int maxPoints(int[][] points) {
        if (points == null) {
            return 0;
        }
        if (points.length <= 2) {
            return points.length;
        }

        // key = 3
        // value = {7 , 10}  -> 斜率为3/7的点 有10个
        //         {5,  15}  -> 斜率为3/5的点 有15个
        Map<Integer, Map<Integer, Integer>> map = new HashMap<>();
        int result = 0;
        for (int i = 0; i < points.length; i++) {
            map.clear();
            // 共点数量
            int samePosition = 1;
            int sameX = 0;
            int sameY = 0;
            int line = 0;
            for (int j = i + 1; j < points.length; j++) {
                int x = points[j][0] - points[i][0];
                int y = points[j][1] - points[i][1];
                // x 和 y 在同一个点上
                if (x == 0 && y == 0) {
                    samePosition++;
                } else if (x == 0) {
                    sameX++;
                } else if (y == 0) {
                    sameY++;

                    // 普通斜率
                } else {
                    int gcd = gcd(x, y);
                    x /= gcd;
                    y /= gcd;
                    if (!map.containsKey(x)) {
                        map.put(x, new HashMap<>());
                    }
                    if (!map.get(x).containsKey(y)) {
                        map.get(x).put(y, 0);
                    }
                    map.get(x).put(y, map.get(x).get(y) + 1);
                    line = Math.max(line, map.get(x).get(y));
                }
            }
            result = Math.max(result, Math.max(Math.max(sameX, sameY), line) + samePosition);
        }
        return result;
    }

    // 拿到 a 和 b 的最大公约数
    // O(1)
    public static int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }

}

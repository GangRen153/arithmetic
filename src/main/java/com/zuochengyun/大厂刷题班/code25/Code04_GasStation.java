package com.zuochengyun.大厂刷题班.code25;

/**
 * 加油站出发点问题
 *
 * 返回结果数组，每一个出发点是否是良好出发点
 * 时间复杂度O(N)，额外空间复杂度O(1)
 *
 * 测试链接 : <a href="https://leetcode.com/problems/gas-station/" />
 * @author 钢人
 */
public class Code04_GasStation {

    public static int canCompleteCircuit(int[] gas, int[] cost) {
        // TODO
        return 0;
    }

    public static int lastIndex(int index, int size) {
        return index == 0 ? (size - 1) : index - 1;
    }

    public static int nextIndex(int index, int size) {
        return index == size - 1 ? 0 : (index + 1);
    }

}

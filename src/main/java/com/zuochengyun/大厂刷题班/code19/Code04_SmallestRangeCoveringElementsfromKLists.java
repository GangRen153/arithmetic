package com.zuochengyun.大厂刷题班.code19;

import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

/**
 * 给定一个二维有序数组，这些数组行不一定等长，而且行有序，列不一定有序
 * 在给定一个范围，但是要求这个二维数组的行，至少有一个数在这个范围上
 * 求划分这个二维数组的最小区间
 * <p>
 * 例如：
 * 范围[1~5]
 * [
 * [1,6]
 * [3,7]
 * [4,33]
 * ]
 * [1,3,4] → 最小区间范围就是 3~4
 * 只要数组中的数在 1~5 范围就行，但是尽量缩小到最窄区间
 * <p>
 * 测试链接 : <a href="https://leetcode.com/problems/smallest-range-covering-elements-from-k-lists/" />
 *
 * @author 钢人
 */
public class Code04_SmallestRangeCoveringElementsfromKLists {

    public static class Node {
        public int value;
        public int arrid;
        public int index;

        public Node(int v, int ai, int i) {
            value = v;
            arrid = ai;
            index = i;
        }
    }

    public static int[] smallestRange(List<List<Integer>> nums) {
        int N = nums.size();
        TreeSet<Node> orderSet = new TreeSet<>((o1, o2) -> o1.value != o2.value ? o1.value - o2.value : o1.arrid - o2.arrid);
        for (int i = 0; i < N; i++) {
            orderSet.add(new Node(nums.get(i).get(0), i, 0));
        }
        boolean set = false;
        int a = 0;
        int b = 0;

        while (orderSet.size() == N) {
            Node min = orderSet.first();
            Node max = orderSet.last();
            if (!set || (max.value - min.value < b - a)) {
                set = true;
                a = min.value;
                b = max.value;
            }
            min = orderSet.pollFirst();
            int arrid = min.arrid;
            int index = min.index + 1;
            if (index != nums.get(arrid).size()) {
                orderSet.add(new Node(nums.get(arrid).get(index), arrid, index));
            }
        }
        return new int[]{a, b};
    }

}

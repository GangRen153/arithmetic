package com.zuochengyun.大厂刷题班.code19;

/**
 * 给定一个正数N，比如N = 13，在纸上把所有数都列出来如下：
 * 1 2 3 4 5 6 7 8 9 10 11 12 13
 * 可以数出1这个字符出现了6次
 * 给定一个正数N，如果把1~N都列出来，
 * 返回1这个字符出现的多少次
 *
 * @author 钢人
 */
public class Code03_OneNumber {

    public static int solution1(int num) {
        if (num < 1) {
            return 0;
        }
        int count = 0;
        for (int i = 1; i != num + 1; i++) {
            count += get1Nums(i);
        }
        return count;
    }

    public static int get1Nums(int num) {
        int res = 0;
        while (num != 0) {
            if (num % 10 == 1) {
                res++;
            }
            num /= 10;
        }
        return res;
    }


    // 1 ~ num 这个范围上，画了几道1
    public static int solution2(int num) {
        if (num < 1) {
            return 0;
        }

        // num 有几位数
        int len = getLenOfNum(num);
        // 让最高位变成1，其他低位补齐
        // 例如：7872328738273 -> 1000000000000
        int tmp1 = powerBaseOf10(len - 1);

        // num最高位 num / tmp1
        int first = num / tmp1;
        // 最高位有几个1
        int firstOneNum = first == 1 ? num % tmp1 + 1 : tmp1;

        // 除去最高位之外，剩下1的数量
        // 最高位1 10(k-2次方) * (k-1) * 1
        // 最高位first 10(k-2次方) * (k-1) * first
        int otherOneNum = first * (len - 1) * (tmp1 / 10);
        // 最高位1出现的次数 + 除了最高位之后的数出现1的次数 + 去掉最高位剩余的数出现的1（例如：千位，百位，十位，个位这样递推）
        return firstOneNum + otherOneNum + solution2(num % tmp1);
    }

    public static int powerBaseOf10(int base) {
        return (int) Math.pow(10, base);
    }

    public static int getLenOfNum(int num) {
        int len = 0;
        while (num != 0) {
            len++;
            num /= 10;
        }
        return len;
    }

}

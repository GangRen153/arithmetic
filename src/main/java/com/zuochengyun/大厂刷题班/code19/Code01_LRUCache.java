package com.zuochengyun.大厂刷题班.code19;

import java.util.HashMap;

/**
 * 给定一个数据结构，专门存放缓存，假设这个缓存只能存放3个数
 * 超出3个数，放第4个数的时候，淘汰掉时间最久的那个数
 * 如果是更新操作，要刷新时间
 * 求实现
 *
 * 思路：
 * 把新加的数据放到双向链表尾节点，把旧节点放到头节点，并且把添加的节点放到 map 中
 * 方便添加的时候查看这个节点有没有，如果有是更新如果没有是新增
 * 更新操作从 map 中把对象直接拿出来，直接取出链表指针引用，挂到尾节点即可
 *
 * <a href="https://leetcode.com/problems/lru-cache/" />
 * @author 钢人
 */
public class Code01_LRUCache {

    public static class Node<K, V> {
        public K key;
        public V value;
        // 前指针
        public Node<K, V> last;
        // 尾指针
        public Node<K, V> next;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private MyCache<Integer, Integer> cache;

    public Code01_LRUCache(int capacity) {
        cache = new MyCache<>(capacity);
    }

    public int get(int key) {
        Integer ans = cache.get(key);
        return ans == null ? -1 : ans;
    }

    public void put(int key, int value) {
        cache.set(key, value);
    }

    public static class NodeDoubleLinkedList<K, V> {

        private Node<K, V> head;
        private Node<K, V> tail;
        public NodeDoubleLinkedList() {
            head = null;
            tail = null;
        }

        // 现在来了一个新的node，请挂到尾巴上去
        public void addNode(Node<K, V> newNode) {
            if (newNode == null) {
                return;
            }
            if (head == null) {
                head = newNode;
                tail = newNode;
            } else {
                tail.next = newNode;
                newNode.last = tail;
                tail = newNode;
            }
        }

        // node 入参，一定保证！node在双向链表里！
        // node原始的位置，左右重新连好，然后把node分离出来
        // 挂到整个链表的尾巴上
        public void moveNodeToTail(Node<K, V> node) {
            if (tail == node) {
                return;
            }
            if (head == node) {
                head = node.next;
                head.last = null;
            } else {
                node.last.next = node.next;
                node.next.last = node.last;
            }
            node.last = tail;
            node.next = null;
            tail.next = node;
            tail = node;
        }

        public Node<K, V> removeHead() {
            if (head == null) {
                return null;
            }
            Node<K, V> res = head;
            if (head == tail) {
                head = null;
                tail = null;
            } else {
                head = res.next;
                res.next = null;
                head.last = null;
            }
            return res;
        }
    }

    public static class MyCache<K, V> {
        private HashMap<K, Node<K, V>> keyNodeMap;
        private NodeDoubleLinkedList<K, V> nodeList;
        private final int capacity;

        public MyCache(int cap) {
            keyNodeMap = new HashMap<K, Node<K, V>>();
            nodeList = new NodeDoubleLinkedList<K, V>();
            capacity = cap;
        }

        public V get(K key) {
            if (keyNodeMap.containsKey(key)) {
                Node<K, V> res = keyNodeMap.get(key);
                nodeList.moveNodeToTail(res);
                return res.value;
            }
            return null;
        }

        public void set(K key, V value) {
            if (keyNodeMap.containsKey(key)) {
                Node<K, V> node = keyNodeMap.get(key);
                node.value = value;
                nodeList.moveNodeToTail(node);
            } else { // 新增！
                Node<K, V> newNode = new Node<K, V>(key, value);
                keyNodeMap.put(key, newNode);
                nodeList.addNode(newNode);
                if (keyNodeMap.size() == capacity + 1) {
                    removeMostUnusedCache();
                }
            }
        }

        /**
         * 删除头节点
         */
        private void removeMostUnusedCache() {
            Node<K, V> removeNode = nodeList.removeHead();
            keyNodeMap.remove(removeNode.key);
        }
    }

}

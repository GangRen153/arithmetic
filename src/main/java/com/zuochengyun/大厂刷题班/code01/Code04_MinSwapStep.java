package com.zuochengyun.大厂刷题班.code01;

/**
 * 一个数组中只有两种字符'G'和’B’，可以让所有的G都放在左侧，所有的B都放在右侧
 * 或者可以让所有的G都放在右侧，所有的B都放在左侧，但是只能在相邻字符之间进行交换操作，
 * 返回至少需要交换几次
 *
 * @author 钢人
 */
public class Code04_MinSwapStep {

    public static int minSteps2(String s) {
        if (s == null || "".equals(s)) {
            return 0;
        }
        char[] chars = s.toCharArray();
        int step1 = 0;
        int step2 = 0;
        int gi = 0;
        int bi = 0;
        for (int i = 0; i < chars.length; i++) {
            // G 往左边走（统计每一次交换需要移动的步数）
            if (chars[i] == 'G') {
                step1 += i - (gi++);
            } else {// B 往左边走
                step2 += i - (bi++);
            }
        }
        return Math.min(step1, step2);
    }

}

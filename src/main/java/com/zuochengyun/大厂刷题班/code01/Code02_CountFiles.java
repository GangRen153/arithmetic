package com.zuochengyun.大厂刷题班.code01;

import java.io.File;
import java.util.Stack;

/**
 * 统计某个目录下的文件数量，目录不算
 *
 * @author 钢人
 */
public class Code02_CountFiles {

    public static int getFileNumber(String folderPath) {
        File root = new File(folderPath);
        if (!root.isDirectory() && !root.isFile()) {
            return 0;
        }
        if (root.isFile()) {
            return 1;
        }
        Stack<File> stack = new Stack<>();
        stack.add(root);
        int files = 0;
        while (!stack.isEmpty()) {
            File folder = stack.pop();
            for (File next : folder.listFiles()) {
                if (next.isFile()) {
                    files++;
                }
                if (next.isDirectory()) {
                    stack.push(next);
                }
            }
        }
        return files;
    }

    public static void main(String[] args) {
        // 你可以自己更改目录
        String path = "/Users/zuochengyun/Desktop/";
        System.out.println(getFileNumber(path));
    }

}

package com.zuochengyun.大厂刷题班.code01;

/**
 * 给定两个非负数组x和hp，长度都是N，再给定一个正数range
 * x有序，x[i]表示i号怪兽在x轴上的位置；hp[i]表示i号怪兽的血量
 * 再给定一个正数range，表示如果法师释放技能的范围长度
 * 被打到的每只怪兽损失1点血量。
 * 返回要把所有怪兽血量清空，至少需要释放多少次AOE技能？
 *
 * @author 钢人
 */
public class Code06_AOE {

    public static int minAoe2(int[] x, int[] hp, int range) {
        int N = x.length;
        int ans = 0;
        for (int i = 0; i < N; i++) {
            // 找到最左侧存活怪兽的位置
            if (hp[i] > 0) {
                int triggerPost = i;
                // 从 i 位置开始，找到 i~range 范围的最右侧 R 位置，在此位置上进行 aoe
                // 怪兽 x[i]~x[triggerPost] 之间的间距不能超过 range
                while (triggerPost < N && x[triggerPost] - x[i] <= range) {
                    triggerPost++;
                }
                // 统计 aoe 掉最左侧的怪兽需要的次数
                ans += hp[i];
                // 将 i~range 范围的数全部减 x[L]
                aoe(x, hp, i, triggerPost - 1, range);
            }
        }
        return ans;
    }

    public static void aoe(int[] x, int[] hp, int L, int trigger, int range) {
        int N = x.length;
        int R = trigger;
        // 在不越界的情况下，范围怪兽 x[i]~x[triggerPost] 之间的间距不能超过 range，找到最佳 aoe 的 R 位置
        while (R < N && x[R] - x[trigger] <= range) {
            R++;
        }
        // 最左侧怪兽的血量
        int minus = hp[L];
        // 同时 L~R 范围上给怪兽全部减去 minus 血量
        for (int i = L; i < R; i++) {
            hp[i] = Math.max(0, hp[i] - minus);
        }
    }

    public static int minAoe3(int[] x, int[] hp, int range) {
        // TODO 线段树方式
        return 0;
    }

}

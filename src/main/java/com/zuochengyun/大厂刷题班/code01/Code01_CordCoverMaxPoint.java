package com.zuochengyun.大厂刷题班.code01;

/**
 * 给定一个有序数组arr，代表坐落在 X 轴上的点
 * 给定一个正数 K，代表绳子的长度，即使绳子边缘处盖住点也算盖住
 * 返回绳子最多压中几个点？
 *
 * @author 钢人
 */
public class Code01_CordCoverMaxPoint {

    /**
     * 二分法
     */
    public static int maxPoint1(int[] arr, int L) {
        int res = 1;
        for (int i = 0; i < arr.length; i++) {
            int nearest = nearest(arr, i, arr[i] - L);
            res = Math.max(res, i - nearest + 1);
        }
        return res;
    }

    private static int nearest(int[] arr, int R, int value) {
        int L = 0;
        int index= R;
        while (L <= R) {
            int mid = L + ((R - L) >> 1);
            if (arr[mid] >= value) {
                index = mid;
                R = mid - 1;
            } else {
                L = mid + 1;
            }
        }
        return index;
    }

    public static int maxPoint2(int[] arr, int L) {
        int left = 0;
        int right = 0;
        int N = arr.length;
        int max = 0;
        while (left < N) {
            while (right < N && arr[right] - arr[left] <= L) {
                right++;
            }
            max = Math.max(max, right - (left++));
        }
        return max;
    }


}

package com.zuochengyun.大厂刷题班.code01;

/**
 * 给定正数 n，返回大于等于，且最接近n的，2的某次方的值
 * 例如：n=7，返回8，n=14，返回16
 *
 * @author 钢人
 */
public class Code03_Near2Power {

    public static int tableSizeFor(int n) {
        // 加入 n 刚好是 2 的某次方，可以有效避免错误计算，所以需要减减
        n--;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        // 只移动到 16，因为 int 的最大位数是32
        n |= n >>> 16;
        // n < 0 是避免输入 n 是负数
        return (n < 0) ? 1 : n + 1;
    }

}

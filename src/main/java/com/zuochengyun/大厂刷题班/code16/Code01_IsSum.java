package com.zuochengyun.大厂刷题班.code16;

import java.util.HashSet;

/**
 * 给定一个数组，可正可负，子序列是否能累加出k
 * 1）正常怎么做？
 * 2）如果arr中的数值很大，但是arr的长度不大，怎么做？
 *
 * @author 钢人
 */
public class Code01_IsSum {

    public static boolean isSum1(int[] arr, int sum) {
        if (sum == 0) {
            return true;
        }
        if (arr == null || arr.length == 0) {
            return false;
        }
        return process1(arr, arr.length - 1, sum);
    }

    public static boolean process1(int[] arr, int i, int sum) {
        if (sum == 0) {
            return true;
        }
        if (i == -1) {
            return false;
        }
        return process1(arr, i - 1, sum) || process1(arr, i - 1, sum - arr[i]);
    }

    /**
     * arr 数组中，数的值比较小，数组长度比较长
     */
    public static boolean isSum3(int[] arr, int sum) {
        if (sum == 0) {
            return true;
        }
        // sum != 0
        if (arr == null || arr.length == 0) {
            return false;
        }

        int min = 0;
        int max = 0;
        // 累加和范围
        for (int num : arr) {
            min += Math.min(num, 0);
            max += Math.max(num, 0);
        }

        if (sum < min || sum > max) {
            return false;
        }

        int N = arr.length;
        boolean[][] dp = new boolean[N][max - min + 1];
        dp[0][-min] = true;
        dp[0][arr[0] - min] = true;
        for (int i = 1; i < N; i++) {
            for (int j = min; j <= max; j++) {
                // dp[i][j] = dp[i-1][j]
                dp[i][j - min] = dp[i - 1][j - min];
                // dp[i][j] |= dp[i-1][j - arr[i]]
                int next = j - min - arr[i];
                dp[i][j - min] |= (next >= 0 && next <= max - min && dp[i - 1][next]);
            }
        }
        // dp[N-1][sum]
        return dp[N - 1][sum - min];
    }

    /**
     * 如果arr中的数值特别大，动态规划方法依然会很慢
     * 此时如果arr的数字个数不算多(40以内)，哪怕其中的数值很大，分治的方法也将是最优解
     */
    public static boolean isSum4(int[] arr, int sum) {
        if (sum == 0) {
            return true;
        }
        if (arr == null || arr.length == 0) {
            return false;
        }
        if (arr.length == 1) {
            return arr[0] == sum;
        }
        int N = arr.length;
        int mid = N >> 1;
        HashSet<Integer> leftSum = new HashSet<>();
        HashSet<Integer> rightSum = new HashSet<>();
        // 0...mid-1
        process4(arr, 0, mid, 0, leftSum);
        // mid..N-1
        process4(arr, mid, N, 0, rightSum);
        for (int l : leftSum) {
            if (rightSum.contains(sum - l)) {
                return true;
            }
        }
        return false;
    }

    public static void process4(int[] arr, int i, int end, int pre, HashSet<Integer> ans) {
        if (i == end) {
            ans.add(pre);
        } else {
            process4(arr, i + 1, end, pre, ans);
            process4(arr, i + 1, end, pre + arr[i], ans);
        }
    }

}

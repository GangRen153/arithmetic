package com.zuochengyun.大厂刷题班.code16;

import java.util.Arrays;
import java.util.HashSet;

/**
 * 给定一个正数数组arr，返回arr的子序列不能累加出的最小正数
 * 1）正常怎么做？
 * 2）如果arr中肯定有1这个值，怎么做？
 *
 * 例如：能累加出 1，2，3，6
 * 返回 4
 *
 * @author 钢人
 */
public class Code02_SmallestUnFormedSum {

    public static int unformedSum1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 1;
        }
        HashSet<Integer> set = new HashSet<Integer>();
        process(arr, 0, 0, set);
        int min = Integer.MAX_VALUE;
        for (int i = 0; i != arr.length; i++) {
            min = Math.min(min, arr[i]);
        }
        for (int i = min + 1; i != Integer.MIN_VALUE; i++) {
            if (!set.contains(i)) {
                return i;
            }
        }
        return 0;
    }

    public static void process(int[] arr, int i, int sum, HashSet<Integer> set) {
        if (i == arr.length) {
            set.add(sum);
            return;
        }
        process(arr, i + 1, sum, set);
        process(arr, i + 1, sum + arr[i], set);
    }

    /**
     * 已知arr中肯定有1这个数
     */
    public static int unformedSum3(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        // O (N * logN)
        Arrays.sort(arr);
        int range = 1;
        // 累加和最小正正数一定在 1~N 范围上
        for (int i = 1; i != arr.length; i++) {
            // 排完序之后累加结果一定是往上的
            // 所以只要出现比当前 range 大的数，直接返回，后续不可能累加出比 range 小的数
            if (arr[i] > range + 1) {
                return range + 1;

                // 累加结果比当前 range 的数小，后续累加还是有可能累加出 range 的，继续尝试
            } else {
                range += arr[i];
            }
        }
        return range + 1;
    }

}

package com.zuochengyun.大厂刷题班.code16;

import java.util.Arrays;

/**
 * 给定一个数组，在给定一个 n
 * 问：如果想让数组中的子序列累加和能搞定 1~n 范围上所有的数，那么这个数组中缺多少个数
 *
 * 测试链接：<a href="https://leetcode.com/problems/patching-array/" />
 * @author 钢人
 */
public class Code03_MinPatches {

    /**
     * 例如
     * 1，2，4 -> 能搞定的范围是1~7的范围
     * 1，2，4，8 -> 能搞定的范围是1~15的范围
     * 1，2，4，8，16 -> 能搞定的范围是1~31的范围
     * ...
     */
    public static int minPatches(int[] arr, int aim) {
        // 缺多少个数字
        int patches = 0;
        // 已经完成了1 ~ range的目标（这里是2进制的整倍数范围）
        long range = 0;
        Arrays.sort(arr);
        for (int i = 0; i != arr.length; i++) {
            // 要求：1 ~ arr[i]-1 范围被搞定！
            while (arr[i] - 1 > range) {
                // range + 1 是缺的数字，向右扩充
                range += range + 1;
                patches++;
                if (range >= aim) {
                    return patches;
                }
            }
            // 要求被满足了！
            range += arr[i];
            if (range >= aim) {
                return patches;
            }
        }
        while (aim >= range + 1) {
            range += range + 1;
            patches++;
        }
        return patches;
    }

}

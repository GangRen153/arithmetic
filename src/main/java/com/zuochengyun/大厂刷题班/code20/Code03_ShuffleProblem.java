package com.zuochengyun.大厂刷题班.code20;

import java.util.Arrays;

/**
 * 给定一个长度为偶数的数组arr
 * 分为左部分和右部分，左部分：arr[L1……Ln]，右部分： arr[R1……Rn]
 * 请原地把arr调整成arr[L1,R1,L2,R2,L3,R3,…,Ln,Rn]
 *
 * @author 钢人
 */
public class Code03_ShuffleProblem {

    public static void wiggleSort(int[] arr) {
        if (arr == null || arr.length == 0) {
            return;
        }
        Arrays.sort(arr);
    }

}

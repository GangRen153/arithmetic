package com.zuochengyun.大厂刷题班.code07;

import java.util.HashMap;
import java.util.Map;

/**
 * String str, int K, String[] parts, int[] record
 * str 一定要分割成 k 部分，分割出来的每部分在 parts 里必须得有，那一部分的得分在 record 里
 * 请问，str切成k个部分，最大得分是多少？
 *
 * @author 钢人
 */
public class Code06_SplitStringMaxValue {

    public static int maxRecord1(String str, int K, String[] parts, int[] record) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        Map<String, Integer> records = new HashMap<>();
        for (int i = 0; i < parts.length; i++) {
            records.put(parts[i], record[i]);
        }
        return process(str, 0, K, records);
    }

    public static int process(String str, int index, int rest, Map<String, Integer> records) {
        if (rest < 0) {
            return -1;
        }
        if (index == str.length()) {
            return rest == 0 ? 0 : -1;
        }
        int ans = -1;
        for (int end = index; end < str.length(); end++) {
            // 尝试分解
            String first = str.substring(index, end + 1);
            // 得到分解后的得分
            int next = records.containsKey(first) ? process(str, end + 1, rest - 1, records) : -1;
            // 如果分解是有效的，计算得分
            if (next != -1) {
                ans = Math.max(ans, records.get(first) + next);
            }
        }
        return ans;
    }

    public static int maxRecord2(String str, int K, String[] parts, int[] record) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        HashMap<String, Integer> records = new HashMap<>();
        for (int i = 0; i < parts.length; i++) {
            records.put(parts[i], record[i]);
        }
        int N = str.length();
        int[][] dp = new int[N + 1][K + 1];
        for (int rest = 1; rest <= K; rest++) {
            dp[N][rest] = -1;
        }
        for (int index = N - 1; index >= 0; index--) {
            for (int rest = 0; rest <= K; rest++) {
                int ans = -1;
                for (int end = index; end < N; end++) {
                    String first = str.substring(index, end + 1);
                    int next = rest > 0 && records.containsKey(first) ? dp[end + 1][rest - 1] : -1;
                    if (next != -1) {
                        ans = Math.max(ans, records.get(first) + next);
                    }
                }
                dp[index][rest] = ans;
            }
        }
        return dp[0][K];
    }

}

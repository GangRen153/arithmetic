package com.zuochengyun.大厂刷题班.code07;

import java.util.HashSet;
import java.util.Set;

/**
 * 给定一个有序数组arr，其中值可能为正、负、0
 * 返回arr中每个数都平方之后不同的结果有多少种？
 *
 * 给定一个数组arr，先递减然后递增，
 * 返回arr中有多少个不同的数字？
 *
 * @author 钢人
 */
public class Code04_Power2Diffs {

    public static int diff1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        Set<Integer> set = new HashSet<>();
        for (int cur : arr) {
            set.add(cur * cur);
        }
        return set.size();
    }

    /**
     * 也有就统计数组中每个数的绝对值有多少个不同结果
     */
    public static int diff2(int[] arr) {
        int N = arr.length;
        int L = 0;
        int R = N - 1;
        int count = 0;
        int leftAbs = 0;
        int rightAbs = 0;
        // 用左右指针处理，谁大谁移动
        while (L <= R) {
            count++;
            leftAbs = Math.abs(arr[L]);
            rightAbs = Math.abs(arr[R]);
            // 左边比右边小
            if (leftAbs < rightAbs) {
                // R 是有效指针范围 && R 位置的绝对值等于 rightAbs，说明重复了，左移，直到不重复为止
                while (R >= 0 && Math.abs(arr[R]) == rightAbs) {
                    R--;
                }

                // 左边比右边大
            } else if (leftAbs > rightAbs) {
                // L 是有效指针范围 && L 位置的绝对值等于 leftAbs，说明重复了，右移，直到不重复为止
                while (L < N && Math.abs(arr[L]) == leftAbs) {
                    L++;
                }

                // 说明左右一样大
            } else {
                // 与上同理，指针移动到不重复为止
                while (L < N && Math.abs(arr[L]) == leftAbs) {
                    L++;
                }
                while (R >= 0 && Math.abs(arr[R]) == rightAbs) {
                    R--;
                }
            }
        }
        return count;
    }

}

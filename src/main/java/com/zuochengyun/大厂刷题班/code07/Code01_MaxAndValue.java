package com.zuochengyun.大厂刷题班.code07;

/**
 * 给定一个正数组成的数组，长度一定大于1，求数组中哪两个数与的结果最大
 *
 * @author 钢人
 */
public class Code01_MaxAndValue {

    /**
     * O(N^2)的暴力解
     */
    public static int maxAndValue1(int[] arr) {
        int N = arr.length;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < N; i++) {
            for (int j = i + 1; j < N; j++) {
                max = Math.max(max, arr[i] & arr[j]);
            }
        }
        return max;
    }

    public static int maxAndValue2(int[] arr) {
        // 有效范围：arr[0...M-1]  垃圾区域：arr[M...]
        int M = arr.length;
        int ans = 0;
        for (int bit = 30; bit >= 0; bit--) {
            // arr[0...M-1] arr[M...]
            int i = 0;
            int tmp = M;
            // i~M-1 范围上，找有没有哪个数二进制 bit 位上是1的
            while (i < M) {
                // 如果arr[i]的二进制bit位上不是1，说明算不出最大的，先扩到垃圾区域
                if ((arr[i] & (1 << bit)) == 0) {
                    swap(arr, i, --M);
                } else {// 左边界右移
                    i++;
                }
            }
            // 如果刚好有两个数的二进制bit位上是1，说明这两个数一定是最大的数
            if (M == 2) {
                return arr[0] & arr[1];
            }
            // 如果没有两个数二进制bit位都是1的，那么bit位减1在开始尝试新的取最大值
            if (M < 2) {
                M = tmp;

                // 如果 bit 位上有1的数不止2个，计算结果
            } else {
                ans |= (1 << bit);
            }
        }
        return ans;
    }

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

}

package com.zuochengyun.大厂刷题班.code07;

/**
 * 给定一个数组arr，返回如果排序之后，相邻两数的最大差值
 * 要求：时间复杂度O(N)
 *
 * 思路例如：
 * 桶1:[9]
 * 桶2:[10]
 * 桶3:[]
 * 桶4:[40,49]
 * 桶5:[55]
 *
 * @author 钢人
 */
public class Code03_MaxGap {

    public static int maxGap(int[] nums) {
        if (nums == null || nums.length < 2) {
            return 0;
        }
        int len = nums.length;
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < len; i++) {
            min = Math.min(min, nums[i]);
            max = Math.max(max, nums[i]);
        }
        if (min == max) {
            return 0;
        }

        // 不止一种数，min~max一定有range,  len个数据，准备len+1个桶
        boolean[] hasNum = new boolean[len + 1];
        // maxs[i] i号桶收集的所有数字的最大值
        int[] maxs = new int[len + 1];
        // mins[i] i号桶收集的所有数字的最小值
        int[] mins = new int[len + 1];
        // 桶号
        int bid = 0;
        for (int i = 0; i < len; i++) {
            // 决定 i 位置的数进哪个桶
            bid = bucket(nums[i], len, min, max);
            // 看是否 bid 已经有数了，取最小值
            mins[bid] = hasNum[bid] ? Math.min(mins[bid], nums[i]) : nums[i];
            // 看是否 bid 已经有数了，取最大值
            maxs[bid] = hasNum[bid] ? Math.max(maxs[bid], nums[i]) : nums[i];
            hasNum[bid] = true;
        }
        int res = 0;
        // 上一个非空桶的最大值
        int lastMax = maxs[0];
        int i = 1;
        // 众多桶里面，一定有桶是空的
        for (; i <= len; i++) {
            // 不是空桶
            if (hasNum[i]) {
                res = Math.max(res, mins[i] - lastMax);
                lastMax = maxs[i];
            }
        }
        return res;
    }

    // 如果当前的数是num，整个范围是min~max，分成了len + 1份
    // 返回num该进第几号桶
    public static int bucket(long num, long len, long min, long max) {
        return (int) ((num - min) * len / (max - min));
    }

}

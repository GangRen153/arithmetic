package com.zuochengyun.大厂刷题班.code07;

/**
 * 给定一个二叉树，在这个二叉树节点上放相机
 * 相机覆盖的范围的当前节点+父节点+子节点
 * 问，在这颗二叉树上，最少放几个相机，能把整个二叉树全覆盖
 *
 * 测试链接 : <a href="https://leetcode.com/problems/binary-tree-cameras/"/>
 * @author 钢人
 */
public class Code02_MinCameraCover {

    public static class TreeNode {
        public int value;
        public TreeNode left;
        public TreeNode right;
    }

    // 潜台词：x是头节点，x下方的点都被覆盖的情况下
    public static class Info {
        // x没有被覆盖，x为头的树至少需要几个相机
        public long uncovered;
        // x被相机覆盖，但是x没相机，x为头的树至少需要几个相机
        public long coveredNoCamera;
        // x被相机覆盖了，并且x上放了相机，x为头的树至少需要几个相机
        public long coveredHasCamera;

        public Info(long un, long no, long has) {
            uncovered = un;
            coveredNoCamera = no;
            coveredHasCamera = has;
        }
    }

    public static int minCameraCover1(TreeNode root) {
        Info data = process1(root);
        return (int) Math.min(data.uncovered + 1, Math.min(data.coveredNoCamera, data.coveredHasCamera));
    }

    public static Info process1(TreeNode X) {
        // 如果是 null，x 节点不需要相机，默认认为是被覆盖的
        if (X == null) {
            return new Info(Integer.MAX_VALUE, 0, Integer.MAX_VALUE);
        }
        Info left = process1(X.left);
        Info right = process1(X.right);
        // x uncovered x自己不被覆盖，x下方所有节点，都被覆盖
        // 左孩子： 左孩子没被覆盖，左孩子以下的点都被覆盖
        // 左孩子被覆盖但没相机，左孩子以下的点都被覆盖
        // 左孩子被覆盖也有相机，左孩子以下的点都被覆盖
        long uncovered = left.coveredNoCamera + right.coveredNoCamera;
        // x下方的点都被covered，x也被cover，但x上没相机
        long coveredNoCamera = Math.min(
                left.coveredHasCamera + right.coveredHasCamera,
                Math.min(left.coveredHasCamera + right.coveredNoCamera,
                        left.coveredNoCamera + right.coveredHasCamera));

        // x下方的点都被covered，x也被cover，且x上有相机
        long coveredHasCamera =
                Math.min(left.uncovered, Math.min(left.coveredNoCamera, left.coveredHasCamera))
                        + Math.min(right.uncovered, Math.min(right.coveredNoCamera, right.coveredHasCamera))
                        + 1;
        return new Info(uncovered, coveredNoCamera, coveredHasCamera);
    }


    // 以x为头，x下方的节点都是被covered，x自己的状况，分三种
    public static enum Status {
        /**
         * UNCOVERED：没覆盖
         * COVERED_NO_CAMERA：覆盖了没相机
         * COVERED_HAS_CAMERA：覆盖了有相机
         */
        UNCOVERED, COVERED_NO_CAMERA, COVERED_HAS_CAMERA
    }

    // 以x为头，x下方的节点都是被covered，得到的最优解中：
    // x是什么状态，在这种状态下，需要至少几个相机
    public static class Data {
        public Status status;
        public int cameras;

        public Data(Status status, int cameras) {
            this.status = status;
            this.cameras = cameras;
        }
    }

    public static int minCameraCover2(TreeNode root) {
        Data data = process2(root);
        return data.cameras + (data.status == Status.UNCOVERED ? 1 : 0);
    }

    public static Data process2(TreeNode X) {
        if (X == null) {
            return new Data(Status.COVERED_NO_CAMERA, 0);
        }
        Data left = process2(X.left);
        Data right = process2(X.right);
        // 左孩子和右孩子一共用来多个相机
        int cameras = left.cameras + right.cameras;
        // 左孩子和右孩子有一个没被覆盖，当前节点就得放相机
        if (left.status == Status.UNCOVERED || right.status == Status.UNCOVERED) {
            return new Data(Status.COVERED_HAS_CAMERA, cameras + 1);
        }
        // 左孩子和右孩子有一个放了相机，当前节点就不用放相机了，但是被覆盖
        if (left.status == Status.COVERED_HAS_CAMERA || right.status == Status.COVERED_HAS_CAMERA) {
            return new Data(Status.COVERED_NO_CAMERA, cameras);
        }
        // 那么最后一种可能，左孩子和右孩子都被覆盖了，但是没相机
        return new Data(Status.UNCOVERED, cameras);
    }


}

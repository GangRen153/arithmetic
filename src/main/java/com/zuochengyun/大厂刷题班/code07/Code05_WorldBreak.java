package com.zuochengyun.大厂刷题班.code07;

import java.util.HashSet;
import java.util.Set;

/**
 * 假设所有字符都是小写字母. 大字符串是str. arr是去重的单词表, 每个单词都不是空字符串且可以使用任意次
 * 使用arr中的单词有多少种拼接str的方式. 返回方法数
 *
 * @author 钢人
 */
public class Code05_WorldBreak {

    public static int ways(String str, String[] arr) {
        Set<String> set = new HashSet<>();
        for (String candidate : arr) {
            set.add(candidate);
        }
        return process(str, 0, set);
    }

    public static int process(String str, int i, Set<String> set) {
        // 没字符串需要分解了！
        if (i == str.length()) {
            return 1;
        }
        int ways = 0;
        // 从 i 位置为前缀开始尝试分解，以此右移
        for (int end = i; end < str.length(); end++) {
            String pre = str.substring(i, end + 1);
            // 如果以 i 位置为前缀刚好 set 有，在尝试后面的分解
            if (set.contains(pre)) {
                ways += process(str, end + 1, set);
            }
        }
        return ways;
    }

    public static int ways2(String str, String[] arr) {
        if (str == null || str.length() == 0 || arr == null || arr.length == 0) {
            return 0;
        }
        HashSet<String> map = new HashSet<>();
        for (String s : arr) {
            map.add(s);
        }
        int N = str.length();
        int[] dp = new int[N + 1];
        dp[N] = 1;
        for (int i = N - 1; i >= 0; i--) {
            for (int end = i; end < N; end++) {
                if (map.contains(str.substring(i, end + 1))) {
                    dp[i] += dp[end + 1];
                }
            }
        }
        return dp[0];
    }

}

package com.zuochengyun.大厂刷题班.code30;

/**
 * 问一棵树是否是对称树
 *
 * <a href="https://leetcode.cn/problems/symmetric-tree/" />
 * @author 钢人
 */
public class Problem_0101_SymmetricTree {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
    }

    public boolean isSymmetric(TreeNode root) {
        return isMirror(root, root);
    }

    public static boolean isMirror(TreeNode head1, TreeNode head2) {
        if (head1 == null && head2 == null) {
            return true;
        }
        if (head1 != null && head2 != null) {
            return head1.val == head2.val
                    && isMirror(head1.left, head2.right)
                    && isMirror(head1.right, head2.left);
        }
        // 一个为空，一个不为空  false
        return false;
    }

}

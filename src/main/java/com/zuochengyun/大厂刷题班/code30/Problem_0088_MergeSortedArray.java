package com.zuochengyun.大厂刷题班.code30;

/**
 * 给定两个有序数组arr1，arr2，两个数组都有一个有效长度 m 和 n，将arr2有效长度的数合并到arr1中
 * arr1足够长，将两个数组合后要求集体有序
 *
 * 思路：
 * 归并合并
 *
 * 测试链接：<a href="https://leetcode.cn/problems/merge-sorted-array/" />
 * @author 钢人
 */
public class Problem_0088_MergeSortedArray {

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int index = nums1.length;
        while (m > 0 && n > 0) {
            if (nums1[m - 1] >= nums2[n - 1]) {
                nums1[--index] = nums1[--m];
            } else {
                nums1[--index] = nums2[--n];
            }
        }
        while (m > 0) {
            nums1[--index] = nums1[--m];
        }
        while (n > 0) {
            nums1[--index] = nums2[--n];
        }
    }

}

package com.zuochengyun.大厂刷题班.code30;

/**
 * 验证是否是搜索二叉树
 *
 * 测试链接：<a href="https://leetcode.cn/problems/validate-binary-search-tree/" />
 * @author 钢人
 */
public class Problem_0098_ValidateBinarySearchTree {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
    }

    public boolean isValidBST(TreeNode root) {
        if (root == null) {
            return true;
        }
        TreeNode cur = root;
        TreeNode mostRight = null;
        Integer pre = null;
        boolean ans = true;
        while (cur != null) {
            mostRight = cur.left;
            if (mostRight != null) {
                while (mostRight.right != null && mostRight.right != cur) {
                    mostRight = mostRight.right;
                }
                if (mostRight.right == null) {
                    mostRight.right = cur;
                    cur = cur.left;
                    continue;
                } else {
                    mostRight.right = null;
                }
            }
            if (pre != null && pre >= cur.val) {
                // 不直接返回是为了恢复整棵树
                ans = false;
            }
            pre = cur.val;
            cur = cur.right;
        }
        return ans;
    }

}

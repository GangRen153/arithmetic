package com.zuochengyun.大厂刷题班.code30;

/**
 * 给定一个1~26范围数字字符串，1~26分别对应A~Z，要求转成英文字符串
 * 问有多少种
 *
 * @author 钢人
 */
public class Problem_0091_DecodeWays {

    public static int numDecodings1(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        char[] str = s.toCharArray();
        return process(str, 0);
    }

    public static int process(char[] str, int index) {
        if (index == str.length) {
            return 1;
        }
        if (str[index] == '0') {
            return 0;
        }
        int ways = process(str, index + 1);
        if (index + 1 == str.length) {
            return ways;
        }
        int num = (str[index] - '0') * 10 + str[index + 1] - '0';
        if (num < 27) {
            ways += process(str, index + 2);
        }
        return ways;
    }

    public static int numDecodings2(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        char[] str = s.toCharArray();
        int N = str.length;
        // dp[i] -> process(str, index)返回值 index 0 ~ N
        int[] dp = new int[N + 1];
        dp[N] = 1;

        // dp依次填好 dp[i] dp[i+1] dp[i+2]
        for (int i = N - 1; i >= 0; i--) {
            if (str[i] != '0') {
                dp[i] = dp[i + 1];
                if (i + 1 == str.length) {
                    continue;
                }
                int num = (str[i] - '0') * 10 + str[i + 1] - '0';
                if (num <= 26) {
                    dp[i] += dp[i + 2];
                }
            }
        }
        return dp[0];
    }

}

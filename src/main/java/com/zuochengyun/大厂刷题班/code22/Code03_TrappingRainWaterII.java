package com.zuochengyun.大厂刷题班.code22;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * 接雨水问题
 * <p>
 * 给定一个二维数组，表示一个巨型锅，水只能从上下左右四个位置流失
 * 问这个巨型锅能接多少雨水
 * <p>
 * 测试链接 : <a href="https://leetcode.com/problems/trapping-rain-water-ii/" />
 *
 * @author 钢人
 */
public class Code03_TrappingRainWaterII {

    public static class Node {
        public int value;
        public int row;
        public int col;

        public Node(int v, int r, int c) {
            value = v;
            row = r;
            col = c;
        }
    }

    public static int trapRainWater(int[][] heightMap) {
        if (heightMap == null || heightMap.length == 0 || heightMap[0] == null || heightMap[0].length == 0) {
            return 0;
        }
        int N = heightMap.length;
        int M = heightMap[0].length;
        // 记录哪些点进过堆了
        boolean[][] isEnter = new boolean[N][M];
        // 构建小根堆
        PriorityQueue<Node> heap = new PriorityQueue<>(Comparator.comparingInt(a -> a.value));
        // 把二维数组最外围的四条边的数放到小根堆中
        for (int col = 0; col < M - 1; col++) {
            isEnter[0][col] = true;
            heap.add(new Node(heightMap[0][col], 0, col));
        }
        for (int row = 0; row < N - 1; row++) {
            isEnter[row][M - 1] = true;
            heap.add(new Node(heightMap[row][M - 1], row, M - 1));
        }
        for (int col = M - 1; col > 0; col--) {
            isEnter[N - 1][col] = true;
            heap.add(new Node(heightMap[N - 1][col], N - 1, col));
        }
        for (int row = N - 1; row > 0; row--) {
            isEnter[row][0] = true;
            heap.add(new Node(heightMap[row][0], row, 0));
        }

        int water = 0;
        int max = 0;
        while (!heap.isEmpty()) {
            // 弹出当前最外围最小的一个高度
            Node cur = heap.poll();
            // 看能不能推高高度
            max = Math.max(max, cur.value);
            // 最低高度所在的行和列
            int r = cur.row;
            int c = cur.col;
            // 行的左侧还有数并且没进过小根堆
            if (r > 0 && !isEnter[r - 1][c]) {
                // 计算当前左侧部分能接多少雨水
                water += Math.max(0, max - heightMap[r - 1][c]);
                // 把当前左侧的高度加到堆中，标记
                isEnter[r - 1][c] = true;
                heap.add(new Node(heightMap[r - 1][c], r - 1, c));
            }
            // 行的右侧还有数并且没进过小根堆
            if (r < N - 1 && !isEnter[r + 1][c]) {
                // 同上推论
                water += Math.max(0, max - heightMap[r + 1][c]);
                isEnter[r + 1][c] = true;
                heap.add(new Node(heightMap[r + 1][c], r + 1, c));
            }
            // 列的上侧还有数并且没进过小根堆
            if (c > 0 && !isEnter[r][c - 1]) {
                water += Math.max(0, max - heightMap[r][c - 1]);
                isEnter[r][c - 1] = true;
                heap.add(new Node(heightMap[r][c - 1], r, c - 1));
            }
            // 列的下侧还有数并且没进过小根堆
            if (c < M - 1 && !isEnter[r][c + 1]) {
                water += Math.max(0, max - heightMap[r][c + 1]);
                isEnter[r][c + 1] = true;
                heap.add(new Node(heightMap[r][c + 1], r, c + 1));
            }
        }
        return water;
    }

}

package com.zuochengyun.大厂刷题班.code22;

import java.util.HashMap;

/**
 * 找杆儿拼窗户问题
 * <p>
 * 给定一个数组，找出拼接出来两个等长的杆儿，并且不相容的杆儿（杆儿不能重复使用）
 * 拼出来的杆儿尽量大，杆儿的高度就是窗户的高度
 * 例如：[1,2,3,5]
 * 只能是 2和3拼起来5，5单独一个杆儿，拼起来的窗户最大高度就是5
 * <p>
 * 测试链接 : <a href="https://leetcode.com/problems/tallest-billboard/" />
 *
 * @author 钢人
 */
public class Code05_TallestBillboard {

    public int tallestBillboard(int[] rods) {
        // 记录 key 的差值
        HashMap<Integer, Integer> dp = new HashMap<>(), cur;
        dp.put(0, 0);
        for (int num : rods) {
            if (num != 0) {
                // cur 内部数据完全和dp一样
                cur = new HashMap<>(dp);
                for (int d : cur.keySet()) {
                    // 最好的一对，较小集合的累加和
                    int diffMore = cur.get(d);
                    // x决定放入，比较大的那个
                    dp.put(d + num, Math.max(diffMore, dp.getOrDefault(num + d, 0)));
                    // x决定放入，比较小的那个
                    // 新的差值 Math.abs(x - d)
                    // 之前差值为Math.abs(x - d)，的那一对，就要和这一对，决策一下
                    // 之前那一对，较小集合的累加和diffXD
                    int diffXD = dp.getOrDefault(Math.abs(num - d), 0);
                    // x决定放入比较小的那个, 但是放入之后，没有超过这一对较大的那个
                    if (d >= num) {
                        dp.put(d - num, Math.max(diffMore + num, diffXD));
                    } else { // x决定放入比较小的那个, 但是放入之后，没有超过这一对较大的那个
                        dp.put(num - d, Math.max(diffMore + d, diffXD));
                    }
                }
            }
        }
        return dp.get(0);
    }

}

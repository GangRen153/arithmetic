package com.zuochengyun.大厂刷题班.code22;

/**
 * 接雨水问题
 *
 * 给定一个一维数组，数组中的值代表高度
 * 求在这个数组范围上，在这个数组上最多能接多少雨水
 *
 * 例如：
 * [3,1,5,7,3,6]
 * 3~5能接2点雨水，7~6能解3点雨水
 *
 * 测试链接 : <a href="https://leetcode.com/problems/trapping-rain-water/" />
 * @author 钢人
 */
public class Code02_TrappingRainWater {

    public static int trap(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        int N = arr.length;
        // L 从 1 开始，因为 0 位置接不到雨水
        int L = 1;
        // 左边最大高度
        int leftMax = arr[0];
        // R 从倒数第二个位置开始，因为最后一个位置也接不到雨水
        int R = N - 2;
        // 右边最大高度
        int rightMax = arr[N - 1];
        // 统计雨水数量
        int water = 0;
        while (L <= R) {
            // 如果左边比右边低
            if (leftMax <= rightMax) {
                // 计算水量，arr[L] 位置是中间接水的位置
                // 左边做高位置减去后面接水位置的高度就是允许接水的水量
                water += Math.max(0, leftMax - arr[L]);
                // 求左侧的最高位置，看接水的位置和当前左侧最高位置哪个高
                leftMax = Math.max(leftMax, arr[L++]);
            } else {
                // 与上方一样，只是在计算右侧的接水位置，arr[R] 也是接水位置的高度
                water += Math.max(0, rightMax - arr[R]);
                // 右侧往左侧靠，看高度是多少
                rightMax = Math.max(rightMax, arr[R--]);
            }
        }
        return water;
    }

}

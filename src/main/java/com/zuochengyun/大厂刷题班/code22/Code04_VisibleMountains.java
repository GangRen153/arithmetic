package com.zuochengyun.大厂刷题班.code22;

import java.util.Stack;

/**
 * 可见山峰对儿，自己和自己是看不见的
 *
 * 例如：[1，3，5，3，7，3，7]
 * 7<-1->5，3->5，5->7，5<-3->7，1<-7->7，7<-3->7，1<-7->7
 *
 * @author 钢人
 */
public class Code04_VisibleMountains {

    public static class Record {
        public int value;
        public int times;

        public Record(int value) {
            this.value = value;
            this.times = 1;
        }
    }

    public static int getVisibleNum(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        int N = arr.length;
        int maxIndex = 0;
        // 先在环中找到其中一个最大值的位置，哪一个都行
        for (int i = 0; i < N; i++) {
            maxIndex = arr[maxIndex] < arr[i] ? i : maxIndex;
        }
        Stack<Record> stack = new Stack<Record>();
        // 先把(最大值,1)这个记录放入stack中
        stack.push(new Record(arr[maxIndex]));
        // 从最大值位置的下一个位置开始沿next方向遍历
        int index = nextIndex(maxIndex, N);
        // 用“小找大”的方式统计所有可见山峰对
        int res = 0;
        // 遍历阶段开始，当index再次回到maxIndex的时候，说明转了一圈，遍历阶段就结束
        while (index != maxIndex) {
            while (stack.peek().value < arr[index]) {
                int k = stack.pop().times;
                res += getInternalSum(k) + 2 * k;
            }
            // 当前数字arr[index]要进入栈了，如果和当前栈顶数字一样就合并
            // 不一样就把记录(arr[index],1)放入栈中
            if (stack.peek().value == arr[index]) {
                stack.peek().times++;
            } else { // >
                stack.push(new Record(arr[index]));
            }
            index = nextIndex(index, N);
        }

        // 清算阶段开始了
        // 清算阶段的第1小阶段
        while (stack.size() > 2) {
            int times = stack.pop().times;
            res += getInternalSum(times) + 2 * times;
        }

        // 清算阶段的第2小阶段
        if (stack.size() == 2) {
            int times = stack.pop().times;
            res += getInternalSum(times)
                    + (stack.peek().times == 1 ? times : 2 * times);
        }

        // 清算阶段的第3小阶段
        res += getInternalSum(stack.pop().times);
        return res;
    }

    /**
     * 环形数组中当前位置为i，数组长度为size，返回i的下一个位置
     */
    public static int nextIndex(int i, int size) {
        return i < (size - 1) ? (i + 1) : 0;
    }

    public static int getInternalSum(int k) {
        return k == 1 ? 0 : (k * (k - 1) / 2);
    }


    /**
     * 测试方法
     */
    public static boolean isVisible(int[] arr, int lowIndex, int highIndex) {
        // “大找小”的情况直接返回false
        if (arr[lowIndex] > arr[highIndex]) {
            return false;
        }
        int size = arr.length;
        boolean walkNext = true;
        int mid = nextIndex(lowIndex, size);
        while (mid != highIndex) {
            if (arr[mid] > arr[lowIndex]) {
                // next方向失败
                walkNext = false;
                break;
            }
            mid = nextIndex(mid, size);
        }

        boolean walkLast = true;
        mid = lastIndex(lowIndex, size);

        // lowIndex通过last方向走到highIndex，沿途不能出现比arr[lowIndex]大的数
        while (mid != highIndex) {
            if (arr[mid] > arr[lowIndex]) {
                // last方向失败
                walkLast = false;
                break;
            }
            mid = lastIndex(mid, size);
        }
        // 有一个成功就是能相互看见
        return walkNext || walkLast;
    }

    /**
     * 环形数组中当前位置为i，数组长度为size，返回i的上一个位置
     */
    public static int lastIndex(int i, int size) {
        return i > 0 ? (i - 1) : (size - 1);
    }
}

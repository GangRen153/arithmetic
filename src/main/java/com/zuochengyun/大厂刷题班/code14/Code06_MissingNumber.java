package com.zuochengyun.大厂刷题班.code14;

/**
 * 给定一个数组，有正有负有0，返回缺失的最小正整数
 * 例如：[1,3,4,6]
 * 返回 2
 *
 * 时间复杂度 O(N)，空间复杂度O(1)
 *
 * 测试链接：<a href="https://leetcode.com/problems/first-missing-positive/" />
 * @author 钢人
 */
public class Code06_MissingNumber {

    public static int firstMissingPositive(int[] arr) {
        int L = 0;
        int R = arr.length;
        while (L != R) {
            // L 位置的数刚好是 L+1，右移
            // 也就是尽可能让 arr[0]=1,arr[1]=2,arr[2]=3 ... arr[L]=L+1
            // 如果本身 L 位置已经是 L 了，说明这是个有效数，往右扩
            if (arr[L] == L + 1) {
                L++;

                // R 向左移，与 L 位置交换，让下次 while 循环继续让 L 位置尝试跟 L+1 比
                // 把垃圾数字（负数或0，或者比L位置大的数）让最右侧移动
                // arr[L] <= L:表示已经收集过了
                // || 数组中的数比 R 还大
                // || L 位置的数应该去的位置已经有了一个 L 了（比如，17 位置有个27，按正常来说27应该去 26 位置，但是 26 位置上已经有了一个27了，说明是垃圾数字）
            } else if (arr[L] <= L || arr[L] > R || arr[arr[L] - 1] == arr[L]) {
                // 把垃圾数扔到最右侧，R向左扩
                swap(arr, L, --R);

                // 剩下一种情况就是，L 位置的数应该去的位置不是上，不是 L 位置的数，直接交换
                // 例如：17 位置是个27，27应该去 26 位置，26 位置不是27，那么就将 17 位置上的数和 26 位置的数交换
            } else {
                swap(arr, L, arr[L] - 1);
            }
        }
        return L + 1;
    }

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

}

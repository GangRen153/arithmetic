package com.zuochengyun.大厂刷题班.code14;

/**
 * 给定一个搜索二叉树
 * 但是有两个节点顺序是错的，如何调对
 *
 * 测试链接 : <a href="https://leetcode.com/problems/recover-binary-search-tree/" />
 * @author 钢人
 */
public class Code05_RecoverBinarySearchTree {

    public static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int v) {
            val = v;
        }
    }

    public static void recoverTree(TreeNode root) {
        // TODO
    }

}

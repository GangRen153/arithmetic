package com.zuochengyun.大厂刷题班.code14;

import java.util.TreeSet;

/**
 * 请返回arr中，求子数组的累加和，是<=K的并且是最大的
 * 返回这个最大的累加和
 *
 * @author 钢人
 */
public class Code02_MaxSubArraySumLessOrEqualK {

    public static int getMaxLessOrEqualK(int[] arr, int K) {
        // 记录i之前的，前缀和，按照有序表组织
        TreeSet<Integer> set = new TreeSet<>();
        // 一个数也没有的时候，就已经有一个前缀和是0了
        set.add(0);
        int max = Integer.MIN_VALUE;
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            // 计算累加和
            sum += arr[i];
            // 返回等于或大于给定元素的最小元素
            if (set.ceiling(sum - K) != null) {
                max = Math.max(max, sum - set.ceiling(sum - K));
            }
            // 当前的前缀和加入到set中去
            set.add(sum);
        }
        return max;
    }

}

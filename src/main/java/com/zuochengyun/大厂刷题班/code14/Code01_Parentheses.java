package com.zuochengyun.大厂刷题班.code14;

/**
 * 给定一个只由左括号和右括号的字符串
 * 返回最长的有效括号子串的长度
 *
 * 测试链接 : <a href="https://leetcode.com/problems/longest-valid-parentheses/" />
 * @author 钢人
 */
public class Code01_Parentheses {

    public static int longestValidParentheses(String s) {
        if (s == null || s.length() < 2) {
            return 0;
        }
        char[] str = s.toCharArray();
        // dp[i] : 子串必须以i位置结尾的情况下，往左最远能扩出多长的有效区域
        int[] dp = new int[str.length];

        int pre = 0;
        int ans = 0;

        for (int i = 1; i < str.length; i++) {
            if (str[i] == ')') {
                // 找到前一个位置的（ 括号
                pre = i - dp[i - 1] - 1;
                if (pre >= 0 && str[pre] == '(') {
                    dp[i] = dp[i - 1] + 2 + (pre > 0 ? dp[pre - 1] : 0);
                }
            }
            // 与之前比较取较大的
            ans = Math.max(ans, dp[i]);
        }
        return ans;
    }

}

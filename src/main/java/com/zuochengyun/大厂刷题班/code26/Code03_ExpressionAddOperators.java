package com.zuochengyun.大厂刷题班.code26;

import java.util.LinkedList;
import java.util.List;

/**
 * 给定一个数字字符串，在给定一个target
 * 可以在数字中间加入 +、-、* 三种符合，三种符号怎么加结果等于 target
 * 返回所有可能
 * <p>
 * 例如：
 * "123" target = 6
 * 1+2+3 -> 6
 * 1*2+3 -> 6
 * <p>
 * 测试链接 : <a href="https://leetcode.com/problems/expression-add-operators/" />
 *
 * @author 钢人
 */
public class Code03_ExpressionAddOperators {

    public static List<String> addOperators(String num, int target) {
        List<String> ret = new LinkedList<>();
        if (num.length() == 0) {
            return ret;
        }
        // 沿途 num 字符串每个空都填符号的情况下，最长字符数组
        char[] path = new char[num.length() * 2 - 1];
        char[] digits = num.toCharArray();
        long n = 0;
        // 0~i 部分作为第一部分
        for (int i = 0; i < digits.length; i++) {
            n = n * 10 + digits[i] - '0';
            path[i] = digits[i];
            dfs(ret, path, i + 1, 0, n, digits, i + 1, target);
            if (n == 0) {
                break;
            }
        }
        return ret;
    }

    /**
     * @param res   收集的答案
     * @param path  沿途填的符号（数学计算公式）
     * @param len   沿途长度
     * @param left  左侧第一部分的值
     * @param cur   当前部分的数（也就是left后面部分的数）
     * @param num   原始 num 字符数组
     * @param index 当前走到了 num 的 index 位置
     * @param aim   target 值
     */
    public static void dfs(List<String> res, char[] path, int len,
                           long left, long cur,
                           char[] num, int index, int aim) {
        // 到了结束位置的时候，计算结果
        if (index == num.length) {
            if (left + cur == aim) {
                res.add(new String(path, 0, len));
            }
            return;
        }

        // 当前 i~index 部分的值
        long n = 0;
        int j = len + 1;
        // 尝试每一中 cur 结果
        for (int i = index; i < num.length; i++) {
            // 当前 i~index 部分的值
            n = n * 10 + num[i] - '0';
            // 当前num
            path[j++] = num[i];
            path[len] = '+';
            dfs(res, path, j, left + cur, n, num, i + 1, aim);
            path[len] = '-';
            dfs(res, path, j, left + cur, -n, num, i + 1, aim);
            path[len] = '*';
            dfs(res, path, j, left, cur * n, num, i + 1, aim);
            if (num[index] == '0') {
                break;
            }
        }
    }
}

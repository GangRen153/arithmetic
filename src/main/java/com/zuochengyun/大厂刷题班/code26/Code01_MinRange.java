package com.zuochengyun.大厂刷题班.code26;

import java.util.TreeSet;

/**
 * 最小包含区间
 *
 * 输入：nums = [[4,10,15,24,26], [0,9,12,20], [5,18,22,30]]
 * 输出：[20,24]
 * 解释：
 * 列表 1：[4, 10, 15, 24, 26]，24 在区间 [20,24] 中。
 * 列表 2：[0, 9, 12, 20]，20 在区间 [20,24] 中。
 * 列表 3：[5, 18, 22, 30]，22 在区间 [20,24] 中。
 *
 * 测试链接 : <a href="https://leetcode.com/problems/smallest-range-covering-elements-from-k-lists/" />
 * @author 钢人
 */
public class Code01_MinRange {

    public static class Node {
        public int val;
        public int arrIdx;
        public int idx;

        public Node(int value, int arrIndex, int index) {
            val = value;
            arrIdx = arrIndex;
            idx = index;
        }
    }

    public static int minRange1(int[][] m) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < m[0].length; i++) {
            for (int j = 0; j < m[1].length; j++) {
                for (int k = 0; k < m[2].length; k++) {
                    min = Math.min(min,
                            Math.abs(m[0][i] - m[1][j]) + Math.abs(m[1][j] - m[2][k]) + Math.abs(m[2][k] - m[0][i]));
                }
            }
        }
        return min;
    }

    public static int minRange2(int[][] matrix) {
        int N = matrix.length;
        TreeSet<Node> set = new TreeSet<>((o1, o2) -> o1.val != o2.val ? (o1.val - o2.val) : (o1.arrIdx - o2.arrIdx));
        for (int i = 0; i < N; i++) {
            set.add(new Node(matrix[i][0], i, 0));
        }
        int min = Integer.MAX_VALUE;
        while (set.size() == N) {
            min = Math.min(min, set.last().val - set.first().val);
            // 弹出第一个
            Node cur = set.pollFirst();
            if (cur.idx < matrix[cur.arrIdx].length - 1) {
                set.add(new Node(matrix[cur.arrIdx][cur.idx + 1], cur.arrIdx, cur.idx + 1));
            }
        }
        return min << 1;
    }

}

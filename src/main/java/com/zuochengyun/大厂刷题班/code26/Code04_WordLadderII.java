package com.zuochengyun.大厂刷题班.code26;

import java.util.*;

/**
 * 给定一个 start 字符串，在给定一个 end 字符串，还有一个单词表
 * 单词表中每个字符串的长度和 start、end 字符串的长度都一样，并且单词表里一定包含 end 字符串
 * start 字符串每次只能改变一个字符，改变完之后的字符串一定要在单词表里存在
 * 最终要将 start 字符串变成 end 字符串
 * 问：如果有这样一个变换路径，返回最短变换路径，也就是至少要变几次
 * <p>
 * 测试链接 : <a href="https://leetcode.com/problems/word-ladder-ii/" />
 *
 * @author 钢人
 */
public class Code04_WordLadderII {

    public static List<List<String>> findLadders(String start, String end, List<String> list) {
        list.add(start);
        // 生成每一个字符串 word 的邻居表，也就是哪些字符串边一个字符就能变成 word
        HashMap<String, List<String>> nexts = getNexts(list);
        // 生成距离表
        HashMap<String, Integer> distances = getDistances(start, nexts);
        // 沿途演变路径过程
        LinkedList<String> pathList = new LinkedList<>();
        // 收集演变可能性
        List<List<String>> res = new ArrayList<>();
        getShortestPaths(start, end, nexts, distances, pathList, res);
        return res;
    }

    // cur 当前来到的字符串 可变
    // to 目标，固定参数
    // nexts 每一个字符串的邻居表
    // cur 到开头距离5 -> 到开头距离是6的支路 distances距离表
    // path : 来到cur之前，深度优先遍历之前的历史是什么
    // res : 当遇到cur，把历史，放入res，作为一个结果
    public static void getShortestPaths(String cur, String end, HashMap<String, List<String>> nexts,
                                        HashMap<String, Integer> distances, LinkedList<String> path, List<List<String>> res) {
        path.add(cur);
        if (end.equals(cur)) {
            res.add(new LinkedList<>(path));
        } else {
            // 遍历 cur 的邻居表
            for (String next : nexts.get(cur)) {
                if (distances.get(next) == distances.get(cur) + 1) {
                    // 尝试 next 字符串变成 end 字符串
                    getShortestPaths(next, end, nexts, distances, path, res);
                }
            }
        }
        path.pollLast();
    }

    public static HashMap<String, Integer> getDistances(String start, HashMap<String, List<String>> nexts) {
        HashMap<String, Integer> distances = new HashMap<>();
        // start 走到 end 默认是 0
        distances.put(start, 0);
        Queue<String> queue = new LinkedList<>();
        queue.add(start);
        HashSet<String> set = new HashSet<>();
        set.add(start);
        while (!queue.isEmpty()) {
            String cur = queue.poll();
            // 遍历 cur 的邻居表
            for (String next : nexts.get(cur)) {
                // 如果出现了不包含，看到这个单词的距离是多少
                if (!set.contains(next)) {
                    distances.put(next, distances.get(cur) + 1);
                    queue.add(next);
                    set.add(next);
                }
            }
        }
        return distances;
    }

    public static HashMap<String, List<String>> getNexts(List<String> words) {
        HashSet<String> dict = new HashSet<>(words);
        HashMap<String, List<String>> nexts = new HashMap<>();
        for (int i = 0; i < words.size(); i++) {
            // 生成每个 word 单词的邻居表
            nexts.put(words.get(i), getNext(words.get(i), dict));
        }
        return nexts;
    }

    public static List<String> getNext(String word, HashSet<String> dict) {
        ArrayList<String> res = new ArrayList<String>();
        char[] chs = word.toCharArray();
        // 遍历 a~z 每个字符
        for (char cur = 'a'; cur <= 'z'; cur++) {
            // 尝试变换每个位置的字符，变成 cur 字符
            for (int i = 0; i < chs.length; i++) {
                // 但是不能变成 cur 自己，不然就重复了
                if (chs[i] != cur) {
                    char tmp = chs[i];
                    // word i 位置替换成 cur 字符
                    chs[i] = cur;
                    // 替换完之后看有没有邻居字符串，如果有则添加到 word 邻居表里
                    if (dict.contains(String.valueOf(chs))) {
                        res.add(String.valueOf(chs));
                    }
                    chs[i] = tmp;
                }
            }
        }
        // 返回 word 单词的邻居表
        return res;
    }

}

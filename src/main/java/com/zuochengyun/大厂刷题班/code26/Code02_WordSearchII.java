package com.zuochengyun.大厂刷题班.code26;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * 给地一个二维字符数组，在给定一堆单词
 * 从二维表中某个位置开始，可以上下左右去走，不能走回头路
 * 问，哪些字符能走出来，哪些走不出来
 *
 * 思路：
 * 把字符串转成前缀树，尝试去走前缀树，而不是循环遍历尝试每个字符串
 *
 * 测试链接 : <a href="https://leetcode.com/problems/word-search-ii/" />
 * @author 钢人
 */
public class Code02_WordSearchII {

    public static class TrieNode {
        // 下一个出现的字符链路
        public TrieNode[] nexts;
        // 经过这个字符的个数
        public int pass;
        // 结束字符
        public boolean end;

        public TrieNode() {
            nexts = new TrieNode[26];
            pass = 0;
            end = false;
        }

    }

    public static List<String> findWords(char[][] board, String[] words) {
        // 前缀树最顶端的头
        TrieNode head = new TrieNode();
        HashSet<String> set = new HashSet<>();
        // 生成前缀树
        for (String word : words) {
            if (!set.contains(word)) {
                fillWord(head, word);
                set.add(word);
            }
        }
        // 结果
        List<String> ans = new ArrayList<>();
        // 沿途走过的字符，收集起来，存在path里
        LinkedList<Character> path = new LinkedList<>();
        // 开始尝试
        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board[0].length; col++) {
                process(board, row, col, path, head, ans);
            }
        }
        return ans;
    }

    public static int process(char[][] board, int row, int col, LinkedList<Character> path, TrieNode cur, List<String> res) {
        char cha = board[row][col];
        // 如果当前位置字符走过了，直接返回0
        if (cha == 0) {
            return 0;
        }

        int index = cha - 'a';
        // 说明没有单词字符途径这个路径，直接结束
        if (cur.nexts[index] == null || cur.nexts[index].pass == 0) {
            return 0;
        }
        // 没有走回头路且能登上去
        cur = cur.nexts[index];
        // 把当前走到的字符加到path里
        path.addLast(cha);
        // 当前字符后续搞定几个单词
        int fix = 0;
        // 是不是有字符串以当前字符结尾
        if (cur.end) {
            // 把途径的字符转成字符串，得到一个答案
            res.add(generatePath(path));
            cur.end = false;
            fix++;
        }
        // 走到的当前字符设置标志位
        board[row][col] = 0;
        // 往上、下、左、右，四个方向尝试
        if (row > 0) {
            fix += process(board, row - 1, col, path, cur, res);
        }
        if (row < board.length - 1) {
            fix += process(board, row + 1, col, path, cur, res);
        }
        if (col > 0) {
            fix += process(board, row, col - 1, path, cur, res);
        }
        if (col < board[0].length - 1) {
            fix += process(board, row, col + 1, path, cur, res);
        }
        // 恢复现场
        board[row][col] = cha;
        // 这个字符走过了，回退的时候要恢复现场
        path.pollLast();
        // pass 减去途径搞定多少个字符串
        cur.pass -= fix;
        return fix;
    }

    public static String generatePath(LinkedList<Character> path) {
        char[] str = new char[path.size()];
        int index = 0;
        for (Character cha : path) {
            str[index++] = cha;
        }
        return String.valueOf(str);
    }

    public static void fillWord(TrieNode head, String word) {
        head.pass++;
        char[] chs = word.toCharArray();
        int index = 0;
        TrieNode node = head;
        for (int i = 0; i < chs.length; i++) {
            index = chs[i] - 'a';
            if (node.nexts[index] == null) {
                node.nexts[index] = new TrieNode();
            }
            node = node.nexts[index];
            node.pass++;
        }
        node.end = true;
    }

}

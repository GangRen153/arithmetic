package com.zuochengyun.大厂刷题班.code24;

/**
 * 吉祥里程表题
 * <p>
 * 正常的里程表是 1，2，3，4，5，6，7，8，9，10，11，12，13...
 * 但是要求里程里的数字不包含4的里程数
 * 给定一个 num，返回不含有4的真实历程表里，有多少个数
 * 例如：33
 * 返回从 1~33 的里程数，但不包含 4 的历程表里有多少个数
 * 答案是 30 个数，如果从 0 开始，则是 31 个数
 *
 * @author 钢人
 */
public class Code03_NotContains4 {

    public static long[] arr = { 0L, 1L, 9L, 81L, 729L, 6561L, 59049L, 531441L, 4782969L, 43046721L, 387420489L,
            3486784401L, 31381059609L, 282429536481L, 2541865828329L, 22876792454961L, 205891132094649L,
            1853020188851841L, 16677181699666569L, 150094635296999121L, 1350851717672992089L };

    public static long notContains4Nums2(long num) {
        if (num <= 0) {
            return 0;
        }
        // num的十进制位数，len
        int len = decimalLength(num);
        // 35621
        // 10000
        long offset = offset(len);

        // 第一位数字
        long first = num / offset;
        return arr[len] - 1 + (first - (first < 4 ? 1 : 2)) * arr[len] + process(num % offset, offset / 10, len - 1);
    }

    // num之前，有开头！
    // 在算0的情况下，num是第几个数字
    // num 76217
    // 10000
    // 6217
    // 1000
    // len
    public static long process(long num, long offset, int len) {
        if (len == 0) {
            return 1;
        }
        long first = num / offset;
        return (first < 4 ? first : (first - 1)) * arr[len] + process(num % offset, offset / 10, len - 1);
    }

    /**
     * num的十进制位数
     * 比如 num = 7653210，返回 7
     */
    public static int decimalLength(long num) {
        int len = 0;
        while (num != 0) {
            len++;
            num /= 10;
        }
        return len;
    }

    /**
     * 给定一个长度，返回以 1 开头，后面都是 0 的长度是 len 位数的正数
     * 例如：len = 4，返回 1000
     */
    public static long offset(int len) {
        long offset = 1;
        for (int i = 1; i < len; i++) {
            offset *= 10L;
        }
        return offset;
    }

}

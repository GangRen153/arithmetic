package com.zuochengyun.大厂刷题班.code24;

import java.util.HashMap;

/**
 * 给定一个数组长度一定大于6，砍成四部分，砍掉的数就没了
 * 并且每个部分都得有数要求累加和一样大
 * 返回能否切成这样的结构
 *
 * 思路：
 * 先切第一刀，后面切的时候所切的位置同时要等于每切一刀的累加和
 * 如果发现切不出来，第一刀往后切一位，再次尝试后面的切法
 *
 * @author 钢人
 */
public class Code01_Split4Parts {

    public static boolean canSplits2(int[] arr) {
        if (arr == null || arr.length < 7) {
            return false;
        }
        // key 某一个累加和， value出现的位置
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        int sum = arr[0];
        for (int i = 1; i < arr.length; i++) {
            map.put(sum, i);
            sum += arr[i];
        }
        // 第一刀左侧的累加和
        int lsum = arr[0];
        // s1是第一刀的位置
        for (int s1 = 1; s1 < arr.length - 5; s1++) {
            int checkSum = lsum * 2 + arr[s1];
            if (map.containsKey(checkSum)) {
                int s2 = map.get(checkSum);
                checkSum += lsum + arr[s2];
                if (map.containsKey(checkSum)) {
                    int s3 = map.get(checkSum);
                    if (checkSum + arr[s3] + lsum == sum) {
                        return true;
                    }
                }
            }
            lsum += arr[s1];
        }
        return false;
    }

}

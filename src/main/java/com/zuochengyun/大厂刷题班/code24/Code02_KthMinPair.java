package com.zuochengyun.大厂刷题班.code24;

/**
 * 给定一个数组 arr，任意两个数组成一对儿
 * 这个数组一定一个组成 N² 个数字对儿，自己和自己也算一对儿
 * 问，这些数字对儿经过排序后，返回低 K 小的数字对儿
 *
 * 思路：
 * 排序数组
 * 0 位置做为第一维数的组合一定可以解决前 1~N 个最小数
 * 1 位置做为第一维数的组合一定可以解决前 N+1~2*N 个最小数
 * 2 位置做为第一维数的组合一定可以解决前 2*N+1~3*N 个最小数
 * 3 位置做为第一维数的组合一定可以解决前 3*N+1~4*N 个最小数
 * ...
 * 以此类推，找到第 K 小的数，先找到以一维数是几的范围上，在找第二维的数是几
 *
 * @author 钢人
 */
public class Code02_KthMinPair {

    // O(N)的复杂度，你肯定蒙了
    public static int[] kthMinPair3(int[] arr, int k) {
        int N = arr.length;
        if (k > N * N) {
            return null;
        }
        // 在无序数组中，找到第K小的数，返回值
        // 第K小，以1作为开始
        int firstNum = getMinKth(arr, (k - 1) / N);
        // 第1维数字
        int lessFristNumSize = 0;
        int firstNumSize = 0;
        for (int i = 0; i < N; i++) {
            if (arr[i] < firstNum) {
                lessFristNumSize++;
            }
            if (arr[i] == firstNum) {
                firstNumSize++;
            }
        }
        int rest = k - (lessFristNumSize * N);
        return new int[] { firstNum, getMinKth(arr, (rest - 1) / firstNumSize) };
    }

    // 改写快排，时间复杂度O(N)
    // 在无序数组arr中，找到，如果排序的话，arr[index]的数是什么？
    public static int getMinKth(int[] arr, int index) {
        int L = 0;
        int R = arr.length - 1;
        int pivot = 0;
        int[] range;
        while (L < R) {
            pivot = arr[L + (int) (Math.random() * (R - L + 1))];
            range = partition(arr, L, R, pivot);
            if (index < range[0]) {
                R = range[0] - 1;
            } else if (index > range[1]) {
                L = range[1] + 1;
            } else {
                return pivot;
            }
        }
        return arr[L];
    }

    public static int[] partition(int[] arr, int L, int R, int pivot) {
        int less = L - 1;
        int more = R + 1;
        int cur = L;
        while (cur < more) {
            if (arr[cur] < pivot) {
                swap(arr, ++less, cur++);
            } else if (arr[cur] > pivot) {
                swap(arr, cur, --more);
            } else {
                cur++;
            }
        }
        return new int[] { less + 1, more - 1 };
    }

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

}

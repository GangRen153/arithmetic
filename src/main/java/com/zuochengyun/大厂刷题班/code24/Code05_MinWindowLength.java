package com.zuochengyun.大厂刷题班.code24;

/**
 * 最小包含子串
 * 给定字符串str1和字符串str2
 * 在str1中最小子串能包含str2的所有字符
 *
 * 例如
 * str1:FFFFbcacsFFFF，str2:bacs
 * 返回 4，str1的 bcacs 能包含 str2 的所有字符
 *
 * 测试链接 : <a href="https://leetcode.com/problems/minimum-window-substring/" />
 * @author 钢人
 */
public class Code05_MinWindowLength {

    public static int minLength(String s1, String s2) {
        if (s1 == null || s2 == null || s1.length() < s2.length()) {
            return Integer.MAX_VALUE;
        }

        char[] str1 = s1.toCharArray();
        char[] str2 = s2.toCharArray();
        int[] map = new int[256];
        for (int i = 0; i != str2.length; i++) {
            map[str2[i]]++;
        }
        int all = str2.length;

        int L = 0;
        int R = 0;
        int minLen = Integer.MAX_VALUE;
        while (R != str1.length) {
            map[str1[R]]--;
            if (map[str1[R]] >= 0) {
                all--;
            }
            // str2 字符全抵消完了
            if (all == 0) {
                // 看最左侧的字符有没有被抵消成 0，如果是抵消成0了，还回来，往左移
                while (map[str1[L]] < 0) {
                    map[str1[L++]]++;
                }
                // 比较之前得到的最小窗口范围
                minLen = Math.min(minLen, R - L + 1);
                all++;
                // 左侧边界从下一个位置作为开始位置尝试
                map[str1[L++]]++;
            }
            R++;
        }
        return minLen == Integer.MAX_VALUE ? 0 : minLen;
    }

}

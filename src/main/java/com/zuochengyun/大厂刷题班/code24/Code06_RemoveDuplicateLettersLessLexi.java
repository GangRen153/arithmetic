package com.zuochengyun.大厂刷题班.code24;

/**
 * 给定一个字符串
 * 每种字符只保留一个，在所有可能的字符中，返回字典序最小的一个
 *
 * 比如：
 * abba
 * 可以删成ab 或 ba，ba的字典序就不如 ab，所以返回 ab
 *
 * 测试链接 : <a href="https://leetcode.com/problems/remove-duplicate-letters/" />
 * @author 钢人
 */
public class Code06_RemoveDuplicateLettersLessLexi {

    public static String removeDuplicateLetters1(String str) {
        if (str == null || str.length() < 2) {
            return str;
        }
        int[] map = new int[256];
        for (int i = 0; i < str.length(); i++) {
            map[str.charAt(i)]++;
        }
        int minACSIndex = 0;
        for (int i = 0; i < str.length(); i++) {
            minACSIndex = str.charAt(minACSIndex) > str.charAt(i) ? i : minACSIndex;
            if (--map[str.charAt(i)] == 0) {
                break;
            }
        }
        // 0...break(之前) minACSIndex
        // str[minACSIndex] 剩下的字符串str[minACSIndex+1...] -> 去掉str[minACSIndex]字符 -> s'
        // s'...
        return str.charAt(minACSIndex) + removeDuplicateLetters1(
                str.substring(minACSIndex + 1).replaceAll(String.valueOf(str.charAt(minACSIndex)), ""));
    }

}

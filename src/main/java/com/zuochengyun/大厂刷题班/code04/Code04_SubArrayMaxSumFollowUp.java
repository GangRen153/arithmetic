package com.zuochengyun.大厂刷题班.code04;

/**
 * 美团原题
 *
 * 返回一个数组中，选择的数字不能相邻的情况下（也就是不能连续选择两个要的数）
 * 最大子序列累加和
 *
 * @author 钢人
 */
public class Code04_SubArrayMaxSumFollowUp {

    public static int maxSum(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int N = arr.length;
        if (N == 1) {
            return arr[0];
        }
        if (N == 2) {
            return Math.max(arr[0], arr[1]);
        }
        int[] dp = new int[N];
        dp[0] = arr[0];
        dp[1] = Math.max(arr[0], arr[1]);
        for (int i = 2; i < N; i++) {
            // i-1 位置的数 比较 (i)+(i-2)位置的数，谁大要谁
            dp[i] = Math.max(Math.max(dp[i - 1], arr[i]), arr[i] + dp[i - 2]);
        }
        return dp[N - 1];
    }

}

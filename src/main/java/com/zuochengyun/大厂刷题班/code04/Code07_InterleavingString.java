package com.zuochengyun.大厂刷题班.code04;

/**
 * 字符串交错组成（顺序不能是乱的）
 * <p>
 * 测试链接 : <a href="https://leetcode.com/problems/interleaving-string/"/>
 *
 * @author 钢人
 */
public class Code07_InterleavingString {

    /**
     * 动态规划样本对应模型
     *
     * @param s1 字符串1
     * @param s2 字符串2
     * @param s3 字符串3
     * @return 看字符串3是否是字符串1和字符串2的交错组合
     */
    public static boolean isInterleave(String s1, String s2, String s3) {
        // TODO
        if (s1 == null || s2 == null || s3 == null) {
            return false;
        }
        char[] str1 = s1.toCharArray();
        char[] str2 = s2.toCharArray();
        char[] str3 = s3.toCharArray();
        if (str3.length != str1.length + str2.length) {
            return false;
        }
        boolean[][] dp = new boolean[str1.length + 1][str2.length + 1];
        dp[0][0] = true;
        // 检查第一列
        for (int i = 1; i <= str1.length; i++) {
            if (str1[i - 1] != str3[i - 1]) {
                break;
            }
            dp[i][0] = true;
        }
        // 检查第一行
        for (int j = 1; j <= str2.length; j++) {
            if (str2[j - 1] != str3[j - 1]) {
                break;
            }
            dp[0][j] = true;
        }
        for (int i = 1; i <= str1.length; i++) {
            for (int j = 1; j <= str2.length; j++) {
                if ((str1[i - 1] == str3[i + j - 1] && dp[i - 1][j]) || (str2[j - 1] == str3[i + j - 1] && dp[i][j - 1])) {
                    dp[i][j] = true;
                }
            }
        }
        return dp[str1.length][str2.length];
    }

}

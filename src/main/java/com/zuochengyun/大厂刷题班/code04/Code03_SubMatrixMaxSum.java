package com.zuochengyun.大厂刷题班.code04;

/**
 * 返回一个二维数组中，子矩阵最大累加和
 *
 * 测试链接 : <a href="https://leetcode-cn.com/problems/max-submatrix-lcci/"/>
 * @author 钢人
 */
public class Code03_SubMatrixMaxSum {

    /**
     * 暴力解
     */
    public static int maxSum(int[][] m) {
        if (m == null || m.length == 0 || m[0].length == 0) {
            return 0;
        }
        // O(N^2 * M)
        int N = m.length;
        int M = m[0].length;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < N; i++) {
            // i~j
            int[] s = new int[M];
            for (int j = i; j < N; j++) {
                for (int k = 0; k < M; k++) {
                    s[k] += m[j][k];
                }
                max = Math.max(max, maxSubArray(s));
            }
        }
        return max;
    }

    public static int maxSubArray(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int max = Integer.MIN_VALUE;
        int cur = 0;
        for (int i = 0; i < arr.length; i++) {
            cur += arr[i];
            max = Math.max(max, cur);
            cur = cur < 0 ? 0 : cur;
        }
        return max;
    }


    /**
     * leetcode 题是求子矩阵最大累加和的矩阵位置
     */
    public static int[] getMaxMatrix(int[][] m) {
        int N = m.length;
        int M = m[0].length;
        int max = Integer.MIN_VALUE;
        int cur = 0;
        // 矩阵四个点的位置
        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        // 遍历行
        for (int i = 0; i < N; i++) {
            // 求每一行的子数组累加和
            int[] s = new int[M];
            // 遍历行上的 i~j 范围的子数组
            for (int j = i; j < N; j++) {
                cur = 0;
                int begin = 0;
                // 遍历列求子数组累加和
                for (int k = 0; k < M; k++) {
                    // 得出 j 行，k 列上的矩阵
                    s[k] += m[j][k];
                    // 当前值加上 j 行 k 列位置的数
                    cur += s[k];
                    // 如果当前矩阵累加和比之前得出的子矩阵累加和大，更新当前子矩阵四个点的位置及累加和
                    if (max < cur) {
                        max = cur;
                        a = i;
                        b = begin;
                        c = j;
                        d = k;
                    }
                    // 当前子矩阵累加和小于0，置0重新算，并且扩大矩阵范围再次尝试
                    if (cur < 0) {
                        cur = 0;
                        begin = k + 1;
                    }
                }
            }
        }
        return new int[] { a, b, c, d };
    }

}

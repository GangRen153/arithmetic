package com.zuochengyun.大厂刷题班.code04;

/**
 * 返回一个数组中，子数组最大累加和，有正有负
 *
 * 测试链接 : <a href="https://leetcode.com/problems/maximum-subarray/"/>
 * @author 钢人
 */
public class Code02_SubArrayMaxSum {

    public static int maxSubArray(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int max = Integer.MIN_VALUE;
        int cur = 0;
        for (int i = 0; i < arr.length; i++) {
            cur += arr[i];
            max = Math.max(max, cur);
            cur = Math.max(cur, 0);
        }
        return cur;
    }

    public static int maxSubArray2(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        // 上一步，dp的值
        // dp[0]
        int pre = arr[0];
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            // 上一步的累加和+当前i位置 比较 当前i位置的数（如果当前i位置的数是负数，累加之后就会变小）
            pre = Math.max(arr[i], arr[i] + pre);
            // 比较一下跟之前的累加和哪个大
            max =  Math.max(max, pre);
        }
        return max;
    }

}

package com.zuochengyun.大厂刷题班.code04;

/**
 * 小孩儿分糖问题
 *
 * 每个孩子至少分配到 1 个糖果
 * 相邻两个孩子评分更高的孩子会获得更多的糖果，评分相等的没有限制
 *
 * 测试链接 : <a href="https://leetcode.com/problems/candy/"/>
 * @author 钢人
 */
public class Code05_CandyProblem {

    // 这是原问题的优良解
    // 时间复杂度O(N)，额外空间复杂度O(N)
    public static int candy1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int N = arr.length;
        int[] left = new int[N];
        // 从左到右计算评分坡度
        for (int i = 1; i < N; i++) {
            if (arr[i - 1] < arr[i]) {
                left[i] = left[i - 1] + 1;
            }
        }
        int[] right = new int[N];
        // 从右到左计算评分坡度
        for (int i = N - 2; i >= 0; i--) {
            if (arr[i] > arr[i + 1]) {
                right[i] = right[i + 1] + 1;
            }
        }
        int ans = 0;
        // 取从左到右和从右到左的计算结果取最大值
        for (int i = 0; i < N; i++) {
            ans += Math.max(left[i], right[i]);
        }
        return ans + N;
    }

    // 这是原问题空间优化后的解
    // 时间复杂度O(N)，额外空间复杂度O(1)
    public static int candy2(int[] arr) {
        // TODO
        return 0;
    }

}

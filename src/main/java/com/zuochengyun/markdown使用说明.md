http://markdown.p2hp.com/

# 标题

<h1>标题</h1>

<p>段落1</p>
<p>段落2</p>

**粗体字**   <strong>粗体字</strong><br>
*斜体字*   <em>斜体字</em><br>
***粗斜体*** <strong><em>粗斜体</em></strong>

分界线（水平线）
***
---
___

> 块引用
>
> 块段落
>> 块嵌套
> #### 具有其他元素的块引用
>
> - 无项列表
> - 无项字体：**粗体**
> ***

1. 有序表第一项
2. 第二项
    1. 缩进项
3. 第三项

<ol>
    <li>有序表第一项</li>
    <li>第二项
        <ol>
            <li>缩进项</li>
        </ol>
    </li>
    <li>第三项</li>
</ol>

- 无项列表1
    - 缩进无项列表
    - > 嵌套其他元素
    -
        -
- 无项列表2

<ul>
    <li>无项列表1
        <ul>
            <li>缩进无项列表</li>
        </ul>
    </li>
    <li>无项列表2</li>
</ul>

`代码`<br>
``转移刻度线``

[百度链接](http://www.baidu.com "光标悬停内容提示")

邮件链接：<http://marksown.p2hp.com>

<img title="图片" src="C:\Users\钢人\Pictures\WLKQBingWallpapers\2baT3OkXyiCfqOxH.jpg"/>

<a href="C:\Users\钢人\Pictures\WLKQBingWallpapers\2baT3OkXyiCfqOxH.jpg" title="图片链接">图片链接</a>

| 表格     |   描述   |           说明 |
|:-------|:------:|-------------:|
| 表格列左对齐 | 表格列居中  |       表格列右对齐 |
| 表格列    |  表格列   |          表格列 | 
| `代码`   | **粗体** | 管道字符： &#124; |

管道字符：&#124;

```java
public static void main(String[]args) {
    System.out.println("代码块");
}
```

~~~shell
lsof -i:3306
~~~

[链接标题ID](#heading-ids)


~~删除线~~

- [x] 选项1
- [x] 选项2
- [ ] 选项3

自动网址链接：http://www.example.com

